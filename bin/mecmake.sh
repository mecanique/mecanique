#!/bin/bash

#Script writted by Quentin VIGNAUD, 2014
#Public domain, there is NO WARRANTY, to the extent permitted by law.

if [ $# -lt 1 ]
then
	echo "Invalid number of parameters."
	exit
else
	if [ "$1" == "-h" -o "$1" == "--help" -o "$1" == "help" ]
	then
		echo -e "Script of configuration and management of \"Mécanique\" plugins sources.\nTo run in the source directory of a plugin (/src/mecanique-plugins/[name]).\nUse: mecmake COMMAND\nCommands:\n\tcheck\tCheck the configuration file of the current plugin.\n\tdebpkg MODE\tCreate Debian packages.\n\t\tModes:\n\t\tbin \"First name LAST NAME <email@address.tld>\" ARCHITECTURE\tBinary installation package of the plugin, for the given architecture.\n\t\tdev \"First name LAST NAME <email@address.tld>\"\tDevelopment package of the plugin.\n\t\tdoc \"First name LAST NAME <email@address.tld>\"\tDocumentation package of the plugin.\n\tdocgen\tGenerates or updates the plugin documentation.\n\tlrelease\tMade the release of the translations.\n\tlupdate\tMade the update of the translations.\n\tqmake\tGenerates a new Makefile with qmake.\n\trefrelease\tMade the release of the reference.\n\tresgen\tGenerates the file \"resource.qrc\" based on the contents of the \"resources\" folder.\n\tsetup\tSets up files for a \"Mécanique\" plugin project."
		exit
	fi
	if [ ! -f "mecanique.conf" ]
	then
		echo "Configuration file not found."
		exit
	fi
fi

#On cherche le nom du plugin.
NAME=`grep --color=never --no-messages "^NAME=" "mecanique.conf"`
if [ $? -ne 0 ]
then
	echo "Name not found in the configuration file of the plugin."
	exit
fi
NAME=${NAME#"NAME="}

#On cherche la version du plugin.
VERSION=`grep --color=never --no-messages "^VERSION=" "mecanique.conf"`
if [ $? -ne 0 ]
then
	echo "Version not found in the configuration file of the plugin."
	exit
fi
VERSION=${VERSION#"VERSION="}

#On cherche le rôle du plugin.
ROLE=`grep --color=never --no-messages "^ROLE=" "mecanique.conf"`
if [ $? -ne 0 ]
then
	echo "Role not found in the configuration file of the plugin."
	exit
fi
ROLE=${ROLE#"ROLE="}

#On cherche le type du plugin.
TYPE=`grep --color=never --no-messages "^TYPE=" "mecanique.conf"`
if [ $? -ne 0 ]
then
	echo "Type not found in the configuration file of the plugin."
	exit
fi
TYPE=${TYPE#"TYPE="}

#On cherche le titre du plugin.
TITLE=`grep --color=never --no-messages "^TITLE=" "mecanique.conf"`
if [ $? -ne 0 ]
then
	echo "Title not found in the configuration file of the plugin."
	exit
fi
TITLE=${TITLE#"TITLE="}

#On cherche la description du plugin.
BRIEF=`grep --color=never --no-messages "^BRIEF=" "mecanique.conf"`
if [ $? -ne 0 ]
then
	echo "Description not found in the configuration file of the plugin."
	exit
fi
BRIEF=${BRIEF#"BRIEF="}

#On cherche les dépendances du plugin.
DEPENDS=`grep --color=never --no-messages "^DEPENDS=" "mecanique.conf"`
if [ $? -ne 0 ]
then
	echo "Depedencies not found in the configuration file of the plugin."
	exit
fi
DEPENDS=${DEPENDS#"DEPENDS="}

#On cherche les suggestions d'installation du plugin.
SUGGESTS=`grep --color=never --no-messages "^SUGGESTS=" "mecanique.conf"`
if [ $? -ne 0 ]
then
	echo "Suggestions not found in the configuration file of the plugin."
	exit
fi
SUGGESTS=${SUGGESTS#"SUGGESTS="}

#On cherche les dépendances de développement du plugin
DEVDEPENDS=`grep --color=never --no-messages "^DEVDEPENDS=" "mecanique.conf"`
if [ $? -ne 0 ]
then
	echo "Development depedencies not found in the configuration file of the plugin."
	exit
fi
DEVDEPENDS=${DEVDEPENDS#"DEVDEPENDS="}

#On cherche les suggestions de développement du plugin.
DEVSUGGESTS=`grep --color=never --no-messages "^DEVSUGGESTS=" "mecanique.conf"`
if [ $? -ne 0 ]
then
	echo "Development suggestions not found in the configuration file of the plugin."
	exit
fi
DEVSUGGESTS=${DEVSUGGESTS#"DEVSUGGESTS="}

case "$1" in
	"check")
		echo -e "Name:\t«$NAME»\nVersion:\t«$VERSION»\nRole:\t«$ROLE»\nType:\t«$TYPE»\nTitle:\t«$TITLE»\nDescription:\t«$BRIEF»\nDependencies:\t$DEPENDS\nSuggestions:\t$SUGGESTS\nDevelopment dependencies:\t$DEVDEPENDS\nDevelopment suggestions:\t$DEVSUGGESTS\n"
	;;
	"debpkg")
		#Répertoire de travail
		REPNAME="/tmp/$NAME.$$"
		#Nom général du paquet
		PKGNAME="mecanique-plugin-$NAME"
		
		if [ $# -lt 3 ]
		then
			echo "Invalid number of parameters."
			exit
		else
			#Mainteneur du paquet (Prénom NOM <adresse@email.tld>)
			MAINTAINER="$3"
			case "$2" in
				"bin")
					if [ $# -lt 4 ]
					then
						echo "Invalid number of parameters."
						exit
					fi
					#Architecture de compilation (i386, amd64, ...)
					ARCHITECTURE="$4"
					
					#Nom du paquet binaire
					BINPKGNAME="${PKGNAME}_${VERSION}_$ARCHITECTURE"
					
					#On construit l'arborescence du paquet binaire
					mkdir -p "$REPNAME/$BINPKGNAME/DEBIAN"
					mkdir -p "$REPNAME/$BINPKGNAME/usr/share/doc/$PKGNAME"
					mkdir -p "$REPNAME/$BINPKGNAME/usr/share/mecanique/translations"
					mkdir -p "$REPNAME/$BINPKGNAME/usr/share/mecanique/reference"
					mkdir -p "$REPNAME/$BINPKGNAME/usr/lib/mecanique"
				
					find "../../../share/mecanique/translations" -type f -wholename "../../../share/mecanique/translations/$NAME.*.qm" -exec cp -f {} "$REPNAME/$BINPKGNAME/usr/share/mecanique/translations" \;
					find "../../../share/mecanique/reference" -type f -wholename "../../../share/mecanique/reference/$TYPE.*.html" -exec cp -f {} "$REPNAME/$BINPKGNAME/usr/share/mecanique/reference" \;
					find "../../../lib/mecanique" -type f -wholename "../../../lib/mecanique/*$NAME*" -exec cp -f {} "$REPNAME/$BINPKGNAME/usr/lib/mecanique" \;
					find "$REPNAME/$BINPKGNAME/usr/lib/mecanique" -type f -executable -exec strip {} \;
					cp -f "copyright" "$REPNAME/$BINPKGNAME/usr/share/doc/$PKGNAME"
					gzip -qc --best changelog > "$REPNAME/$BINPKGNAME/usr/share/doc/$PKGNAME/changelog.gz"
		
					#On crée le fichier "control"
					INSTALLSIZE=`du -s "$REPNAME/$BINPKGNAME/usr/"`
					INSTALLSIZE=${INSTALLSIZE%$REPNAME/$BINPKGNAME/usr/}
					echo -e "Package: $PKGNAME\nVersion: $VERSION\nInstalled-Size: $INSTALLSIZE\nMaintainer: $MAINTAINER\nSection: devel\nHomepage: http://mecanique.cc/\nPriority: optional\nArchitecture: $ARCHITECTURE\nDepends: libc6 (>=2.0), libstdc++6 (>=4), $DEPENDS" > "$REPNAME/$BINPKGNAME/DEBIAN/control"
					if [ "$SUGGESTS" != "" ]
					then
						echo -e "Suggests: $SUGGESTS" >> "$REPNAME/$BINPKGNAME/DEBIAN/control"
					fi
					
					echo -e "Description: $BRIEF" >> "$REPNAME/$BINPKGNAME/DEBIAN/control"
					
					while read LINE
					do
						echo " $LINE" >> "$REPNAME/$BINPKGNAME/DEBIAN/control"
					done < description
		
					#On compile le paquet binaire
					fakeroot dpkg-deb --build "$REPNAME/$BINPKGNAME"
					
					mkdir -p "/tmp/mecpkg"
					mv -uv "$REPNAME/$BINPKGNAME.deb" "/tmp/mecpkg"
				;;
				"dev")
					#Nom du paquet de développement
					DEVPKGNAME="$PKGNAME-dev_${VERSION}_all"
					
					#On construit l'arborescence du paquet de développement.
					mkdir -p "$REPNAME/$DEVPKGNAME/DEBIAN"
					mkdir -p "$REPNAME/$DEVPKGNAME/usr/share/doc/$PKGNAME-dev"
					mkdir -p "$REPNAME/$DEVPKGNAME/usr/src/mecanique-plugins"
		
					cp -fR "../$NAME" "$REPNAME/$DEVPKGNAME/usr/src/mecanique-plugins"
					cp -f "$REPNAME/$DEVPKGNAME/usr/src/mecanique-plugins/$NAME/copyright" "$REPNAME/$DEVPKGNAME/usr/share/doc/$PKGNAME-dev"
					gzip -qc --best changelog > "$REPNAME/$DEVPKGNAME/usr/share/doc/$PKGNAME-dev/changelog.gz"
		
					#On crée le fichier "control"
					INSTALLSIZE=`du -s "$REPNAME/$DEVPKGNAME/usr/"`
					INSTALLSIZE=${INSTALLSIZE%$REPNAME/$DEVPKGNAME/usr/}
					echo -e "Package: $PKGNAME-dev\nVersion: $VERSION\nInstalled-Size: $INSTALLSIZE\nMaintainer: $MAINTAINER\nSection: devel\nHomepage: http://mecanique.cc/\nPriority: optional\nArchitecture: all\nDepends: $DEVDEPENDS" > "$REPNAME/$DEVPKGNAME/DEBIAN/control"
					if [ "$DEVSUGGESTS" != "" ]
					then
						echo -e "Suggests: $DEVSUGGESTS" >> "$REPNAME/$DEVPKGNAME/DEBIAN/control"
					fi
					
					echo -e "Description: Development package of $NAME\n $BRIEF" >> "$REPNAME/$DEVPKGNAME/DEBIAN/control"
					
					while read LINE
					do
						echo " $LINE" >> "$REPNAME/$DEVPKGNAME/DEBIAN/control"
					done < description
		
					#On compile le paquet de développement
					fakeroot dpkg-deb --build "$REPNAME/$DEVPKGNAME"
					
					mkdir -p "/tmp/mecpkg"
					mv -uv "$REPNAME/$DEVPKGNAME.deb" "/tmp/mecpkg"
				;;
				"doc")
					#Nom du paquet de documentation
					DOCPKGNAME="$PKGNAME-doc_${VERSION}_all"

					#On construit l'arborescence du paquet de documentation.
					mkdir -p "$REPNAME/$DOCPKGNAME/DEBIAN"
					mkdir -p "$REPNAME/$DOCPKGNAME/usr/share/doc/$PKGNAME-doc"
					mkdir -p "$REPNAME/$DOCPKGNAME/usr/share/mecanique/doc/mecanique-plugins"
		
					cp -fR "../../../share/mecanique/doc/mecanique-plugins/$NAME" "$REPNAME/$DOCPKGNAME/usr/share/mecanique/doc/mecanique-plugins"
					cp -f "copyright" "$REPNAME/$DOCPKGNAME/usr/share/doc/$PKGNAME-doc"
					gzip -qc --best changelog > "$REPNAME/$DOCPKGNAME/usr/share/doc/$PKGNAME-doc/changelog.gz"
		
					#On crée le fichier "control"
					INSTALLSIZE=`du -s "$REPNAME/$DOCPKGNAME/usr/"`
					INSTALLSIZE=${INSTALLSIZE%$REPNAME/$DOCPKGNAME/usr/}
					echo -e "Package: $PKGNAME-doc\nVersion: $VERSION\nInstalled-Size: $INSTALLSIZE\nMaintainer: $MAINTAINER\nSection: doc\nHomepage: http://mecanique.cc/\nPriority: optional\nArchitecture: all\nSuggests: mecanique-doc (>=0.1), mecanique-plugin-$NAME-dev (=$VERSION)\nDescription: Documentation package of $TITLE\n $BRIEF" > "$REPNAME/$DOCPKGNAME/DEBIAN/control"
					while read LINE
					do
						echo " $LINE" >> "$REPNAME/$DOCPKGNAME/DEBIAN/control"
					done < description
		
					#On compile le paquet de documentation
					fakeroot dpkg-deb --build "$REPNAME/$DOCPKGNAME"
					
					mkdir -p "/tmp/mecpkg"
					mv -uv "$REPNAME/$DOCPKGNAME.deb" "/tmp/mecpkg"
				;;
				*)
					echo "Package type unknown."
				;;
			esac
		fi
	;;
	"docgen")
		#Déplacement de la documentation vers le répertoire commun
		mkdir -p "../../../share/mecanique/doc/mecanique-plugins/$NAME"
		find * -maxdepth 1 -wholename "doc/*" -exec cp -fR {} "../../../share/mecanique/doc/mecanique-plugins/$NAME" \;
		
		#Recherche et génération depuis les Doxyfiles
		bash -c "cd ../../../share/mecanique/doc/mecanique-plugins/$NAME ; find * -type f -name Doxyfile > /tmp/mecdoxyfiles.list ; exit ;"
		while read TEMP_DOXYFILE
		do
			bash -c "cd `dirname "../../../share/mecanique/doc/mecanique-plugins/$NAME/$TEMP_DOXYFILE"` ; doxygen Doxyfile ; exit ;"
		done < /tmp/mecdoxyfiles.list
		
		##
		echo "The documentation has been generated or updated into « ../../../share/mecanique/doc/mecanique-plugins/$NAME »."
	;;
	"lrelease")
		lrelease "$NAME.pro"
		
		find * -type f -wholename "translations/$NAME.*.qm" -exec cp -f {} "../../../share/mecanique/translations" \;
	;;
	"lupdate")
		lupdate "$NAME.pro"
	;;
	"qmake")
		qmake -makefile
	;;
	"refrelease")
		find * -type f -wholename "reference/$TYPE.*.html" -exec cp -f {} "../../../share/mecanique/reference" \;
	;;
	"resgen")
		mkdir -p "resources"
		bash -c "cd resources ; find * -type f > /tmp/mecres.list ; exit ;"

		TEMP_RESOURCES_CONTENT="<!DOCTYPE RCC>\n<RCC version=\"1.0\">\n\t<qresource>\n"
		while read TEMP_RESOURCE
		do
			TEMP_RESOURCES_CONTENT="$TEMP_RESOURCES_CONTENT\t\t<file alias=\"$TEMP_RESOURCE\">resources/$TEMP_RESOURCE</file>\n"
		done < /tmp/mecres.list
		TEMP_RESOURCES_CONTENT="$TEMP_RESOURCES_CONTENT\t</qresource>\n</RCC>\n"
		
		echo -e "$TEMP_RESOURCES_CONTENT" > resources.qrc
	;;
	"setup")
		if [ ! -d "../plugin" ]
		then
			echo "Directory \"../plugin\" not found."
			exit
		fi
		
		#On installe le ".pro"
		cp -n "../../../share/mecanique/dev/bases/pluginproject.pro" "$NAME.pro"
		
		sed -i -r "s/\[PLUGIN NAME\]/$NAME/g" "$NAME.pro"
		
		#On installe le "metadata.json"
		cp -n "../../../share/mecanique/dev/bases/pluginmetadata.json" "metadata.json"
		sed -i -r "s/\[PLUGIN NAME\]/$NAME/g" "metadata.json"
		
		#On crée les fichiers annexes nécessaires
		touch "changelog"
		touch "copyright"
		touch "description"
		
		#On crée les sous-répertoires
		mkdir "doc"
		mkdir "reference"
		mkdir "resources"
		mkdir "translations"
		
		#On installe les fichiers de configuration de la documentation
		mkdir "doc/fr"
		cp -f "../../../share/mecanique/dev/bases/Doxyfile-plugins-fr" "./doc/fr/Doxyfile"
		cp -n "../../../share/mecanique/dev/bases/main.dox-plugins-fr" "./doc/fr/main.dox"
		cp -n "../../../share/mecanique/dev/bases/footer.html-plugins-fr" "./doc/fr/footer.html"
		sed -i -r "s/^PROJECT_NAME[[:blank:]]*=.*$/PROJECT_NAME\t= \"Mécanique – $TITLE\"/g" "./doc/fr/Doxyfile"
		sed -i -r "s/^PROJECT_NUMBER[[:blank:]]*=.*$/PROJECT_NUMBER\t= $VERSION/g" "./doc/fr/Doxyfile"
		sed -i -r "s/^PROJECT_BRIEF[[:blank:]]*=.*$/PROJECT_BRIEF\t= \"$BRIEF\"/g" "./doc/fr/Doxyfile"
		sed -i -r "s/^INPUT[[:blank:]]*=.*$/INPUT\t= ..\/..\/..\/..\/..\/..\/src\/mecanique-plugins\/$NAME \\\\/g" "./doc/fr/Doxyfile"
		sed -i -r "s/^EXCLUDE[[:blank:]]*=.*$/EXCLUDE\t= ..\/..\/..\/..\/..\/..\/src\/mecanique-plugins\/$NAME\/resources \\\\ /g" "./doc/fr/Doxyfile"
		sed -i -r "s/\[PLUGIN NAME\]/$NAME/g" "./doc/fr/Doxyfile"
		sed -i -r "s/\[PLUGIN TITLE\]/$TITLE/g" "./doc/fr/main.dox"
		
		#On installe le fichier de référence minimal
		touch "reference/$TYPE.en.html"
	;;
	*)
		echo "Invalid action."
	;;
esac


