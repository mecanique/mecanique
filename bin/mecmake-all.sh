#!/bin/bash

#Script writted by Quentin VIGNAUD, 2014
#Public domain, there is NO WARRANTY, to the extent permitted by law.

if [ $# -lt 1 ]
then
	echo "Invalid number of parameters."
	exit
else
	if [ "$1" == "-h" -o "$1" == "--help" -o "$1" == "help" ]
	then
		echo -e "Script of configuration and management of \"Mécanique\" plugins sources.\nTo run in the parent directory of the source directories of plugins (/src/mecanique-plugins).\nUse: mecmake-all COMMAND\nCommands:\n\tcheck\tCheck the configuration file of the plugins.\n\tdebpkg MODE\tCreate Debian packages.\n\t\tModes:\n\t\tbin \"First name LAST NAME <email@address.tld>\" ARCHITECTURE\tBinary installation package of the plugins, for the given architecture.\n\t\tdev \"First name LAST NAME <email@address.tld>\"\tDevelopment package of the plugins.\n\t\tdoc \"First name LAST NAME <email@address.tld>\"\tDocumentation package of the plugins.\n\tdocconf\tConfigure the plugins documentations.\n\tdocgen\tGenerates or updates the plugin documentation.\n\tlrelease\tExecute the \"lrelease\" command with the project files of the plugins.\n\tlupdate\tExecute the \"lupdate\" command with the project files of the plugins.\n\tmake\tExecute the Makefiles of all the plugins sources subdirectories, it is possible to give an additionnal parameter, like \"make clean\".\n\tqmake\tGenerates new Makefiles with qmake.\n\trefrelease\tMade the release of the references.\n\tresgen\tGenerates the files \"resource.qrc\" based on the contents of the \"resources\" folders."
		exit
	fi
fi

find * -maxdepth 0 -type d -not -name plugin > /tmp/mecpluginsrep.list

case "$1" in
	"check")
		while read PLUGIN
		do
			echo "=== Plugin « $PLUGIN » ==="
			cd $PLUGIN
			../../../bin/mecmake check
			cd ..
		done < /tmp/mecpluginsrep.list
	;;
	"debpkg")
		#On vérifie les paramètres.
		case "$2" in
			"bin")
				if [ $# -lt 4 ]
				then
					echo "Invalid number of parameters."
					exit
				fi
			;;
			"dev")
				if [ $# -lt 3 ]
				then
					echo "Invalid number of parameters."
					exit
				fi
			;;
			"doc")
				if [ $# -lt 3 ]
				then
					echo "Invalid number of parameters."
					exit
				fi
			;;
			*)
				echo "Parameter \"$2\" invalid."
				exit
			;;
		esac
		while read PLUGIN
		do
			echo "=== Plugin « $PLUGIN » ==="
			cd $PLUGIN
			../../../bin/mecmake debpkg $2 "$3" $4
			cd ..
		done < /tmp/mecpluginsrep.list
	;;
	"docgen")
		echo "=== General documentation for plugins ==="
		bash -c "cd ../../share/mecanique/doc/mecanique-plugins/plugin/fr ; doxygen Doxyfile ; exit ;"
		
		while read PLUGIN
		do
			echo "=== Plugin « $PLUGIN » ==="
			cd $PLUGIN
			../../../bin/mecmake docgen
			cd ..
		done < /tmp/mecpluginsrep.list
	;;
	"lrelease")
		while read PLUGIN
		do
			echo "=== Plugin « $PLUGIN » ==="
			cd $PLUGIN
			../../../bin/mecmake lrelease
			cd ..
		done < /tmp/mecpluginsrep.list
	;;
	"lupdate")
		while read PLUGIN
		do
			echo "=== Plugin « $PLUGIN » ==="
			cd $PLUGIN
			../../../bin/mecmake lupdate
			cd ..
		done < /tmp/mecpluginsrep.list
	;;
	"make")
		while read PLUGIN
		do
			echo "=== Plugin « $PLUGIN » ==="
			cd $PLUGIN
			"$@"
			if [ $? -ne 0 ]
			then
				cd ..
				exit $?
			fi
			cd ..
		done < /tmp/mecpluginsrep.list
	;;
	"qmake")
		while read PLUGIN
		do
			echo "=== Plugin « $PLUGIN » ==="
			cd $PLUGIN
			../../../bin/mecmake qmake
			cd ..
		done < /tmp/mecpluginsrep.list
	;;
	"refrelease")
		while read PLUGIN
		do
			echo "=== Plugin « $PLUGIN » ==="
			cd $PLUGIN
			../../../bin/mecmake refrelease
			cd ..
		done < /tmp/mecpluginsrep.list
	;;
	"resgen")
		while read PLUGIN
		do
			echo "=== Plugin « $PLUGIN » ==="
			cd $PLUGIN
			../../../bin/mecmake resgen
			cd ..
		done < /tmp/mecpluginsrep.list
	;;
	*)
		echo "Invalid action."
		exit
	;;
esac


