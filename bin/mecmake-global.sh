#!/bin/bash

#Script writted by Quentin VIGNAUD, 2014
#Public domain, there is NO WARRANTY, to the extent permitted by law.

if [ $# -lt 1 ]
then
	echo "Invalid number of parameters."
	exit
else
	if [ "$1" == "-h" -o "$1" == "--help" -o "$1" == "help" ]
	then
		echo -e "Script of configuration and management of \"Mécanique\" project sources.\nTo run in the root directory of the project.\nUse: mecmake-global COMMAND\nCommands:\n\tdebpkg MODE\tCreate Debian packages.\n\t\tModes:\n\t\tbin \"First name LAST NAME <email@address.tld>\" ARCHITECTURE\tBinary installation packages, for the given architecture.\n\t\tdev \"First name LAST NAME <email@address.tld>\"\tDevelopment packages.\n\t\tdoc \"First name LAST NAME <email@address.tld>\"\tDocumentation packages.\n\tdocgen\tGenerates or updates all the project documentation.\n\tlrelease\tExecute the \"lrelease\" command with the project files of Mécanique and its plugins.\n\tlupdate\tExecute the \"lupdate\" command with the project files of Mécanique and its plugins.\n\tmake\tExecute the Makefiles of all the sources subdirectories, it is possible to give an additionnal parameter, like \"make clean\".\n\tmkbat\tGenerates a MS Windows batch file to compile automatically all sources.\n\tqmake\tGenerates new Makefiles with qmake.\n\trefrelease\tMade the releases of the plugins types references.\n\ttgzpkg\tGenerates an archive of all \"Mécanique\" data."
		exit
	fi
fi

case "$1" in
	"debpkg")
		#On vérifie les paramètres.
		case "$2" in
			"bin")
				if [ $# -lt 4 ]
				then
					echo "Invalid number of parameters."
					exit
				fi
			;;
			"dev")
				if [ $# -lt 3 ]
				then
					echo "Invalid number of parameters."
					exit
				fi
			;;
			"doc")
				if [ $# -lt 3 ]
				then
					echo "Invalid number of parameters."
					exit
				fi
			;;
			*)
				echo "Parameter \"$2\" invalid."
				exit
			;;
		esac
		
		cd src/mecanique
		../../bin/mecmake-mecanique debpkg $2 "$3" $4
		cd ../mecanique-plugins
		../../bin/mecmake-all debpkg $2 "$3" $4
		cd ../..
	;;
	"docgen")
		#On actualise la documentation générale
		bash -c "cd src/mecanique-plugins ; find * -maxdepth 0 -type d -not -name plugin > /tmp/mecplugins.list ; exit ;"
		mkdir -p "share/mecanique/doc/general/fr/"
		cp -f "share/mecanique/dev/bases/Doxyfile-general-fr" "share/mecanique/doc/general/fr/Doxyfile"
		cp -f "share/mecanique/dev/bases/main.dox-general-fr" "share/mecanique/doc/general/fr/main.dox"
		cp -f "share/mecanique/dev/bases/footer.html-general-fr" "share/mecanique/doc/general/fr/footer.html"
		TEMP_PLUGINS_ENUM=""
		while read TEMP_PLUGIN
		do
			TEMP_PLUGINS_ENUM="$TEMP_PLUGINS_ENUM\t- <a href=\"..\/..\/..\/mecanique-plugins\/$TEMP_PLUGIN\/fr\/html\/index.html\">$TEMP_PLUGIN<\/a>\n"
		done < /tmp/mecplugins.list
		sed -i -r "s/\[LIST\]/$TEMP_PLUGINS_ENUM/g" "share/mecanique/doc/general/fr/main.dox"
		bash -c "cd share/mecanique/doc/general/fr/; doxygen Doxyfile; exit;" &> /dev/null
		
		#On actualise chaque documentation particulière
		echo "=== Main program « Mécanique » ==="
		cd src/mecanique
		../../bin/mecmake-mecanique docgen
		cd ../mecanique-plugins
		../../bin/mecmake-all docgen
		cd ../..
		
		#On actualise la documentation du système résultant
		bash -c "cd src/mecanique-plugins ; find * -mindepth 2 -maxdepth 3 -type d > /tmp/mecresultant.list ; exit ;"
		sed -i -r "s/\/{1}/\\\\\\\\\//g" "/tmp/mecresultant.list"
		mkdir -p "share/mecanique/doc/resultant/fr/"
		cp -f "share/mecanique/dev/bases/Doxyfile-resultant-fr" "share/mecanique/doc/resultant/fr/Doxyfile"
		cp -f "share/mecanique/dev/bases/main.dox-resultant-fr" "share/mecanique/doc/resultant/fr/main.dox"
		cp -f "share/mecanique/dev/bases/footer.html-resultant-fr" "share/mecanique/doc/resultant/fr/footer.html"
		TEMP_RESULTANT_ENUM=""
		while read TEMP_RESULTANT
		do
			TEMP_RESULTANT_ENUM="$TEMP_RESULTANT_ENUM..\/..\/..\/..\/..\/src\/mecanique-plugins\/$TEMP_RESULTANT \\\\\\n\\t\\t"
		done < /tmp/mecresultant.list
		TEMP_RESULTANT_ENUM="$TEMP_RESULTANT_ENUM. ..\/..\/..\/..\/..\/src\/mecanique\/resources\/src ..\/..\/..\/..\/..\/src\/mecanique\/resources\/src\/mecanique"
		sed -i -r "s/^INPUT[[:blank:]]*=.*$/INPUT\t= $TEMP_RESULTANT_ENUM/g" "share/mecanique/doc/resultant/fr/Doxyfile"
		bash -c "cd share/mecanique/doc/resultant/fr/; doxygen Doxyfile; exit;" &> /dev/null
	;;
	"lrelease")
		echo "=== Main program « Mécanique » ==="
		cd src/mecanique
		lrelease mecanique.pro
		cd ../mecanique-plugins
		../../bin/mecmake-all lrelease
		cd ../..
	;;
	"lupdate")
		echo "=== Main program « Mécanique » ==="
		cd src/mecanique
		lupdate mecanique.pro
		cd ../mecanique-plugins
		../../bin/mecmake-all lupdate
		cd ../..
	;;
	"make")
		echo "=== Main program « Mécanique » ==="
		cd src/mecanique
		"$@"
		if [ $? -ne 0 ]
		then
			cd ../..
			exit $?
		fi
		cd ../mecanique-plugins
		../../bin/mecmake-all "$@"
		cd ../..
	;;
	"mkbat")
		echo -e "cd ..\\src\\mecanique\nqmake -makefile\nmingw32-make\nmingw32-make clean\ncd ..\\mecanique-plugins" > bin/mecmakewin.bat
		
		cd src/mecanique-plugins
		find * -maxdepth 0 -type d -not -name plugin > /tmp/mecpluginsrep.list
		cd ../..
		
		while read PLUGIN
		do
			echo -e "cd \"$PLUGIN\"\nqmake -makefile\nmingw32-make\nmingw32-make clean\ncd .." >> bin/mecmakewin.bat
		done < /tmp/mecpluginsrep.list
		
		echo "cd .." >> bin/mecmakewin.bat
		
		unix2dos -o bin/mecmakewin.bat
		
		echo -e "\"bin/mecmakewin.bat\" generated."
		
	;;
	"qmake")
		echo "=== Main program « Mécanique » ==="
		cd src/mecanique
		../../bin/mecmake-mecanique qmake
		cd ../mecanique-plugins
		../../bin/mecmake-all qmake
		cd ../..
	;;
	"refrelease")
		cd src/mecanique-plugins
		../../bin/mecmake-all refrelease
		cd ../..
	;;
	"tgzpkg")
		#Archive de travail
		TARNAME="/tmp/mecanique.tar"
		rm $TARNAME 2> /dev/null
		tar -cf $TARNAME "include/mecanique"
		find * -type f -wholename "bin/mecanique*" -or -wholename "bin/mecmake*" -exec tar -rf $TARNAME {} \;
		#tar -f $TARNAME "include/mecanique"
		tar -rf $TARNAME "lib/mecanique"
		tar -rf $TARNAME "share/mecanique"
		tar -rf $TARNAME "src/mecanique"
		tar -rf $TARNAME "src/mecanique-plugins"
		gzip -c --best $TARNAME > "/tmp/mecanique.tar.gz"
	;;
	*)
		echo "Invalid action."
		exit
	;;
esac
