#!/bin/bash

#Script writted by Quentin VIGNAUD, 2014
#Public domain, there is NO WARRANTY, to the extent permitted by law.

#On cherche la version de Mécanique.
VERSION=`grep --color=never --no-messages "^VERSION=" "mecanique.conf"`
if [ $? -ne 0 ]
then
	echo "Version not found in the configuration file of Mécanique."
	exit
fi
VERSION=${VERSION#"VERSION="}

#On cherche les dépendances de Mécanique.
DEPENDS=`grep --color=never --no-messages "^DEPENDS=" "mecanique.conf"`
if [ $? -ne 0 ]
then
	echo "Depedencies not found in the configuration file of Mécanique."
	exit
fi
DEPENDS=${DEPENDS#"DEPENDS="}

#On cherche les suggestions d'installation de Mécanique.
SUGGESTS=`grep --color=never --no-messages "^SUGGESTS=" "mecanique.conf"`
if [ $? -ne 0 ]
then
	echo "Suggestions not found in the configuration file of Mécanique."
	exit
fi
SUGGESTS=${SUGGESTS#"SUGGESTS="}

#On cherche les dépendances de développement de Mécanique.
DEVDEPENDS=`grep --color=never --no-messages "^DEVDEPENDS=" "mecanique.conf"`
if [ $? -ne 0 ]
then
	echo "Development depedencies not found in the configuration file of Mécanique."
	exit
fi
DEVDEPENDS=${DEVDEPENDS#"DEVDEPENDS="}

#On cherche les suggestions de développement de Mécanique.
DEVSUGGESTS=`grep --color=never --no-messages "^DEVSUGGESTS=" "mecanique.conf"`
if [ $? -ne 0 ]
then
	echo "Development suggestions not found in the configuration file of Mécanique."
	exit
fi
DEVSUGGESTS=${DEVSUGGESTS#"DEVSUGGESTS="}

if [ $# -lt 1 ]
then
	echo "Invalid number of parameters."
	exit
else
	if [ "$1" == "-h" -o "$1" == "--help" -o "$1" == "help" ]
	then
		echo -e "Script of configuration and management of \"Mécanique\" program sources.\nTo run in the source directory of the program (src/mecanique).\nUse: mecmake-mecanique COMMAND\nCommands:\n\tcheck\tCheck the configuration file of Mécanique.\n\tdebpkg MODE\tCreate Debian packages.\n\t\tModes:\n\t\tbin \"First name LAST NAME <email@address.tld>\" ARCHITECTURE\tBinary installation package of the program, for the given architecture.\n\t\tdev \"First name LAST NAME <email@address.tld>\"\tDevelopment package of the program.\n\t\tdoc \"First name LAST NAME <email@address.tld>\"\tDocumentation package of the program.\n\tdocgen\tGenerates or updates the program documentation.\n\tqmake\tGenerates a new Makefile with qmake.\n\tresgen\tGenerates the file \"resource.qrc\" based on the contents of the \"resources\" folder."
		exit
	fi
fi


case "$1" in
	"check")
		echo -e "Version:\t«$VERSION»\nDependencies:\t$DEPENDS\nSuggestions:\t$SUGGESTS\nDevelopment dependencies:\t$DEVDEPENDS\nDevelopment suggestions:\t$DEVSUGGESTS\n"
	;;
	"debpkg")
		#Répertoire de travail
		REPNAME="/tmp/mecanique.$$"
		#Nom général du paquet
		PKGNAME="mecanique"
		
		if [ $# -lt 3 ]
		then
			echo "Invalid number of parameters."
			exit
		else
			#Mainteneur du paquet (Prénom NOM <adresse@email.tld>)
			MAINTAINER="$3"
			case "$2" in
				"bin")
					if [ $# -lt 4 ]
					then
						echo "Invalid number of parameters."
						exit
					fi
					#Architecture de compilation (i386, amd64, ...)
					ARCHITECTURE="$4"
					
					#Nom du paquet binaire
					BINPKGNAME="${PKGNAME}_${VERSION}_$ARCHITECTURE"
					
					#On construit l'arborescence du paquet binaire
					mkdir -p "$REPNAME/$BINPKGNAME/DEBIAN"
					mkdir -p "$REPNAME/$BINPKGNAME/usr/share/doc/$PKGNAME"
					mkdir -p "$REPNAME/$BINPKGNAME/usr/share/mecanique/translations"
					mkdir -p "$REPNAME/$BINPKGNAME/usr/share/applications"
					mkdir -p "$REPNAME/$BINPKGNAME/usr/share/mime/packages"
					mkdir -p "$REPNAME/$BINPKGNAME/usr/bin"
				
					find "../../share/mecanique/translations" -type f -wholename "../../share/mecanique/translations/mecanique.*.qm" -exec cp -f {} "$REPNAME/$BINPKGNAME/usr/share/mecanique/translations" \;
					cp -fR "../../share/mecanique/images" "$REPNAME/$BINPKGNAME/usr/share/mecanique"
					cp -f "../../share/mecanique/dev/Mécanique.desktop" "$REPNAME/$BINPKGNAME/usr/share/applications"
					cp -f "../../share/mecanique/dev/mecanique.mime.xml" "$REPNAME/$BINPKGNAME/usr/share/mime/packages/mecanique.xml"
					find "../../bin" -type f -wholename "../../bin/mecanique*" -exec cp -f {} "$REPNAME/$BINPKGNAME/usr/bin" \;
					find "$REPNAME/$BINPKGNAME/usr/bin" -type f -executable -exec strip {} \;
					cp -f "copyright" "$REPNAME/$BINPKGNAME/usr/share/doc/$PKGNAME"
					gzip -qc --best changelog > "$REPNAME/$BINPKGNAME/usr/share/doc/$PKGNAME/changelog.gz"
		
					#On crée le fichier "control"
					INSTALLSIZE=`du -s "$REPNAME/$BINPKGNAME/usr/"`
					INSTALLSIZE=${INSTALLSIZE%$REPNAME/$BINPKGNAME/usr/}
					echo -e "Package: $PKGNAME\nVersion: $VERSION\nInstalled-Size: $INSTALLSIZE\nMaintainer: $MAINTAINER\nSection: devel\nHomepage: http://mecanique.cc/\nPriority: optional\nArchitecture: $ARCHITECTURE\nDepends: $DEPENDS" > "$REPNAME/$BINPKGNAME/DEBIAN/control"
					if [ "$SUGGESTS" != "" ]
					then
						echo -e "Suggests: $SUGGESTS" >> "$REPNAME/$BINPKGNAME/DEBIAN/control"
					fi
					
					echo -en "Description:" >> "$REPNAME/$BINPKGNAME/DEBIAN/control"
					while read LINE
					do
						echo " $LINE" >> "$REPNAME/$BINPKGNAME/DEBIAN/control"
					done < description
		
					#On compile le paquet binaire
					fakeroot dpkg-deb --build "$REPNAME/$BINPKGNAME"
					
					mkdir -p "/tmp/mecpkg"
					mv -uv "$REPNAME/$BINPKGNAME.deb" "/tmp/mecpkg"
				;;
			"dev")
					#Nom du paquet de développement
					DEVPKGNAME="$PKGNAME-dev_${VERSION}_all"
					
					#On construit l'arborescence du paquet de développement.
					mkdir -p "$REPNAME/$DEVPKGNAME/DEBIAN"
					mkdir -p "$REPNAME/$DEVPKGNAME/usr/share/doc/$PKGNAME-dev"
					mkdir -p "$REPNAME/$DEVPKGNAME/usr/include"
					mkdir -p "$REPNAME/$DEVPKGNAME/usr/src"
					mkdir -p "$REPNAME/$DEVPKGNAME/usr/src/mecanique-plugins"
					mkdir -p "$REPNAME/$DEVPKGNAME/usr/share/mecanique"
					mkdir -p "$REPNAME/$DEVPKGNAME/usr/share/mecanique/translations"
		
					cp -fR "../../include/mecanique" "$REPNAME/$DEVPKGNAME/usr/include"
					cp -fR "../mecanique" "$REPNAME/$DEVPKGNAME/usr/src"
					cp -fR "../mecanique-plugins/plugin" "$REPNAME/$DEVPKGNAME/usr/src/mecanique-plugins"
					cp -fR "../../share/mecanique/dev" "$REPNAME/$DEVPKGNAME/usr/share/mecanique"
					find "../../share/mecanique/translations" -type f -wholename "../../share/mecanique/translations/mecanique.*.ts" -exec cp -f {} "$REPNAME/$DEVPKGNAME/usr/share/mecanique/translations" \;
					#On copie le répertoire d'exécutables.
					cp -fR "../../bin" "$REPNAME/$DEVPKGNAME/usr"
					#Et on ne garde que les scripts
					find "$REPNAME/$DEVPKGNAME/usr/bin" -type f -not -wholename "$REPNAME/$DEVPKGNAME/usr/bin/mecmake*" -exec rm -f {} \;
					cp -f "$REPNAME/$DEVPKGNAME/usr/src/mecanique/copyright" "$REPNAME/$DEVPKGNAME/usr/share/doc/$PKGNAME-dev"
					gzip -qc --best changelog > "$REPNAME/$DEVPKGNAME/usr/share/doc/$PKGNAME-dev/changelog.gz"
		
					#On crée le fichier "control"
					INSTALLSIZE=`du -s "$REPNAME/$DEVPKGNAME/usr/"`
					INSTALLSIZE=${INSTALLSIZE%$REPNAME/$DEVPKGNAME/usr/}
					echo -e "Package: $PKGNAME-dev\nVersion: $VERSION\nInstalled-Size: $INSTALLSIZE\nMaintainer: $MAINTAINER\nSection: devel\nHomepage: http://mecanique.cc/\nPriority: optional\nArchitecture: all\nDepends: $DEVDEPENDS" > "$REPNAME/$DEVPKGNAME/DEBIAN/control"
					if [ "$DEVSUGGESTS" != "" ]
					then
						echo -e "Suggests: $DEVSUGGESTS" >> "$REPNAME/$DEVPKGNAME/DEBIAN/control"
					fi
					
					echo "Description: Development package of Mécanique" >> "$REPNAME/$DEVPKGNAME/DEBIAN/control"
					while read LINE
					do
						echo " $LINE" >> "$REPNAME/$DEVPKGNAME/DEBIAN/control"
					done < description
		
					#On compile le paquet de développement
					fakeroot dpkg-deb --build "$REPNAME/$DEVPKGNAME"
					
					mkdir -p "/tmp/mecpkg"
					mv -uv "$REPNAME/$DEVPKGNAME.deb" "/tmp/mecpkg"
				;;
			"doc")
					#Nom du paquet de documentation
					DOCPKGNAME="$PKGNAME-doc_${VERSION}_all"

					#On construit l'arborescence du paquet de documentation.
					mkdir -p "$REPNAME/$DOCPKGNAME/DEBIAN"
					mkdir -p "$REPNAME/$DOCPKGNAME/usr/share/doc/$PKGNAME-doc"
					mkdir -p "$REPNAME/$DOCPKGNAME/usr/share/mecanique/doc"
					mkdir -p "$REPNAME/$DOCPKGNAME/usr/share/mecanique/doc/mecanique-plugins"
		
					cp -fR "../../share/mecanique/doc/mecanique" "$REPNAME/$DOCPKGNAME/usr/share/mecanique/doc"
					cp -fR "../../share/mecanique/doc/general" "$REPNAME/$DOCPKGNAME/usr/share/mecanique/doc"
					cp -fR "../../share/mecanique/doc/resultant" "$REPNAME/$DOCPKGNAME/usr/share/mecanique/doc"
					cp -fR "../../share/mecanique/doc/mecanique-plugins/plugin" "$REPNAME/$DOCPKGNAME/usr/share/mecanique/doc/mecanique-plugins"
					cp -f "copyright" "$REPNAME/$DOCPKGNAME/usr/share/doc/$PKGNAME-doc"
					gzip -qc --best changelog > "$REPNAME/$DOCPKGNAME/usr/share/doc/$PKGNAME-doc/changelog.gz"
		
					#On crée le fichier "control"
					INSTALLSIZE=`du -s "$REPNAME/$DOCPKGNAME/usr/"`
					INSTALLSIZE=${INSTALLSIZE%$REPNAME/$DOCPKGNAME/usr/}
					echo -e "Package: $PKGNAME-doc\nVersion: $VERSION\nInstalled-Size: $INSTALLSIZE\nMaintainer: $MAINTAINER\nSection: doc\nHomepage: http://mecanique.cc/\nPriority: optional\nArchitecture: all\nSuggests: mecanique-dev (=$VERSION)\nDescription: Documentation package of Mécanique" > "$REPNAME/$DOCPKGNAME/DEBIAN/control"
					while read LINE
					do
						echo " $LINE" >> "$REPNAME/$DOCPKGNAME/DEBIAN/control"
					done < description
		
					#On compile le paquet de documentation
					fakeroot dpkg-deb --build "$REPNAME/$DOCPKGNAME"
					
					mkdir -p "/tmp/mecpkg"
					mv -uv "$REPNAME/$DOCPKGNAME.deb" "/tmp/mecpkg"
				;;
			*)
					echo "Forme de paquet inconnue."
				;;
			esac
		fi
	;;
	"docgen")
		bash -c "cd ../../share/mecanique/doc/mecanique/fr/; doxygen Doxyfile; exit;"
		#On actualise également la documentation du système résultant
		bash -c "cd ../../share/mecanique/doc/resultant/fr/; doxygen Doxyfile; exit;" &> /dev/null
	;;
	"qmake")
		qmake -makefile
	;;
	"resgen")
		mkdir -p "resources"
		bash -c "cd resources ; find * -type f > /tmp/mecres.list ; exit ;"

		TEMP_RESOURCES_CONTENT="<!DOCTYPE RCC>\n<RCC version=\"1.0\">\n\t<qresource>\n"
		while read TEMP_RESOURCE
		do
			TEMP_RESOURCES_CONTENT="$TEMP_RESOURCES_CONTENT\t\t<file alias=\"$TEMP_RESOURCE\">resources/$TEMP_RESOURCE</file>\n"
		done < /tmp/mecres.list
		TEMP_RESOURCES_CONTENT="$TEMP_RESOURCES_CONTENT\t</qresource>\n</RCC>\n"
		
		echo -e "$TEMP_RESOURCES_CONTENT" > resources.qrc
	;;
	*)
		echo "Invalid action."
	;;
esac


