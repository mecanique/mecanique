cd ..\src\mecanique
qmake -makefile
mingw32-make
mingw32-make clean
cd ..\mecanique-plugins
cd "arduinoobject"
qmake -makefile
mingw32-make
mingw32-make clean
cd ..
cd "graphicalinterfaceobject"
qmake -makefile
mingw32-make
mingw32-make clean
cd ..
cd "intermecaobject"
qmake -makefile
mingw32-make
mingw32-make clean
cd ..
cd "ipobject"
qmake -makefile
mingw32-make
mingw32-make clean
cd ..
cd "midiobject"
qmake -makefile
mingw32-make
mingw32-make clean
cd ..
cd "standardboolvariable"
qmake -makefile
mingw32-make
mingw32-make clean
cd ..
cd "standarddoublevariable"
qmake -makefile
mingw32-make
mingw32-make clean
cd ..
cd "standardfunction"
qmake -makefile
mingw32-make
mingw32-make clean
cd ..
cd "standardintvariable"
qmake -makefile
mingw32-make
mingw32-make clean
cd ..
cd "standardobject"
qmake -makefile
mingw32-make
mingw32-make clean
cd ..
cd "standardproject"
qmake -makefile
mingw32-make
mingw32-make clean
cd ..
cd "standardsignal"
qmake -makefile
mingw32-make
mingw32-make clean
cd ..
cd "standardstringvariable"
qmake -makefile
mingw32-make
mingw32-make clean
cd ..
cd "standarduintvariable"
qmake -makefile
mingw32-make
mingw32-make clean
cd ..
cd "timerobject"
qmake -makefile
mingw32-make
mingw32-make clean
cd ..
cd ..
