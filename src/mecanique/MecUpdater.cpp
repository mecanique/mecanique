/*
© Quentin VIGNAUD, 2014

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#include "MecUpdater.h"

MecUpdater::Download::Download() : size (0)
{
}

MecUpdater::Download::Download(QString Name, QString Src, qint64 Size, QByteArray Hash) : name (Name), src (Src), size (Size), hash (Hash)
{
}

MecUpdater::Execute::Execute() : closeAll (false), environment (true)
{
}

MecUpdater::Execute::Execute(QString Name, QString Parameters, bool CloseAll, bool Environment) : name (Name), parameters (Parameters), closeAll (CloseAll), environment (Environment)
{
}

MecUpdater::MecUpdater(MecPluginsManager *PluginsManager) : m_pluginsManager (PluginsManager)
{
	m_network = new QNetworkAccessManager(this);
	m_updateReply = 0;
	m_isUpgrade = false;
	m_totalDownloadSize = 0;
	m_downloadedSize = 0;
	m_indexActions = 0;
	
	m_progressDialog = new QProgressDialog(tr("Download in progress..."), tr("Cancel"), 0, 1);
		connect(m_progressDialog, SIGNAL(canceled()), SLOT(cancel()));
	
	m_currentDownload = 0;
	m_currentExecute = 0;
	
	QDir tempDir(QDir::tempPath() + "/mecanique-updates");
	if (tempDir.exists() and tempDir.count() != 0)
		{
		QStringList tempFiles(tempDir.entryList());
		for (int i=0 ; i < tempFiles.size() ; i++)
			{
				QFile(QDir::tempPath() + "/mecanique-updates/" + tempFiles.at(i)).remove();
			}
		}
	else if (!tempDir.exists()) tempDir.mkpath(tempDir.path());
}

MecUpdater::~MecUpdater()
{
	while (m_actions.size() > 0)
		{
		m_actionsType.takeFirst() ? delete (Download*)m_actions.takeFirst() : delete (Execute*)m_actions.takeFirst();
		}
		
	delete m_currentDownload;
	delete m_network;
	delete m_currentExecute;
}

void MecUpdater::run()
{
	//Préparation de la requête.
	QNetworkRequest tempRequest(QUrl("https://updates.mecanique.cc/updater.php"));
	
	QString tempOS;
	#ifdef Q_OS_LINUX
	tempOS = "Linux";
	#elif defined Q_OS_MAC
	tempOS = "Macintosh ";
	switch (QSysInfo::macVersion())
		{
		case QSysInfo::MV_10_6 :
		tempOS += "OS X 10.6";
		break;
		case QSysInfo::MV_10_7 :
		tempOS += "OS X 10.7";
		break;
		case QSysInfo::MV_10_8 :
		tempOS += "OS X 10.8";
		break;
		case QSysInfo::MV_10_9 :
		tempOS += "OS X 10.9";
		break;
		case QSysInfo::MV_10_10 :
		tempOS += "OS X 10.10";
		break;
		default:
		tempOS += "-undefined-";
		break;
		}
	#elif defined Q_OS_WIN
	tempOS = "Windows ";
	switch (QSysInfo::windowsVersion())
		{
		case QSysInfo::WV_XP :
		tempOS += "XP";
		break;
		case QSysInfo::WV_2003 :
		tempOS += "2003";
		break;
		case QSysInfo::WV_VISTA :
		tempOS += "Vista";
		break;
		case QSysInfo::WV_WINDOWS7 :
		tempOS += "7";
		break;
		case QSysInfo::WV_WINDOWS8 :
		tempOS += "8";
		break;
		case QSysInfo::WV_WINDOWS8_1 :
		tempOS += "8.1";
		break;
		default:
		tempOS += "-undefined-";
		break;
		}
	#endif
	
	QUrlQuery tempParams;
	
	tempParams.addQueryItem("version", QCoreApplication::applicationVersion());
	tempParams.addQueryItem("os", tempOS);
	tempParams.addQueryItem("locale", QLocale::system().name());
	
	QList<MecAbstractPlugin*> tempPlugins = m_pluginsManager->plugins();
	for (int i=0 ; i < tempPlugins.size() ; i++)
		{
		tempParams.addQueryItem("plugin-" + tempPlugins.at(i)->name(), "");
		tempParams.addQueryItem("plugin-" + tempPlugins.at(i)->name() + "-version", QString::number(tempPlugins.at(i)->version(), 'f'));
		tempParams.addQueryItem("plugin-" + tempPlugins.at(i)->name() + "-role", MecPluginsManager::roleText(tempPlugins.at(i)->role(), false));
		tempParams.addQueryItem("plugin-" + tempPlugins.at(i)->name() + "-type", tempPlugins.at(i)->type());
		}
	
	tempRequest.setHeader(QNetworkRequest::ContentTypeHeader, "application/x-www-form-urlencoded");
	tempRequest.setRawHeader("User-Agent", QString("Mecanique/" + QCoreApplication::applicationVersion() + " (" + tempOS + "; " + QLocale::system().name() + ")").toLatin1());
	
	m_updateReply = m_network->post(tempRequest, tempParams.toString(QUrl::FullyEncoded).toLatin1());
	connect(m_network, SIGNAL(sslErrors(QNetworkReply*, QList<QSslError>)), m_updateReply, SLOT(ignoreSslErrors()));
	connect(m_updateReply, SIGNAL(finished()), SLOT(updatesReceived()));
}

void MecUpdater::cancel()
{
	while (m_actions.size() > 0)
		{
		m_actionsType.takeFirst() ? delete (Download*)m_actions.takeFirst() : delete (Execute*)m_actions.takeFirst();
		}
	m_indexActions = 0;

	if (m_currentDownload != 0) {
		disconnect(m_currentDownload, SIGNAL(finished()), this, SLOT(downloadFinished()));
		m_currentDownload->stop();
	}
	
	if (m_currentExecute != 0) m_currentExecute->terminate();
	
	m_progressDialog->hide();
}

void MecUpdater::parseUpdates(QByteArray UpdatesXml)
{
	QXmlStreamReader reader(UpdatesXml);
	
	reader.readNextStartElement();
	
	if (reader.name() == "update")
		{
		if (reader.attributes().value("type") == "update") m_isUpgrade = false;
		else if (reader.attributes().value("type") == "upgrade") m_isUpgrade = true;
		else if (reader.attributes().value("type") == "none") return;
		else
			{
			return;
			}
		
		while (reader.readNext() and (!(reader.isEndElement() and reader.name() == "update")) and !reader.atEnd())
			{
			if (reader.name() == "download" and reader.tokenType() == QXmlStreamReader::StartElement)
				{
				MecUpdater::Download *tempDownload = new MecUpdater::Download();
				tempDownload->name = reader.attributes().value("name").toString();
				tempDownload->src = reader.attributes().value("src").toString();
				tempDownload->size = reader.attributes().value("size").toString().toLongLong();
				tempDownload->hash = reader.attributes().value("hash").toString().toLatin1();
				
				m_actions.append(tempDownload);
				
				m_totalDownloadSize += tempDownload->size;
				
				m_actionsType.append(true);
				}
			else if (reader.name() == "execute" and reader.tokenType() == QXmlStreamReader::StartElement)
				{
				MecUpdater::Execute *tempExecute = new MecUpdater::Execute();
				
				tempExecute->name = reader.attributes().value("name").toString();
				tempExecute->parameters = reader.attributes().value("parameters").toString();

				if (reader.attributes().value("closeall") == "true") tempExecute->closeAll = true;
				else tempExecute->closeAll = false;
				
				if (reader.attributes().value("environment") == "updatedir") tempExecute->environment = false;
				else tempExecute->environment = true;
				
				m_actions.append(tempExecute);
				
				m_actionsType.append(false);
				}
			}
		}
	else
		{
		return;
		}

	if (reader.error() != QXmlStreamReader::NoError)
		{
		while (m_actions.size() > 0)
			{
			m_actionsType.takeFirst() ? delete (Download*)m_actions.takeFirst() : delete (Execute*)m_actions.takeFirst();
			}
		return;
		}
}

bool MecUpdater::askUser()
{
	QString TextSize(tr("Download size:"));
	if (m_totalDownloadSize <= 1024 * 2) TextSize += QString::number(m_totalDownloadSize) + " o";
	else if (m_totalDownloadSize > 1024 * 2 and m_totalDownloadSize <= 1024 * 1000) TextSize += QString::number(m_totalDownloadSize / 1024) + " Kio";
	else TextSize += QString::number(m_totalDownloadSize / (1024 * 1024)) + " Mio";
	
	QString TextCloseAll;
	bool MustCloseAll = false;
	for (int i=0 ; i < m_actions.size() and !MustCloseAll ; i++)
		{
		if (m_actionsType.at(i) == false and ((Execute*)m_actions.at(i))->closeAll) MustCloseAll=true;
		}
	if (MustCloseAll) TextCloseAll = "<p>" + tr("Mécanique and all related programs should be stopped.") + "</p>";
	
	QString TextUpdate;
	QString TitleBox;
	if (!m_isUpgrade)
		{
		TextUpdate = tr("An update of the components of Mécanique is available, do you want to download and apply it now?", "Text corresponding to an update / minor changes.");
		TitleBox = tr("Update available");
		}
	else
		{
		TextUpdate = tr("An upgrade of Mécanique is available, do you want to download and install it now?", "Text corresponding to an upgrade of Mécanique / change version / major changes.");
		TitleBox = tr("New version available");
		}
	
	if (QMessageBox::information(0, TitleBox,
			"<h3>" + TitleBox + "</h3><p>" + TextUpdate + "</p><p>" + TextSize + "</p>" + TextCloseAll,
			QMessageBox::Apply | QMessageBox::Close, QMessageBox::Apply)
		== QMessageBox::Apply)
		{
		return true;
		}
	else return false;
}
	
void MecUpdater::doAction()
{
	if (m_indexActions >= m_actions.size())
		{
		m_progressDialog->hide();
		return;
		}
	
	if (m_actionsType.at(m_indexActions) == true)//Download
		{
		MecUpdater::Download *tempDownload = (Download*)m_actions.at(m_indexActions);
		delete m_currentDownload;
		
		m_currentDownload = new MecDownloader(tempDownload->src, QDir::tempPath() + "/mecanique-updates/" + tempDownload->name, m_network);
		
		connect(m_currentDownload, SIGNAL(bytesWritten(qint64)), SLOT(downloadProgress(qint64)));
		connect(m_currentDownload, SIGNAL(finished()), SLOT(downloadFinished()));
		
		m_currentDownload->start();
		
		if (!m_progressDialog->isVisible()) m_progressDialog->show();
		}
	else //Execute
		{
		MecUpdater::Execute *tempExecute = (Execute*)m_actions.at(m_indexActions);
		delete m_currentExecute;
		
		if (!tempExecute->closeAll)
			{
			m_currentExecute = new QProcess(this);
			
			connect(m_currentExecute, SIGNAL(finished(int, QProcess::ExitStatus)), SLOT(execFinished()));
			
			if (!tempExecute->environment)
				{
				QProcessEnvironment tempEnv = QProcessEnvironment::systemEnvironment();
				tempEnv.insert("PATH", QDir::tempPath() + "\\mecanique-updates" + ";" + tempEnv.value("PATH"));
				m_currentExecute->setProcessEnvironment(tempEnv);
				m_currentExecute->start(QDir::tempPath() + "\\mecanique-updates\\" + tempExecute->name + " " + tempExecute->parameters);
				}
			else m_currentExecute->start(tempExecute->name + " " + tempExecute->parameters);
			}
		else //tempExecute.closeAll == true
			{
			m_progressDialog->hide();
			QMessageBox::StandardButton tempButton = QMessageBox::information(0, tr("Mécanique closure required"), tr("<h3>Mécanique closing required</h3><p>Mécanique must be closed to continue its update.</p>"), QMessageBox::Apply, QMessageBox::Apply);
			
			if (tempButton == QMessageBox::Apply)
				{
				connect(qApp, SIGNAL(aboutToQuit()), SLOT(appQuit()));
				//appQuit();
				qApp->closeAllWindows();
				}
			else return;
			}
		}
}
	
void MecUpdater::updatesReceived()
{
	/*
	Code normalement effectif, mais suspendu pour la procédure actuelle de mise à jour.
	if (m_updateReply->error() == QNetworkReply::NoError)
		{
		parseUpdates(m_updateReply->readAll());
		
		if (m_actions.size() > 0)
			{
			if (askUser())
				{
				m_progressDialog->setMaximum(m_totalDownloadSize);
				doAction();
				}
			else
				{
				return;
				}
			}
		else
			{
			return;
			}
		}
	else
		{
		return;
		}*/
	//Code de remplacement actuel.
	if (m_updateReply->error() == QNetworkReply::NoError)
		{
		QString tempReply = QString::fromUtf8(m_updateReply->readAll());
		
		if (tempReply != "none" and tempReply.startsWith("http"))
			{
			QMessageBox::information(0, tr("Updates available..."), tr("<h3>Updates available</h3><p>Updates of Mécanique are available, to download it, see following:<br /><a href=\"%1\">%1</a></p>").arg(tempReply));
			}
		}
	else
		{
		return;
		}
}
	
void MecUpdater::downloadProgress(qint64 Number)
{
	m_downloadedSize += Number;
	m_progressDialog->setValue(m_downloadedSize);
}

void MecUpdater::downloadFinished()
{
	if (m_currentDownload->networkReply()->error() != QNetworkReply::NoError)
		{
		QMessageBox::StandardButton tempButton = QMessageBox::critical(m_progressDialog, tr("Download error"), tr("<h3>Download error</h3><p>The download of the file “%1” from “%2” has failed.<br />What do you want to do?</p>").arg(((Download*)m_actions.at(m_indexActions))->name).arg(((Download*)m_actions.at(m_indexActions))->src), QMessageBox::Retry | QMessageBox::Cancel, QMessageBox::Retry);
		
		if (tempButton == QMessageBox::Retry)
			{
			m_downloadedSize -= ((Download*)m_actions.at(m_indexActions))->size;
			doAction();
			return;
			}
		else //tempButton == QMessageBox::Cancel
			{
			m_progressDialog->hide();
			return;
			}
		}
	else
		{
		//Vérification du fichier.
		QFile tempFile(QDir::tempPath() + "/mecanique-updates/" + ((Download*)m_actions.at(m_indexActions))->name);
		if (tempFile.open(QIODevice::ReadOnly))
			{
			QCryptographicHash tempHash(QCryptographicHash::Md5);
			tempHash.addData(&tempFile);
			
			if (tempHash.result().toHex() == ((Download*)m_actions.at(m_indexActions))->hash)
				{
				m_indexActions++;
				doAction();
				return;
				}
			else
				{
				QMessageBox::StandardButton tempButton = QMessageBox::critical(m_progressDialog, tr("Download error"), tr("<h3>Download error</h3><p>The checking of the file “%1” from “%2” has failed, he has been probably corrupted during the download.<br />What do you want to do?</p>").arg(((Download*)m_actions.at(m_indexActions))->name).arg(((Download*)m_actions.at(m_indexActions))->src), QMessageBox::Retry | QMessageBox::Cancel, QMessageBox::Retry);
				if (tempButton == QMessageBox::Retry)
					{
					m_downloadedSize -= ((Download*)m_actions.at(m_indexActions))->size;
					doAction();
					return;
					}
				else //tempButton == QMessageBox::Cancel
					{
					m_progressDialog->hide();
					return;
					}
				}
			}
		else
			{
			QMessageBox::StandardButton tempButton = QMessageBox::critical(m_progressDialog, tr("Download error"), tr("<h3>Download error</h3><p>The reading of the file “%1” from “%2” has failed.<br />What do you want to do?</p>").arg(((Download*)m_actions.at(m_indexActions))->name).arg(((Download*)m_actions.at(m_indexActions))->src), QMessageBox::Retry | QMessageBox::Cancel, QMessageBox::Retry);
			if (tempButton == QMessageBox::Retry)
				{
				m_downloadedSize -= ((Download*)m_actions.at(m_indexActions))->size;
				doAction();
				return;
				}
			else //tempButton == QMessageBox::Cancel
				{
				m_progressDialog->hide();
				return;
				}
			}
		}
}
	
void MecUpdater::execFinished()
{
	if (m_currentExecute->exitStatus() == QProcess::NormalExit and m_currentExecute->exitCode() == 0)
		{
		m_indexActions++;
		doAction();
		return;
		}
	else
		{
		QMessageBox::critical(m_progressDialog, tr("Update error"), tr("<h3>Update error</h3><p>The update process has failed.</p>"));
		return;
		}
}

void MecUpdater::appQuit()
{
	QProcess::startDetached(QDir::tempPath() + "\\mecanique-updates\\" + ((Execute*)m_actions.at(m_indexActions))->name + " " + ((Execute*)m_actions.at(m_indexActions))->parameters);
}



