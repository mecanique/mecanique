/*
© Quentin VIGNAUD, 2013

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#include "MecCompiler.h"

MecCompiler::MecCompiler(MecProject* const Project, MecAbstractPluginsManager* const PluginsManager, QObject* Parent) : QObject(Parent), m_project (Project), m_pluginsManager (PluginsManager)
{
m_qmake = 0;
m_make = 0;
m_running = false;
reset();
}

MecCompiler::~MecCompiler()
{
reset();
}
	
MecProject* MecCompiler::project() const
{
return m_project;
}

MecAbstractPluginsManager* MecCompiler::pluginsManager() const
{
return m_pluginsManager;
}

MecCompiler::Error MecCompiler::error() const
{
return m_error;
}
	
QString MecCompiler::buildDirectory() const
{
return m_buildDirectory;
}
	
QString MecCompiler::log() const
{
return m_log;
}
	
bool MecCompiler::isRunning() const
{
return m_running;
}
	
void MecCompiler::start()
{
if (!m_running)
	{
	reset();
	
	m_running = true;
	emit started();
	emit runningChanged(m_running);
	
	prepareBuildDirectory();
	
	if (m_running) collectCompilers();
	if (m_running) collectResources();
	if (m_running) installResources();
	if (m_running) collectHeaders();
	if (m_running) installHeaders();
	if (m_running) collectSources();
	if (m_running) installSources();
	if (m_running) collectProjectInstructions();
	if (m_running) installProjectInstructions();
	if (m_running) loadQmake();//Après quoi l'algorithme de compilation enchaînera après l'arrêt du processus « qmake »
	}
}

void MecCompiler::stop()
{
stop(MecCompiler::Cancelled);
}

void MecCompiler::setBuildDirectory(QString BuildDirectory)
{
if (!m_running)
	{
	m_buildDirectory = BuildDirectory;
	emit buildDirectoryChanged(m_buildDirectory);
	}
}
	
void MecCompiler::prepareBuildDirectory()
{
QFileInfo tempFileInfo(m_buildDirectory);
if (!tempFileInfo.exists())
	{
	writeLog(tr("Error #") + "1.1" + tr(": The build directory “") + m_buildDirectory + tr("” is not existing.\n"));
	stop(MecCompiler::BuildDirectoryError);
	}
else if (!tempFileInfo.isDir())
	{
	writeLog(tr("Error #") + "1.2" + tr(": “") + m_buildDirectory + tr("” is not a directory.\n"));
	stop(MecCompiler::BuildDirectoryError);
	}
else if (!tempFileInfo.isWritable())
	{
	writeLog(tr("Error #") + "1.3" + tr(": The build directory “") + m_buildDirectory + tr("” is not writable.\n"));
	stop(MecCompiler::BuildDirectoryError);
	}
else
	{
	QDir tempDir(m_buildDirectory);
	tempDir.mkdir("bin");
	tempDir.mkdir("doc");
	tempDir.mkdir("lib");
	tempDir.mkdir("share");
	tempDir.mkdir("src");
	tempDir.mkdir("var");
	tempDir.mkdir("etc");
	writeLog(tr("Build directory “") + m_buildDirectory + tr("” checked.\n"));
	}
}

void MecCompiler::collectCompilers()
{
MecAbstractPlugin *tempPlugin = m_pluginsManager->plugin(m_project->elementRole(), m_project->elementType());

if (tempPlugin == 0)
	{
	writeLog(tr("Error #") + "2.1" + tr(": The project “") + m_project->elementName() + tr("” don't have appropriate plugin.\n"));
	stop(MecCompiler::NoPlugin);
	}
else
	{
	m_elementCompiler.append(tempPlugin->elementCompiler(m_project, this));
	
	bool Ok = true;
	int i = 0;
	do
		{
		m_elementCompiler.append(m_elementCompiler.at(i)->subCompilers());
		QList<MecPluginError> tempErrors = m_elementCompiler.at(i)->pluginsErrors();
		
		if (!tempErrors.isEmpty())
			{
			Ok = false;
			
			for (int j=0 ; j < tempErrors.size() ; j++)
				{
				writeLog(tr("Error #") + "2.2" + tr(": The element “") + tempErrors.at(i).element->elementName() + tr("” (type “") + tempErrors.at(i).element->elementType() + tr("”) don't have appropriate plugin."));
				if (tempErrors.at(i).name.isEmpty()) writeLog(" " + tr("No particular plugin wanted.\n"));
				else writeLog(" " + tr("Plugin “") + tempErrors.at(i).name + tr("” wanted.\n"));
				}
			}
		i++;
		}
		while (i < m_elementCompiler.size());
	
	if (!Ok)
		{
		stop(MecCompiler::NoPlugin);
		}
	}
}
	
void MecCompiler::collectResources()
{
m_resources.append(new QResource(":/src/mecanique/mecanique.h"));
m_resources.append(new QResource(":/src/mecanique/Project.h"));
m_resources.append(new QResource(":/src/mecanique/Project.cpp"));
m_resources.append(new QResource(":/src/mecanique/Log.h"));
m_resources.append(new QResource(":/src/mecanique/Log.cpp"));
m_resources.append(new QResource(":/src/mecanique/Object.h"));
m_resources.append(new QResource(":/src/mecanique/Object.cpp"));
m_resources.append(new QResource(":/share/icons/mecanique.png"));
m_resources.append(new QResource(":/share/icons/status/operational.png"));
m_resources.append(new QResource(":/share/icons/status/warning.png"));
m_resources.append(new QResource(":/share/icons/status/unknown.png"));
m_resources.append(new QResource(":/share/icons/status/error.png"));
m_resources.append(new QResource(":/share/translations/mecanique.fr.qm"));
for (int i=0 ; i < m_elementCompiler.size() ; i++)
	{
	m_resources.append(m_elementCompiler.at(i)->resources());
	}
//Suppression des doublons.
QSet<QResource*> tempSet = m_resources.toSet();
m_resources = tempSet.toList();
}

void MecCompiler::installResources()
{
for (int i=0 ; i < m_resources.size() ; i++)
	{
	QFile tempResource(m_resources.at(i)->fileName());
	
	if (!tempResource.open(QIODevice::ReadOnly))
		{
		writeLog(tr("Error #") + "4.1" + tr(": The resource “") + m_resources.at(i)->fileName() + tr("” cannot be read. This is a internal error.\n"));
		stop(MecCompiler::SystemCompilerError);
		return;
		}
	
	QFileInfo tempFileInfo(m_buildDirectory + m_resources.at(i)->fileName().remove(0, 1));//remove = suppression du ":" d'entrée d'arborescence des ressources
	QDir tempDir(m_buildDirectory);
	
	if (!tempDir.mkpath(tempFileInfo.absolutePath()))
		{
		writeLog(tr("Error #") + "4.2" + tr(": The directory “") + tempFileInfo.canonicalPath() + tr("” cannot be created.\n"));
		stop(MecCompiler::BuildDirectoryError);
		}
	
	QFile tempFile(m_buildDirectory + m_resources.at(i)->fileName().remove(0, 1));//remove = suppression du ":" d'entrée d'arborescence des ressources
	if (tempFile.open(QIODevice::WriteOnly | QIODevice::Truncate))
		{
		tempFile.write(tempResource.readAll());
		}
	else
		{
		writeLog(tr("Error #") + "4.3" + tr(": The file “") + tempFile.fileName() + tr("” cannot be written.\n"));
		stop(MecCompiler::BuildDirectoryError);
		}
	
	tempResource.close();
	tempFile.close();
	}
}
	
void MecCompiler::collectHeaders()
{
for (int i=0 ; i < m_elementCompiler.size() ; i++)
	{
	m_headers.append(QPair<QString,QString>(m_elementCompiler.at(i)->element()->elementName(), m_elementCompiler.at(i)->header()));
	}
}

void MecCompiler::installHeaders()
{
for (int i=0 ; i < m_headers.size() ; i++)
	{
	if (!m_headers.at(i).second.isEmpty())
		{
		QFile tempFile(m_buildDirectory + "/src/" + m_headers.at(i).first + ".h");
		if (tempFile.open(QIODevice::WriteOnly | QIODevice::Truncate))
			{
			tempFile.write(m_headers.at(i).second.toUtf8());
			}
		else
			{
			writeLog(tr("Error #") + "6.1" + tr(": The file “") + tempFile.fileName() + tr("” cannot be written.\n"));
			stop(MecCompiler::BuildDirectoryError);
			}
		
		tempFile.close();
		}
	}
}
	
void MecCompiler::collectSources()
{
for (int i=0 ; i < m_elementCompiler.size() ; i++)
	{
	m_sources.append(QPair<QString,QString>(m_elementCompiler.at(i)->element()->elementName(), m_elementCompiler.at(i)->source()));
	}
}

void MecCompiler::installSources()
{
for (int i=0 ; i < m_sources.size() ; i++)
	{
	if (!m_sources.at(i).second.isEmpty())
		{
		QFile tempFile(m_buildDirectory + "/src/" + m_sources.at(i).first + ".cpp");
		if (tempFile.open(QIODevice::WriteOnly | QIODevice::Truncate))
			{
			tempFile.write(m_sources.at(i).second.toUtf8());
			}
		else
			{
			writeLog(tr("Error #") + "8.1" + tr(": The file “") + tempFile.fileName() + tr("” cannot be written.\n"));
			stop(MecCompiler::BuildDirectoryError);
			}
		
		tempFile.close();
		}
	}
//Special for "main.cpp"
QFile tempFile(m_buildDirectory + "/src/main.cpp");
if (tempFile.open(QIODevice::WriteOnly | QIODevice::Truncate))
	{
	QFile tempMainFile(":/src/main.cpp");
	if (tempMainFile.open(QIODevice::ReadOnly))
		{
		QString tempContent = tempMainFile.readAll();
		tempMainFile.close();
		
		tempContent.replace("[[PROJECTNAME]]", project()->elementName());
		
		tempFile.write(tempContent.toUtf8());
		}
	else
		{
		writeLog(tr("Error #") + "8.2" + tr(": The original file “main.cpp” cannot be read. This is an internal error.\n"));
		stop(MecCompiler::SystemCompilerError);
		}
	tempFile.close();
	}
else
	{
	writeLog(tr("Error #") + "8.1" + tr(": The file “") + tempFile.fileName() + tr("” cannot be written.\n"));
	stop(MecCompiler::BuildDirectoryError);
	}
}
	
void MecCompiler::collectProjectInstructions()
{
m_projectInstructions += tr("#####\n#Generated automatically by Mécanique\n#\n");
m_projectInstructions += "#" + QDateTime::currentDateTime().toString("dd'/'MM'/'yyyy HH:mm:ss.zzz") + "\n#####\n\n\n##Mécanique-core area\n";

m_projectInstructions += "TEMPLATE = app\nDESTDIR = ../bin\nCONFIG += \nQT += widgets\nDEPENDPATH += . \nINCLUDEPATH += . \nCODECFORSRC = UTF-8\nCODECFORTR = UTF-8\n\n";

m_projectInstructions += "##Mécanique-plugins area\n";

m_projectInstructions += "HEADERS += ";
m_projectInstructions += "./mecanique/Project.h ./mecanique/Log.h ./mecanique/Object.h ";
for (int i=0 ; i < m_headers.size() ; i++)
	{
	if (!m_headers.at(i).second.isEmpty()) m_projectInstructions += m_headers.at(i).first + ".h ";
	}
m_projectInstructions += "\n\n";

m_projectInstructions += "SOURCES += ";
m_projectInstructions += "main.cpp ./mecanique/Project.cpp ./mecanique/Log.cpp ./mecanique/Object.cpp ";
for (int i=0 ; i < m_sources.size() ; i++)
	{
	if (!m_sources.at(i).second.isEmpty()) m_projectInstructions += m_sources.at(i).first + ".cpp ";
	}
m_projectInstructions += "\n\n";

QStringList tempInstructionsList;
for (int i=0 ; i < m_elementCompiler.size() ; i++)
	{
	tempInstructionsList.append(m_elementCompiler.at(i)->projectInstructions());
	} 

//Suppression des doublons.
QSet<QString> tempSet = tempInstructionsList.toSet();
tempInstructionsList = tempSet.toList();

m_projectInstructions += tempInstructionsList.join('\n');
}

void MecCompiler::installProjectInstructions()
{
QFile tempFile(m_buildDirectory + "/src/" + m_project->elementName() + ".pro");
if (tempFile.open(QIODevice::WriteOnly | QIODevice::Truncate))
	{
	tempFile.write(m_projectInstructions.toUtf8());
	tempFile.close();
	}
else
	{
	writeLog(tr("Error #") + "10.1" + tr(": The file “") + tempFile.fileName() + tr("” cannot be written.\n"));
	stop(MecCompiler::BuildDirectoryError);
	}
}
	
void MecCompiler::loadQmake()
{
m_qmake = new QProcess(this);
m_qmake->setWorkingDirectory(m_buildDirectory + "/src");
connect(m_qmake, SIGNAL(error(QProcess::ProcessError)), SLOT(qmakeError(QProcess::ProcessError)));
connect(m_qmake, SIGNAL(readyReadStandardError()), SLOT(qmakeReadOutput()));
connect(m_qmake, SIGNAL(readyReadStandardOutput()), SLOT(qmakeReadOutput()));
connect(m_qmake, SIGNAL(finished(int, QProcess::ExitStatus)), SLOT(qmakeFinished(int, QProcess::ExitStatus)));

m_qmake->start("qmake", QStringList() << "-makefile" << "-Wall");
}
	
void MecCompiler::loadMake()
{
m_make = new QProcess(this);
m_make->setWorkingDirectory(m_buildDirectory + "/src");
connect(m_make, SIGNAL(error(QProcess::ProcessError)), SLOT(makeError(QProcess::ProcessError)));
connect(m_make, SIGNAL(readyReadStandardError()), SLOT(makeReadOutput()));
connect(m_make, SIGNAL(readyReadStandardOutput()), SLOT(makeReadOutput()));
connect(m_make, SIGNAL(finished(int, QProcess::ExitStatus)), SLOT(makeFinished(int, QProcess::ExitStatus)));

#ifndef Q_OS_WIN
m_make->start("make", QStringList() << "--always-make");
#else
m_make->start("mingw32-make", QStringList() << "--always-make");
#endif

}
	
void MecCompiler::writeLog(QString Text)
{
m_log += Text;
emit news();
}

void MecCompiler::reset()
{
m_log.clear();
while (m_elementCompiler.size() != 0) delete m_elementCompiler.takeFirst();
while (m_resources.size() != 0) delete m_resources.takeFirst();
m_headers.clear();
m_sources.clear();
m_projectInstructions.clear();
delete m_qmake;
delete m_make;
}
	
void MecCompiler::stop(MecCompiler::Error Error)
{
if (m_running)
	{
	m_running = false;
	m_error = Error;
	if (m_qmake != 0)
		{
		m_qmake->blockSignals(true);
		m_qmake->terminate();
		}
	if (m_make != 0)
		{
		m_make->blockSignals(true);
		m_make->terminate();
		}
	emit stopped();
	emit runningChanged(m_running);
	emit terminate(Error);
	}
}
	
void MecCompiler::qmakeError(QProcess::ProcessError Error)
{
if (Error != QProcess::Crashed)
	{
	writeLog(tr("Error #") + "11.1" + tr(": The sub-process “qmake” cannot be loaded.\n"));
	stop(MecCompiler::QmakeUnavailable);
	}
else
	{
	writeLog(tr("Error #") + "11.2" + tr(": The sub-process “qmake” has crashed.\n"));
	stop(MecCompiler::SystemCompilerError);
	}
}

void MecCompiler::makeError(QProcess::ProcessError Error)
{
if (Error != QProcess::Crashed)
	{
	writeLog(tr("Error #") + "13.1" + tr(": The sub-process “make” cannot be loaded.\n"));
	stop(MecCompiler::MakeUnavailable);
	}
else
	{
	writeLog(tr("Error #") + "13.2" + tr(": The sub-process “make” has crashed.\n"));
	stop(MecCompiler::SystemCompilerError);
	}
}
	
void MecCompiler::qmakeReadOutput()
{
writeLog(m_qmake->readAllStandardError() + m_qmake->readAllStandardOutput());
}

void MecCompiler::makeReadOutput()
{
writeLog(m_make->readAllStandardError() + m_make->readAllStandardOutput());
}
	
void MecCompiler::qmakeFinished(int ExitCode, QProcess::ExitStatus ExitStatus)
{
if (ExitStatus == QProcess::CrashExit) return; //Cas pris en charge par « qmakeError() »

if (ExitCode == EXIT_SUCCESS)
	{
	loadMake();
	}
else
	{
	writeLog(tr("Error #") + "12.1" + tr(": Error detected by “qmake” sub-process. This is a internal error.\n"));
	stop(MecCompiler::SystemCompilerError);
	}
}
	
void MecCompiler::makeFinished(int ExitCode, QProcess::ExitStatus ExitStatus)
{
if (ExitStatus == QProcess::CrashExit) return; //Cas pris en charge par « makeError() »

if (ExitCode == EXIT_SUCCESS)
	{
	writeLog(tr("Compilation complete.\n"));
	stop(MecCompiler::NoError);
	}
else
	{
	writeLog(tr("Error #") + "14.1" + tr(": Error detected by “make” sub-process.\n"));
	stop(MecCompiler::UserCompilerError);
	}

//Nettoyage du répertoire de compilation.
#ifndef Q_OS_WIN
m_make->start("make", QStringList() << QString("--directory=" + m_buildDirectory + "/src") << "clean");
#else
m_make->start("mingw32-make", QStringList() << QString("--directory=" + m_buildDirectory + "/src") << "clean");

QFile tempLoader(m_buildDirectory + "/bin/loader.bat");
if (tempLoader.open(QIODevice::WriteOnly | QIODevice::Truncate))
	{
	tempLoader.write(QString(
	"@echo off\r\n\
	title M‚canique loader\r\n\
	set PATH=C:\\Mecanique\\bin;C:\\Mecanique\\qt\\5.2.1\\mingw48_32\\bin;C:\\Mecanique\\qt\\Tools\\mingw48_32\\bin;%PATH%\r\n\
	echo Works with M‚canique " + QCoreApplication::applicationVersion() + "\r\n\
	echo Running " + project()->elementName() + "...\r\n\
	" + project()->elementName() + ".exe\r\n\
	exit\r\n"
	).toLocal8Bit());
	
	tempLoader.close();
	}
#endif
}



