/*
© Quentin VIGNAUD, 2013

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#include "MecCompilerDialog.h"

MecCompilerDialog::MecCompilerDialog(MecProject* const Project, MecPluginsManager* const PluginsManager, QString DefaultBuildDirectory, QWidget *Parent, Qt::WindowFlags Flags) : QWidget(Parent, Flags), m_compiler (new MecCompiler(Project, PluginsManager))
{

setWindowTitle(tr("Compilation"));

m_compiler->setBuildDirectory(DefaultBuildDirectory);
connect(m_compiler, SIGNAL(started()), SLOT(started()));
connect(m_compiler, SIGNAL(news()), SLOT(newsAvailable()));
connect(m_compiler, SIGNAL(terminate(MecCompiler::Error)), SLOT(terminate(MecCompiler::Error)));

labelBuildDirectory = new QLabel(tr("Build directory :"), this);

lineEditBuildDirectory = new QLineEdit(DefaultBuildDirectory, this);
	connect(lineEditBuildDirectory, SIGNAL(textEdited(QString)), m_compiler, SLOT(setBuildDirectory(QString)));
	connect(m_compiler, SIGNAL(buildDirectoryChanged(QString)), lineEditBuildDirectory, SLOT(setText(QString)));
	connect(m_compiler, SIGNAL(runningChanged(bool)), lineEditBuildDirectory, SLOT(setDisabled(bool)));

pushButtonChooseBuildDirectory = new QPushButton(QIcon::fromTheme("folder"), tr("Choose…"), this);
	connect(pushButtonChooseBuildDirectory, SIGNAL(clicked(bool)), SLOT(chooseBuildDirectory()));
	connect(m_compiler, SIGNAL(runningChanged(bool)), pushButtonChooseBuildDirectory, SLOT(setDisabled(bool)));

pushButtonCompilation = new QPushButton(QIcon::fromTheme("application-x-executable"), tr("Compile"), this);
	connect(pushButtonCompilation, SIGNAL(clicked(bool)), SLOT(start()));

labelCompilationLog = new QLabel(tr("Compilation log : "), this);

textEditCompilationLog = new QTextEdit(this);
	textEditCompilationLog->setReadOnly(true);

pushButtonExec = new QPushButton(QIcon::fromTheme("media-playback-start"), tr("Execute"), this);
	pushButtonExec->hide();
	connect(pushButtonExec, SIGNAL(clicked(bool)), SLOT(execute()));
	
layoutMain = new QGridLayout(this);
	layoutMain->addWidget(labelBuildDirectory, 0, 0);
	layoutMain->addWidget(lineEditBuildDirectory, 1, 0);
	layoutMain->addWidget(pushButtonChooseBuildDirectory, 1, 1);
	layoutMain->addWidget(pushButtonCompilation, 2, 0, 1, 2);
	layoutMain->addWidget(labelCompilationLog, 3, 0);
	layoutMain->addWidget(textEditCompilationLog, 4, 0, 1, 2);
	layoutMain->addWidget(pushButtonExec, 5, 0, 1, 2);
}

MecCompilerDialog::~MecCompilerDialog()
{
delete m_compiler;
}
	
MecProject* MecCompilerDialog::project() const
{
return m_compiler->project();
}
	
bool MecCompilerDialog::showLicenceInfos()
{
QMessageBox tempDialog;
tempDialog.setWindowTitle(tr("Informations about the licence..."));
tempDialog.setIcon(QMessageBox::Information);
tempDialog.addButton(tr("Accept"), QMessageBox::AcceptRole);
tempDialog.addButton(tr("Reject"), QMessageBox::RejectRole);

tempDialog.setText(tr("<h3>Informations about the licence</h3><p>The program which is going to be generated will integrate of the code under license EUPL (\"European Union Public Licence\"), which will apply as a consequence to the whole work which it constitutes as well as in the works which will be diverted from it and/or which will integrate it of any way, as a video, a show, or any other shape of work.</p><p>It means that you grant to whoever the following rights ones: <ul><li>use the work in any circumstance and for all usage,</li><li>reproduce the work,</li><li>modify the original work, and make derivative works based upon the work,</li><li>communicate to the public, including the right to make available or display the work or copies thereof to the public and perform publicly, as the case may be, the work,</li><li>distribute the work or copies thereof,</li><li>lend and rent the work or copies thereof,</li><li>sub-license rights in the Work or copies thereof,</li></ul>and that you guarantee the concession of these rights, by the terms of the EUPL.</p><p>If you do not arrange right necessities to grant so the work or its sub-works, either that you do not wish to make it, it is necessary to you to acquire another license.</p><p><a href=\"http://joinup.ec.europa.eu/software/page/eupl/licence-eupl\">EUPL in 22 languages</a><br /><a href=\"https://joinup.ec.europa.eu/system/files/EN/EUPL v.1.1 - Licence.pdf\">Text of the licence in English</a></p>"));
/*
<h3>Informations relatives à la licence</h3><p>Le programme qui va être généré intègrera du code sous licence EUPL (<i>« European Union Public Licence »</i>), qui s'appliquera en conséquence à l'ensemble de l'œuvre informatique qu'il constitue ainsi qu'aux œuvres qui en seront dérivées et/ou qui l'intègreront d'une quelconque manière, comme une vidéo, un spectacle, ou toute autre forme d'œuvre.</p><p>Cela signifie que vous concédez à quiconque les droits suivants sur celles-ci : <ul><li>utiliser l’œuvre en toutes circonstances et pour tout usage,</li><li>reproduire l'œuvre,</li><li>modifier l’œuvre originale, et de faire des œuvres dérivées sur la base de l’œuvre,</li><li>communiquer, présenter ou représenter l'œuvre ou copie de celle-ci au public, en ce compris le droit de mettre celles-ci à la disposition du public,</li><li>distribuer l’œuvre ou des copies de celles-ci,</li><li>prêter et louer l’œuvre ou des copies de celles-ci,</li><li>sous-licencier les droits concédés ici sur l’œuvre ou sur des copies de celles-ci,</li></ul>et que vous vous portez garant de la concession de ces droits, d'après les termes de la licence EUPL.</p><p>Si vous ne disposez pas des droits nécessaires pour concéder ainsi l'œuvre ou ses dérivés, ou bien que vous ne souhaitez pas le faire, il vous faut acquérir une autre licence.</p><p><a href=\"http://joinup.ec.europa.eu/software/page/eupl/licence-eupl\">Page d'accès au texte de la licence (22 langues)</a><br /><a href=\"http://joinup.ec.europa.eu/system/files/FR/EUPL v.1.1 - Licence.pdf\">Texte de la licence en français</a></p><small>[FR-français]</small>
*/

if (tempDialog.exec() == QMessageBox::AcceptRole) return true;
else return false;
}
	
void MecCompilerDialog::start()
{
if (showLicenceInfos()) m_compiler->start();
}

void MecCompilerDialog::chooseBuildDirectory()
{
QString oldDir = m_compiler->buildDirectory();
if (oldDir.isEmpty()) oldDir = QDir::homePath();

QString tempDir = QFileDialog::getExistingDirectory(this, tr("Choose build directory for compilation"), oldDir);

if (!tempDir.isEmpty()) m_compiler->setBuildDirectory(tempDir);
}

void MecCompilerDialog::closeEvent(QCloseEvent *Event)
{
	if (m_compiler != 0 and m_compiler->isRunning())
	{
		m_compiler->stop();
	}
	
	Event->accept();
}

void MecCompilerDialog::started()
{
pushButtonCompilation->setText(tr("Stop"));
pushButtonCompilation->setIcon(QIcon::fromTheme("cancel"));
disconnect(pushButtonCompilation, SIGNAL(clicked(bool)), this, SLOT(start()));
connect(pushButtonCompilation, SIGNAL(clicked(bool)), m_compiler, SLOT(stop()));
pushButtonExec->hide();
}

void MecCompilerDialog::newsAvailable()
{
textEditCompilationLog->setPlainText(m_compiler->log());
textEditCompilationLog->verticalScrollBar()->setValue(textEditCompilationLog->verticalScrollBar()->maximum());
}

void MecCompilerDialog::terminate(MecCompiler::Error Error)
{
pushButtonCompilation->setText(tr("Compile"));
pushButtonCompilation->setIcon(QIcon::fromTheme("application-x-executable"));
disconnect(pushButtonCompilation, SIGNAL(clicked(bool)), m_compiler, SLOT(stop()));
connect(pushButtonCompilation, SIGNAL(clicked(bool)), this, SLOT(start()));
if (Error == MecCompiler::NoError) pushButtonExec->show();
textEditCompilationLog->verticalScrollBar()->setValue(textEditCompilationLog->verticalScrollBar()->maximum());
}

#include <iostream>
void MecCompilerDialog::execute()
{
	#ifndef Q_OS_WIN
	QStringList tempExecutable = QDir(m_compiler->buildDirectory() + "/bin").entryList(QStringList() << project()->elementName(), QDir::Files | QDir::Executable);
	#else
	QStringList tempExecutable = QDir(m_compiler->buildDirectory() + "/bin").entryList(QStringList() << project()->elementName() + ".exe", QDir::Files | QDir::Executable);
	#endif
	if (tempExecutable.size() > 0) QProcess::startDetached("\"" + m_compiler->buildDirectory() + "/bin/" + tempExecutable.at(0) + "\"");
}


