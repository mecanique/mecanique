/*
© Quentin VIGNAUD, 2014

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
 /****************************************************************************
 **
 ** Copyright (C) 2014 Digia Plc and/or its subsidiary(-ies).
 ** Contact: http://www.qt-project.org/legal
 **
 ** This file is part of the examples of the Qt Toolkit.
 **
 ** $QT_BEGIN_LICENSE:BSD$
 ** You may use this file under the terms of the BSD license as follows:
 **
 ** "Redistribution and use in source and binary forms, with or without
 ** modification, are permitted provided that the following conditions are
 ** met:
 **   * Redistributions of source code must retain the above copyright
 **     notice, this list of conditions and the following disclaimer.
 **   * Redistributions in binary form must reproduce the above copyright
 **     notice, this list of conditions and the following disclaimer in
 **     the documentation and/or other materials provided with the
 **     distribution.
 **   * Neither the name of Digia Plc and its Subsidiary(-ies) nor the names
 **     of its contributors may be used to endorse or promote products derived
 **     from this software without specific prior written permission.
 **
 **
 ** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 ** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 ** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 ** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 ** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 ** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 ** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 ** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 ** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 ** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 ** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
 **
 ** $QT_END_LICENSE$
 **
 ****************************************************************************/

#include "MecPlainTextEdit.h"

MecPlainTextEdit::MecPlainTextEdit(QWidget *Parent) : QPlainTextEdit(Parent)
{
	lineNumberArea = new MecPlainTextEdit::LineNumberArea(this);
	
	setUndoRedoEnabled(false);
	setTabStopWidth(40);

	connect(this, SIGNAL(blockCountChanged(int)), this, SLOT(updateLineNumberAreaWidth()));
	connect(this, SIGNAL(updateRequest(QRect,int)), this, SLOT(updateLineNumberArea(QRect,int)));
	connect(this, SIGNAL(cursorPositionChanged()), this, SLOT(highlightCurrentLine()));
	connect(this, SIGNAL(cursorPositionChanged()), this, SLOT(matchParentheses()));

	updateLineNumberAreaWidth();
	highlightCurrentLine();
}

int MecPlainTextEdit::lineNumberAreaWidth()
{
	int digits = 1;
	int max = qMax(1, blockCount());
	while (max >= 10) {
		max /= 10;
		++digits;
	}

	int space = 3 + fontMetrics().width(QLatin1Char('9')) * digits;

	return space;
}

void MecPlainTextEdit::lineNumberAreaPaintEvent(QPaintEvent *Event)
{
	QPainter painter(lineNumberArea);
	painter.fillRect(Event->rect(), Qt::lightGray);


	QTextBlock block = firstVisibleBlock();
	int blockNumber = block.blockNumber();
	int top = (int) blockBoundingGeometry(block).translated(contentOffset()).top();
	int bottom = top + (int) blockBoundingRect(block).height();

	while (block.isValid() && top <= Event->rect().bottom()) {
		if (block.isVisible() && bottom >= Event->rect().top()) {
			QString number = QString::number(blockNumber + 1);
			painter.setPen(Qt::black);
			painter.drawText(0, top, lineNumberArea->width(), fontMetrics().height(),
							 Qt::AlignRight, number);
		}

		block = block.next();
		top = bottom;
		bottom = top + (int) blockBoundingRect(block).height();
		++blockNumber;
	}
}

void MecPlainTextEdit::resizeEvent(QResizeEvent *Event)
{
	QPlainTextEdit::resizeEvent(Event);

	QRect cr = contentsRect();
	lineNumberArea->setGeometry(QRect(cr.left(), cr.top(), lineNumberAreaWidth(), cr.height()));
}

bool MecPlainTextEdit::matchLeftParenthesis(QTextBlock currentBlock, int i, int numLeftParentheses, MecPlainTextEdit::CharType type)
{
    MecSyntaxHighlighter::TextBlockData *data = static_cast<MecSyntaxHighlighter::TextBlockData *>(currentBlock.userData());
    QVector<MecSyntaxHighlighter::ParenthesisInfo*> infos = data->parentheses();

    int docPos = currentBlock.position();
    for (; i < infos.size(); ++i) {
        MecSyntaxHighlighter::ParenthesisInfo *info = infos.at(i);

		if (info->character == (type == MecPlainTextEdit::Parenthesis ? '{' : '(')) continue;
		else if (info->character == (type == MecPlainTextEdit::Parenthesis ? '}' : ')')) continue;

        if (info->character == (type == MecPlainTextEdit::Parenthesis ? '(' : '{')) {
            ++numLeftParentheses;
            continue;
        }

        if (info->character == (type == MecPlainTextEdit::Parenthesis ? ')' : '}') && numLeftParentheses == 0) {
            createParenthesisSelection(docPos + info->position);
            return true;
        } else
            --numLeftParentheses;
    }

    currentBlock = currentBlock.next();
    if (currentBlock.isValid())
        return matchLeftParenthesis(currentBlock, 0, numLeftParentheses, type);

    return false;
}

bool MecPlainTextEdit::matchRightParenthesis(QTextBlock currentBlock, int i, int numRightParentheses, MecPlainTextEdit::CharType type)
{
    MecSyntaxHighlighter::TextBlockData *data = static_cast<MecSyntaxHighlighter::TextBlockData*>(currentBlock.userData());
    QVector<MecSyntaxHighlighter::ParenthesisInfo*> infos = data->parentheses();

    int docPos = currentBlock.position();
    for (; i > -1 && infos.size() > 0; --i) {
        MecSyntaxHighlighter::ParenthesisInfo *info = infos.at(i);
        
            
        if (type == MecPlainTextEdit::Parenthesis) {
        	if (info->character == ')') {
            	++numRightParentheses;
        	}
        	else if (info->character == '(' && numRightParentheses == 0) {
            	createParenthesisSelection(docPos + info->position);
            	return true;
        	} else if (info->character == '(') {
            	--numRightParentheses;
            }
        }
        else if (type == MecPlainTextEdit::Brace) {
        	if (info->character == '}') {
            	++numRightParentheses;
        	}
        	else if (info->character == '{' && numRightParentheses == 0) {
            	createParenthesisSelection(docPos + info->position);
            	return true;
        	} else if (info->character == '{') {
            	--numRightParentheses;
            }
        }
    }

    currentBlock = currentBlock.previous();
    if (currentBlock.isValid())
        return matchRightParenthesis(currentBlock, static_cast<MecSyntaxHighlighter::TextBlockData*>(currentBlock.userData())->parentheses().size()-1, numRightParentheses, type);

    return false;
}

void MecPlainTextEdit::createParenthesisSelection(int pos)
{
    QList<QTextEdit::ExtraSelection> selections = extraSelections();

    QTextEdit::ExtraSelection selection;
    QTextCharFormat format = selection.format;
    format.setBackground(Qt::yellow);
    selection.format = format;
    
    QTextCursor cursor = textCursor();
    cursor.setPosition(pos);
    cursor.movePosition(QTextCursor::NextCharacter, QTextCursor::KeepAnchor);
    selection.cursor = cursor;

    selections.append(selection);

    setExtraSelections(selections);
}

void MecPlainTextEdit::updateLineNumberAreaWidth()
{
	setViewportMargins(lineNumberAreaWidth(), 0, 0, 0);
}

void MecPlainTextEdit::updateLineNumberArea(const QRect &Rect, int Dy)
{
	if (Dy)
		lineNumberArea->scroll(0, Dy);
	else
		lineNumberArea->update(0, Rect.y(), lineNumberArea->width(), Rect.height());

	if (Rect.contains(viewport()->rect()))
		updateLineNumberAreaWidth();
}

void MecPlainTextEdit::highlightCurrentLine()
{
	QList<QTextEdit::ExtraSelection> extraSelections;

	if (!isReadOnly()) {
		QTextEdit::ExtraSelection selection;

		QColor lineColor = QColor(0xEB, 0xEB, 0xEB);

		selection.format.setBackground(lineColor);
		selection.format.setProperty(QTextFormat::FullWidthSelection, true);
		selection.cursor = textCursor();
		selection.cursor.clearSelection();
		extraSelections.append(selection);
	}

	setExtraSelections(extraSelections);
}

void MecPlainTextEdit::matchParentheses()
{
    QList<QTextEdit::ExtraSelection> selections;
    setExtraSelections(selections);

    MecSyntaxHighlighter::TextBlockData *data = static_cast<MecSyntaxHighlighter::TextBlockData*>(textCursor().block().userData());

    if (data) {
        QVector<MecSyntaxHighlighter::ParenthesisInfo*> infos = data->parentheses();

        int pos = textCursor().block().position();
        for (int i = 0; i < infos.size(); ++i) {
            MecSyntaxHighlighter::ParenthesisInfo *info = infos.at(i);

            int curPos = textCursor().position() - textCursor().block().position();
            if (info->position == curPos - 1 && info->character == '(') {
                if (matchLeftParenthesis(textCursor().block(), i + 1, 0, MecPlainTextEdit::Parenthesis))
                    createParenthesisSelection(pos + info->position);
            } else if (info->position == curPos - 1 && info->character == ')') {
                if (matchRightParenthesis(textCursor().block(), i - 1, 0, MecPlainTextEdit::Parenthesis))
                    createParenthesisSelection(pos + info->position);
            } else if (info->position == curPos - 1 && info->character == '{') {
                if (matchLeftParenthesis(textCursor().block(), i + 1, 0, MecPlainTextEdit::Brace))
                    createParenthesisSelection(pos + info->position);
            } else if (info->position == curPos - 1 && info->character == '}') {
                if (matchRightParenthesis(textCursor().block(), i - 1, 0, MecPlainTextEdit::Brace))
                    createParenthesisSelection(pos + info->position);
            }
        }
    }
}
