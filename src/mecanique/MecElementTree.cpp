/*
© Quentin VIGNAUD, 2013 - 2014

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#include "MecElementTree.h"

MecElementTree::MecElementTree(MecAbstractElement* const Element, MecEditor* const Editor, QWidget *Parent) : QTreeWidget(Parent), m_element (Element), m_editor (Editor)
{
setHeaderLabels(QStringList() << tr("Name") << tr("Type"));
setSelectionMode(QAbstractItemView::SingleSelection);
setAnimated(true);

setDragDropMode(QAbstractItemView::DropOnly);
setDropIndicatorShown(true);
setAcceptDrops(true);

header()->setSectionResizeMode(QHeaderView::ResizeToContents);

connect(this, SIGNAL(currentItemChanged(QTreeWidgetItem*, QTreeWidgetItem*)), SLOT(elementItemSelectionChanged(QTreeWidgetItem*)));
connect(this, SIGNAL(itemActivated(QTreeWidgetItem*, int)), SLOT(elementItemActivated(QTreeWidgetItem*, int)));

addElementItem(Element);
}

MecElementTree::~MecElementTree()
{
//Rien de particulier.
}
	
MecAbstractElement* MecElementTree::element() const
{
return m_element;
}

MecAbstractElement* MecElementTree::currentElement() const
{
QTreeWidgetItem *tempItem = currentItem();
if (tempItem == 0) return 0;
MecAbstractElement *tempElement = findElementWithItem(tempItem);
if (tempElement != 0) return tempElement;
else return 0;
}

QVariant MecElementTree::state() const
{
	QHash<QString, QVariant> tempProperties;
	for (int i=0 ; i < m_match.keys().size() ; i++)
	{
		QHash<QString, QVariant> tempItemProperties;
		QTreeWidgetItem *tempItem = findItemWithElement(m_match.keys().at(i));
		
		QString tempAddress = m_match.keys().at(i)->address(m_element);
		
		bool tempExpanded = tempItem->isExpanded();
		bool tempSelected = tempItem->isSelected();
		
		tempItemProperties.insert("expanded", QVariant(tempExpanded));
		tempItemProperties.insert("selected", QVariant(tempSelected));
		
		tempProperties.insert(tempAddress, QVariant(tempItemProperties));
	}
	return tempProperties;
}

void MecElementTree::setState(QVariant State)
{
	setAnimated(false);
	
	QHash<QString, QVariant> tempProperties = State.toHash();
	QList<QString> tempAddresses = tempProperties.keys();
	
	for (int i=0 ; i < tempAddresses.size() ;  i++)
	{
		QHash<QString, QVariant> tempItemProperties = tempProperties.value(tempAddresses.at(i)).toHash();
		
		MecAbstractElement *tempElement = m_element->childElement(tempAddresses.at(i));
		QTreeWidgetItem *tempItem = findItemWithElement(tempElement);
		
		//Cas de l'élément racine.
		if (!tempAddresses.at(i).contains('.')) tempItem = topLevelItem(0);
		
		if (tempItem == 0) continue;
		tempItem->setExpanded(tempItemProperties.value("expanded").toBool());
		tempItem->setSelected(tempItemProperties.value("selected").toBool());
	}
	
	setAnimated(true);
}

void MecElementTree::changeCurrentElement(MecAbstractElement* Element)
{
setCurrentItem(findItemWithElement(Element));
}

void MecElementTree::dragMoveEvent(QDragMoveEvent *Event)
{
	if (Event->mimeData()->hasFormat("application/x-mecanique-element-address"))
		{
		QString tempAddress = QString::fromUtf8(Event->mimeData()->data("application/x-mecanique-element-address"));
		MecAbstractElement *tempElement = m_element->childElement(tempAddress);
		
		QTreeWidgetItem *tempItem = itemAt(Event->pos());
		if (tempItem != 0 and tempElement != 0)
			{
			tempItem = tempItem->parent();
			MecAbstractElement *tempParentElement = findElementWithItem(tempItem);
			if (tempParentElement != 0)
				{
				if (tempParentElement == tempElement->parentElement()) Event->accept();
				else Event->ignore();
				}
			else Event->ignore();
			}
		else Event->ignore();
		}
	else
		{
		Event->ignore();
		}
}

void MecElementTree::dropEvent(QDropEvent *Event)
{
	if (Event->mimeData()->hasFormat("application/x-mecanique-element-address"))
		{
		QString tempAddress = QString::fromUtf8(Event->mimeData()->data("application/x-mecanique-element-address"));
		MecAbstractElement *tempElement = m_element->childElement(tempAddress);
		
		QTreeWidgetItem *tempItemUnder = itemAt(Event->pos());
		
		if (tempItemUnder != 0 and tempElement != 0)
			{
			QTreeWidgetItem *tempParentItem = tempItemUnder->parent();
			MecAbstractElement *tempParentElement = findElementWithItem(tempParentItem);
			if (tempParentElement != 0)
				{
				//État de restauration.
				QVariant tempState = state();
				
				if (tempParentElement == tempElement->parentElement())
					{
					int tempNewIndex = tempParentItem->indexOfChild(tempItemUnder);
					int tempOldIndex = tempParentElement->childList()->indexOf(tempElement);
					tempParentElement->childList()->removeAt(tempOldIndex);
			
					tempParentElement->childList()->insert(tempNewIndex, tempElement);
					tempParentElement->emitChildListChanged();
					
					Event->accept();
					
					if (findItemWithElement(tempElement) != tempItemUnder) m_editor->addEditStep(tr("Move “%1”").arg(tempElement->elementName()));
					}
				else Event->ignore();
				
				//Restauration visuelle.
				setState(tempState);
				}
			else Event->ignore();
			}
		else Event->ignore();
		}
	else
		{
		Event->ignore();
		}
}

QMimeData* MecElementTree::mimeData(const QList<QTreeWidgetItem *> Items) const
{
	if (Items.isEmpty()) return 0;
	
	QString tempAddresses;
	
	for (int i=0 ; i < Items.size() ; i++)
		{
		tempAddresses += findElementWithItem(Items.at(i))->address() + "\n";
		}
	
	QMimeData *tempData = new QMimeData();
	tempData->setData("application/x-mecanique-element-address", tempAddresses.toUtf8());
	
	return tempData;
}

QStringList MecElementTree::mimeTypes() const
{
	return QStringList() << "application/x-mecanique-element-address";
}

void MecElementTree::mouseMoveEvent(QMouseEvent *Event)
{
	QTreeWidgetItem *tempItem = itemAt(Event->pos());
	if (m_itemOriginDragDrop != 0 and tempItem != m_itemOriginDragDrop)
	{
		MecAbstractElement *tempElement = findElementWithItem(m_itemOriginDragDrop);
		if (tempElement != 0)
			{
			QByteArray tempAddress = tempElement->address().toUtf8();
			
			QMimeData *tempMime = new QMimeData();
			tempMime->setData("application/x-mecanique-element-address", tempAddress);
			
			QDrag *tempDrag = new QDrag(this);
			tempDrag->setMimeData(tempMime);
			tempDrag->setDragCursor(QPixmap(), Qt::CopyAction);
			
			tempDrag->exec(Qt::CopyAction | Qt::MoveAction, Qt::CopyAction);
			}
	}
}

void MecElementTree::mousePressEvent(QMouseEvent *Event)
{
	QTreeView::mousePressEvent(Event);
	
	QTreeWidgetItem *tempItem = itemAt(Event->pos());
	if (tempItem != 0 and Event->button() == Qt::LeftButton)
		{
		m_itemOriginDragDrop = tempItem;
		}
}

void MecElementTree::mouseReleaseEvent(QMouseEvent *Event)
{
	QTreeView::mouseReleaseEvent(Event);
	
	m_itemOriginDragDrop = 0;
}

Qt::DropActions MecElementTree::supportedDropActions() const
{
	return Qt::CopyAction;
}

void MecElementTree::contextMenuEvent(QContextMenuEvent *Event)
{
	QTreeWidgetItem* tempItem = itemAt(Event->pos());
	if (tempItem != 0) {
		Event->accept();
		m_editor->elementContextMenu(findElementWithItem(tempItem), Event->globalPos());
	} else {
		Event->ignore();
	}
}

MecAbstractElement* MecElementTree::findElementWithItem(QTreeWidgetItem *Item) const
{
MecAbstractElement *tempElement = 0;
QMapIterator<MecAbstractElement*, QTreeWidgetItem*> tempIterator(m_match);
while (tempIterator.hasNext() and tempElement == 0)
	{
	tempIterator.next();
	if (tempIterator.value() == Item) tempElement = tempIterator.key();
	}
return tempElement;
}

QTreeWidgetItem* MecElementTree::findItemWithElement(MecAbstractElement *Element) const
{
QTreeWidgetItem *tempItem = 0;

QMapIterator<MecAbstractElement*, QTreeWidgetItem*> tempIterator(m_match);

while (tempIterator.hasNext() and tempItem == 0)
	{
	tempIterator.next();

	if (tempIterator.key() == Element) tempItem = tempIterator.value();
	}
return tempItem;
}
	
void MecElementTree::addElementItem(MecAbstractElement *Element)
{
if (findItemWithElement(Element) != 0) return;//Il existe déjà un item pour cet élément.

QTreeWidgetItem *tempItem = 0;
if (findItemWithElement(Element->parentElement()) == 0) tempItem = new QTreeWidgetItem(this);
else tempItem = new QTreeWidgetItem(findItemWithElement(Element->parentElement()));
m_match.insert(Element, tempItem);

switch (Element->elementRole())
	{
		case MecAbstractElement::Project :
			tempItem->setIcon(0, QIcon(":/share/icons/project.png"));
			break;
		case MecAbstractElement::Object :
			tempItem->setIcon(0, QIcon(":/share/icons/object.png"));
			break;
		case MecAbstractElement::Function :
			tempItem->setIcon(0, QIcon(":/share/icons/function.png"));
			break;
		case MecAbstractElement::Signal :
			tempItem->setIcon(0, QIcon(":/share/icons/signal.png"));
			break;
		case MecAbstractElement::Variable :
			tempItem->setIcon(0, QIcon(":/share/icons/variable.png"));
			break;
	}
tempItem->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled | Qt::ItemIsDragEnabled | Qt::ItemIsDropEnabled);

nameChanged(Element);
typeChanged(Element);
childListChanged(Element);

connect(Element, SIGNAL(destroyed(QObject*)), SLOT(removeElementItem(QObject*)));
connect(Element, SIGNAL(childListChanged(MecAbstractElement*)), SLOT(childListChanged(MecAbstractElement*)));
connect(Element, SIGNAL(nameChanged(MecAbstractElement*)), SLOT(nameChanged(MecAbstractElement*)));
connect(Element, SIGNAL(typeChanged(MecAbstractElement*)), SLOT(typeChanged(MecAbstractElement*)));
}

void MecElementTree::removeElementItem(QObject *ObjectElement)
{
MecAbstractElement *Element = static_cast<MecAbstractElement*>(ObjectElement);
if (findItemWithElement(Element) == 0)
	{
	return;
	}

QTreeWidgetItem *tempItem = m_match.take(Element);
QList<QTreeWidgetItem*> tempList = tempItem->takeChildren();
while (!tempList.isEmpty())
	{
	delete m_match.take(findElementWithItem(tempList.takeFirst()));
	}
delete tempItem;

//Ceci est en soi superflu dans le cas général où ce slot a été activé par le signal "destroyed(QObject*)"
disconnect(Element, SIGNAL(destroyed(QObject*)), this, SLOT(removeElementItem(QObject*)));
disconnect(Element, SIGNAL(childListChanged(MecAbstractElement*)), this, SLOT(childListChanged(MecAbstractElement*)));
disconnect(Element, SIGNAL(nameChanged(MecAbstractElement*)), this, SLOT(nameChanged(MecAbstractElement*)));
disconnect(Element, SIGNAL(typeChanged(MecAbstractElement*)), this, SLOT(typeChanged(MecAbstractElement*)));
}
	
void MecElementTree::childListChanged(MecAbstractElement *Element)
{
QTreeWidgetItem *tempItem = findItemWithElement(Element);
if (tempItem == 0) return;

//On s'assure tout d'abord que tous les éléments enfants ont un item correspondant.
for (int i=0 ; i < Element->childElements().size() ; i++)
	{
	QTreeWidgetItem *tempChildItem = findItemWithElement(Element->childElements().at(i));
	if (tempChildItem == 0) addElementItem(Element->childElements().at(i));
	}

//Puis on les réordonne si nécessaire.
QList<QTreeWidgetItem*> tempItems = tempItem->takeChildren();
for (int i=0 ; i < Element->childElements().size() ; i++)
	{
	QTreeWidgetItem *tempChildItem = findItemWithElement(Element->childElements().at(i));
	
	if (tempItems.indexOf(tempChildItem) != i)
		{
		tempItems.removeOne(tempChildItem);
		tempItems.insert(i, tempChildItem);
		i--;
		}
	}

tempItem->addChildren(tempItems);
}

void MecElementTree::nameChanged(MecAbstractElement *Element)
{
QTreeWidgetItem *tempItem = findItemWithElement(Element);
if (tempItem == 0) return;

tempItem->setText(0, Element->elementName());
}

void MecElementTree::typeChanged(MecAbstractElement *Element)
{
QTreeWidgetItem *tempItem = findItemWithElement(Element);
if (tempItem == 0) return;

tempItem->setText(1, Element->elementType());
tempItem->setIcon(1, QIcon(":/share/icons/types/" + Element->elementType() + ".png"));
}

void MecElementTree::elementItemSelectionChanged(QTreeWidgetItem* Item)
{
if (Item == 0) return;
MecAbstractElement *tempElement = findElementWithItem(Item);
if (tempElement != 0) emit elementCurrentChange(tempElement);

//Décoloration générale.
QMapIterator<MecAbstractElement*, QTreeWidgetItem*> tempIterator(m_match);
while (tempIterator.hasNext())
	{
	tempIterator.next();
	tempIterator.value()->setBackground(0, QBrush(Qt::white));
	}

//Coloration des éléments connectés.
//Connectés à un signal.
if (tempElement->elementRole() == MecAbstractElement::Signal)
	{
	MecAbstractSignal *tempSignal = static_cast<MecAbstractSignal*>(tempElement);
	
	for (int i=0 ; i < tempSignal->connectedFunctions().size() ; i++)
		{
		QTreeWidgetItem *tempItem = findItemWithElement(tempSignal->connectedFunctions().at(i));
		if (tempItem != 0)
			{
			tempItem->setBackground(0, QBrush(QColor("#A9F5F2")));
			if (tempSignal->connectedFunctions().at(i)->parentElement() != 0)
				{
				tempItem = findItemWithElement(tempSignal->connectedFunctions().at(i)->parentElement());
				if (tempItem != 0)
					{
					tempItem->setBackground(0, QBrush(QColor("#E0F8F7")));
					}
				}
			}
		}
	}

//Connectés à une fonction.
if (tempElement->elementRole() == MecAbstractElement::Function)
	{
	MecAbstractFunction *tempFunction = static_cast<MecAbstractFunction*>(tempElement);
	
	for (int i=0 ; i < tempFunction->connectedSignals().size() ; i++)
		{
		QTreeWidgetItem *tempItem = findItemWithElement(tempFunction->connectedSignals().at(i));
		if (tempItem != 0)
			{
			tempItem->setBackground(0, QBrush(QColor("#A9F5F2")));
			if (tempFunction->connectedSignals().at(i)->parentElement() != 0)
				{
				tempItem = findItemWithElement(tempFunction->connectedSignals().at(i)->parentElement());
				if (tempItem != 0)
					{
					tempItem->setBackground(0, QBrush(QColor("#E0F8F7")));
					}
				}
			}
		}
	}
}

void MecElementTree::elementItemActivated(QTreeWidgetItem* Item, int Column)
{
if (Item == 0) return;
MecAbstractElement *tempElement = findElementWithItem(Item);
if (tempElement != 0) emit elementActivated(tempElement, Column);
}



