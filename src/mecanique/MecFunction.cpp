/*
© Quentin VIGNAUD, 2013

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#include "MecFunction.h"

MecFunction::MecFunction(QString Name, QString Type, MecAbstractElement *ParentElement) : m_elementRole (MecAbstractElement::Function), m_parentElement (0)
{
setParentElement(ParentElement);
setElementName(Name);
setElementType(Type);
}

MecFunction::~MecFunction()
{
emit destroyed(this);
while (!m_childElements.isEmpty()) delete m_childElements.at(0);//La suppression du sous-élément le fait se retirer lui-même de la présente liste.
setParentElement(0);//Retire l'élément présent de la liste des enfants de son parent.
}

QString MecFunction::elementName() const
{
return m_elementName;
}
	
QString MecFunction::elementType() const
{
return m_elementType;
}
	
MecAbstractElement::ElementRole MecFunction::elementRole() const
{
return m_elementRole;
}
	
MecAbstractElement* MecFunction::parentElement() const
{
return m_parentElement;
}

bool MecFunction::setParentElement(MecAbstractElement *ParentElement)
{
if (ParentElement == 0)
	{
	changeParentElement(ParentElement);
	return true;
	}
else
	{
	if (ParentElement->elementRole() == MecAbstractElement::Project or ParentElement->elementRole() == MecAbstractElement::Object)
		{
		changeParentElement(ParentElement);
		return true;
		}
	else return false;
	}
}

QString MecFunction::code() const
{
return m_code;
}

QList<MecAbstractElement*> MecFunction::childElements() const
{
return m_childElements;
}
	
QList<MecAbstractVariable*> MecFunction::childVariables() const
{
QList<MecAbstractVariable*> tempList;
for (int i=0 ; i < childElements().size() ; i++)
	{
	if (childElements().at(i)->elementRole() == MecAbstractElement::Variable)
		{
		tempList.append(static_cast<MecAbstractVariable*>(childElements().at(i)));
		}
	}
return tempList;
}

MecAbstractElement* MecFunction::childElement(QString Address) const
{
	QStringList tempNames = Address.split('.');
	if (tempNames.size() >= 2 and tempNames.at(0) == elementName())
	{
		for (int i=0 ; i < childElements().size() ; i++)
		{
			if (childElements().at(i)->elementName() == tempNames.at(1) and tempNames.size() != 2)
			{
				tempNames.removeFirst();
				return childElements().at(i)->childElement(tempNames.join('.'));
			}
			else if (childElements().at(i)->elementName() == tempNames.at(1) and tempNames.size() == 2)
			{
				return childElements().at(i);
			}
		}
	}
	
	return 0;
}

QString MecFunction::address(MecAbstractElement *RootElement) const
{
	QString tempAddress = elementName();
	const MecAbstractElement *tempParentElement = parentElement();
	while (tempParentElement != 0)
		{
		tempAddress.prepend(tempParentElement->elementName() + '.');
		if (tempParentElement == RootElement) break;
		tempParentElement = tempParentElement->parentElement();
		}
	return tempAddress;
}

QList<MecAbstractSignal*> MecFunction::connectedSignals() const
{
	MecAbstractElement *tempRootElement = parentElement();
	while (tempRootElement->parentElement() != 0) tempRootElement = tempRootElement->parentElement();
	
	QList<MecAbstractElement*> tempElements;
		tempElements.append(tempRootElement);
	for (int i=0 ; i < tempElements.size() ; i++)
		{
		tempElements.append(tempElements.at(i)->childElements());
		}
	
	QList<MecAbstractSignal*> tempConnectedSignals;
	for (int i=0 ; i < tempElements.size() ; i++)
		{
		if (tempElements.at(i)->elementRole() == MecAbstractElement::Signal)
			{
			MecAbstractSignal *tempSignal = static_cast<MecAbstractSignal*>(tempElements.at(i));
			
			bool found=false;
			for (int j=0 ; j < tempSignal->connectedFunctions().size() && !found ; j++)
				{
				if (tempSignal->connectedFunctions().at(j) == this)
					{
					tempConnectedSignals.append(tempSignal);
					found = true;
					}
				}
			}
		}
	
	return tempConnectedSignals;
}

bool MecFunction::connectSignal(MecAbstractSignal* Signal)
{
	return Signal->connectFunction(this);
}

void MecFunction::disconnectSignal(MecAbstractSignal* Signal)
{
	Signal->disconnectFunction(this);
}

bool MecFunction::isConnectableSignal(MecAbstractSignal* Signal)
{
	return Signal->isConnectableFunction(this);
}

bool MecFunction::setElementName(QString Name)
{
bool Ok = standardSyntax().exactMatch(Name) and Name != "settings" and Name != "setSettings" and Name != "more";//Vérification de la syntaxe standarde et des exceptions.
if (Ok and m_parentElement != 0)//Vérification de la disponibilité du nom.
	{
	QStringList NamesUsed;
	for (int i=0 ; i < m_parentElement->childElements().size() ; i++)//On prend les noms des éléments frères.
		{
		if (m_parentElement->childElements().at(i) != this) NamesUsed.append(m_parentElement->childElements().at(i)->elementName());
		}
	for (int i=0 ; m_parentElement->parentElement() != 0 and i < m_parentElement->parentElement()->childElements().size() ; i++)//On prend les noms des éléments oncles.
		{
		NamesUsed.append(m_parentElement->parentElement()->childElements().at(i)->elementName());
		}
	
	Ok = !(NamesUsed.contains(Name));
	}

if (Ok)
	{
	m_elementName = Name;
	emit nameChanged(this);
	return true;
	}
else return false;
}

bool MecFunction::setElementType(QString Type)
{
if (standardSyntax().exactMatch(Type))
	{
	m_elementType = Type;
	emit typeChanged(this);
	return true;
	}
else return false;
}

void MecFunction::setCode(QString Code)
{
m_code = Code;
emit codeChanged(this);
}

void MecFunction::emitConnectedSignalsChanged()
{
emit connectedSignalsChanged(this);
}

bool MecFunction::changeParentElement(MecAbstractElement *ParentElement)
{
if (ParentElement != this)
	{
	if (m_parentElement != 0)
		{
		m_parentElement->childList()->removeAll(this);
		m_parentElement->emitChildListChanged();
		}
	
	m_parentElement = ParentElement;
	if (ParentElement != 0)
		{
		ParentElement->childList()->append(this);
		ParentElement->emitChildListChanged();
		}
	
	blockSignals(true);//On bloque les signaux pour éviter une émission inutile.
	if (!setElementName(m_elementName))//Vérification de la disponibilité du nom dans le nouveau parent.
		{
		blockSignals(false);
		for (QString tempName=m_elementName.prepend("_") ; !setElementName(tempName) ; tempName.prepend("_")) {}
		}
	blockSignals(false);
	
	setParent(ParentElement);//QObject
	emit parentChanged(this);
	return true;
	}
else return false;
}

QList<MecAbstractElement*>* MecFunction::childList()
{
return &m_childElements;
}

void MecFunction::emitChildListChanged()
{
emit childListChanged(this);
}

bool operator==(MecFunction const &a, MecFunction const &b)
{
	bool result = (a.elementName() == b.elementName())
				&& (a.elementType() == b.elementType())
				&& (a.code() == b.code()
				&& (a.childElements().size() == b.childElements().size()));
	
	for (int i=0 ; i < a.childElements().size() && result ; i++)
		{
		if (a.childElements().at(i)->elementRole() == b.childElements().at(i)->elementRole())
			{
			MecAbstractElement *first = a.childElements().at(i);
			MecAbstractElement *second = b.childElements().at(i);
			
			if (first->elementRole() == MecAbstractElement::Variable)
				{
				result = ( *(static_cast<MecVariable*>(first)) == *(static_cast<MecVariable*>(second)) );
				}
			
			}
		else result=false;
		}
	
	return result;
}

bool operator!=(MecFunction const &a, MecFunction const &b)
{
	return !(a==b);
}
	
	
