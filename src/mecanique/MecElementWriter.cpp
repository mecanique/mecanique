/*
© Quentin VIGNAUD, 2013

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#include "MecElementWriter.h"

MecElementWriter::MecElementWriter(QIODevice *Device, const MecAbstractElement *Element) : QXmlStreamWriter(Device), m_element (Element)
{
startWriting();
}

MecElementWriter::MecElementWriter(QByteArray *Array, const MecAbstractElement *Element) : QXmlStreamWriter(Array), m_element (Element)
{
startWriting();
}

MecElementWriter::MecElementWriter(QString *String, const MecAbstractElement *Element) : QXmlStreamWriter(String), m_element (Element)
{
startWriting();
}

void MecElementWriter::startWriting()
{
setAutoFormatting(true);
setAutoFormattingIndent(-1);

writeStartDocument("1.0", false);
QString tempDTD("<!DOCTYPE ");
switch (m_element->elementRole())
	{
	case MecAbstractElement::Project :
		tempDTD += "project";
		break;
	case MecAbstractElement::Object :
		tempDTD += "object";
		break;
	case MecAbstractElement::Function :
		tempDTD += "function";
		break;
	case MecAbstractElement::Signal :
		tempDTD += "signal";
		break;
	case MecAbstractElement::Variable:
		tempDTD += "variable";
		break;
	}
tempDTD += " PUBLIC \"-//Mecanique//Mecanique 1//EN\" \"http://repository.mecanique.cc/data/dtd/mecanique.1.dtd\">";
writeDTD(tempDTD);

writeElement(m_element);

writeEndDocument();
}
	
void MecElementWriter::writeElement(const MecAbstractElement *Element)
{
if (Element == 0) return;
if (Element->elementRole() == MecAbstractElement::Project)//On traite un projet
	{
	const MecProject *tempProject = static_cast<const MecProject*>(Element);
	writeStartElement("project");
	writeAttribute("name", tempProject->elementName());
	
	writeStartElement("title");
	writeCharacters(tempProject->title());
	writeEndElement();
	
	writeStartElement("synopsis");
	writeCharacters(tempProject->synopsis());
	writeEndElement();
	
	for (int i=0 ; i < tempProject->childElements().size() ; i++)
		{
		writeElement(tempProject->childElements().at(i));
		}
	
	writeEndElement();
	}
else if (Element->elementRole() == MecAbstractElement::Object)//On traite un objet
	{
	const MecObject *tempObject = static_cast<const MecObject*>(Element);
	writeStartElement("object");
	writeAttribute("name", tempObject->elementName());
	writeAttribute("type", tempObject->elementType());
	
	for (int i=0 ; i < tempObject->childElements().size() ; i++)
		{
		writeElement(tempObject->childElements().at(i));
		}
	
	writeEndElement();
	}
else if (Element->elementRole() == MecAbstractElement::Function)//On traite une fonction
	{
	const MecFunction *tempFunction = static_cast<const MecFunction*>(Element);
	writeStartElement("function");
	writeAttribute("name", tempFunction->elementName());
	writeAttribute("type", tempFunction->elementType());
	
	for (int i=0 ; i < tempFunction->childElements().size() ; i++)
		{
		writeElement(tempFunction->childElements().at(i));
		}
	
	writeStartElement("code");
	writeCharacters(tempFunction->code());
	writeEndElement();
	
	writeEndElement();
	}
else if (Element->elementRole() == MecAbstractElement::Signal)//On traite un signal
	{
	const MecSignal *tempSignal = static_cast<const MecSignal*>(Element);
	writeStartElement("signal");
	writeAttribute("name", tempSignal->elementName());
	
	for (int i=0 ; i < tempSignal->childElements().size() ; i++)
		{
		writeElement(tempSignal->childElements().at(i));
		}
	
	for (int i=0 ; i < tempSignal->connectedFunctions().size() ; i++)
		{
		QString tempAddress = tempSignal->connectedFunctions().at(i)->elementName();
		const MecAbstractElement *tempParentElement = tempSignal->connectedFunctions().at(i)->parentElement();
		while (tempParentElement != 0)
			{
			tempAddress.prepend(tempParentElement->elementName() + ".");
			tempParentElement = tempParentElement->parentElement();
			}
		writeStartElement("connection");
		writeAttribute("function", tempAddress);
		writeEndElement();
		}
	
	writeEndElement();
	}
else if (Element->elementRole() == MecAbstractElement::Variable)//On traite une variable
	{
	const MecVariable *tempVariable = static_cast<const MecVariable*>(Element);
	writeStartElement("variable");
	writeAttribute("name", tempVariable->elementName());
	writeAttribute("type", tempVariable->elementType());
	writeAttribute("default", tempVariable->defaultValue().toString());
	writeEndElement();
	}
}


