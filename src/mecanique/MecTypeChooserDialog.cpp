/*
© Quentin VIGNAUD, 2014

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#include "MecTypeChooserDialog.h"

MecTypeChooserDialog::MecTypeChooserDialog(QWidget * Parent, Qt::WindowFlags F) : QDialog(Parent, F)
{
	setWindowTitle(tr("Choose type…"));

	layoutMain = new QGridLayout(this);
	
	scrollAreaTypes = new QScrollArea(this);
		scrollAreaTypes->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
	widgetTypes = new QWidget(scrollAreaTypes);
	layoutTypes = new QGridLayout(widgetTypes);
	
	buttonCancel = new QPushButton(tr("&Cancel"), this);
		connect(buttonCancel, SIGNAL(clicked(bool)), SLOT(reject()));
	
	layoutMain->addWidget(scrollAreaTypes, 0, 0, 5, 6);
	layoutMain->addWidget(buttonCancel, 5, 5);
}

MecTypeChooserDialog::~MecTypeChooserDialog()
{

}
	
void MecTypeChooserDialog::addType(QString Type)
{
	MecTypeChooserDialogItem *tempItem = new MecTypeChooserDialogItem(widgetTypes);
		connect(tempItem, SIGNAL(selected(QString)), SLOT(typeSelected(QString)));
	
	tempItem->setType(Type);
	
	layoutTypes->addWidget(tempItem, m_items.size() / 5, m_items.size() % 5);
	
	m_items.append(tempItem);
}

QString MecTypeChooserDialog::getSelectedType() const
{
	for (int i=0 ; i < m_items.size() ; i++)
	{
		if (m_items.at(i)->isSelected()) {
			return m_items.at(i)->getType();
		}
	}
	return QString();
}
	
int MecTypeChooserDialog::exec()
{
	scrollAreaTypes->setWidget(widgetTypes);
	scrollAreaTypes->setMinimumWidth(widgetTypes->width() + 20);
	return QDialog::exec();
}
	
void MecTypeChooserDialog::typeSelected(QString Type)
{
	accept();
}
	
