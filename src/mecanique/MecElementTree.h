/*
© Quentin VIGNAUD, 2013 - 2014

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#ifndef __MECELEMENTTREE_H__
#define __MECELEMENTTREE_H__

#include <QtWidgets>
#include "MecAbstractElement.h"
class MecEditor;
#include "MecEditor.h"

/**
\brief	Classe de représentation visuelle d'une arborescence d'éléments.
*/
class MecElementTree : public QTreeWidget
{
	Q_OBJECT

public:
	/**
	\brief	Constructeur.

	Crée une arborescence ayant comme racine \e Element.
	*/
	MecElementTree(MecAbstractElement* const Element, MecEditor* const Editor, QWidget *Parent=0);
	/**
	\brief	Destructeur.
	*/
	~MecElementTree();

	///Retourne l'élément racine.
	MecAbstractElement* element() const;

	///Retourne l'élément actuellement sélectionné.
	MecAbstractElement* currentElement() const;

	///Retourne l'état actuel de l'arborescence.
	QVariant state() const;
	///Restaure l'état de l'arborescence selon les indications spécifées.
	void setState(QVariant State);


public slots:
	///Change l'élément courant.
	void changeCurrentElement(MecAbstractElement* Element);

signals:
	///L'élément courant a changé.
	void elementCurrentChange(MecAbstractElement* Element);
	///Un élément a été activé.
	void elementActivated(MecAbstractElement* Element, int Column);

protected:
	///Gère le déplacement de cliquer/déposer.
	virtual void dragMoveEvent(QDragMoveEvent *Event);
	///Gère le déposer de cliquer/déposer.
	virtual void dropEvent(QDropEvent *Event);
	///Retourne le contenu MIME correspondant aux items.
	virtual QMimeData* mimeData(const QList<QTreeWidgetItem *> Items) const;
	///Indique quels types MIME sont supportés.
	virtual QStringList mimeTypes() const;
	///Gère le déclenchement d'un cliquer/déposer si nécessaire.
	virtual void mouseMoveEvent(QMouseEvent *Event);
	///Gère l'amorçage d'un cliquer/déposer.
	virtual void mousePressEvent(QMouseEvent *Event);
	///Gère le désamorçage d'un cliquer/déposer.
	virtual void mouseReleaseEvent(QMouseEvent *Event);
	///Retourne les actions cliquer/déposer supportées.
	virtual Qt::DropActions supportedDropActions() const;
	///Est appelé lors de la demande d'un menu contextuel.
	virtual void contextMenuEvent(QContextMenuEvent *Event);


private:
	///Élément racine.
	MecAbstractElement* const m_element;

	///Éditeur principal.
	MecEditor* const m_editor;

	///Correspondance élément/item.
	QMap<MecAbstractElement*, QTreeWidgetItem*> m_match;
	/**
	\brief	Retourne l'élément correspondant à \e Item.

	0 est retourné s'il n'existe pas de correspondance.
	*/
	MecAbstractElement* findElementWithItem(QTreeWidgetItem *Item) const;
	/**
	\brief	Retourne l'item correspondant à \e Element.

	0 est retourné s'il n'existe pas de correspondance.
	*/
	QTreeWidgetItem* findItemWithElement(MecAbstractElement *Element) const;

	///Item sélectionné lors du début d'appui de la souris.
	QTreeWidgetItem *m_itemOriginDragDrop;


private slots:
	/**
	\brief	Ajoute un item représentant \e Element.
	*/
	void addElementItem(MecAbstractElement *Element);
	/**
	\brief	Supprime un item représentant \e ObjectElement.
	*/
	void removeElementItem(QObject *ObjectElement);

	///Connecté à MecAbstractElement::childListChanged(MecElement*)
	void childListChanged(MecAbstractElement *Element);
	///Connecté à MecAbstractElement::nameChanged(MecElement*)
	void nameChanged(MecAbstractElement *Element);
	///Connecté à MecAbstractElement::typeChanged(MecElement*)
	void typeChanged(MecAbstractElement *Element);

	///Connecté à QTreeWidget::currentItemChanged(QTreeWidgetItem* current, QTreeWidgetItem* previous)
	void elementItemSelectionChanged(QTreeWidgetItem* Item);
	///Connecté à QTreeWidget::itemActivated(QTreeWidgetItem* item, int column)
	void elementItemActivated(QTreeWidgetItem* Item, int Column);


};

#endif /* __MECELEMENTTREE_H__ */

