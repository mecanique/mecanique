/*
© Quentin VIGNAUD, 2014

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#ifndef __MECDOWNLOADER_H__
#define __MECDOWNLOADER_H__

#include <QtCore>
#include <QtNetwork>

/**
\brief	Classe de gestion d'un téléchargement.
*/
class MecDownloader : public QObject
{
	Q_OBJECT

public:
	/**
	\brief	Constructeur.

	\param	FileUrl	URL du fichier à télécharger.
	\param	FileName	Nom du fichier à écrire.
	\param	NetworkAccessManager	Gestionnaire réseau à utiliser.
	*/
	MecDownloader(const QUrl FileUrl, const QString FileName, QNetworkAccessManager *NetworkAccessManager);
	/**
	\brief	Destructeur.
	*/
	~MecDownloader();

	///Retourne l'URL du fichier téléchargé.
	QUrl fileUrl() const;
	///Retourne le nom du fichier à écrire.
	QString fileName() const;
	///Retourne un pointeur sur la réponse réseau.
	QNetworkReply* networkReply() const;

public slots:
	/**
	\brief	Démarre le téléchargement.

	Ne fait rien si le téléchargement est déjà en cours ou si le fichier de destination n'est pas accessible en écriture.
	*/
	void start();
	/**
	\brief	Arrête le téléchargement.
	*/
	void stop();

signals:
	/**
	\brief	Est émis lorsqu'une écriture a eu lieue.

	\param	BytesWritten	Nombre d'octets qui viennent d'être écrits (n'inclut pas le nombre d'octets précédemment écrits).
	*/
	void bytesWritten(qint64 BytesWritten);
	///Est émis lorsque le téléchargement est terminé.
	void finished();

private:
	///Gestionnaire réseau.
	QNetworkAccessManager *m_networkAccessManager;
	///Réponse réseau.
	QNetworkReply *m_reply;
	///URL de téléchargement.
	QUrl m_fileUrl;
	///Nom du fichier de destination.
	QString m_fileName;
	///Fichier de destination.
	QFile *m_file;

private slots:
	/**
	\brief	Appelé lorsque de nouvelles données sont reçues du réseau.

	Procède à l'écriture de ces données dans le fichier de destination et émet bytesWritten().
	*/
	void downloadProgress();
	/**
	\brief	Appelé lorsque le téléchargement a pris fin.

	Clôt l'écriture du fichier et émet finished().
	*/
	void downloadFinished();
};

#endif /* __MECDOWNLOADER_H__ */

