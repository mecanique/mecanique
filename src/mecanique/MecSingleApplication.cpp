/*
© Quentin VIGNAUD, 2014

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#include "MecSingleApplication.h"

MecSingleApplication::MecSingleApplication() : QObject(0), m_isFirst (false), m_timer (0), m_pluginsManager (0)
{
	QString Key("mecanique-" + QCoreApplication::applicationVersion());
	m_shared = new QSharedMemory(Key, this);
	
	if (m_shared->create(MECSHAREDSIZE))
		{
		m_isFirst = true;
		
		//Initialisation de l'espace mémoire.
		for (int i=0 ; i < MECSHAREDSIZE ; i++)
			{
			((char*)m_shared->data())[i] = 0;
			}
		}
	else
		{
		if (m_shared->error() == QSharedMemory::AlreadyExists)
			{
			m_isFirst = false;
			m_shared->attach();
			}
		else
			{
			std::cout << "Shared memory error: " << m_shared->errorString().toStdString() << std::endl;
			}
		}
}

MecSingleApplication::~MecSingleApplication()
{
}
	
bool MecSingleApplication::isFirst()
{
	return m_isFirst;
}
	
void MecSingleApplication::openFile(QString FileName)
{
	QString Content;
	
	do
		{
		m_shared->lock();
		Content = ((char*)m_shared->constData());
		m_shared->unlock();
		if (Content != "/Ready/") QThread::sleep(1);
		}
	while (Content != "/Ready/");
	
	QByteArray Array = FileName.toUtf8();
	
	m_shared->lock();
	
	//On recopie le contenu du bytearray dans le segment partagé.
	for (int i=0 ; i < MECSHAREDSIZE ; i++)
		{
		if (i < Array.size()) ((char*)m_shared->data())[i] = Array.at(i);
		else ((char*)m_shared->data())[i] = 0;
		}
	
	m_shared->unlock();
	
}
	
void MecSingleApplication::start(MecPluginsManager *PluginsManager)
{
	if (m_timer != 0 or !m_isFirst) return;
	
	m_pluginsManager = PluginsManager;

	m_shared->lock();
	strcpy(((char*)m_shared->data()), "/Ready/");
	m_shared->unlock();
	
	m_timer = new QTimer(this);
	m_timer->setInterval(1000);
	m_timer->setSingleShot(false);
	
	connect(m_timer, SIGNAL(timeout()), SLOT(check()));
	
	m_timer->start();
}

void MecSingleApplication::check()
{
	m_shared->lock();
	QString Content = ((char*)m_shared->constData());
	m_shared->unlock();
	
	if (Content != "/Ready/")
		{
		if (!Content.isEmpty())
			{
			MecEditor *tempEditor = new MecEditor(m_pluginsManager);
			tempEditor->open(Content);
			tempEditor->show();
			}
		else
			{
			MecEditor *tempEditor = new MecEditor(m_pluginsManager);
			tempEditor->show();
			}
		
		m_shared->lock();
		strcpy(((char*)m_shared->data()), "/Ready/");
		m_shared->unlock();
		}
}

