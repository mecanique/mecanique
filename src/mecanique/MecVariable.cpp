/*
© Quentin VIGNAUD, 2013

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#include "MecVariable.h"

MecVariable::MecVariable(QString Name, QString Type, MecAbstractElement *ParentElement) : m_elementRole (MecAbstractElement::Variable), m_parentElement (0)
{
setParentElement(ParentElement);
setElementName(Name);
setElementType(Type);
}

MecVariable::MecVariable(QString Name, QString Type, QVariant DefaultValue, MecAbstractElement *ParentElement) : m_elementRole (MecAbstractElement::Variable), m_parentElement (0)
{
setParentElement(ParentElement);
setElementName(Name);
setElementType(Type);
setDefaultValue(DefaultValue);
}

MecVariable::~MecVariable()
{
emit destroyed(this);
while (!m_childElements.isEmpty()) delete m_childElements.at(0);//La suppression du sous-élément le fait se retirer lui-même de la présente liste.
setParentElement(0);//Retire l'élément présent de la liste des enfants de son parent.
}

QString MecVariable::elementName() const
{
return m_elementName;
}
	
QString MecVariable::elementType() const
{
return m_elementType;
}
	
MecAbstractElement::ElementRole MecVariable::elementRole() const
{
return m_elementRole;
}
	
MecAbstractElement* MecVariable::parentElement() const
{
return m_parentElement;
}

bool MecVariable::setParentElement(MecAbstractElement *ParentElement)
{
if (ParentElement == 0)
	{
	changeParentElement(ParentElement);
	return true;
	}
else
	{
	if (ParentElement->elementRole() == MecAbstractElement::Project or ParentElement->elementRole() == MecAbstractElement::Object or ParentElement->elementRole() == MecAbstractElement::Function or ParentElement->elementRole() == MecAbstractElement::Signal)
		{
		changeParentElement(ParentElement);
		return true;
		}
	else return false;
	}
}

QVariant MecVariable::defaultValue() const
{
return m_defaultValue;
}

QList<MecAbstractElement*> MecVariable::childElements() const
{
return m_childElements;
}

MecAbstractElement* MecVariable::childElement(QString Address) const
{
	//Une variable ne peut avoir d'éléments enfants.
	return 0;
}

QString MecVariable::address(MecAbstractElement *RootElement) const
{
	QString tempAddress = elementName();
	const MecAbstractElement *tempParentElement = parentElement();
	while (tempParentElement != 0)
		{
		tempAddress.prepend(tempParentElement->elementName() + '.');
		if (tempParentElement == RootElement) break;
		tempParentElement = tempParentElement->parentElement();
		}
	return tempAddress;
}

bool MecVariable::setElementName(QString Name)
{
bool Ok = standardSyntax().exactMatch(Name) and Name != "settings" and Name != "setSettings" and Name != "more";//Vérification de la syntaxe standarde et des exceptions.
if (Ok and m_parentElement != 0)//Vérification de la disponibilité du nom.
	{
	QStringList NamesUsed;
	for (int i=0 ; i < m_parentElement->childElements().size() ; i++)//On prend les noms des éléments frères.
		{
		if (m_parentElement->childElements().at(i) != this) NamesUsed.append(m_parentElement->childElements().at(i)->elementName());
		}
	for (int i=0 ; m_parentElement->parentElement() != 0 and i < m_parentElement->parentElement()->childElements().size() ; i++)//On prend les noms des éléments oncles.
		{
		NamesUsed.append(m_parentElement->parentElement()->childElements().at(i)->elementName());
		}
	
	Ok = !(NamesUsed.contains(Name));
	}

if (Ok)
	{
	m_elementName = Name;
	emit nameChanged(this);
	return true;
	}
else return false;
}

bool MecVariable::setElementType(QString Type)
{
if (standardSyntax().exactMatch(Type))
	{
	m_elementType = Type;
	emit typeChanged(this);
	return true;
	}
else return false;
}

void MecVariable::setDefaultValue(QVariant DefaultValue)
{
m_defaultValue = DefaultValue;
emit defaultValueChanged(this);
}

bool MecVariable::changeParentElement(MecAbstractElement *ParentElement)
{	
if (ParentElement != this)
	{
	if (m_parentElement != 0)
		{
		m_parentElement->childList()->removeAll(this);
		m_parentElement->emitChildListChanged();
		}
	
	m_parentElement = ParentElement;
	if (ParentElement != 0)
		{
		ParentElement->childList()->append(this);
		ParentElement->emitChildListChanged();
		}
	
	blockSignals(true);//On bloque les signaux pour éviter une émission inutile.
	if (!setElementName(m_elementName))//Vérification de la disponibilité du nom dans le nouveau parent.
		{
		blockSignals(false);
		for (QString tempName=m_elementName.prepend("_") ; !setElementName(tempName) ; tempName.prepend("_")) {}
		}
	blockSignals(false);
	
	setParent(ParentElement);//QObject
	emit parentChanged(this);
	return true;
	}
else
	{
	return false;
	}
}

QList<MecAbstractElement*>* MecVariable::childList()
{
return &m_childElements;
}

void MecVariable::emitChildListChanged()
{
emit childListChanged(this);
}

bool operator==(MecVariable const &a, MecVariable const &b)
{
	bool result = (a.elementName() == b.elementName())
				&& (a.elementType() == b.elementType());
				
	return result;
}

bool operator!=(MecVariable const &a, MecVariable const &b)
{
	return !(a==b);
}



