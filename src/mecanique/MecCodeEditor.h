/*
© Quentin VIGNAUD, 2014

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#ifndef __MECCODEEDITOR_H__
#define __MECCODEEDITOR_H__

#include <MecAbstractCodeEditor>
#include <MecAbstractEditor>
#include <QGridLayout>
#include <QScrollBar>
#include "MecPlainTextEdit.h"
#include "MecSyntaxHighlighter.h"

/**
\brief	Classe d'édition du code de fonction.
*/
class MecCodeEditor : public MecAbstractCodeEditor
{
	Q_OBJECT

public:
	///Constructeur.
	MecCodeEditor(MecAbstractFunction* const Function, MecAbstractEditor* MainEditor, QWidget* Parent=0);
	///Destructeur.
	~MecCodeEditor();

	///Retourne la fonction dont le code est édité.
	MecAbstractFunction* function() const;

	///Éditeur principal.
	MecAbstractEditor* mainEditor() const;

	/**
	\brief	Retourne un QVariant contenant toutes les données indiquant l'état actuel de l'éditeur.

	Cette donnée est vouée à être passée ultérieurement en paramètre à setState(QVariant) pour restaurer l'état visuel de l'éditeur.
	*/
	virtual QVariant state();
	/**
	\brief	Restaure l'état de l'éditeur selon les indications spécifées.

	\see	state()
	*/
	virtual void setState(QVariant State);

private:
	///Fonction dont le code est édité.
	MecAbstractFunction* const m_function;
	///Éditeur général.
	MecAbstractEditor* m_mainEditor;

	///Layout d'organisation.
	QGridLayout *layoutMain;
	///Éditeur de texte.
	MecPlainTextEdit *m_textEdit;


private slots:
	///Est déclenché lorsque le code de la fonction a changé.
	void codeFunctionChanged(MecAbstractFunction *Function);
	///Est déclenché lorsque la zone d'édition du code est modifiée.
	void textEditCodeChanged();

};

#endif /* __MECCODEEDITOR_H__ */

