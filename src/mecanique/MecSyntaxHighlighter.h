/*
© Quentin VIGNAUD, 2014

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#ifndef __MECSYNTAXHIGHLIGHTER_H__
#define __MECSYNTAXHIGHLIGHTER_H__

#include <QSyntaxHighlighter>
#include <MecAbstractFunction>
#include <MecAbstractEditor>

/**
\brief	Classe de gestion de la coloration syntaxique.

\note	Cette classe gère également l'établissement des informations de parenthèses pour une reconnaissance ultérieure.
\see	MecPlainTextEdit
*/
class MecSyntaxHighlighter : public QSyntaxHighlighter
{
	Q_OBJECT

public:

	/**
	\brief	Structure de règle de coloration.
	*/
	struct Rule {
		///Constructeur par défaut.
		Rule();
		///Constructeur paramétré.
		Rule(QString Expression, QTextCharFormat Format);
		///Expression (Regex) de reconnaissance.
		QString expression;
		///Formatage de caractère à appliquer à la section reconnue.
		QTextCharFormat format;
	};

	/**
	\brief	Structure de représentation des informations de parenthèses.

	\see	MecPlainTextEdit
	*/
	struct ParenthesisInfo {
		///Caractère correspondant.
		char character;
		///Position dans le texte.
		int position;
	};

	/**
	\brief	Classe-conteneur d'informations de bloc de texte.

	\see	MecPlainTextEdit
	*/
	class TextBlockData : public QTextBlockUserData
	{
	public:
		///Constructeur par défaut.
		TextBlockData();

		///Retourne les informations de parenthèses.
		QVector<ParenthesisInfo*> parentheses();
		///Ajoute une information de parenthèses.
		void insert(ParenthesisInfo *Info);

	private:
		///Informations de parenthèses.
		QVector<ParenthesisInfo*> m_parentheses;
	};

	/**
	\brief	Constructeur.

	\param	Function	Fonction dont le code est à colorer.
	\param	MainEditor	Éditeur principal dans lequel l'édition a lieue.
	\param	Parent	Document parent dont la coloration est à effectuer.
	*/
	MecSyntaxHighlighter(MecAbstractFunction* const Function, MecAbstractEditor* const MainEditor, QTextDocument *Parent);

	/**
	\brief	Retourne les règles de coloration à appliquer au code de la fonction.
	*/
	QVector<Rule> rules() const;

	///Fonction dont le code est à colorer.
	MecAbstractFunction* function() const;
	///Éditeur principal dans lequel l'édition a lieue.
	MecAbstractEditor* mainEditor() const;

	///Liste des fonction générales de Mécanique.
	static QStringList generalFunctions();
	///Liste des mots-clés utilisables.
	static QStringList keywords();
	///Liste des mots-clés « interdits ».
	static QStringList forbiddenKeywords();

protected:
	/**
	\brief	Réimplémentation de QSyntaxHighlighter::highlightBlock().

	Effectue la coloration selon les règles spécifiées par rules(), plus celle des commentaires multilignes, ainsi que la détection des parenthèses.
	*/
	virtual void highlightBlock(const QString &Text);

private:
	///Fonction dont le code est à colorer.
	MecAbstractFunction* const m_function;
	///Éditeur principal dans lequel l'édition a lieue.
	MecAbstractEditor* const m_mainEditor;
};


#endif /* __MECSYNTAXHIGHLIGHTER_H__ */

