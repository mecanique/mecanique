/*
© Quentin VIGNAUD, 2013

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#include <QtCore>
#include <QApplication>

#include <iostream>

#include "MecPluginsManager.h"
#include "MecSingleApplication.h"
#include "MecUpdater.h"
#include "MecEditor.h"

int main(int argc, char *argv[])
{

	QCoreApplication::setApplicationName("Mécanique");
	QCoreApplication::setApplicationVersion("release-1.006");
	QCoreApplication::setOrganizationName("Mécanique");
	QCoreApplication::setOrganizationDomain("mecanique.cc");
	
	QApplication app(argc, argv);
	QStringList args = app.arguments();
	
	MecSingleApplication singleApplication;
	
	//Check if this is the first actual execution.
	if (!singleApplication.isFirst())
	{
		//If a file is requested by the command-line.
		if (args.size() > 1)
		{
			singleApplication.openFile(app.arguments().at(app.arguments().size() - 1));
		}
		else
		{
			singleApplication.openFile(QString());
		}
		
		return EXIT_SUCCESS;
	}

	//Qt translations
	QString locale = QLocale::system().name().section('_', 0, 0);
	QTranslator qtTranslator;
	qtTranslator.load(QString("qt_") + locale, QLibraryInfo::location(QLibraryInfo::TranslationsPath));
	app.installTranslator(&qtTranslator);
	
	//Mécanique translations
	QTranslator mecTranslator;
	mecTranslator.load(QString("mecanique.") + locale, app.applicationDirPath() + QString("/../share/mecanique/translations"));
	app.installTranslator(&mecTranslator);
	
	//Icon theme
	QIcon::setThemeSearchPaths(QStringList("/usr/share/icons") << app.applicationDirPath() + "/../share/icons");
	bool themeFound = false;
	for (int i=0 ; i < QIcon::themeSearchPaths().size() ; i++) if (QDir(QIcon::themeSearchPaths().at(i) + "/Humanity").exists()) {
		QIcon::setThemeName("Humanity");
		themeFound = true;
	}
	if (!themeFound) QIcon::setThemeName("default");
	
	app.setWindowIcon(QIcon(app.applicationDirPath() + "/../share/mecanique/images/medium.png"));
	
	//Unique plugins manager.
	MecPluginsManager pluginsManager;
	pluginsManager.load();
	
	singleApplication.start(&pluginsManager);
	
	//Unique updater.
	MecUpdater updater(&pluginsManager);
	updater.run();
	
	//First window editor.
	MecEditor editor(&pluginsManager);
	
	//If a file is requested by the command-line.
	if (args.size() > 1 and QFileInfo(args.at(args.size() - 1)).exists())
	{
		editor.open(app.arguments().at(app.arguments().size() - 1));
	}
	
	editor.show();
	

	return app.exec();
}


