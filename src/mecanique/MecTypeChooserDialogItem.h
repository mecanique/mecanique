/*
© Quentin VIGNAUD, 2014

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#ifndef __MECTYPECHOOSERDIALOGITEM_H__
#define __MECTYPECHOOSERDIALOGITEM_H__

#include <QtWidgets>

/**
\brief	Représentation d'un type pour une fenêtre de séléection de type.
*/
class MecTypeChooserDialogItem : public QWidget
{
	Q_OBJECT

public:
	/**
	\brief	Constructeur.
	*/
	MecTypeChooserDialogItem(QWidget * Parent=0, Qt::WindowFlags F=0);
	///Destructeur.
	~MecTypeChooserDialogItem();

	/**
	\brief	Retourne le type de l'item.
	*/
	QString getType() const;
	/**
	\brief	Fixe le type de l'item.
	*/
	void setType(QString Type);

	/**
	\brief	Indique si l'item est sélectionné.
	*/
	bool isSelected() const;

signals:
	/**
	\brief	Est émit lorsque l'item est sélectionné
	*/
	void selected(QString Type);

private:
	///Nom de type.
	QString m_type;
	///Indique si le bouton est sélectionné.
	bool m_selected;

	/**
	\brief	Layout principal.
	*/
	QVBoxLayout *layoutMain;

	/**
	\brief	Bouton de sélection.
	*/
	QPushButton *buttonSelect;
	/**
	\brief	Label de nom.
	*/
	QLabel *labelName;

private slots:
	///Est lié au clic de buttonSelect.
	void buttonClicked();
};

#endif /* __MECTYPECHOOSERDIALOGITEM_H__ */

