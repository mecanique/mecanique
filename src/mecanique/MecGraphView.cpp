/*
© Quentin VIGNAUD, 2014

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#include "MecGraphView.h"

MecGraphView::MecGraphView(QString Text, QWidget *Parent) : QLabel(Text, Parent)
{
}

MecGraphView::~MecGraphView()
{
}

void MecGraphView::wheelEvent(QWheelEvent *Event)
{
	if (Event->angleDelta().y() > 0) emit zoomInRequested();
	else if (Event->angleDelta().y() < 0) emit zoomOutRequested();
	
	Event->accept();
}


