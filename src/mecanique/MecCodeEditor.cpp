/*
© Quentin VIGNAUD, 2014

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#include "MecCodeEditor.h"

MecCodeEditor::MecCodeEditor(MecAbstractFunction* const Function, MecAbstractEditor* MainEditor, QWidget* Parent) : m_function (Function), m_mainEditor (MainEditor)
{
	setParent(Parent);
	
	m_textEdit = new MecPlainTextEdit(this);
		connect(m_textEdit, SIGNAL(textChanged()), SLOT(textEditCodeChanged()));
		new MecSyntaxHighlighter(function(), mainEditor(), m_textEdit->document());
	
	layoutMain = new QGridLayout(this);
		layoutMain->addWidget(m_textEdit, 0, 0);
	
	connect(m_function, SIGNAL(codeChanged(MecAbstractFunction*)), SLOT(codeFunctionChanged(MecAbstractFunction*)));
	codeFunctionChanged(function());
}

MecCodeEditor::~MecCodeEditor()
{
}

MecAbstractFunction* MecCodeEditor::function() const
{
	return m_function;
}
	
MecAbstractEditor* MecCodeEditor::mainEditor() const
{
	return m_mainEditor;
}
	
QVariant MecCodeEditor::state()
{
	QHash<QString, QVariant> tempProperties;
	tempProperties.insert("scrollBarPos", m_textEdit->verticalScrollBar()->value());
	tempProperties.insert("cursorPos", m_textEdit->textCursor().position());
	return QVariant(tempProperties);
}

void MecCodeEditor::setState(QVariant State)
{
	QHash<QString, QVariant> tempProperties = State.toHash();
	m_textEdit->verticalScrollBar()->setValue(tempProperties.value("scrollBarPos").toInt());
	
	QTextCursor tempCursor = m_textEdit->textCursor();
	tempCursor.setPosition(tempProperties.value("cursorPos").toInt());
	m_textEdit->setTextCursor(tempCursor);
}

void MecCodeEditor::codeFunctionChanged(MecAbstractFunction *Function)
{
	if (Function != function()) return;
	if (!m_textEdit->hasFocus())//Si la zone d'édition n'a pas le focus (c.-à-d. que l'utilisateur n'est pas à l'origine de la modification), on remplace son contenu.
		{
		disconnect(m_textEdit, SIGNAL(textChanged()), this, SLOT(textEditCodeChanged()));
		m_textEdit->setPlainText(function()->code());
		connect(m_textEdit, SIGNAL(textChanged()), this, SLOT(textEditCodeChanged()));
		}
}

void MecCodeEditor::textEditCodeChanged()
{
	QString oldText = function()->code();
	function()->setCode(m_textEdit->toPlainText());

	if (oldText != function()->code()) mainEditor()->addEditStep(tr("Change code of “%1”").arg(function()->elementName()));
}


