/*
© Quentin VIGNAUD, 2013

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#include "MecEditor.h"

MecEditor::MecEditor(MecPluginsManager *PluginsManager, MecEditor *Parent, Qt::WindowFlags Flags) : QMainWindow(Parent, Flags), m_pluginsManager (PluginsManager), m_element (0), m_rootEditor (0), m_elementTree (0), m_graphDialog (0), m_compilerDialog (0)
{
setWindowTitle(tr("Mécanique"));
setMinimumSize(QSize(800, 600));

setCentralWidget(new QLabel(tr("<center>No elements to edit.</center>"), this));

m_undoRedoManager = new MecUndoRedoManager(this);
	connect(m_undoRedoManager, SIGNAL(indexChanged()), this, SLOT(updateEditMenu()));
	connect(m_undoRedoManager, SIGNAL(indexChanged()), this, SLOT(modified()));

menuFile = menuBar()->addMenu(tr("&File"));
	actionCreate = menuFile->addAction(QIcon::fromTheme("document-new"), tr("&Create..."), this, SLOT(create()), QKeySequence("Ctrl+N"));
	actionOpen = menuFile->addAction(QIcon::fromTheme("document-open"), tr("&Open..."), this, SLOT(open()), QKeySequence("Ctrl+O"));
	actionSave = menuFile->addAction(QIcon::fromTheme("document-save"), tr("&Save"), this, SLOT(save()), QKeySequence("Ctrl+S"));
		actionSave->setEnabled(false);
	actionSaveAs = menuFile->addAction(QIcon::fromTheme("document-save-as"), tr("S&ave as..."), this, SLOT(saveAs()), QKeySequence("Shift+Ctrl+S"));
		actionSaveAs->setEnabled(false);
	menuFile->addSeparator();
	actionNewWindow = menuFile->addAction(QIcon::fromTheme("window-new"), tr("&New window"), this, SLOT(newWindow()));
	actionClose = menuFile->addAction(QIcon::fromTheme("window-close"), tr("&Close"), this, SLOT(close()), QKeySequence("Ctrl+W"));
	actionQuit = menuFile->addAction(QIcon::fromTheme("application-exit"), tr("&Quit"), qApp, SLOT(quit()), QKeySequence("Ctrl+Q"));

menuEdit = menuBar()->addMenu(tr("&Edit"));
	actionUndo = menuEdit->addAction(QIcon::fromTheme("edit-undo"), tr("&Undo"), m_undoRedoManager, SLOT(undo()));
		actionUndo->setEnabled(false);
	actionRedo = menuEdit->addAction(QIcon::fromTheme("edit-redo"), tr("&Redo"), m_undoRedoManager, SLOT(redo()));
		actionRedo->setEnabled(false);
	menuEdit->addSeparator();
	actionAddObject = menuEdit->addAction(QIcon(":/share/icons/object-add.png"), tr("Add &object..."), this, SLOT(addObject()));
		actionAddObject->setEnabled(false);
	actionAddFunction = menuEdit->addAction(QIcon(":/share/icons/function-add.png"), tr("Add &function..."), this, SLOT(addFunction()));
		actionAddFunction->setEnabled(false);
	actionAddSignal = menuEdit->addAction(QIcon(":/share/icons/signal-add.png"), tr("Add &signal..."), this, SLOT(addSignal()));
		actionAddSignal->setEnabled(false);
	actionAddVariable = menuEdit->addAction(QIcon(":/share/icons/variable-add.png"), tr("Add &variable..."), this, SLOT(addVariable()));
		actionAddVariable->setEnabled(false);
	actionRemoveElement = menuEdit->addAction(QIcon::fromTheme("edit-delete"), tr("Remove element"), this, SLOT(removeElement()));
		actionRemoveElement->setEnabled(false);
	menuEdit->addSeparator();
	actionElementReference = menuEdit->addAction(QIcon::fromTheme("help-contents"), tr("Show reference"), this, SLOT(showElementReference()));
		actionElementReference->setEnabled(false);
	menuEdit->addSeparator();
	actionImport = menuEdit->addAction(QIcon::fromTheme("go-jump"), tr("&Import..."), this, SLOT(import()));
		actionImport->setEnabled(false);

menuTools = menuBar()->addMenu(tr("&Tools"));
	actionShowGraphDialog = menuTools->addAction(QIcon::fromTheme("window-new"), tr("Show &graph..."));
		actionShowGraphDialog->setShortcut(QKeySequence("Ctrl+G"));
		actionShowGraphDialog->setEnabled(false);
	actionCompile = menuTools->addAction(QIcon::fromTheme("application-x-executable"), tr("&Compile..."), this, SLOT(compile()));
		actionCompile->setEnabled(false);
	menuTools->addSeparator();
	actionShowPluginsManager = menuTools->addAction(tr("&Plugins manager"), m_pluginsManager, SLOT(show()), QKeySequence("Ctrl+P"));

menuHelp = menuBar()->addMenu(tr("&Help"));
	actionHelp = menuHelp->addAction(QIcon::fromTheme("help"), tr("Summary..."), this, SLOT(help()), QKeySequence("F1"));
	menuHelp->addSeparator();
	actionReportBug = menuHelp->addAction(QIcon::fromTheme("help-hint"), tr("Report a bug..."), this, SLOT(reportBug()));
	menuHelp->addSeparator();
	actionAboutMecanique = menuHelp->addAction(windowIcon(), tr("About &Mécanique..."), this, SLOT(aboutMecanique()));
	actionAboutQt = menuHelp->addAction(tr("About &Qt..."), qApp, SLOT(aboutQt()));

menuContextElement = new QMenu(this);
	menuContextElement->addAction(actionAddObject);
	menuContextElement->addAction(actionAddFunction);
	menuContextElement->addAction(actionAddSignal);
	menuContextElement->addAction(actionAddVariable);
	menuContextElement->addAction(actionRemoveElement);
	menuContextElement->addSeparator();
	menuContextElement->addAction(actionElementReference);

toolBar = addToolBar(tr("&Tools"));
	toolBar->setObjectName("toolBar");
	toolBar->setFloatable(false);
	toolBar->addAction(actionCreate);
	toolBar->addAction(actionOpen);
	toolBar->addAction(actionSave);
	toolBar->addSeparator();
	toolBar->addAction(actionUndo);
	toolBar->addAction(actionRedo);
	toolBar->addSeparator();
	toolBar->addAction(actionAddObject);
	toolBar->addAction(actionAddFunction);
	toolBar->addAction(actionAddSignal);
	toolBar->addAction(actionAddVariable);
	toolBar->addAction(actionRemoveElement);
	toolBar->addSeparator();
	toolBar->addAction(actionElementReference);
	toolBar->addSeparator();
	toolBar->addAction(actionShowGraphDialog);
	toolBar->addSeparator();
	toolBar->addAction(actionCompile);

dockElementTree = new QDockWidget(tr("Element's tree"), this);
	dockElementTree->setObjectName("dockElementTree");
	dockElementTree->setAllowedAreas(Qt::LeftDockWidgetArea | Qt::RightDockWidgetArea);
	dockElementTree->setFeatures(QDockWidget::DockWidgetMovable);
	addDockWidget(Qt::LeftDockWidgetArea, dockElementTree);

}

MecEditor::~MecEditor()
{
delete m_elementTree;
delete m_rootEditor;
delete m_element;
delete m_undoRedoManager;
}
	
bool MecEditor::open(QString FileName)
{
QFile fileInput(FileName);
if (fileInput.open(QIODevice::ReadOnly))
	{
	MecElementReader reader(fileInput.readAll());
	fileInput.close();
	if (reader.element() != 0)
		{
		QString oldFileName = m_fileName;
		m_fileName = FileName;
		if (setElement(reader.element()))
			{
			setWindowTitle(m_element->elementName() + "[*] (" + m_fileName + ") – " + tr("Mécanique"));
			m_undoRedoManager->reset();
			m_undoRedoManager->addStep("Original document0");
			unmodified();
	
			return true;
			}
		else
			{
			m_fileName = oldFileName;
			return false;
			}
		}
	else
		{
		QMessageBox::critical(this, tr("Loading error"), tr("<h3>Loading error</h3><p>The file “") + FileName + tr("” contains no mécanique data.</p>"));
		return false;
		}
	}
else
	{
	QMessageBox::critical(this, tr("Loading error"), tr("<h3>Loading error</h3><p>The file “") + FileName + tr("” could not be opened.</p>"));
	return false;
	}
return false;
}

bool MecEditor::save(QString FileName)
{
QFile fileOutput(FileName);
if (fileOutput.open(QIODevice::WriteOnly | QIODevice::Truncate))
	{
	MecElementWriter writer(&fileOutput, m_element);
		
	fileOutput.close();
	if (fileOutput.error() != QFile::NoError)
		{
		QMessageBox::critical(this, tr("Writing error"), tr("<h3>Writing error</h3><p>") + fileOutput.errorString() + tr("</p>"));
		return false;
		}
	else
		{
		unmodified();
		return true;
		}
	}
else
	{
	QMessageBox::critical(this, tr("Writing error"), tr("<h3>Writing error</h3><p>The file “") + FileName + tr("” could not be opened.</p>"));
	return false;
	}
}

MecAbstractElement* MecEditor::read(QString FileName)
{
QFile fileInput(FileName);
if (fileInput.open(QIODevice::ReadOnly))
	{
	MecElementReader reader(fileInput.readAll());
	fileInput.close();
	
	return reader.element();
	}
else return 0;
}

bool MecEditor::write(MecAbstractElement *Element, QString FileName)
{
QFile fileOutput(FileName);
if (fileOutput.open(QIODevice::WriteOnly | QIODevice::Truncate))
	{
	MecElementWriter writer(&fileOutput, Element);
		
	fileOutput.close();
	if (fileOutput.error() != QFile::NoError)
		{
		return false;
		}
	else return true;
	}
else
	{
	return false;
	}
}

QStringList MecEditor::baseElements(MecAbstractElement::ElementRole Role)
{
QDir tempDir(":/share/base");
QStringList returnList;
switch (Role)
	{
	case MecAbstractElement::Project :
		tempDir.cd("project");
		break;
	case MecAbstractElement::Object :
		tempDir.cd("object");
		break;
	case MecAbstractElement::Function :
		tempDir.cd("function");
		break;
	case MecAbstractElement::Signal :
		tempDir.cd("signal");
		break;
	case MecAbstractElement::Variable :
		tempDir.cd("variable");
		break;
	}
returnList = tempDir.entryList(QDir::Files, QDir::Name);
returnList.replaceInStrings(".mec", "");

return returnList;
}

MecAbstractElement* MecEditor::baseElement(MecAbstractElement::ElementRole Role, QString Name)
{
QString fileName(":/share/base/");
switch (Role)
	{
	case MecAbstractElement::Project :
		fileName.append("project");
		break;
	case MecAbstractElement::Object :
		fileName.append("object");
		break;
	case MecAbstractElement::Function :
		fileName.append("function");
		break;
	case MecAbstractElement::Signal :
		fileName.append("signal");
		break;
	case MecAbstractElement::Variable :
		fileName.append("variable");
		break;
	}
fileName.append("/" + Name + ".mec");
return read(fileName);
}

QString MecEditor::selectType(QStringList Types)
{
	MecTypeChooserDialog tempDialog(this);
	
	for (int i=0 ; i < Types.size() ; i++) tempDialog.addType(Types.at(i));
	
	tempDialog.exec();
	
	return tempDialog.getSelectedType();
}

QStringList MecEditor::reservedNames() const
{
QStringList tempList;

	tempList
		//Structures de contrôle
		<< "if"
		<< "else"
		<< "switch"
		<< "case"
		<< "while"
		<< "do"
		<< "for"
		<< "break"
		<< "continue"
		<< "return"
		<< "emit"
		//Mots-clés divers
		<< "catch"
		<< "class"
		<< "delete"
		<< "enum"
		<< "explicit"
		<< "friend"
		<< "inline"
		<< "namespace"
		<< "new"
		<< "operator"
		<< "private"
		<< "protected"
		<< "public"
		<< "signal"
		<< "signals"
		<< "slot"
		<< "slots"
		<< "signed"
		<< "struct"
		<< "this"
		<< "try"
		<< "typedef"
		<< "typename"
		<< "union"
		<< "virtual"
		<< "volatile"
		//Noms de types de base
		<< "void"
		<< "bool"
		<< "char"
		<< "int"
		<< "float"
		<< "double"
		<< "long"
		<< "short"
		<< "int8_t"
		<< "int16_t"
		<< "int32_t"
		<< "int64_t"
		<< "uint8_t"
		<< "uint16_t"
		<< "uint32_t"
		<< "uint64_t"
		<< "wchar_t"
		<< "qint8"
		<< "qint16"
		<< "qint32"
		<< "qint64"
		<< "qintptr"
		<< "qlonglong"
		<< "qptrdiff"
		<< "qreal"
		<< "quint8"
		<< "quint16"
		<< "quint32"
		<< "quintptr"
		<< "qulonglong"
		<< "uchar"
		<< "uint"
		<< "ulong"
		<< "ushort"
		;

for (int i=0 ; i < pluginsManager()->plugins().size() ; i++)
	{
	tempList += pluginsManager()->plugins().at(i)->type();
	}

return tempList;
}

MecAbstractCodeEditor* MecEditor::codeEditor(MecAbstractFunction* const Function, QWidget* Parent)
{
return new MecCodeEditor(Function, this, Parent);
}
	
MecAbstractPluginsManager* MecEditor::pluginsManager() const
{
return m_pluginsManager;
}

MecAbstractElement* MecEditor::element() const
{
return m_element;
}

bool MecEditor::setElement(MecAbstractElement *Element)
{
	MecAbstractPlugin *tempPlugin = m_pluginsManager->plugin(Element->elementRole(), Element->elementType());
	if (tempPlugin != 0)
		{
		
		MecAbstractElement *tempElement = m_element;
		m_element = Element;
		MecAbstractElementEditor *tempEditor = tempPlugin->elementEditor(Element, this);
		
		if (tempEditor->pluginsErrors().isEmpty())
			{
			setUpdatesEnabled(false);//Évite le « scintillement »
			
			//On remet à zéro l'éditeur.
				//Necéssaire durant la suppression.
				m_element = tempElement;
			delete m_elementTree;
			delete m_rootEditor;
			delete m_element;
			delete m_graphDialog;
			delete m_compilerDialog;
				//Necéssaire pour la création.
				m_element = Element;

			//On effectue alors tout ce qui est nécessaire pour éditer pleinement l'élément.
			m_elementTree = new MecElementTree(m_element, this, dockElementTree);
				connect(m_elementTree, SIGNAL(elementCurrentChange(MecAbstractElement*)), SLOT(updateEditMenu()));
				connect(m_elementTree, SIGNAL(elementActivated(MecAbstractElement*, int)), tempEditor, SLOT(editChild(MecAbstractElement*)));
				connect(tempEditor, SIGNAL(editedElementChange(MecAbstractElement*)), m_elementTree, SLOT(changeCurrentElement(MecAbstractElement*)));
			
			m_rootEditor = tempEditor;
				m_rootEditor->setMinimumSize(QSize(600, 500));
				connect(m_rootEditor, SIGNAL(pluginsErrorsOccured()), SLOT(pluginsErrorsOccured()));
			
			m_graphDialog = new MecGraphDialog(m_element, this);
				connect(actionShowGraphDialog, SIGNAL(triggered()), m_graphDialog, SLOT(updateGraph()));
				connect(actionShowGraphDialog, SIGNAL(triggered()), m_graphDialog, SLOT(showMaximized()));
			
			if (m_element->elementRole() == MecAbstractElement::Project)
				{
				m_compilerDialog = new MecCompilerDialog(static_cast<MecProject*>(m_element), m_pluginsManager, QFileInfo(m_fileName).canonicalPath(), this, Qt::Window);
				m_compilerDialog->setWindowModality(Qt::WindowModal);
				actionCompile->setEnabled(true);
				}
			else
				{
				actionCompile->setEnabled(false);
				}
			
			//Affichage
			setCentralWidget(m_rootEditor);
			dockElementTree->setWidget(m_elementTree);
				m_elementTree->show();
			
			actionSaveAs->setEnabled(true);
			actionImport->setEnabled(true);
			actionShowGraphDialog->setEnabled(true);
			updateEditMenu();
			
			//On liste tous les QTabWidgets enfants de l'éditeur racine.
			QList<QTabWidget*> tempList;
			tempList += m_rootEditor->findChildren<QTabWidget*>(QRegExp(".*"), Qt::FindChildrenRecursively);
			//Et on effectue un redessinage complet des sous-QTabWidgets, en raison d'un bug d'affichage des QTabWidgets lorsqu'ils sont incrémentés.
			for (int i=0 ; i < tempList.size() ; i++)
			{
				for (int j=tempList.at(i)->count() ; j >= 0 ; j--)
				{
					tempList.at(i)->setCurrentIndex(j);
				}
			}
			
			setUpdatesEnabled(true);
			update();
			return true;
			}
		else
			{
			m_element = tempElement;
			pluginsErrorsOccured(tempEditor);
			delete tempEditor;
			return false;
			}
		}
	else
		{
		QMessageBox::critical(this, tr("Plugin error"), tr("<h3>Plugin error</h3><p>The element “") + Element->elementName() + tr("” (type “") + Element->elementType() + tr("”) don't have appropriate plugin.</p>"));
		return false;
		}
}

MecAbstractElement* MecEditor::selectedElement() const
{
if (m_elementTree != 0) return m_elementTree->currentElement();
else return 0;
}

MecElementTree* MecEditor::elementTree() const
{
	return m_elementTree;
}

void MecEditor::addEditStep(QString Name) const
{
	m_undoRedoManager->addStep(Name);
}

void MecEditor::elementContextMenu(MecAbstractElement *Element, QPoint Pos)
{
	m_elementTree->changeCurrentElement(Element);
	
	menuContextElement->exec(Pos);
}

void MecEditor::showElementReference(MecAbstractElement *Element) const
{
	switch (Element->elementRole()) {
		case MecAbstractElement::Project :
			QDesktopServices::openUrl(QUrl("http://www.mecanique.cc/reference/elements"));
			break;
		case MecAbstractElement::Object :
			if (Element->elementType() == "Object") QDesktopServices::openUrl(QUrl("http://www.mecanique.cc/reference/elements"));
			else {
				QDesktopServices::openUrl(QUrl("http://www.mecanique.cc/reference/plugins/" + Element->elementType().toLower() + "-object"));
			}
			break;
		case MecAbstractElement::Function :
			if (Element->parentElement() != 0 and Element->parentElement()->elementRole() == MecAbstractElement::Object) {
				QDesktopServices::openUrl(QUrl("http://www.mecanique.cc/reference/plugins/" + Element->parentElement()->elementType().toLower() + "-object"));
			} else {
				QDesktopServices::openUrl(QUrl("http://www.mecanique.cc/reference/language"));
			}
			break;
		case MecAbstractElement::Signal :
			if (Element->parentElement() != 0 and Element->parentElement()->elementRole() == MecAbstractElement::Object) {
				QDesktopServices::openUrl(QUrl("http://www.mecanique.cc/reference/plugins/" + Element->parentElement()->elementType().toLower() + "-object"));
			} else {
				QDesktopServices::openUrl(QUrl("http://www.mecanique.cc/reference/elements"));
			}
			break;
		case MecAbstractElement::Variable :
			QDesktopServices::openUrl(QUrl("http://www.mecanique.cc/reference/language/" + Element->elementType().toLower()));
			break;
	}
}

MecAbstractElementEditor* MecEditor::rootEditor() const
{
	return m_rootEditor;
}

QString MecEditor::roleText(MecAbstractElement::ElementRole Role) const
{
switch (Role)
	{
	case MecAbstractElement::Project :
		return tr("Project");
		break;
	case MecAbstractElement::Object :
		return tr("Object");
		break;
	case MecAbstractElement::Function :
		return tr("Function");
		break;
	case MecAbstractElement::Signal :
		return tr("Signal");
		break;
	case MecAbstractElement::Variable :
		return tr("Variable");
		break;
	default :
		return QString();
		break;
	}
}

void MecEditor::create()
{
QString Type = selectType(baseElements(MecAbstractElement::Project));

if (Type.isEmpty() == false)
	{
	open(":/share/base/project/" + Type + ".mec");
	m_fileName.clear();
	setWindowTitle(m_element->elementName() + "[*] – " + tr("Mécanique"));
	modified();
	}
}

void MecEditor::open()
{
QString tempFileName = QFileDialog::getOpenFileName(this, tr("Open..."), QDir::homePath(), tr("Mécanique file (*.mec);;All files (*.*)"), 0, QFileDialog::DontResolveSymlinks);
if (tempFileName.isEmpty()) return;

open(tempFileName);
}

void MecEditor::save()
{
if (!m_fileName.isEmpty()) save(m_fileName);
else saveAs();
}

void MecEditor::saveAs()
{
QString tempFileName = QFileDialog::getSaveFileName(this, tr("Save as..."), QDir::homePath(), tr("Mécanique file (*.mec)"), 0, QFileDialog::DontResolveSymlinks);
if (tempFileName.isEmpty()) return;
if (!tempFileName.endsWith(".mec", Qt::CaseInsensitive)) tempFileName.append(".mec");

if (save(tempFileName))
	{
	m_fileName = tempFileName;
	setWindowTitle(m_element->elementName() + "[*] (" + m_fileName + ") – " + tr("Mécanique"));
	}
}

void MecEditor::newWindow()
{
(new MecEditor(m_pluginsManager))->show();
}

void MecEditor::updateEditMenu()
{
//Général.
const MecUndoRedoManager::Step *undoStep = m_undoRedoManager->previousStep();
const MecUndoRedoManager::Step *redoStep = m_undoRedoManager->nextStep();
if (undoStep != 0)
	{
	actionUndo->setEnabled(true);
	actionUndo->setText(tr("&Undo %1").arg(undoStep->name));
	}
else
	{
	actionUndo->setEnabled(false);
	actionUndo->setText(tr("&Undo"));
	}
if (redoStep != 0)
	{
	actionRedo->setEnabled(true);
	actionRedo->setText(tr("&Redo %1").arg(redoStep->name));
	}
else
	{
	actionRedo->setEnabled(false);
	actionRedo->setText(tr("&Redo"));
	}

//Par rapport à l'élément.
MecAbstractElementEditor *tempEditor = m_rootEditor->elementEditor(m_elementTree->currentElement());
if (tempEditor != 0)
	{
	actionAddObject->setEnabled(tempEditor->canAddObject());
	actionAddFunction->setEnabled(tempEditor->canAddFunction());
	actionAddSignal->setEnabled(tempEditor->canAddSignal());
	actionAddVariable->setEnabled(tempEditor->canAddVariable());
	actionElementReference->setEnabled(true);
	
	MecAbstractElementEditor *tempParentEditor = m_rootEditor->elementEditor(m_elementTree->currentElement()->parentElement());
	if (tempParentEditor !=0)
		{
		actionRemoveElement->setEnabled(tempParentEditor->canRemoveChild(m_elementTree->currentElement()));
		}
	else
		{
		actionRemoveElement->setEnabled(false);
		}
	}
else
	{
	actionAddObject->setEnabled(false);
	actionAddFunction->setEnabled(false);
	actionAddSignal->setEnabled(false);
	actionAddVariable->setEnabled(false);
	actionRemoveElement->setEnabled(false);
	actionElementReference->setEnabled(false);
	}
}

void MecEditor::addObject()
{
MecAbstractElementEditor *tempEditor = m_rootEditor->elementEditor(m_elementTree->currentElement());
if (tempEditor != 0) tempEditor->addObject();
}

void MecEditor::addFunction()
{
MecAbstractElementEditor *tempEditor = m_rootEditor->elementEditor(m_elementTree->currentElement());
if (tempEditor != 0) tempEditor->addFunction();
}

void MecEditor::addSignal()
{
MecAbstractElementEditor *tempEditor = m_rootEditor->elementEditor(m_elementTree->currentElement());
if (tempEditor != 0) tempEditor->addSignal();
}

void MecEditor::addVariable()
{
MecAbstractElementEditor *tempEditor = m_rootEditor->elementEditor(m_elementTree->currentElement());
if (tempEditor != 0) tempEditor->addVariable();
}

void MecEditor::removeElement()
{
MecAbstractElementEditor *tempParentEditor = m_rootEditor->elementEditor(m_elementTree->currentElement()->parentElement());
if (tempParentEditor !=0)
	{
	if (tempParentEditor->canRemoveChild(m_elementTree->currentElement()))
		{
		QString tempName = m_elementTree->currentElement()->elementName();
		delete m_elementTree->currentElement();
		addEditStep(tr("Remove element “%1”").arg(tempName));
		}
	}
}

void MecEditor::showElementReference()
{
	showElementReference(m_elementTree->currentElement());
}

void MecEditor::import()
{
QString tempFileName = QFileDialog::getOpenFileName(this, tr("Import..."), QDir::homePath(), tr("Mécanique file (*.mec);;All files (*.*)"), 0, QFileDialog::DontResolveSymlinks);

if (!tempFileName.isEmpty()) {
	MecAbstractElement *tempElement = read(tempFileName);
	if (tempElement != 0 and tempElement->elementRole() == MecAbstractElement::Project)
		{
		while (!tempElement->childElements().isEmpty())
			{
			tempElement->childElements().first()->setParentElement(m_element);
			}
		}
	else
		{
		QMessageBox::critical(this, tr("Import impossible"), tr("<h3>Import impossible</h3><p>The import is impossible: the file specified contains no importable project element.</p>"));
		}
	}
}
	
void MecEditor::compile()
{
if (m_element->elementRole() == MecAbstractElement::Project)
	{
	m_compilerDialog->show();
	}
else
	{
	QMessageBox::critical(this, tr("Compilation impossible"), tr("<h3>Compilation impossible</h3><p>The root element is not a project, the compilation cannot be made.</p>"));
	}
}
	
void MecEditor::help()
{
	QDesktopServices::openUrl(QUrl("http://www.mecanique.cc/reference"));
}

void MecEditor::reportBug()
{
	QDesktopServices::openUrl(QUrl("http://bugs.mecanique.cc/report"));
}

void MecEditor::aboutMecanique()
{
QMessageBox::about(this, tr("About the editor..."), tr("<h3>About the editor</h3><p>Version %1</p><p>Copyrigth © 2013 – 2015 Quentin VIGNAUD</p><p>Licensed under the EUPL, version 1.1 only.<br />You may not use this work except in compliance with the Licence. You may obtain a copy of the Licence at: <a href=\"http://joinup.ec.europa.eu/software/page/eupl/licence-eupl\">http://joinup.ec.europa.eu/software/page/eupl/licence-eupl</a></p><p>Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an “AS IS” basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.<br />See the Licence for the specific language governing permissions and limitations under the Licence.</p>").arg(QCoreApplication::applicationVersion()));
/*
<h3>À propos de l'éditeur</h3><p>Première version</p><p>Copyrigth © 2013 Quentin VIGNAUD</p><p>L'éditeur de projets « Mécanique » est une interface de gestion, d'édition et de compilation des différents éléments des projets, reposant sur l'utilisation de greffons adaptés aux informations et aux tâches demandées.</p><p>Programme concédé sous licence EUPL, version 1.1 uniquement.<br />Vous ne pouvez utiliser la présente œuvre que conformément à la Licence. Vous pouvez obtenir une copie de la Licence à l’adresse suivante : <a href=\"http://joinup.ec.europa.eu/software/page/eupl/licence-eupl\">http://joinup.ec.europa.eu/software/page/eupl/licence-eupl</a></p><p>Sauf obligation légale ou contractuelle écrite, le logiciel distribué sous la Licence est distribué « en l’état », SANS GARANTIES OU CONDITIONS QUELLES QU’ELLES SOIENT, expresses ou implicites.<br />Consultez la Licence pour les autorisations et les restrictions spécifiques relevant de la Licence.</p><small>[FR] français</small>
*/
}

void MecEditor::modified()
{
	setWindowModified(true);
	actionSave->setEnabled(true);
}

void MecEditor::unmodified()
{
	setWindowModified(false);
	actionSave->setEnabled(false);
}

void MecEditor::closeEvent(QCloseEvent *Event)
{
	while (isWindowModified())
	{
		QString tempText;
		if (m_fileName.isEmpty()) tempText = tr("<h3>Unsaved changes: %1</h3><p>Do you want to save changes occurred with %1?</p>").arg(m_element->elementName());
		else tempText = tr("<h3>Unsaved changes: %1</h3><p>Do you want to save changes occurred with %1 in “%2”?</p>").arg(m_element->elementName()).arg(m_fileName);
		
		QMessageBox::StandardButton button = QMessageBox::warning(this, tr("Unsaved changes..."), tempText,
		QMessageBox::Save | QMessageBox::Cancel | QMessageBox::Discard, QMessageBox::Save);
		
		if (button == QMessageBox::Save) save();
		else if (button == QMessageBox::Cancel)
			{
			Event->ignore();
			return;
			}
		else if (button == QMessageBox::Discard)
			{
			Event->accept();
			return;
			}
	}
	
	Event->accept();
}
	
void MecEditor::pluginsErrorsOccured(MecAbstractElementEditor *Editor)
{
if (Editor == 0) Editor = m_rootEditor;
//Name, Version, Role, Type
QString text(tr("<h3>Plugins errors</h3><p>The plugins corresponding to the following datas are required:<br /><table><tr><th>Error</th><th>Name</th><th>Version</th><th>Role</th><th>Type</th></tr>"));

for (int i=0 ; i < Editor->pluginsErrors().size() ; i++)
	{
	text += "<tr>";
	//Error
	text += "<td>";
	switch (Editor->pluginsErrors().at(i).error)
		{
		case MecPluginError::Undefined :
			text += tr("Undefined error");
			break;
		case MecPluginError::Unavailable :
			text += tr("Plugin unavailable");
			break;
		case MecPluginError::CastFailed :
			text += tr("Plugin uncompatible");
			break;
		}
	text += "</td><td>";
	//Name
	if (!(Editor->pluginsErrors().at(i).name.isEmpty()))
		{
		text += Editor->pluginsErrors().at(i).name;
		}
	else text += tr("No special plugin");
	text += "</td><td>";
	//Version
	if (Editor->pluginsErrors().at(i).version != 0)
		{
		text += QString::number(Editor->pluginsErrors().at(i).version, 'g');
		}
	else text += tr("No special version");
	text += "</td><td>";
	//Role
	text += roleText(Editor->pluginsErrors().at(i).role);
	text += "</td><td>";
	//Type
	text += Editor->pluginsErrors().at(i).type;
	text += "</td>";
	
	text += "</tr>";
	}

text += tr("</table><br /></p><p>See the plugins manager for more information.</p>");


QMessageBox::critical(this, tr("Plugins errors…"), text);
}


