/*
© Quentin VIGNAUD, 2014

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#include "MecUndoRedoManager.h"

MecUndoRedoManager::MecUndoRedoManager(MecEditor *Editor) : QObject(Editor), m_editor (Editor), m_index (-1)
{
	m_restoreProcess = false;
}

MecUndoRedoManager::~MecUndoRedoManager()
{
	while (!m_steps.isEmpty()) delete m_steps.takeFirst();
}

const MecUndoRedoManager::Step* MecUndoRedoManager::previousStep() const
{
	if (m_index > 0) return m_steps.at(m_index);
	else return 0;
}

const MecUndoRedoManager::Step* MecUndoRedoManager::nextStep() const
{
	if (m_index < m_steps.size() - 1) return m_steps.at(m_index + 1);
	else return 0;
}

void MecUndoRedoManager::addStep(QString Name)
{
	if (m_restoreProcess) return;
	else if (m_editor->rootEditor() == 0 or m_editor->elementTree() == 0 or m_editor->element() == 0) return;

	Step *tempStep = new Step();
	
	tempStep->name = Name;
	
	QHash<QString, QVariant> tempProperties;
	tempProperties.insert("rootEditor", m_editor->rootEditor()->state());
	tempProperties.insert("elementTree", m_editor->elementTree()->state());
	tempStep->editorState = QVariant(tempProperties);
	
	tempStep->elements = QByteArray();
	MecElementWriter(&(tempStep->elements), m_editor->element());
	
	//S'il existe des pas supérieurs au pas actuel, on les supprime.
	while (m_index < m_steps.size() - 1)
		{
		delete m_steps.takeLast();
		}
	
	m_steps.append(tempStep);
	m_index++;
	
	
	emit indexChanged();
	emit stepAdded();
}
	
void MecUndoRedoManager::undo()
{
	if (m_index > 0)
		{
		restoreStep(m_index - 1, 1);
		m_index--;
		emit indexChanged();
		}
}

void MecUndoRedoManager::redo()
{
	if (m_index < m_steps.size() - 1)
		{
		restoreStep(m_index + 1);
		m_index++;
		emit indexChanged();
		}
}

void MecUndoRedoManager::reset()
{
	if (m_restoreProcess) return;
	m_restoreProcess = false;
	while (!m_steps.isEmpty()) delete m_steps.takeFirst();
	m_index = -1;
	emit indexChanged();
}

void MecUndoRedoManager::restoreStep(int index, int shiftState)
{
	if (m_restoreProcess or index < 0 or index >= m_steps.size()) return;
	m_restoreProcess = true;

	MecElementReader reader(m_steps.at(index)->elements);
	m_editor->setElement(reader.element());
	
	if (m_editor->rootEditor() == 0 or m_editor->elementTree() == 0 or m_editor->element() == 0) return;
	if (index + shiftState >= 0 and index + shiftState < m_steps.size())
		{
		QHash<QString, QVariant> tempProperties = m_steps.at(index + shiftState)->editorState.toHash();

		m_editor->rootEditor()->setState(tempProperties.value("rootEditor"));
		m_editor->elementTree()->setState(tempProperties.value("elementTree"));
		
		}

	m_restoreProcess = false;
}



