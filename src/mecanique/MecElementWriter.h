/*
© Quentin VIGNAUD, 2013

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#ifndef __MECELEMENTWRITER_H__
#define __MECELEMENTWRITER_H__

#include <QXmlStreamWriter>
#include "MecProject.h"
#include "MecObject.h"
#include "MecFunction.h"
#include "MecSignal.h"
#include "MecVariable.h"

/**
\brief	Rédacteur de flux de données d'élément(s).
*/
class MecElementWriter : private QXmlStreamWriter
{
public:
	/**
	\brief	Constructeur.
	\param	Device	Flux dans lequel écrire.
	\param	Element Élément à écrire.
	*/
	MecElementWriter(QIODevice *Device, const MecAbstractElement *Element);
	/**
	\brief	Constructeur.
	\param	Array	Tableau dans lequel écrire.
	\param	Element Élément à écrire.
	*/
	MecElementWriter(QByteArray *Array, const MecAbstractElement *Element);
	/**
	\brief	Constructeur.
	\param	String	Chaîne dans laquelle écrire.
	\param	Element Élément à écrire.
	*/
	MecElementWriter(QString *String, const MecAbstractElement *Element);

private:
	const MecAbstractElement *m_element;

	///Initialise l'écriture.
	void startWriting();

	///Écrit l'élément.
	void writeElement(const MecAbstractElement *Element);
};

#endif /* __MECELEMENTWRITER_H__ */

