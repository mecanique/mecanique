/*
© Quentin VIGNAUD, 2014

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#include "MecTypeChooserDialogItem.h"

MecTypeChooserDialogItem::MecTypeChooserDialogItem(QWidget * Parent, Qt::WindowFlags F) : QWidget(Parent, F), m_selected (false)
{
	layoutMain = new QVBoxLayout(this);
	
	buttonSelect = new QPushButton(this);
		buttonSelect->setIconSize(QSize(64, 64));
		connect(buttonSelect, SIGNAL(clicked(bool)), SLOT(buttonClicked()));
	
	labelName = new QLabel(this);
	
	layoutMain->addWidget(buttonSelect);
	layoutMain->addWidget(labelName);
}

MecTypeChooserDialogItem::~MecTypeChooserDialogItem()
{
}
	
QString MecTypeChooserDialogItem::getType() const
{
	return m_type;
}

void MecTypeChooserDialogItem::setType(QString Type)
{
	buttonSelect->setIcon(QIcon(":/share/icons/types/" + Type + ".png"));
	labelName->setText("<center>" + Type + "</center>");
	m_type = Type;
}

bool MecTypeChooserDialogItem::isSelected() const
{
	return m_selected;
}

void MecTypeChooserDialogItem::buttonClicked()
{
	m_selected = true;
	emit selected(m_type);
}
	

