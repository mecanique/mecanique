/*
© Quentin VIGNAUD, 2014

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#include "MecGraphDialog.h"

MecGraphDialog::MecGraphDialog(MecAbstractElement* const RootElement, QWidget * Parent, Qt::WindowFlags Flags) : QMainWindow(Parent, Flags)
{
	setWindowTitle(tr("Mécanique graph"));
	
	m_graph = new MecGraph(RootElement, this);
		connect(m_graph, SIGNAL(modeChanged(MecGraph::Mode)), SLOT(updateModeActions(MecGraph::Mode)));
		connect(m_graph, SIGNAL(optionsChanged(MecGraph::Options)), SLOT(updateOptionsActions(MecGraph::Options)));
		connect(m_graph, SIGNAL(finished()), SLOT(graphFinished()));
		connect(m_graph, SIGNAL(failed()), SLOT(graphFailed()));
	
	scrollAreaGraph = new QScrollArea(this);
	setCentralWidget(scrollAreaGraph);
	scrollAreaGraph->setWidgetResizable(true);
	
	m_graphView = new MecGraphView("<center>" + tr("No graph.") + "</center>", scrollAreaGraph);
	scrollAreaGraph->setWidget(m_graphView);
	connect(m_graphView, SIGNAL(zoomInRequested()), SLOT(zoomIn()));
	connect(m_graphView, SIGNAL(zoomOutRequested()), SLOT(zoomOut()));
	m_graphView->setContextMenuPolicy(Qt::CustomContextMenu);
	connect(m_graphView, SIGNAL(customContextMenuRequested(QPoint)), SLOT(showContextMenu(QPoint)));
	m_graphView->show();
	
	menuFile = menuBar()->addMenu(tr("&File"));
		actionSaveAs = menuFile->addAction(QIcon::fromTheme("document-save-as"), tr("S&ave as..."), this, SLOT(saveAs()), QKeySequence("Shift+Ctrl+S"));
			actionSaveAs->setEnabled(false);
		menuFile->addSeparator();
		actionRefreshGraph = menuFile->addAction(QIcon::fromTheme("view-refresh"), tr("Refresh graph"), m_graph, SLOT(start()), QKeySequence("F5"));
		menuFile->addSeparator();
		actionClose = menuFile->addAction(QIcon::fromTheme("window-close"), tr("&Close"), this, SLOT(close()), QKeySequence("Ctrl+W"));
	
	menuDisplay = menuBar()->addMenu(tr("&Display"));
		actionZoomIn = menuDisplay->addAction(QIcon::fromTheme("zoom-in"), tr("Zoom &in"), this, SLOT(zoomIn()), QKeySequence("Ctrl++"));
			actionZoomIn->setEnabled(false);
		actionZoomOut = menuDisplay->addAction(QIcon::fromTheme("zoom-out"), tr("Zoom &out"), this, SLOT(zoomOut()), QKeySequence("Ctrl+-"));
			actionZoomOut->setEnabled(false);
		actionNormalSize = menuDisplay->addAction(QIcon::fromTheme("zoom-original"), tr("&Normal size"), this, SLOT(normalSize()), QKeySequence("Ctrl+N"));
			actionNormalSize->setEnabled(false);
		menuDisplay->addSeparator();
		actionFitToWindow = menuDisplay->addAction(QIcon::fromTheme("zoom-fit-best"), tr("&Fit to window"), this, SLOT(fitToWindow()), QKeySequence("Ctrl+F"));
			actionFitToWindow->setCheckable(true);
			actionFitToWindow->setEnabled(false);
	
	menuGraph = menuBar()->addMenu(tr("&Graph"));
		menuGraphMode = menuGraph->addMenu(tr("&Mode"));
			actionHierarchy = menuGraphMode->addAction(tr("&Hierarchy"), m_graph, SLOT(setHierarchyMode()));
				actionHierarchy->setCheckable(true);
			actionPresentation = menuGraphMode->addAction(tr("&Presentation"), m_graph, SLOT(setPresentationMode()));
				actionPresentation->setCheckable(true);
		menuGraph->addSeparator();
		actionShowConnections = menuGraph->addAction(tr("&Show connections"), this, SLOT(assignGraphOptions()));
			actionShowConnections->setCheckable(true);
		actionColorConnections = menuGraph->addAction(tr("&Color connections"), this, SLOT(assignGraphOptions()));
			actionColorConnections->setCheckable(true);
		actionHideVariables = menuGraph->addAction(tr("Hide &variables"), this, SLOT(assignGraphOptions()));
			actionHideVariables->setCheckable(true);
		actionNoBlockStructure = menuGraph->addAction(tr("No block &structure"), this, SLOT(assignGraphOptions()));
			actionNoBlockStructure->setCheckable(true);

	toolBar = addToolBar(tr("&Tools"));
		toolBar->setFloatable(false);
		toolBar->addAction(actionSaveAs);
		toolBar->addSeparator();
		toolBar->addAction(actionRefreshGraph);
		toolBar->addSeparator();
		toolBar->addAction(actionZoomIn);
		toolBar->addAction(actionZoomOut);
		toolBar->addAction(actionNormalSize);
		toolBar->addSeparator();
		toolBar->addAction(actionHierarchy);
		toolBar->addAction(actionPresentation);
		toolBar->addSeparator();
		toolBar->addAction(actionShowConnections);
		toolBar->addAction(actionColorConnections);
		toolBar->addAction(actionHideVariables);
		toolBar->addAction(actionNoBlockStructure);
	
	updateModeActions(m_graph->getMode());
	updateOptionsActions(m_graph->getOptions());
}

MecGraphDialog::~MecGraphDialog()
{
}

bool MecGraphDialog::save(QString FileName)
{
	if (m_graphView->pixmap() != 0)
	{
		return m_graph->graph().save(FileName, "PNG");
	}
	else return false;
}
	
void MecGraphDialog::saveAs()
{
QString tempFileName = QFileDialog::getSaveFileName(this, tr("Save as..."), QDir::homePath() + "/" + ((m_graph->rootElement() != 0) ? (m_graph->rootElement()->elementName() + ".png") : "graphic-mecanique.png"), tr("Portable Network Graphics (*.png)"), 0, QFileDialog::DontResolveSymlinks);
if (tempFileName.isEmpty()) return;
if (!tempFileName.endsWith(".png", Qt::CaseInsensitive)) tempFileName.append(".png");

if (!save(tempFileName))
	{
	QMessageBox::critical(this, tr("Saving error"), tr("<h3>Saving error</h3><p>The graph could not be saved.</p>"));
	}
}

void MecGraphDialog::updateGraph()
{
	m_graph->start();
}

void MecGraphDialog::updateDisplayActions()
{
	actionZoomIn->setEnabled(!actionFitToWindow->isChecked());
	actionZoomOut->setEnabled(!actionFitToWindow->isChecked());
	actionNormalSize->setEnabled(!actionFitToWindow->isChecked());
}

void MecGraphDialog::scaleGraph(double Factor)
{
	if (m_graphView->pixmap() == 0) return;
	m_scaleFactor *= Factor;
	m_graphView->setPixmap(m_graph->graph().scaled(m_scaleFactor * m_graph->graph().size(), Qt::KeepAspectRatio, Qt::SmoothTransformation));
	m_graphView->resize(m_graphView->pixmap()->size());
	
	adjustScrollBar(scrollAreaGraph->horizontalScrollBar(), Factor);
	adjustScrollBar(scrollAreaGraph->verticalScrollBar(), Factor);
	
	actionZoomIn->setEnabled(m_scaleFactor < 1.2);
	actionZoomOut->setEnabled(m_scaleFactor > 0.1);
}

void MecGraphDialog::adjustScrollBar(QScrollBar *ScrollBar, double Factor)
{
	ScrollBar->setValue(int(Factor * ScrollBar->value()
							+ ((Factor - 1) * ScrollBar->pageStep()/2)));
}

void MecGraphDialog::graphFinished()
{
m_graphView->setPixmap(m_graph->graph());
m_scaleFactor = 1.0;
m_graphView->show();

actionSaveAs->setEnabled(true);
actionFitToWindow->setEnabled(true);
updateDisplayActions();

if (!actionFitToWindow->isChecked())
	{
	m_graphView->adjustSize();
	}
}

void MecGraphDialog::graphFailed()
{
actionSaveAs->setEnabled(false);
actionZoomIn->setEnabled(false);
actionZoomOut->setEnabled(false);
actionNormalSize->setEnabled(false);
actionFitToWindow->setEnabled(false);
m_graphView->setText("<center>" + tr("An error has occured while the creation of the graph.") + "</center>");
m_graphView->show();
}

void MecGraphDialog::zoomIn()
{
	if (actionZoomIn->isEnabled()) scaleGraph(1.25);
}

void MecGraphDialog::zoomOut()
{
	if (actionZoomOut->isEnabled()) scaleGraph(0.8);
}

void MecGraphDialog::normalSize()
{
	scaleGraph(1.0/m_scaleFactor);
}

void MecGraphDialog::fitToWindow()
{
	bool fitToWindow = actionFitToWindow->isChecked();
	scrollAreaGraph->setWidgetResizable(fitToWindow);
	if (!fitToWindow) {
		normalSize();
	} else {
		m_graphView->setPixmap(m_graph->graph().scaled(scrollAreaGraph->maximumViewportSize(), Qt::KeepAspectRatio, Qt::SmoothTransformation));
		m_graphView->resize(m_graphView->pixmap()->size());
	}
	updateDisplayActions();
}

void MecGraphDialog::assignGraphOptions()
{
	MecGraph::Options tempOpt = MecGraph::None;
	
	if (actionShowConnections->isChecked()) tempOpt = tempOpt | MecGraph::ShowConnections;
	if (actionColorConnections->isChecked()) tempOpt = tempOpt | MecGraph::ColorConnections;
	if (actionHideVariables->isChecked()) tempOpt = tempOpt | MecGraph::HideVariables;
	if (actionNoBlockStructure->isChecked()) tempOpt = tempOpt | MecGraph::NoBlockStructure;
	
	m_graph->setOptions(tempOpt);
}

void MecGraphDialog::updateModeActions(MecGraph::Mode Mode)
{
	if (Mode == MecGraph::Hierarchy) {
		actionHierarchy->setChecked(true);
		actionPresentation->setChecked(false);
	}
	else if (Mode == MecGraph::Presentation) {
		actionHierarchy->setChecked(false);
		actionPresentation->setChecked(true);
	}
}


void MecGraphDialog::updateOptionsActions(MecGraph::Options Options)
{
	if ((Options & MecGraph::ShowConnections) == MecGraph::ShowConnections) actionShowConnections->setChecked(true);
	else actionShowConnections->setChecked(false);
	if ((Options & MecGraph::ColorConnections) == MecGraph::ColorConnections) actionColorConnections->setChecked(true);
	else actionColorConnections->setChecked(false);
	if ((Options & MecGraph::HideVariables) == MecGraph::HideVariables) actionHideVariables->setChecked(true);
	else actionHideVariables->setChecked(false);
	if ((Options & MecGraph::NoBlockStructure) == MecGraph::NoBlockStructure) actionNoBlockStructure->setChecked(true);
	else actionNoBlockStructure->setChecked(false);
	
}

void MecGraphDialog::showContextMenu(QPoint pos)
{
	QMenu tempMenu(this);
	tempMenu.addAction(actionZoomIn);
	tempMenu.addAction(actionZoomOut);
	tempMenu.addAction(actionNormalSize);
	tempMenu.addSeparator();
	tempMenu.addAction(actionFitToWindow);
	tempMenu.addSeparator();
	tempMenu.addAction(actionRefreshGraph);
	tempMenu.addSeparator();
	tempMenu.addAction(actionSaveAs);
	tempMenu.exec(m_graphView->mapToGlobal(pos));
}



