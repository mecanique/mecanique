/*
© Quentin VIGNAUD, 2014

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#include "MecSyntaxHighlighter.h"

MecSyntaxHighlighter::Rule::Rule() : expression (QString()), format (QTextCharFormat())
{
}

MecSyntaxHighlighter::Rule::Rule(QString Expression, QTextCharFormat Format) : expression (Expression), format (Format)
{
}

MecSyntaxHighlighter::TextBlockData::TextBlockData()
{
}

QVector<MecSyntaxHighlighter::ParenthesisInfo*> MecSyntaxHighlighter::TextBlockData::parentheses()
{
    return m_parentheses;
}


void MecSyntaxHighlighter::TextBlockData::insert(MecSyntaxHighlighter::ParenthesisInfo *Info)
{
    int i = 0;
    while (i < m_parentheses.size() &&
        Info->position > m_parentheses.at(i)->position)
        ++i;

    m_parentheses.insert(i, Info);
}

MecSyntaxHighlighter::MecSyntaxHighlighter(MecAbstractFunction* const Function, MecAbstractEditor* const MainEditor, QTextDocument *Parent) : QSyntaxHighlighter(Parent), m_function (Function), m_mainEditor (MainEditor)
{

}

QVector<MecSyntaxHighlighter::Rule> MecSyntaxHighlighter::rules() const
{
	//Keywords
	MecSyntaxHighlighter::Rule Keyword;
	Keyword.expression = "(\\b" + keywords().join("\\b|\\b") + "\\b)";
	Keyword.format.setFontWeight(QFont::Bold);
	Keyword.format.setForeground(QColor(Qt::darkRed));
	
	//Forbidden keywords
	MecSyntaxHighlighter::Rule ForbiddenKeyword;
		QStringList ListForbiddenKeywords(forbiddenKeywords());
		ListForbiddenKeywords << mainEditor()->baseElements(MecAbstractElement::Project) << mainEditor()->baseElements(MecAbstractElement::Object);
	ForbiddenKeyword.expression = "(\\b" + ListForbiddenKeywords.join("\\b|\\b") + "\\b|\\bQ[^ ]*\\b|\\bmec_[^ ]*\\b)";
	ForbiddenKeyword.format.setFontStrikeOut(true);
	ForbiddenKeyword.format.setForeground(QColor(Qt::red));
	
	//Variables types
	MecSyntaxHighlighter::Rule VariableType;
		QStringList ListVariableTypes(mainEditor()->baseElements(MecAbstractElement::Variable));
		ListVariableTypes << "const" << "static";
	VariableType.expression = "(\\b" + ListVariableTypes.join("\\b|\\b") + "\\b)";
	VariableType.format.setFontWeight(QFont::Bold);
	VariableType.format.setForeground(QColor(Qt::darkGreen));
	
	//General functions
	MecSyntaxHighlighter::Rule GeneralFunction;
		QStringList ListGeneralFunctions(generalFunctions());
	GeneralFunction.expression = "(\\b" + ListGeneralFunctions.join("\\b|\\b") + "\\b)";
	GeneralFunction.format.setForeground(QColor(Qt::darkGreen));
	
	//Parameters
	MecSyntaxHighlighter::Rule Parameter;
		QStringList ListParameters;
		for (int i=0 ; i < function()->childVariables().size() ; i++) ListParameters += function()->childVariables().at(i)->elementName();
	Parameter.expression = "(\\b" + ListParameters.join("\\b|\\b") + "\\b)";
	Parameter.format.setForeground(QColor(Qt::darkMagenta));
	
	//Project/Objects
	MecSyntaxHighlighter::Rule Object;
		QList<MecAbstractElement*> ElementsObjects;
		ElementsObjects.append(mainEditor()->element());
		for (int i=0 ; i < ElementsObjects.size() ; i++)
			{
			for (int j=0 ; j < ElementsObjects.at(i)->childElements().size() ; j++)
				{
				if (ElementsObjects.at(i)->childElements().at(j)->elementRole() == MecAbstractElement::Object)
					ElementsObjects.append(ElementsObjects.at(i)->childElements().at(j));
				}
			}
		QStringList ListObjects;
		for (int i=0 ; i < ElementsObjects.size() ; i++) ListObjects += ElementsObjects.at(i)->elementName();
		ListObjects += "this";
	Object.expression = "(\\b" + ListObjects.join("\\b|\\b") + "\\b)";
	Object.format.setForeground(QColor(Qt::darkBlue));
	Object.format.setFontWeight(QFont::Bold);
	
	//Signals
	MecSyntaxHighlighter::Rule Signal;
		QStringList ListSignals;
		for (int i=0 ; i < function()->parentElement()->childElements().size() ; i++)
			{
			if (function()->parentElement()->childElements().at(i)->elementRole() == MecAbstractElement::Signal)
				ListSignals += function()->parentElement()->childElements().at(i)->elementName();
			}
	Signal.expression = "(\\bemit\\s+" + ListSignals.join("\\b|\\bemit\\s+") + "\\b)";
	Signal.format.setForeground(QColor(Qt::darkBlue));
	
	//Functions and variables
	MecSyntaxHighlighter::Rule OtherElement;
		QList<MecAbstractElement*> Elements;
		Elements.append(mainEditor()->element());
		for (int i=0 ; i < Elements.size() ; i++) Elements.append(Elements.at(i)->childElements());
		QStringList ListElements;
		for (int i=0 ; i < Elements.size() ; i++)
			{
			if (Elements.at(i)->parentElement() == function()->parentElement() and (
					Elements.at(i)->elementRole() == MecAbstractElement::Function or
					Elements.at(i)->elementRole() == MecAbstractElement::Variable
					))
						ListElements.append(Elements.at(i)->elementName());
			else if (Elements.at(i)->parentElement() != 0 and (
					Elements.at(i)->elementRole() == MecAbstractElement::Function or
					Elements.at(i)->elementRole() == MecAbstractElement::Variable
					)) ListElements.append(Elements.at(i)->parentElement()->elementName() + "\\s*->\\s*" + Elements.at(i)->elementName());
			}
	OtherElement.expression = "(\\b" + ListElements.join("\\b|\\b") + "\\b)";
	OtherElement.format.setForeground(QColor(Qt::darkBlue));
	
	//Numbers, characters and strings
	MecSyntaxHighlighter::Rule LiteralValue;
	LiteralValue.expression = "(\\b[0-9]+\\b|\\b0[xX][0-9a-fA-F]+\\b|'.'|\".*\")";
	LiteralValue.format.setForeground(QColor("#C4A000"));
	
	//Escaped character
	MecSyntaxHighlighter::Rule EscapedChar;
	EscapedChar.expression = "\\\\.";
	EscapedChar.format.setForeground(QColor("#A18F3F"));
	
	//Single-line comments
	MecSyntaxHighlighter::Rule SingleLineComment;
	SingleLineComment.expression = "//[^\n]*";
	SingleLineComment.format.setForeground(QColor(Qt::blue));
	
	QVector<MecSyntaxHighlighter::Rule> tempRules;
	tempRules.append(LiteralValue);
	tempRules.append(EscapedChar);
	tempRules.append(OtherElement);
	tempRules.append(Signal);
	tempRules.append(Object);
	tempRules.append(Parameter);
	tempRules.append(GeneralFunction);
	tempRules.append(VariableType);
	tempRules.append(Keyword);
	tempRules.append(ForbiddenKeyword);
	tempRules.append(SingleLineComment);
	
	//On se prémunit des regexp validant du vide (cela entraîne des boucles infinies).
	for (int i=0 ; i < tempRules.size() ; i++)
		{
		if (tempRules.at(i).expression == "(\\b\\b)") tempRules.remove(i);
		}
	
	return tempRules;
}

MecAbstractFunction* MecSyntaxHighlighter::function() const
{
	return m_function;
}

MecAbstractEditor* MecSyntaxHighlighter::mainEditor() const
{
	return m_mainEditor;
}

QStringList MecSyntaxHighlighter::generalFunctions()
{
	return QStringList()
		<< "cos"
		<< "sin"
		<< "tan"
		<< "acos"
		<< "asin"
		<< "atan"
		<< "pow"
		<< "sqrt"
		<< "round"
		<< "trunc"
		<< "ceil"
		<< "floor"
		<< "abs"
		<< "min"
		<< "max"
		<< "exp"
		<< "log"
		<< "ln"
		<< "rand"
		;
}
	
QStringList MecSyntaxHighlighter::keywords()
{
	return QStringList()
		<< "if"
		<< "else"
		<< "switch"
		<< "case"
		<< "while"
		<< "do"
		<< "for"
		<< "break"
		<< "continue"
		<< "return"
		<< "emit"
		<< "true"
		<< "false"
		<< "or"
		<< "and"
		<< "null"
		;
}

QStringList MecSyntaxHighlighter::forbiddenKeywords()
{
	return QStringList()
		<< "catch"
		<< "class"
		<< "delete"
		<< "enum"
		<< "explicit"
		<< "friend"
		<< "inline"
		<< "namespace"
		<< "new"
		<< "operator"
		<< "private"
		<< "protected"
		<< "public"
		<< "signal"
		<< "signals"
		<< "slot"
		<< "slots"
		<< "signed"
		<< "struct"
		<< "try"
		<< "typedef"
		<< "typename"
		<< "union"
		<< "virtual"
		<< "volatile"
		;
}

void MecSyntaxHighlighter::highlightBlock(const QString &Text)
{
	//Traitement des règles de coloration "classiques".
	QVector<MecSyntaxHighlighter::Rule> Rules(rules());
	for (int i=0 ; i < Rules.size() ; i++)
		{
		QRegExp Expression(Rules.at(i).expression);
		int Index = Expression.indexIn(Text);
		while (Index >= 0)
			{
			int Length = Expression.matchedLength();
			setFormat(Index, Length, Rules.at(i).format);
			Index = Expression.indexIn(Text, Index + Length);
			}
		}
	setCurrentBlockState(0);
	
	
	//Traitement des commentaires multi-lignes
	QRegExp CommentStartExpression("[^/]/\\*");
	QRegExp CommentEndExpression("\\*/");
	QTextCharFormat MultiLineCommentFormat;
		MultiLineCommentFormat.setForeground(QColor(Qt::blue));
	
	
	int StartIndex = 0;
	if (previousBlockState() != 1)
		StartIndex = CommentStartExpression.indexIn(Text);

	while (StartIndex >= 0) {
		int EndIndex = CommentEndExpression.indexIn(Text, StartIndex);
		int CommentLength;
		if (EndIndex == -1) {
			setCurrentBlockState(1);
			CommentLength = Text.length() - StartIndex;
		} else {
			CommentLength = EndIndex - StartIndex
				+ CommentEndExpression.matchedLength();
		}
		setFormat(StartIndex, CommentLength, MultiLineCommentFormat);
		StartIndex = CommentStartExpression.indexIn(Text, StartIndex + CommentLength);
	}
	
	//Traitement des parenthèses
	MecSyntaxHighlighter::TextBlockData *Data = new MecSyntaxHighlighter::TextBlockData();

    int LeftPos = Text.indexOf('(');
    while (LeftPos != -1) {
        MecSyntaxHighlighter::ParenthesisInfo *Info = new MecSyntaxHighlighter::ParenthesisInfo();
        Info->character = '(';
        Info->position = LeftPos;

        Data->insert(Info);
        LeftPos = Text.indexOf('(', LeftPos + 1);
    }
    

    int RightPos = Text.indexOf(')');
    while (RightPos != -1) {
        MecSyntaxHighlighter::ParenthesisInfo *Info = new MecSyntaxHighlighter::ParenthesisInfo();
        Info->character = ')';
        Info->position = RightPos;

        Data->insert(Info);
        RightPos = Text.indexOf(')', RightPos + 1);
    }
    
    //Traitement des accolades
	//MecSyntaxHighlighter::TextBlockData *Data = new MecSyntaxHighlighter::TextBlockData();

    LeftPos = Text.indexOf('{');
    while (LeftPos != -1) {
        MecSyntaxHighlighter::ParenthesisInfo *Info = new MecSyntaxHighlighter::ParenthesisInfo();
        Info->character = '{';
        Info->position = LeftPos;

        Data->insert(Info);
        LeftPos = Text.indexOf('{', LeftPos + 1);
    }
    

    RightPos = Text.indexOf('}');
    while (RightPos != -1) {
        MecSyntaxHighlighter::ParenthesisInfo *Info = new MecSyntaxHighlighter::ParenthesisInfo();
        Info->character = '}';
        Info->position = RightPos;

        Data->insert(Info);
        RightPos = Text.indexOf('}', RightPos + 1);
    }
    

    setCurrentBlockUserData(Data);
}

