/*
© Quentin VIGNAUD, 2014

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#include "MecGraph.h"

MecGraph::MecGraph(MecAbstractElement* const RootElement, QObject* Parent) : QObject(Parent), m_rootElement (RootElement), m_mode (MecGraph::Hierarchy), m_options (MecGraph::None)
{
m_graphProcess = 0;
}

MecGraph::~MecGraph()
{
}
	
MecAbstractElement* MecGraph::rootElement() const
{
	return m_rootElement;
}

void MecGraph::setRootElement(MecAbstractElement* const RootElement)
{
	m_rootElement = RootElement;
	emit rootElementChanged(m_rootElement);
}

MecGraph::Mode MecGraph::getMode() const
{
	return m_mode;
}

void MecGraph::setMode(MecGraph::Mode Mode)
{
	m_mode = Mode;
	emit modeChanged(Mode);
}

MecGraph::Options MecGraph::getOptions() const
{
	return m_options;
}

void MecGraph::setOptions(MecGraph::Options Options)
{
	m_options = Options;
	emit optionsChanged(Options);
}
	
QPixmap MecGraph::graph() const
{
return m_graph;
}

QString MecGraph::graphScript()
{
if (m_rootElement == 0) return QString();

QString tempString;
	
tempString += "digraph g {\n\
	graph [\n\
		bgcolor = transparent\n\
	]\n\
	node [\n\
		fontsize = \"10\"\n\
		shape = box\n\
		style = \"filled\"\n\
		height = 0.0\n\
	]\n";

	//Liste de tous les éléments.
	QList<MecAbstractElement*> tempList;
	tempList.append(m_rootElement);
	for (int i=0 ; i < tempList.size() ; i++)
	{
		tempList.append(tempList.at(i)->childElements());
	}
	//Si on ne veut pas avoir les variables.
	if ((m_options & MecGraph::HideVariables) == MecGraph::HideVariables)
	{
		for (int i=0 ; i < tempList.size() ; i++)
		{
			if (tempList.at(i)->elementRole() == MecAbstractElement::Variable)
			{
				tempList.removeAt(i);
				i--;
			}
		}
	}
	
	//On ajoute le nœud de chacun.
	for (int i=0 ; i < tempList.size() ; i++)
	{
	tempString += nodeElement(tempList.at(i)) + "\n";
	}
	
	//On ajoute le sous-graphe de chacun si demandé.
	if ((m_options & MecGraph::NoBlockStructure) != MecGraph::NoBlockStructure) tempString += subGraphElement(m_rootElement, m_options);
	
	//On connecte les nœuds parents (cas de respect de la hiérarchie).
	for (int i=0 ; m_mode == MecGraph::Hierarchy && i < tempList.size() ; i++)
	{
		for (int j=0 ; j < tempList.at(i)->childElements().size() ; j++)
		{
			if ((m_options & MecGraph::HideVariables) == MecGraph::HideVariables)
			{
				if (tempList.at(i)->childElements().at(j)->elementRole() != MecAbstractElement::Variable)
				{
					tempString += "\t" + integralNameElement(tempList.at(i)) + " -> " + integralNameElement(tempList.at(i)->childElements().at(j)) + " [dir = \"none\"]\n";
				}
			}
			else tempString += "\t" + integralNameElement(tempList.at(i)) + " -> " + integralNameElement(tempList.at(i)->childElements().at(j)) + " [dir = \"none\"]\n";
		}
	}
	//On connecte les nœuds pour affichage "présentation".
	for (int i=0 ; m_mode == MecGraph::Presentation && i < tempList.size() ; i++)
	{
		for (int j=0 ; j < tempList.at(i)->childElements().size() ; j++)
		{
			if ((m_options & MecGraph::HideVariables) == MecGraph::HideVariables)
			{
				if (tempList.at(i)->childElements().at(j)->elementRole() != MecAbstractElement::Variable)
				{
					if (j == 0 || tempList.at(i)->childElements().at(j)->elementRole() == MecAbstractElement::Object) tempString += "\t" + integralNameElement(tempList.at(i)) + " -> " + integralNameElement(tempList.at(i)->childElements().at(j)) + " [dir = \"none\" color=\"transparent\"]\n";
					else
					{
						//On s'assure que l'élément précédent n'est pas une variable.
						int tempDecrement = 0;
						MecAbstractElement *precedingElement = 0;
						do
						{
							tempDecrement--;
							precedingElement = tempList.at(i)->childElements().at(j + tempDecrement);
						} while (precedingElement->elementRole() == MecAbstractElement::Variable);
						tempString += "\t" + integralNameElement(precedingElement) + " -> " + integralNameElement(tempList.at(i)->childElements().at(j)) + " [dir = \"none\" color=\"transparent\"]\n";
					}
				}
			}
			else
			{
				if (j == 0 || tempList.at(i)->childElements().at(j)->elementRole() == MecAbstractElement::Object) tempString += "\t" + integralNameElement(tempList.at(i)) + " -> " + integralNameElement(tempList.at(i)->childElements().at(j)) + " [dir = \"none\" color=\"transparent\"]\n";
				else
				{
					if (tempList.at(i)->childElements().at(j-1)->childElements().size() > 0)
					{
						tempString += "\t" + integralNameElement(tempList.at(i)->childElements().at(j-1)->childElements().at(tempList.at(i)->childElements().at(j-1)->childElements().size()-1)) + " -> " + integralNameElement(tempList.at(i)->childElements().at(j)) + " [dir = \"none\" color=\"transparent\"]\n";
					}
					else
					{
						tempString += "\t" + integralNameElement(tempList.at(i)->childElements().at(j-1)) + " -> " + integralNameElement(tempList.at(i)->childElements().at(j)) + " [dir = \"none\" color=\"transparent\"]\n";
					}
				}
			}
		}
	}
	
	if ((m_options & MecGraph::ShowConnections) == MecGraph::ShowConnections)
	{
		QStringList tempColors;
		tempColors	<< "red"
					<< "darkred"
					<< "green"
					<< "darkgreen"
					<< "blue"
					<< "darkblue"
					<< "cyan"
					<< "darkcyan"
					<< "magenta"
					<< "darkmagenta"
					<< "yellow"
					<< "darkyellow"
					<< "darkgray"
					<< "lightgray";
		int indexColors = 0;
		
		//On connecte les signaux aux fonctions.
		for (int i=0 ; i < tempList.size() ; i++)
		{
			if (tempList.at(i)->elementRole() != MecAbstractElement::Signal) continue;
			MecAbstractSignal *tempSignal = static_cast<MecAbstractSignal*>(tempList.at(i));
			for (int j=0 ; j < tempSignal->connectedFunctions().size() ; j++)
			{
				//Si la fonction pointée ne fait pas partie du graphe.
				if (!tempList.contains(tempSignal->connectedFunctions().at(j))) continue;
			
				if ((m_options & MecGraph::ColorConnections) == MecGraph::ColorConnections)
				{
					tempString += "\t" + integralNameElement(tempList.at(i)) + " -> " + integralNameElement(tempSignal->connectedFunctions().at(j)) + " [ style=dashed color=\"" + QColor(tempColors.at(indexColors)).name() + "\"]\n";
					
					indexColors++;
					if (indexColors >= tempColors.size()) indexColors = 0;
				}
				else tempString += "\t" + integralNameElement(tempList.at(i)) + " -> " + integralNameElement(tempSignal->connectedFunctions().at(j)) + " [ style=dashed ]\n";
			}
		}
	}

tempString += "}\n";

return tempString;
}

void MecGraph::setHierarchyMode()
{
	setMode(MecGraph::Hierarchy);
}

void MecGraph::setPresentationMode()
{
	setMode(MecGraph::Presentation);
}
	
void MecGraph::start()
{
if (m_rootElement == 0 or m_graphProcess != 0) return;
else
	{
	QString tempFileName(QDir::tempPath() + "/mecagraph" + QString::number(QCoreApplication::applicationPid()) + m_rootElement->elementName() + ".dot");
	QFile tempFile(tempFileName, this);
	if (tempFile.open(QIODevice::WriteOnly | QIODevice::Truncate))
		{
		tempFile.write(graphScript().toUtf8());
		tempFile.close();
		
		m_graphProcess = new QProcess(this);
			connect(m_graphProcess, SIGNAL(error(QProcess::ProcessError)), SLOT(graphProcessError(QProcess::ProcessError)));
			connect(m_graphProcess, SIGNAL(finished(int, QProcess::ExitStatus)), SLOT(graphProcessFinished(int, QProcess::ExitStatus)));
		
		m_graphProcess->start("dot", QStringList() << "-Tpng" << tempFileName);
		}
	else
		{
		emit failed();
		return;
		}
	}
}

QString MecGraph::integralNameElement(MecAbstractElement* const Element)
{
QString tempString(Element->elementName());
MecAbstractElement *tempParent = Element->parentElement();
while (tempParent != 0)
	{
	tempString = tempParent->elementName() + "_" + tempString;
	tempParent = tempParent->parentElement();
	}
return tempString;
}
	
QString MecGraph::nodeElement(MecAbstractElement* const Element)
{
QString tempString;
tempString += "\t" + integralNameElement(Element) + " [\n\t\tlabel = < <TABLE BORDER=\"0\" CELLBORDER=\"0\" CELLSPACING=\"0\"><TR><TD><IMG SRC=\"" + QDir::tempPath() + "/" + Element->elementType() + ".png\" SCALE=\"true\"/></TD><TD>" + Element->elementType() + " " + Element->elementName() + "</TD></TR></TABLE> >\n\t\tcolor = ";
switch (Element->elementRole())
	{
	case MecAbstractElement::Project :
		tempString += "darkorange1";
		break;
	case MecAbstractElement::Object :
		tempString += "blue1";
		break;
	case MecAbstractElement::Function :
		tempString += "green1";
		break;
	case MecAbstractElement::Signal :
		tempString += "red1";
		break;
	case MecAbstractElement::Variable :
		tempString += "yellow1";
		break;
	}
tempString += "\n\t]\n";

QIcon(":/share/icons/types/" + Element->elementType() + ".png").pixmap(24, 24).save(QDir::tempPath() + "/" + Element->elementType() + ".png", "PNG", 100);

return tempString;
}
	
QString MecGraph::subGraphElement(MecAbstractElement* const Element, MecGraph::Options Options)
{
QString tempString;
tempString += "\tsubgraph cluster_" + integralNameElement(Element) + " {\n\t\tshape = none\n";
tempString += "\t\t" + integralNameElement(Element) + "\n";
for (int i=0 ; i < Element->childElements().size() ; i++)
	{
	if ((Options & MecGraph::HideVariables) == MecGraph::HideVariables)
		{
			if (Element->childElements().at(i)->elementRole() != MecAbstractElement::Variable)
			{
			tempString += "\t\t" + subGraphElement(Element->childElements().at(i), Options) + "\n";
			}
		}
	else tempString += "\t\t" + subGraphElement(Element->childElements().at(i), Options) + "\n";
	}
tempString += "\t}\n";
return tempString;
}

void MecGraph::graphProcessError(QProcess::ProcessError Error)
{
	std::cout << "Graph generation error: " << Error << std::endl;
	emit failed();
	delete m_graphProcess;
	m_graphProcess = 0;
}

void MecGraph::graphProcessFinished(int ExitCode, QProcess::ExitStatus ExitStatus)
{
	std::cout << "Graph generated." << std::endl;
	if (ExitStatus == QProcess::NormalExit)
	{
		QPixmap tempPixmap;
		if (tempPixmap.loadFromData(m_graphProcess->readAllStandardOutput(), "PNG"))
		{
			std::cout << "Load pixmap checked." << std::endl;
			m_graph = tempPixmap;
			emit finished();
		}
		else
		{
			std::cout << "Load pixmap failed." << std::endl;
			emit failed();
		}
	}
	else
	{
	emit failed();
	}
	
	delete m_graphProcess;
	m_graphProcess = 0;
}


