/*
© Quentin VIGNAUD, 2013

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#ifndef __MECFUNCTION_H__
#define __MECFUNCTION_H__

#include <QtCore>
#include <MecAbstractFunction>
#include "MecVariable.h"

/**
\brief	Classe de représentation d'une fonction.
*/
class MecFunction : public MecAbstractFunction
{
	Q_OBJECT

public:
	/**
	\brief	Constructeur.
	\param	Name	Nom de la fonction.
	\param	Type	Type de retour.
	*/
	MecFunction(QString Name, QString Type, MecAbstractElement *ParentElement=0);
	///Destructeur.
	~MecFunction();

	///Retourne le nom de l'élément.
	QString elementName() const;

	///Retourne le type de l'élément.
	QString elementType() const;

	///Retourne le rôle de l'élément.
	MecAbstractElement::ElementRole elementRole() const;

	///Retourne l'élément parent.
	MecAbstractElement* parentElement() const;
	/**
	\brief	Assigne le parent.

	Une MecFunction ne peut avoir qu'un MecObject ou MecProject comme parent, la fonction ne fait rien si ce critère n'est pas satisfait.
	\return	\e true si le critère est rempli, sinon \e false.
	*/
	bool setParentElement(MecAbstractElement *ParentElement);

	///Code de la fonction.
	QString code() const;

	///Retourne les éléments enfants.
	QList<MecAbstractElement*> childElements() const;
	///Retourne les variables enfants.
	QList<MecAbstractVariable*> childVariables() const;
	/**
	\brief	Retourne l'élément enfant situé à l'adresse indiquée.

	\param	Address	Adresse de l'élément demandé.
	\note	L'élément actuel doît être la racine de l'adresse, sinon la recherche échouera.

	\return	L'élément situé à l'adresse indiquée, ou 0 en cas d'échec.
	*/
	MecAbstractElement* childElement(QString Address) const;

	/**
	\brief	Retourne l'adresse de l'élément.

	\param	RootElement	Élément à considérer comme la racine de l'adresse.

	L'adresse d'un élément est de la forme « nameroot.nameparentelement.nameelement » ; la fonction retourne une adresse en prenant comme racine RootElement, ou le plus grand parent connu si RootElement ne fait pas partie de l'arbre d'appartenance.
	*/
	QString address(MecAbstractElement *RootElement=0) const;
	
	///Retourne la liste des signaux connectés.
	QList<MecAbstractSignal*> connectedSignals() const;
	/**
	\brief	Ajoute une connexion avec le signal \e Signal.
	
	Si \e Signal est déjà connecté à la fonction, la fonction ne fait rien.
	Si un signal venait à être modifiée de manière incompatible avec la fonction, celui-ci serait alors retiré de la liste des signaux connectés.
	\note	Le signal ne devient pas enfant de la fonction.
	\see	isConnectableSignal(MecAbstractSignal*)
	\return	\e true si la connexion est possible, sinon \e false.
	*/
	bool connectSignal(MecAbstractSignal* Signal);
	/**
	\brief	Supprime \e Signal de la liste des signaux connectés.
	*/
	void disconnectSignal(MecAbstractSignal* Signal);
	/**
	\brief	Indique si \e Signal est connectable avec cette fonction.
	
	\note	\e Signal n'est pas connecté par cette fonction.
	\see	MecAbstractSignal::isConnectableFunction(MecAbstractFunction*)
	*/
	bool isConnectableSignal(MecAbstractSignal* Signal);

public slots:
	/**
	\brief	Fixe un nouveau nom pour l'élément.

	Si \e Name n'est pas respectueux de la syntaxe standarde ou n'est pas disponible à ce niveau, \e false est retourné et l'ancien nom est conservé.
	*/
	bool setElementName(QString Name);

	/**
	\brief	Fixe un nouveau type pour l'élément.

	Si \e Type n'est pas respectueux de la syntaxe standarde, \e false est retourné et l'ancien type est conservé.
	*/
	bool setElementType(QString Type);

	///Fixe le code de la fonction.
	void setCode(QString Code);
	
	///Provoque l'émission du signal "connectedSignalsChanged".
	void emitConnectedSignalsChanged();

private:
	///Nom de l'élément.
	QString m_elementName;
	///Type de l'élément.
	QString m_elementType;
	///Rôle de l'élément.
	const MecAbstractElement::ElementRole m_elementRole;

	///Code de la fonction.
	QString m_code;

	///Élément parent.
	MecAbstractElement *m_parentElement;
	///Élements enfants.
	QList<MecAbstractElement*> m_childElements;


private slots:
	/**
	\brief	Effectue les traitements nécessaires pour le changement de parent.
	*/
	bool changeParentElement(MecAbstractElement *ParentElement);
	///Retourne un pointeur sur la liste des enfants.
	QList<MecAbstractElement*>* childList();
	///Provoque l'émission du signal childListChanged(MecAbstractElement*).
	void emitChildListChanged();


};

/**
\brief	Compare toutes les propriétés ainsi que celles des éléments enfants, a l'exception de l'élément parent.
*/
bool operator==(MecFunction const &a, MecFunction const &b);
///\see	operator==(MecFunction const &a, MecFunction const &b)
bool operator!=(MecFunction const &a, MecFunction const &b);

#endif /* __MECFUNCTION_H__ */

