/*
© Quentin VIGNAUD, 2013

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#include "MecSignal.h"

MecSignal::MecSignal(QString Name, MecAbstractElement *ParentElement) : m_elementRole (MecAbstractElement::Signal), m_parentElement (0)
{
setElementType("void");
setParentElement(ParentElement);
setElementName(Name);
connect(this, SIGNAL(childListChanged(MecAbstractElement*)), SLOT(childVariablesChanged()), Qt::DirectConnection);
}

MecSignal::~MecSignal()
{
emit destroyed(this);
while (!m_childElements.isEmpty()) delete m_childElements.at(0);//La suppression du sous-élément le fait se retirer lui-même de la présente liste.
setParentElement(0);//Retire l'élément présent de la liste des enfants de son parent.
}

QString MecSignal::elementName() const
{
return m_elementName;
}
	
QString MecSignal::elementType() const
{
return m_elementType;
}
	
MecAbstractElement::ElementRole MecSignal::elementRole() const
{
return m_elementRole;
}
	
MecAbstractElement* MecSignal::parentElement() const
{
return m_parentElement;
}

bool MecSignal::setParentElement(MecAbstractElement *ParentElement)
{
if (ParentElement == 0)
	{
	changeParentElement(ParentElement);
	return true;
	}
else
	{
	if (ParentElement->elementRole() == MecAbstractElement::Project or ParentElement->elementRole() == MecAbstractElement::Object)
		{
		changeParentElement(ParentElement);
		return true;
		}
	else return false;
	}
}

QList<MecAbstractElement*> MecSignal::childElements() const
{
return m_childElements;
}
	
QList<MecAbstractVariable*> MecSignal::childVariables() const
{
QList<MecAbstractVariable*> tempList;
for (int i=0 ; i < childElements().size() ; i++)
	{
	if (childElements().at(i)->elementRole() == MecAbstractElement::Variable)
		{
		tempList.append(static_cast<MecAbstractVariable*>(childElements().at(i)));
		}
	}
return tempList;
}

MecAbstractElement* MecSignal::childElement(QString Address) const
{
	QStringList tempNames = Address.split('.');
	if (tempNames.size() >= 2 and tempNames.at(0) == elementName())
	{
		for (int i=0 ; i < childElements().size() ; i++)
		{
			if (childElements().at(i)->elementName() == tempNames.at(1) and tempNames.size() != 2)
			{
				tempNames.removeFirst();
				return childElements().at(i)->childElement(tempNames.join('.'));
			}
			else if (childElements().at(i)->elementName() == tempNames.at(1) and tempNames.size() == 2)
			{
				return childElements().at(i);
			}
		}
	}
	
	return 0;
}

QString MecSignal::address(MecAbstractElement *RootElement) const
{
	QString tempAddress = elementName();
	const MecAbstractElement *tempParentElement = parentElement();
	while (tempParentElement != 0)
		{
		tempAddress.prepend(tempParentElement->elementName() + '.');
		if (tempParentElement == RootElement) break;
		tempParentElement = tempParentElement->parentElement();
		}
	return tempAddress;
}
	
QList<MecAbstractFunction*> MecSignal::connectedFunctions() const
{
return m_connectedFunctions;
}

bool MecSignal::connectFunction(MecAbstractFunction* Function)
{
if (Function == 0) return false;
if (m_connectedFunctions.contains(Function)) return true;
else if (isConnectableFunction(Function))
	{
	connect(Function, SIGNAL(childListChanged(MecAbstractElement*)), this, SLOT(functionModified(MecAbstractElement*)), Qt::DirectConnection);
	connect(Function, SIGNAL(destroyed(QObject*)), this, SLOT(functionDestroyed(QObject*)), Qt::DirectConnection);
	m_connectedFunctions.append(Function);
	emit connectedFunctionsChanged(this);
	static_cast<MecFunction*>(Function)->emitConnectedSignalsChanged();
	return true;
	}
else return false;
}

void MecSignal::disconnectFunction(MecAbstractFunction* Function)
{
disconnect(Function, SIGNAL(childListChanged(MecAbstractElement*)), this, SLOT(functionModified(MecAbstractElement*)));
disconnect(Function, SIGNAL(destroyed(QObject*)), this, SLOT(functionDestroyed(QObject*)));
m_connectedFunctions.removeAll(Function);
emit connectedFunctionsChanged(this);
static_cast<MecFunction*>(Function)->emitConnectedSignalsChanged();
}

bool MecSignal::isConnectableFunction(MecAbstractFunction* Function)
{
if (Function == 0) return false;
bool Ok = (childVariables().size() >= Function->childVariables().size());//Vérification du nombre de variables.
for (int i=0 ; i < Function->childVariables().size() and Ok ; i++)
	{
	Ok = (Function->childVariables().at(i)->elementType() == childVariables().at(i)->elementType());//Vérification des types des variables communes.
	}
return Ok;
}

bool MecSignal::setElementName(QString Name)
{
bool Ok = standardSyntax().exactMatch(Name) and Name != "settings" and Name != "setSettings" and Name != "more";//Vérification de la syntaxe standarde et des exceptions.
if (Ok and m_parentElement != 0)//Vérification de la disponibilité du nom.
	{
	QStringList NamesUsed;
	for (int i=0 ; i < m_parentElement->childElements().size() ; i++)//On prend les noms des éléments frères.
		{
		if (m_parentElement->childElements().at(i) != this) NamesUsed.append(m_parentElement->childElements().at(i)->elementName());
		}
	for (int i=0 ; m_parentElement->parentElement() != 0 and i < m_parentElement->parentElement()->childElements().size() ; i++)//On prend les noms des éléments oncles.
		{
		NamesUsed.append(m_parentElement->parentElement()->childElements().at(i)->elementName());
		}
	
	Ok = !(NamesUsed.contains(Name));
	}

if (Ok)
	{
	m_elementName = Name;
	emit nameChanged(this);
	return true;
	}
else return false;
}

bool MecSignal::setElementType(QString Type)
{
m_elementType = QString("void");
return (Type == "void");
}

bool MecSignal::changeParentElement(MecAbstractElement *ParentElement)
{
if (ParentElement != this)
	{
	if (m_parentElement != 0)
		{
		m_parentElement->childList()->removeAll(this);
		m_parentElement->emitChildListChanged();
		}
	
	m_parentElement = ParentElement;
	if (ParentElement != 0)
		{
		ParentElement->childList()->append(this);
		ParentElement->emitChildListChanged();
		}
	
	blockSignals(true);//On bloque les signaux pour éviter une émission inutile.
	if (!setElementName(m_elementName))//Vérification de la disponibilité du nom dans le nouveau parent.
		{
		blockSignals(false);
		for (QString tempName=m_elementName.prepend("_") ; !setElementName(tempName) ; tempName.prepend("_")) {}
		}
	blockSignals(false);
	
	
	setParent(ParentElement);//QObject
	emit parentChanged(this);
	return true;
	}
else return false;
}

QList<MecAbstractElement*>* MecSignal::childList()
{
return &m_childElements;
}

void MecSignal::emitChildListChanged()
{
emit childListChanged(this);
}

void MecSignal::childVariablesChanged()
{
for (int i=0 ; i < m_connectedFunctions.size() ; i++)
	{
	if (!isConnectableFunction(m_connectedFunctions.at(i)))
		{
		disconnectFunction(m_connectedFunctions.at(i));
		i--;
		}
	}
}

void MecSignal::functionModified(MecAbstractElement* ElementFunction)
{
MecAbstractFunction *Function = static_cast<MecAbstractFunction*>(ElementFunction);
if (!isConnectableFunction(Function)) disconnectFunction(Function);
}

void MecSignal::functionDestroyed(QObject* ObjectFunction)
{
disconnectFunction(static_cast<MecAbstractFunction*>(ObjectFunction));
}

bool operator==(MecSignal const &a, MecSignal const &b)
{
	bool result = (a.elementName() == b.elementName())
				&& (a.elementType() == b.elementType())
				&& (a.childElements().size() == b.childElements().size())
				&& (a.connectedFunctions().size() == b.connectedFunctions().size());
	
	for (int i=0 ; i < a.childElements().size() && result ; i++)
		{
		if (a.childElements().at(i)->elementRole() == b.childElements().at(i)->elementRole())
			{
			MecAbstractElement *first = a.childElements().at(i);
			MecAbstractElement *second = b.childElements().at(i);
			
			if (first->elementRole() == MecAbstractElement::Variable)
				{
				result = ( *(static_cast<MecVariable*>(first)) == *(static_cast<MecVariable*>(second)) );
				}
			
			}
		else result=false;
		}
	
	for (int i=0 ; i < a.connectedFunctions().size() && result ; i++)
		{
		MecFunction *first = static_cast<MecFunction*>(a.connectedFunctions().at(i));
		MecFunction *second = static_cast<MecFunction*>(b.connectedFunctions().at(i));
		
		result = ( *(first) == *(second) );
			
		}
	
	return result;
}

bool operator!=(MecSignal const &a, MecSignal const &b)
{
	return !(a==b);
}

