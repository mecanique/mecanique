/*
© Quentin VIGNAUD, 2014

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#ifndef __MECSINGLEAPPLICATION_H__
#define __MECSINGLEAPPLICATION_H__

#include <cstring>
#include <iostream>
#include <QtCore>
#include "MecPluginsManager.h"
#include "MecEditor.h"

///Espace (en octets) alloué pour le segment partagé.
#define MECSHAREDSIZE	1024

/**
\brief	Classe de gestion de l'« application unique ».

Cette classe met en œuvre les mécanismes permettant à Mécanique de toujours être sous forme d'application unique.
Ceci est utile notamment pour ne pas effectuer deux fois en parallèle les mises à jour.
*/
class MecSingleApplication : public QObject
{
	Q_OBJECT

public:
	/**
	\brief	Constructeur.

	Crée ou attache le segment de mémoire partagée pour mettre en œuvre l'échange.
	*/
	MecSingleApplication();
	/**
	\brief	Destructeur.
	*/
	~MecSingleApplication();


	///Indique si la présente exécution de Mécanique est la première à avoir été lancée.
	bool isFirst();

	///Demande à la première exécution d'ouvrir le fichier spécifié.
	void openFile(QString FileName);

	///Démarre la gestion en tant que première exécution.
	void start(MecPluginsManager *PluginsManager);

private:
	///Segment de mémoire partagée.
	QSharedMemory *m_shared;
	///Indique si il s'agit de la première exécution.
	bool m_isFirst;
	///Timer de rappel pour la vérification du segment partagé.
	QTimer *m_timer;
	///Gestionnaire de plugins à donner aux nouveaux éditeurs.
	MecPluginsManager *m_pluginsManager;

private slots:
	///Vérifie le segment partagé.
	void check();
};

#endif /* __MECSINGLEAPPLICATION_H__ */

