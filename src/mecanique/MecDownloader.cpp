/*
© Quentin VIGNAUD, 2014

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#include "MecDownloader.h"

MecDownloader::MecDownloader(const QUrl FileUrl, const QString FileName, QNetworkAccessManager *NetworkAccessManager) : QObject()
{
m_networkAccessManager = NetworkAccessManager;
m_fileUrl = FileUrl;
m_fileName = FileName;
m_reply = 0;
m_file = 0;
}

MecDownloader::~MecDownloader()
{
	delete m_reply;
	delete m_file;
}
	
QUrl MecDownloader::fileUrl() const
{
	return m_fileUrl;
}

QString MecDownloader::fileName() const
{
	return m_fileName;
}

QNetworkReply* MecDownloader::networkReply() const
{
	return m_reply;
}

void MecDownloader::start()
{
	if (m_reply != 0) return;
	
	m_file = new QFile(m_fileName, this);
	if (!m_file->open(QIODevice::WriteOnly | QIODevice::Truncate)) return;
	
	QNetworkRequest tempRequest(m_fileUrl);
	
	QString tempOS;
	#ifdef Q_OS_LINUX
	tempOS = "Linux";
	#elif defined Q_OS_MAC
	tempOS = "Macintosh ";
	switch (QSysInfo::macVersion())
		{
		case QSysInfo::MV_10_6 :
		tempOS += "OS X 10.6";
		break;
		case QSysInfo::MV_10_7 :
		tempOS += "OS X 10.7";
		break;
		case QSysInfo::MV_10_8 :
		tempOS += "OS X 10.8";
		break;
		case QSysInfo::MV_10_9 :
		tempOS += "OS X 10.9";
		break;
		default:
		tempOS += "-undefined-";
		break;
		}
	#elif defined Q_OS_WIN
	tempOS = "Windows ";
	switch (QSysInfo::windowsVersion())
		{
		case QSysInfo::WV_XP :
		tempOS += "XP";
		break;
		case QSysInfo::WV_2003 :
		tempOS += "2003";
		break;
		case QSysInfo::WV_VISTA :
		tempOS += "Vista";
		break;
		case QSysInfo::WV_WINDOWS7 :
		tempOS += "7";
		break;
		case QSysInfo::WV_WINDOWS8 :
		tempOS += "8";
		break;
		case QSysInfo::WV_WINDOWS8_1 :
		tempOS += "8.1";
		break;
		default:
		tempOS += "-undefined-";
		break;
		}
	#endif
	
	tempRequest.setRawHeader("User-Agent", QString("Mecanique/" + QCoreApplication::applicationVersion() + " (" + tempOS + "; " + QLocale::system().name() + ")").toLatin1());
	
	m_reply = m_networkAccessManager->get(tempRequest);
	connect(m_reply, SIGNAL(readyRead()), SLOT(downloadProgress()));
	connect(m_reply, SIGNAL(finished()), SLOT(downloadFinished()));
}

void MecDownloader::stop()
{
	m_reply->abort();
	m_file->close();
}

void MecDownloader::downloadProgress()
{
	QByteArray tempArray(m_reply->readAll());
	m_file->write(tempArray);
	
	emit bytesWritten(tempArray.size());
}

void MecDownloader::downloadFinished()
{
	m_file->close();
	
	emit finished();
}

