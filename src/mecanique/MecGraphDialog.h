/*
© Quentin VIGNAUD, 2014

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#ifndef __MECGRAPHDIALOG_H__
#define __MECGRAPHDIALOG_H__

#include <QtWidgets>
#include "MecGraph.h"
#include "MecGraphView.h"

/**
\brief	Classe de dialogue utilisateur avec un graphe.
*/
class MecGraphDialog : public QMainWindow
{
	Q_OBJECT

public:
	/**
	\brief	Constructeur.
	\param	RootElement	Élément racine du graphe.
	\param	Parent	Widget parent.
	\param	Falgs	Drapeaux de fenêtre.
	*/
	MecGraphDialog(MecAbstractElement* const RootElement, QWidget * Parent=0, Qt::WindowFlags Flags=0);
	/**
	\brief	Destructeur.
	*/
	~MecGraphDialog();

	/**
	\brief	Enregistre le graphe au format PNG.

	\return	true si l'enregistrement s'est fait normalement, sinon false.
	*/
	bool save(QString FileName);

public slots:
	///Ouvre une fenêtre de dialogue utilisateur pour enregistrer le graphe.
	void saveAs();
	
	///Demande l'actualisation du graphe.
	void updateGraph();

signals:


private:
	///Générateur de graphe.
	MecGraph *m_graph;

	///Zone d'affichage du graphe.
	QScrollArea *scrollAreaGraph;
	///Vue d'affichage du graphe.
	MecGraphView *m_graphView;
	///Facteur d'agrandissement de l'image.
	double m_scaleFactor;

	QMenu *menuFile;
	QAction *actionSaveAs;
	QAction *actionRefreshGraph;
	QAction *actionClose;
	
	QMenu *menuDisplay;
	QAction *actionZoomIn;
	QAction *actionZoomOut;
	QAction *actionNormalSize;
	QAction *actionFitToWindow;
	
	QMenu *menuGraph;
		QMenu *menuGraphMode;
		QAction *actionHierarchy;
		QAction *actionPresentation;
	QAction *actionShowConnections;
	QAction *actionColorConnections;
	QAction *actionHideVariables;
	QAction *actionNoBlockStructure;

	QToolBar *toolBar;
	
	///Rend ou verrouille la disponibilité des actions d'affichage.
	void updateDisplayActions();
	///Ajuste l'agrandissement du graphe selon Factor.
	void scaleGraph(double Factor);
	///Repositionne la barre de défilement selon Factor.
	void adjustScrollBar(QScrollBar *ScrollBar, double Factor);

private slots:
	///Est déclenché lorsque le graphe à été généré.
	void graphFinished();

	///Est déclenché lorsque le graphe n'a pu être généré.
	void graphFailed();
	
	///Zoome d'un cran.
	void zoomIn();
	///Dézoome d'un cran.
	void zoomOut();
	///Affiche le graphe dans sa taille réelle.
	void normalSize();
	///Fait tenir l'intégralité du graphe dans la fenêtre.
	void fitToWindow();
	
	///Assigne les options.
	void assignGraphOptions();
	///Recoche les actions d'options selon celles spécifiées.
	void updateModeActions(MecGraph::Mode Mode);
	///Recoche les actions d'options selon celles spécifiées.
	void updateOptionsActions(MecGraph::Options Options);
	
	///Affiche le menu contextuel du graphe.
	void showContextMenu(QPoint pos);
};

#endif /* __MECGRAPHDIALOG_H__ */

