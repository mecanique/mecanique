/*
© Quentin VIGNAUD, 2013

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#ifndef __MECEDITOR_H__
#define __MECEDITOR_H__

#include <QtWidgets>
#include <MecAbstractEditor>
#include <MecAbstractElement>
#include <MecAbstractElementEditor>

#include "MecPluginsManager.h"
class MecUndoRedoManager;
#include "MecUndoRedoManager.h"
class MecElementTree;
#include "MecElementTree.h"
#include "MecElementReader.h"
#include "MecElementWriter.h"
#include "MecGraphDialog.h"
#include "MecCompilerDialog.h"
#include "MecCodeEditor.h"
#include "MecTypeChooserDialog.h"

/**
\brief	Éditeur principal.
*/
class MecEditor : public QMainWindow, public MecAbstractEditor
{
	Q_OBJECT

public:
	MecEditor(MecPluginsManager *PluginsManager, MecEditor *Parent=0, Qt::WindowFlags Flags=0);
	~MecEditor();

	bool open(QString FileName);
	bool save(QString FileName);

	/**
	\brief	Lit le fichier \e FileName et retourne l'élément correspondant.
	\return	L'élément racine du fichier, ou bien 0 si une erreur est survenue.
	*/
	MecAbstractElement* read(QString FileName);
	/**
	\brief	Écrit dans le fichier \e FileName l'élément \e Element.
	\return	\e true si l'écriture a aboutit, sinon \e false.
	*/
	bool write(MecAbstractElement *Element, QString FileName);

	/**
	\brief	Retourne la liste des noms d'éléments de base du rôle indiqué.
	*/
	QStringList baseElements(MecAbstractElement::ElementRole Role);
	/**
	\brief	Retourne l'élément de base de rôle et de nom indiqué.
	\return	0 si rien n'a été trouvé à ce nom.
	\see	baseElements()
	*/
	MecAbstractElement* baseElement(MecAbstractElement::ElementRole Role, QString Name);
	/**
	\brief	Demande à l'utilisateur de choisir parmi les types proposés.

	\return	Le type sélectionné, ou une chaîne vide si aucun.
	*/
	QString selectType(QStringList Types);
	/**
	\brief	Retourne la liste des noms réservés.

	Les noms réservés sont, par exemple, les noms de types (int, uint, Object, Project, etc…) et ne peuvent servir comme noms pour des éléments ou des variables internes à des fonctions définies par l'utilisateur.
	*/
	QStringList reservedNames() const;
	/**
	\brief	Retourne un éditeur de code pour la fonction spécifiée.

	Le widget \e Parent est assigné comme parent de l'éditeur retourné.
	*/
	MecAbstractCodeEditor* codeEditor(MecAbstractFunction* const Function, QWidget* Parent=0);

	///Retourne le gestionnaire de plugins.
	MecAbstractPluginsManager* pluginsManager() const;

	///Retourne l'élément racine.
	MecAbstractElement* element() const;
	/**
	\brief	Affecte un nouvel élément racine.

	Les éditeurs d'éléments, l'arborescence et l'élément actuel sont supprimés, et de nouveaux éditeurs d'éléments sont appelés.

	\return	\e true	si l'affectation a eu lieue, \e false si une erreur est survenue (greffon manquant par exemple).
	*/
	bool setElement(MecAbstractElement *Element);
	///Retourne l'élément actuellement sélectionné.
	MecAbstractElement* selectedElement() const;

	///Retourne l'éditeur racine.
	MecAbstractElementEditor* rootEditor() const;
	///Retourne l'arborescence d'éléments.
	MecElementTree* elementTree() const;

	///Ajoute un pas d'édition (annuler/rétablir).
	void addEditStep(QString Name) const;
	
	/**
	\brief	Ouvre le menu contextuel correspondant à l'élément spécifié, à l'emplacement donné.
	
	\param	Element		Élément sur lequel l'appel a eu lieu.
	\param	Position	Position globale de l'appel.
	*/
	void elementContextMenu(MecAbstractElement *Element, QPoint Pos);
	
	/**
	\brief	Ouvre la référence de l'élément donné en paramètre.
	*/
	void showElementReference(MecAbstractElement *Element) const;

	///Retourne le texte correspondant au rôle d'élément.
	QString roleText(MecAbstractElement::ElementRole Role) const;

public slots:
	void create();
	void open();
	void save();
	void saveAs();
	void newWindow();

	void updateEditMenu();
	void addObject();
	void addFunction();
	void addSignal();
	void addVariable();
	void removeElement();
	void showElementReference();
	void import();

	void compile();

	void help();
	void reportBug();
	void aboutMecanique();

	/**
	\brief	Met le statut de la fenêtre à « modifiée ».

	Appelle « setWindowsModified(bool) », et débloque les actions liées.
	*/
	void modified();
	/**
	\brief	Met le statut de la fenêtre à « non modifiée ».

	Appelle « setWindowsModified(bool) », et bloque les actions liées.
	*/
	void unmodified();

signals:


private:
	//Copie interdite
	MecEditor(const MecEditor& E);
	MecEditor& operator=(const MecEditor& E);

	MecPluginsManager *m_pluginsManager;

	MecUndoRedoManager *m_undoRedoManager;

	QString m_fileName;

	MecAbstractElement *m_element;
	MecAbstractElementEditor *m_rootEditor;

	MecElementTree *m_elementTree;

	MecGraphDialog *m_graphDialog;

	MecCompilerDialog *m_compilerDialog;

	QMenu *menuFile;
	QAction *actionCreate;
	QAction *actionOpen;
	QAction *actionSave;
	QAction *actionSaveAs;
	QAction *actionNewWindow;
	QAction *actionClose;
	QAction *actionQuit;
	QMenu *menuEdit;
	QAction *actionUndo;
	QAction *actionRedo;
	QAction *actionAddObject;
	QAction *actionAddFunction;
	QAction *actionAddSignal;
	QAction *actionAddVariable;
	QAction *actionRemoveElement;
	QAction *actionElementReference;
	QAction *actionImport;
	QMenu *menuTools;
	QAction *actionShowGraphDialog;
	QAction *actionCompile;
	QAction *actionShowPluginsManager;
	QMenu *menuHelp;
	QAction *actionHelp;
	QAction *actionReportBug;
	QAction *actionAboutMecanique;
	QAction *actionAboutQt;
	
	QMenu *menuContextElement;

	QToolBar *toolBar;

	QDockWidget *dockElementTree;

	void closeEvent(QCloseEvent *Event);

private slots:
	void pluginsErrorsOccured(MecAbstractElementEditor *Editor=0);

};

#endif /* __MECEDITOR_H__ */

