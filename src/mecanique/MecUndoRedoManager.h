/*
© Quentin VIGNAUD, 2014

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#ifndef __MECUNDOREDOMANAGER_H__
#define __MECUNDOREDOMANAGER_H__

#include <QObject>
#include <MecAbstractElementEditor>
class MecEditor;
#include "MecEditor.h"
#include "MecElementTree.h"
#include "MecElementReader.h"
#include "MecElementWriter.h"

/**
\brief	Classe de gestion du système annuler/rétablir.
*/
class MecUndoRedoManager : public QObject
{
	Q_OBJECT

public:
	/**
	\brief	Point d'« enregistrement » des actions d'édition.
	*/
	struct Step {
		///Nom du point (en langage humain).
		QString name;
		///Status de l'éditeur racine.
		QVariant editorState;
		///Enregistrement de l'arborescence des éléments.
		QByteArray elements;
	};

	/**
	\brief	Constructeur.

	\param	Editor	Éditeur à partir duquel les enregistrement sont à effectuer.
	*/
	MecUndoRedoManager(MecEditor *Editor);
	///Destructeur.
	~MecUndoRedoManager();

	/**
	\brief	Indique quel point est situé avant l'actuel (correspondant à « précédent »).

	\return	Un pointeur sur le point correspondant, ou 0 si aucun.
	*/
	const Step* previousStep() const;
	/**
	\brief	Indique quel point est situé après l'actuel (correspondant à « suivant »).

	\return	Un pointeur sur le point correspondant, ou 0 si aucun.
	*/
	const Step* nextStep() const;

public slots:
	/**
	\brief	Ajoute un point d'enregistrement après le point actuel.

	\param	Name	Nom du point d'enregistrement (c.-à-d. de l'action qui lui est liée).

	\note	Ne fait rien si une restauration est en cours.
	*/
	void addStep(QString Name);

	/**
	\brief	Retourne au point précédent.

	Ne fait rien s'il n'existe pas de point précédent.
	*/
	void undo();
	/**
	\brief	Rétabli le point suivant.

	Ne fait rien s'il n'existe pas de point suivant.
	*/
	void redo();

	/**
	\brief	Remet à zéro le gestionnaire.

	\note	Ne fait rien si une restauration est en cours.
	*/
	void reset();

signals:
	///Indique que undo() ou redo() a été appelé.
	void indexChanged();
	///Indique qu'un point d'enregistrement a été ajouté.
	void stepAdded();

private:
	///Éditeur principal.
	MecEditor *m_editor;

	///Liste des points d'enregistrements.
	QList<Step*> m_steps;
	///Index actuel dans la liste des points d'enregistrement.
	int m_index;

	/**
	\brief	Restaure le pas indiqué.

	\param	index	Index du pas à restaurer.
	\param	shiftState	Indique l'état des éditeurs à appliquer.
	*/
	void restoreStep(int index, int shiftState=0);
	///Indique si une restauration est en cours.
	bool m_restoreProcess;

};

#endif /* __MECUNDOREDOMANAGER_H__ */

