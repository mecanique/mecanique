/*
© Quentin VIGNAUD, 2013

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#ifndef __LOG_H__
#define __LOG_H__

#include <iostream>
#include <QtWidgets>

/**
\brief	Classe de gestion du journal.

Tient à jour le journal, aussi bien dans sa version graphique que fichier.
*/
class Log : public QMainWindow
{
	Q_OBJECT
	
	public:
	/**
	\brief	Constructeur.
	
	Ouvre un fichier de nom « ../var/log - dd-MM-yyyy hh.mm.ss.txt ».
	*/
	Log(QWidget *Parent=0);
	/**
	\brief	Destructeur.
	
	Ferme le fichier log.
	*/
	~Log();
	
	/**
	\brief	Ajoute un message dans le fichier log.
	
	\param	Name	Nom de l'objet à inscrire dans le log.
	\param	Text	Contenu du message.
	
	\note	\e Text peut contenir du code HTML, auquel cas celui-ci sera interprété pour l'affichage graphique, mais il ne sera en revanche pas inscrit dans le fichier log. Faire donc attention à la présence des caractère « < » et « > » dans les messages.
	*/
	void write(QString Name, QString Text);
	
	signals:
	
	
	private:
	///Temps de référence pour l'incription des messages.
	QTime time;
	///Afficheur graphique du log.
	QTextEdit *textEditLog;
	///Fichier de log.
	QFile *fileLog;
	
};

#endif /* __LOG_H__ */

