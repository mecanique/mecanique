/*
© Quentin VIGNAUD, 2013

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#include "Object.h"

Object::Object(QString Name, QString Type, Project* const Project) : m_name (Name), m_type (Type), m_project (Project)
{
m_status = Object::Unknown;
m_infos = QString(tr("Unknown state"));
project()->addObject(this);
setParent(project());
//Chargement des traductions.
QTranslator *translator = new QTranslator(qApp);
	//Fichier selon la nomenclature « [TYPE_OBJET].[LOCALE].qm » dans le répertoire « share/translations ».
	translator->load(m_type + "." + QLocale::system().name().section('_', 0, 0), qApp->applicationDirPath() + QString("/../share/translations"));
	qApp->installTranslator(translator);
}

Object::~Object()
{
}

Project* Object::project() const
{
return m_project;
}
	
QString Object::name() const
{
return m_name;
}

QString Object::type() const
{
return m_type;
}

Object::Status Object::status() const
{
return m_status;
}

QString Object::infos() const
{
return m_infos;
}

void Object::changeStatus(Object::Status Status)
{
m_status = Status;
emit statusChanged(this);
}

void Object::changeInfos(QString Text)
{
m_infos = Text;
emit infosChanged(this);
}


