/*
© Quentin VIGNAUD, 2013

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#ifndef __OBJECT_H__
#define __OBJECT_H__

#include <QtCore>
#include "mecanique.h"
#include "Project.h"
class Project;

/**
\brief	Classe d'objet.

Permet la gestion de l'objet et ses fonctionnalités avec le système global.
*/
class Object : public QObject
{
	Q_OBJECT
	
	public:
	/**
	\brief	Statut de l'objet.
	*/
	enum Status
		{
		///Objet non-opérationnel
		Error = -1,
		///Objet opérationnel, mais non configuré ou de manière inadaptée.
		Warning = 0,
		///Objet dont l'état est inconnu.
		Unknown = 1,
		///Objet pleinement opérationnel.
		Operational = 2
		};
	
	/**
	\brief	Constructeur.
	
	\param	Name	Nom de l'objet.
	\param	Type	Type de l'objet.
	\param	Project	Projet de rattachement.
	
	Déclare l'objet au projet, et charge le traducteur correspondant s'il existe (fichier selon la nomenclature « [TYPE_OBJET].[LOCALE].qm » dans le répertoire « share/translations »).
	*/
	Object(QString Name, QString Type, Project* const Project);
	///Destructeur.
	virtual ~Object();
	
	/**
	\brief	Retourne le projet de rattachement.
	*/
	Project* project() const;
	/**
	\brief	Retourne le nom.
	*/
	QString name() const;
	/**
	\brief	Retourne le type.
	*/
	QString type() const;
	/**
	\brief	Retourne le statut de l'objet.
	*/
	Object::Status status() const;
	/**
	\brief	Retourne le texte informatif actuel de l'objet.
	*/
	QString infos() const;
	
	/**
	\brief	Demande l'ouverture des réglages ou de l'interface de l'objet.
	*/
	virtual void more() = 0;
	
	/**
	\brief	Retourne les paramètres actuels de l'objet.
	
	Ces paramètres sont voués à être enregistrés pour une utilisation ultérieure. 
	*/
	virtual QVariant settings() = 0;
	/**
	\brief	Charge les paramètres à appliquer à l'objet.
	*/
	virtual void setSettings(QVariant Settings) = 0;
	
	signals:
	///Est émit lors du chagement de statut.
	void statusChanged(Object* I);
	///Est émit lors du changement de texte informatif.
	void infosChanged(Object* I);
	
	protected:
	/**
	\brief	Change le statut de l'objet.
	*/
	void changeStatus(Object::Status Status);
	/**
	\brief	Change le texte informatif de l'objet.
	*/
	void changeInfos(QString Text);
	
	private:
	///Nom de l'objet.
	const QString m_name;
	///Type de l'objet.
	const QString m_type;
	///Projet de rattachement;
	Project* const m_project;
	///Statut de l'objet.
	Object::Status m_status;
	///Texte informatif de l'objet.
	QString m_infos;
	
};

#endif /* __OBJECT_H__ */

