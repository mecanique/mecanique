/*
© Quentin VIGNAUD, 2013

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#ifndef __PROJECT_H__
#define __PROJECT_H__

#include <QtWidgets>
#include "mecanique.h"
#include "Log.h"
#include "Object.h"
class Object;

/**
\brief	Classe de gestion de projet.

Cette classe est également l'interface utilisateur.
*/
class Project : public QMainWindow
{
	Q_OBJECT
	
	public:
	///Constructeur.
	Project();
	///Destructeur.
	virtual ~Project();
	
	///Retourne le log d'exécution.
	Log* log() const;
	
	///Fixe le titre du projet.
	void setTitle(QString Title);
	///Fixe le synopsis du projet.
	void setSynopsis(QString Synopsis);
	
	///Ajoute un objet au projet actuel.
	void addObject(Object* Object);
	
	private:
	///Menu d'exécution.
	QMenu *menuExecution;
		///Action de chargement des paramètres.
		QAction *actionLoadSettings;
		///Action d'enregistrement des paramètres.
		QAction *actionSaveSettings;
		///Action de visionnage du journal.
		QAction *actionSeeLog;
		///Action d'arrêt.
		QAction *actionQuit;
	
	///Menu d'à propos.
	QMenu *menuAbout;
		///Action « À propos de Mécanique ».
		QAction *actionAbout;
		///Action d'affichage de la licence.
		QAction *actionLicence;
	
	///Widget principal de l'interface.
	QWidget *widgetMain;
	
	///Arborescence des objets.
	QTreeWidget *treeWidgetObjects;
	
	///Widget d'affichage des informations du projet.
	QWidget *widgetInfos;
		///Label d'affichage du titre.
		QLabel *labelTitle;
		///Zone d'affichage du synopsis.
		QTextEdit *textEditSynopsis;
	///Layout du widget d'affichage des infos.
	QGridLayout *layoutWidgetInfos;
	
	///Widget d'affichage de la licence.
	QWidget *widgetLicence;
		///Label d'affichage de la licence.
		QLabel *labelLicence;
		///Bouton d'affichage de la licence.
		QPushButton *pushButtonLicence;
	///Layout de licence.
	QHBoxLayout *layoutWidgetLicence;
	
	///Layout du widget principal de l'interface.
	QGridLayout *layoutMain;
	
	///Journal d'exécution.
	Log *m_log;
	
	///Titre du projet.
	QString m_title;
	///Synopsis du projet.
	QString m_synopsis;
	
	///Liste des objets déclarés du projet.
	QList<Object*> m_objects;
	///Liste des entrées d'arborescence des objets.
	QList<QTreeWidgetItem*> m_objectItems;
	
	///Demande confirmation de la fermeture.
	virtual void closeEvent(QCloseEvent *Event);
	
	private slots:
	/**
	\brief	Chargement de paramètres.
	
	Ouvre une boite de dialogue l'utilisateur pour spécifier le fichier de paramètres à charger, et les charge.
	*/
	void loadSettings();
	/**
	\brief	Enregistrement des paramètres.
	
	Ouvre une boite de dialogue pour enregistrer les paramètres dans un fichier, et les y enregistre.
	*/
	void saveSettings();
	
	/**
	\brief	Ouvre la boite de dialogue à propos de Mécanique.
	*/
	void about();
	/**
	\brief	Affiche la licence du projet.
	*/
	void licence();
	
	/**
	\brief	Prend en charge le changement de statut d'un objet.
	
	Change l'indicateur de statut de l'item pour celui spécifié par l'objet.
	*/
	void statusChanged(Object *Object);
	/**
	\brief	Prend en charge le changement de texte d'information d'un objet.
	
	Change le texte visible de l'item par le texte d'information de l'objet.
	*/
	void infosChanged(Object *Object);
	
	/**
	\brief	Prend en charge l'activation d'un item.
	
	Appelle la fonction « more() » de l'objet correspondant à l'item.
	*/
	void itemActivated(QTreeWidgetItem *Item);
	
};

#endif /* __PROJECT_H__ */

