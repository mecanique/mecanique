/*
© Quentin VIGNAUD, 2013

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#include "Log.h"

Log::Log(QWidget *Parent) : QMainWindow(Parent)
{
setWindowTitle(tr("Execution log"));

textEditLog = new QTextEdit(this);
	textEditLog->setReadOnly(true);
setCentralWidget(textEditLog);

fileLog = new QFile(QCoreApplication::applicationDirPath() + "/../var/log - " + QDateTime::currentDateTime().toString("dd-MM-yyyy hh.mm.ss") + ".txt", this);
	if(fileLog->open(QIODevice::WriteOnly | QIODevice::Truncate | QIODevice::Unbuffered))
	{
		statusBar()->showMessage(tr("Log writes in \"") + "/var/" + QFileInfo(*fileLog).fileName() + tr("\"."));
	}
	else
	{
		statusBar()->showMessage(tr("The file \"") + "/var/" + QFileInfo(*fileLog).fileName() + tr("\" cannot be written. The log did not saved."));
	}

time.start();
}

Log::~Log()
{
fileLog->close();
}

void Log::write(QString Name, QString Text)
{
QString tempText(time.addMSecs(time.elapsed()).toString("HH:mm:ss.zzz ") + Name + ": " + Text);
textEditLog->append(tempText);
fileLog->write(QString(tempText + "\n").remove(QRegExp("<.*>")).toUtf8());
std::cout << tempText.remove(QRegExp("<.*>")).toStdString() << std::endl;
}

