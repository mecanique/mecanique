/*
© Quentin VIGNAUD, 2013

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#include "Project.h"

Project::Project() : QMainWindow()
{
setWindowIcon(QIcon(QCoreApplication::applicationDirPath() + "/../share/icons/mecanique.png"));

m_log = new Log();

widgetMain = new QWidget(this);

menuExecution = menuBar()->addMenu(tr("&Execution"));
	actionLoadSettings = menuExecution->addAction(QIcon::fromTheme("document-open"), tr("&Load settings..."), this, SLOT(loadSettings()), QKeySequence("Ctrl+O"));
	actionSaveSettings = menuExecution->addAction(QIcon::fromTheme("document-save-as"), tr("&Save settings..."), this, SLOT(saveSettings()), QKeySequence("Ctrl+S"));
	menuExecution->addSeparator();
	actionSeeLog = menuExecution->addAction(QIcon::fromTheme("text-x-generic"), tr("See &log..."), m_log, SLOT(show()), QKeySequence("Ctrl+L"));
	menuExecution->addSeparator();
	actionQuit = menuExecution->addAction(QIcon::fromTheme("application-exit"), tr("&Quit"), this, SLOT(close()), QKeySequence("Ctrl+Q"));

menuAbout = menuBar()->addMenu(tr("&About"));
	actionAbout = menuAbout->addAction(windowIcon(), tr("About &Mécanique..."), this, SLOT(about()));
	actionLicence = menuAbout->addAction(QIcon::fromTheme("application-certificate"), tr("&Licence"), this, SLOT(licence()));

treeWidgetObjects = new QTreeWidget(widgetMain);
	treeWidgetObjects->setHeaderLabels(QStringList() << tr("Name") << tr("Type") << tr("Infos"));
	treeWidgetObjects->setSelectionMode(QAbstractItemView::SingleSelection);
	treeWidgetObjects->header()->setSectionResizeMode(QHeaderView::ResizeToContents);
	connect(treeWidgetObjects, SIGNAL(itemActivated(QTreeWidgetItem*, int)), SLOT(itemActivated(QTreeWidgetItem*)));

widgetInfos = new QWidget(widgetMain);
	labelTitle = new QLabel(widgetInfos);
	textEditSynopsis = new QTextEdit(widgetInfos);
		textEditSynopsis->setReadOnly(true);
layoutWidgetInfos = new QGridLayout(widgetInfos);
	layoutWidgetInfos->addWidget(labelTitle, 0, 0);
	layoutWidgetInfos->addWidget(textEditSynopsis, 1, 0);

widgetLicence = new QWidget(widgetMain);
	labelLicence = new QLabel(tr("Work licenced under EUPL."), widgetLicence);
	pushButtonLicence = new QPushButton(QIcon::fromTheme("application-certificate"), tr("See the licence"), widgetLicence);
		connect(pushButtonLicence, SIGNAL(clicked(bool)), SLOT(licence()));
layoutWidgetLicence = new QHBoxLayout(widgetLicence);
	layoutWidgetLicence->addWidget(labelLicence);
	layoutWidgetLicence->addStretch();
	layoutWidgetLicence->addWidget(pushButtonLicence);

layoutMain = new QGridLayout(widgetMain);
	layoutMain->addWidget(treeWidgetObjects, 0, 0);
	layoutMain->addWidget(widgetInfos, 1, 0);
	layoutMain->addWidget(widgetLicence, 2, 0);

setCentralWidget(widgetMain);
}

Project::~Project()
{
while (m_objects.size() != 0) delete m_objects.takeFirst();
delete m_log;
}

Log* Project::log() const
{
return m_log;
}

void Project::setTitle(QString Title)
{
setWindowTitle(Title + " – Mécanique");
labelTitle->setText("<h3>" + Title + "</h3>");
}

void Project::setSynopsis(QString Synopsis)
{
textEditSynopsis->setPlainText(Synopsis);
}

void Project::addObject(Object* Object)
{
if (m_objects.contains(Object)) return;
m_objects.append(Object);
QTreeWidgetItem *tempItem = new QTreeWidgetItem(QStringList() << Object->name() << Object->type() << Object->infos());
tempItem->setIcon(0, QIcon(QCoreApplication::applicationDirPath() + "/../share/icons/types/" + Object->type() + ".png"));
m_objectItems.append(tempItem);
treeWidgetObjects->addTopLevelItem(tempItem);

connect(Object, SIGNAL(statusChanged(Object*)), SLOT(statusChanged(Object*)));
connect(Object, SIGNAL(infosChanged(Object*)), SLOT(infosChanged(Object*)));
statusChanged(Object);
}

void Project::closeEvent(QCloseEvent *Event)
{
if (QMessageBox::question(this, tr("Quit the program?"), tr("<h3>Are you sure you want to exit the program?</h3><p>All connections and controls will be closed.</p>"), QMessageBox::Yes | QMessageBox::No, QMessageBox::No) == QMessageBox::Yes)
	{
	m_log->close();
	Event->accept();
	qApp->quit();
	}
else Event->ignore();
}

void Project::loadSettings()
{
QString tempName = QFileDialog::getOpenFileName(this, tr("Load settings"), QCoreApplication::applicationDirPath() + "/../etc", tr("Settings file (*.conf)"));
if (!tempName.isEmpty())
	{
	QSettings tempSettings(tempName, QSettings::IniFormat, this);
	tempSettings.setIniCodec("UTF-8");
	for (int i=0 ; i < m_objects.size() ; i++)
		{
		m_objects.at(i)->setSettings(tempSettings.value(m_objects.at(i)->name()));
		}
	}
}

void Project::saveSettings()
{
QString tempName = QFileDialog::getSaveFileName(this, tr("Save settings"), QCoreApplication::applicationDirPath() + "/../etc", tr("Settings file (*.conf)"));
if (!tempName.isEmpty())
	{
	if (!tempName.endsWith(".conf", Qt::CaseInsensitive)) tempName.append(".conf");
	QSettings tempSettings(tempName, QSettings::IniFormat, this);
	tempSettings.setIniCodec("UTF-8");
	for (int i=0 ; i < m_objects.size() ; i++)
		{
		tempSettings.setValue(m_objects.at(i)->name(), m_objects.at(i)->settings());
		}
	}
}
	
void Project::about()
{
QMessageBox::about(this, tr("About Mécanique"), tr("<h3>About Mécanique</h3><p>This work was produced with “Mécanique”</p><p>Copyrigth © 2013 – 2015 Quentin VIGNAUD</p><p>Mécanique is a free and open-source set of programs and source codes to establish links between events and tasks to perform, with the aim to generate sequences more or less complex of real mechanical actions based on virtual events.</p><p><a href=\"http://www.mecanique.cc/\">www.mecanique.cc</a></p>"));
}

void Project::licence()
{
QMessageBox tempBox(this);
tempBox.setWindowTitle(tr("Licence"));
tempBox.setIconPixmap(QIcon::fromTheme("application-certificate").pixmap(64, 64));
tempBox.setText(tr("<h3>Licence</h3><p>This work is distributed under the EUPL version 1.1 (<i>European Union Public Licence</i>).</p><p>This means that anyone is free to the following rights:<ul><li>use the work in any circumstance and for all usage,</li><li>reproduce the work,</li><li>modify the original work, and make derivative works based upon the work,</li><li>communicate to the public, including the right to make available or display the work or copies thereof to the public and perform publicly, as the case may be, the work,</li><li>distribute the work or copies thereof,</li><li>lend and rent the work or copies thereof,</li><li>sub-license rights in the Work or copies thereof,</li></ul>by the terms of the EUPL.</p><p>You may not use this work except in compliance with the Licence.<br />You may obtain a copy of the Licence at: <a href=\"http://joinup.ec.europa.eu/software/page/eupl/licence-eupl\">http://joinup.ec.europa.eu/software/page/eupl/licence-eupl</a>.<br /><br />Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an “AS IS” basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.<br />See the Licence for the specific language governing permissions and limitations under the Licence.</p>"));
/*
Cette œuvre est distribuée sous licence EUPL version 1.1.
Cela signifie que quiconque est libre des droits suivants :
utiliser l’Œuvre en toute circonstance et pour tout usage,
reproduire l’Œuvre,
modifier l’Œuvre Originale, et de faire des Œuvres Dérivées sur la base de l’Œuvre,
communiquer, présenter ou représenter l'œuvre ou copie de celle-ci au public,
en ce compris le droit de mettre celles-ci à la disposition du public,
distribuer l’Œuvre ou des copies de celles-ci,
prêter et louer l’Œuvre ou des copies de celles-ci,
sous-licencier les droits concédés ici sur l’Œuvre ou sur des copies de celles-ci,
selon les termes de la licence EUPL.
*/
tempBox.addButton(QMessageBox::Ok);
tempBox.exec();
}

void Project::statusChanged(Object *Object)
{
QTreeWidgetItem *tempItem = m_objectItems.at(m_objects.indexOf(Object));
switch (Object->status())
	{
	case Object::Error :
		tempItem->setIcon(2, QIcon(QCoreApplication::applicationDirPath() + "/../share/icons/status/error.png"));
		break;
	case Object::Warning :
		tempItem->setIcon(2, QIcon(QCoreApplication::applicationDirPath() + "/../share/icons/status/warning.png"));
		break;
	case Object::Unknown :
		tempItem->setIcon(2, QIcon(QCoreApplication::applicationDirPath() + "/../share/icons/status/unknown.png"));
		break;
	case Object::Operational :
		tempItem->setIcon(2, QIcon(QCoreApplication::applicationDirPath() + "/../share/icons/status/operational.png"));
		break;
	}
}

void Project::infosChanged(Object *Object)
{
QTreeWidgetItem *tempItem = m_objectItems.at(m_objects.indexOf(Object));
tempItem->setText(2, Object->infos());
}
	
void Project::itemActivated(QTreeWidgetItem *Item)
{
m_objects.at(m_objectItems.indexOf(Item))->more();
}


