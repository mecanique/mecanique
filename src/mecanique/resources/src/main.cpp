/*
© Quentin VIGNAUD, 2013

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#include <QtCore>
#include "mecanique/mecanique.h"
#include "mecanique/Project.h"
#include "[[PROJECTNAME]].h"

double min(double x, double y)
{
	if (x < y) return x;
	else return y;
}

double max(double x, double y)
{
	if (x > y) return x;
	else return y; 
}

double ln(double value)
{
	return log(value);
}

int rand(int min, int max)
{
	return qrand() % (max-min) + min;
}

int main(int argc, char *argv[])
{
	//Initialization of Qt.
	QApplication app(argc, argv);

	//Initialization of translations.
	QString locale = QLocale::system().name().section('_', 0, 0);
	QTranslator qtTranslator;
	qtTranslator.load(QString("qt_") + locale, QLibraryInfo::location(QLibraryInfo::TranslationsPath));
	app.installTranslator(&qtTranslator);
	QTranslator mecTranslator;
	mecTranslator.load(QString("mecanique.") + locale, app.applicationDirPath() + QString("/../share/translations"));
	app.installTranslator(&mecTranslator);

	//Initialization of random-system.
	qsrand(time(NULL));
	
	app.setWindowIcon(QIcon(app.applicationDirPath() + "/../share/icons/mecanique.png"));
	QIcon::setThemeName("Humanity");
	
	Project *project = new [[PROJECTNAME]]();
	project->show();

	return app.exec();
}


