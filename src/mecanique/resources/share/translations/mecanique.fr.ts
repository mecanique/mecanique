<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="fr_FR">
<context>
    <name>Log</name>
    <message>
        <location filename="../../src/mecanique/Log.cpp" line="24"/>
        <source>Execution log</source>
        <translation>Journal d&apos;exécution</translation>
    </message>
    <message>
        <location filename="../../src/mecanique/Log.cpp" line="33"/>
        <source>Log writes in &quot;</source>
        <translation>Journalisation dans « </translation>
    </message>
    <message>
        <location filename="../../src/mecanique/Log.cpp" line="33"/>
        <source>&quot;.</source>
        <translation> ».</translation>
    </message>
    <message>
        <location filename="../../src/mecanique/Log.cpp" line="37"/>
        <source>The file &quot;</source>
        <translation>Le fichier « </translation>
    </message>
    <message>
        <location filename="../../src/mecanique/Log.cpp" line="37"/>
        <source>&quot; cannot be written. The log did not saved.</source>
        <translation> » ne peut être lu. La journalisation ne peut être enregistrée.</translation>
    </message>
</context>
<context>
    <name>Object</name>
    <message>
        <location filename="../../src/mecanique/Object.cpp" line="25"/>
        <source>Unknown state</source>
        <translation>État inconnu</translation>
    </message>
</context>
<context>
    <name>Project</name>
    <message>
        <location filename="../../src/mecanique/Project.cpp" line="30"/>
        <source>&amp;Execution</source>
        <translation>&amp;Exécution</translation>
    </message>
    <message>
        <location filename="../../src/mecanique/Project.cpp" line="31"/>
        <source>&amp;Load settings...</source>
        <translation>&amp;Charger des paramètres…</translation>
    </message>
    <message>
        <location filename="../../src/mecanique/Project.cpp" line="32"/>
        <source>&amp;Save settings...</source>
        <translation>&amp;Sauvegarder les paramètres…</translation>
    </message>
    <message>
        <location filename="../../src/mecanique/Project.cpp" line="34"/>
        <source>See &amp;log...</source>
        <translation>Voir le &amp;journal…</translation>
    </message>
    <message>
        <location filename="../../src/mecanique/Project.cpp" line="36"/>
        <source>&amp;Quit</source>
        <translation>&amp;Quitter</translation>
    </message>
    <message>
        <location filename="../../src/mecanique/Project.cpp" line="38"/>
        <source>&amp;About</source>
        <translation>&amp;À propos</translation>
    </message>
    <message>
        <location filename="../../src/mecanique/Project.cpp" line="39"/>
        <source>About &amp;Mécanique...</source>
        <translation>À propos de &amp;Mécanique…</translation>
    </message>
    <message>
        <location filename="../../src/mecanique/Project.cpp" line="40"/>
        <source>&amp;Licence</source>
        <translation>&amp;Licence</translation>
    </message>
    <message>
        <location filename="../../src/mecanique/Project.cpp" line="43"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../../src/mecanique/Project.cpp" line="43"/>
        <source>Type</source>
        <translation>Type</translation>
    </message>
    <message>
        <location filename="../../src/mecanique/Project.cpp" line="43"/>
        <source>Infos</source>
        <translation>Infos</translation>
    </message>
    <message>
        <location filename="../../src/mecanique/Project.cpp" line="57"/>
        <source>Work licenced under EUPL.</source>
        <translation>Travail distribué sous licence EUPL.</translation>
    </message>
    <message>
        <location filename="../../src/mecanique/Project.cpp" line="58"/>
        <source>See the licence</source>
        <translation>Voir la licence</translation>
    </message>
    <message>
        <location filename="../../src/mecanique/Project.cpp" line="111"/>
        <source>Quit the program?</source>
        <translation>Quitter le programme ?</translation>
    </message>
    <message>
        <location filename="../../src/mecanique/Project.cpp" line="111"/>
        <source>&lt;h3&gt;Are you sure you want to exit the program?&lt;/h3&gt;&lt;p&gt;All connections and controls will be closed.&lt;/p&gt;</source>
        <translation>&lt;h3&gt;Êtes-vous sûr de vouloir quitter la programme ?&lt;/h3&gt;&lt;p&gt;Toutes les connexions et contrôles seront fermés.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../../src/mecanique/Project.cpp" line="122"/>
        <source>Load settings</source>
        <translation>Charger des paramètres</translation>
    </message>
    <message>
        <location filename="../../src/mecanique/Project.cpp" line="122"/>
        <location filename="../../src/mecanique/Project.cpp" line="136"/>
        <source>Settings file (*.conf)</source>
        <translation>Fichier de paramètres (*.conf)</translation>
    </message>
    <message>
        <location filename="../../src/mecanique/Project.cpp" line="136"/>
        <source>Save settings</source>
        <translation>Enregistrer les paramètres</translation>
    </message>
    <message>
        <location filename="../../src/mecanique/Project.cpp" line="151"/>
        <source>About Mécanique</source>
        <translation>À propos de Mécanique</translation>
    </message>
    <message>
        <location filename="../../src/mecanique/Project.cpp" line="151"/>
        <source>&lt;h3&gt;About Mécanique&lt;/h3&gt;&lt;p&gt;This work was produced with “Mécanique”&lt;/p&gt;&lt;p&gt;Copyrigth © 2013 – 2015 Quentin VIGNAUD&lt;/p&gt;&lt;p&gt;Mécanique is a free and open-source set of programs and source codes to establish links between events and tasks to perform, with the aim to generate sequences more or less complex of real mechanical actions based on virtual events.&lt;/p&gt;&lt;p&gt;&lt;a href=&quot;http://www.mecanique.cc/&quot;&gt;www.mecanique.cc&lt;/a&gt;&lt;/p&gt;</source>
        <translation>&lt;h3&gt;À propos de Mécanique&lt;/h3&gt;&lt;p&gt;Ce travail a été produit avec « Mécanique »&lt;/p&gt;&lt;p&gt;Copyright © 2013 – 2015 Quentin VIGNAUD&lt;/p&gt;&lt;p&gt;Mécanique est un ensemble open-source et libre de programme et modules permettant d&apos;établir des liens logiques entre des évènements et des tâches à accomplir, dans le but de générer des séquences d&apos;actions mécaniques basées sur des évènements virtualisés.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../../src/mecanique/Project.cpp" line="157"/>
        <source>Licence</source>
        <translation>Licence</translation>
    </message>
    <message>
        <location filename="../../src/mecanique/Project.cpp" line="159"/>
        <source>&lt;h3&gt;Licence&lt;/h3&gt;&lt;p&gt;This work is distributed under the EUPL version 1.1 (&lt;i&gt;European Union Public Licence&lt;/i&gt;).&lt;/p&gt;&lt;p&gt;This means that anyone is free to the following rights:&lt;ul&gt;&lt;li&gt;use the work in any circumstance and for all usage,&lt;/li&gt;&lt;li&gt;reproduce the work,&lt;/li&gt;&lt;li&gt;modify the original work, and make derivative works based upon the work,&lt;/li&gt;&lt;li&gt;communicate to the public, including the right to make available or display the work or copies thereof to the public and perform publicly, as the case may be, the work,&lt;/li&gt;&lt;li&gt;distribute the work or copies thereof,&lt;/li&gt;&lt;li&gt;lend and rent the work or copies thereof,&lt;/li&gt;&lt;li&gt;sub-license rights in the Work or copies thereof,&lt;/li&gt;&lt;/ul&gt;by the terms of the EUPL.&lt;/p&gt;&lt;p&gt;You may not use this work except in compliance with the Licence.&lt;br /&gt;You may obtain a copy of the Licence at: &lt;a href=&quot;http://joinup.ec.europa.eu/software/page/eupl/licence-eupl&quot;&gt;http://joinup.ec.europa.eu/software/page/eupl/licence-eupl&lt;/a&gt;.&lt;br /&gt;&lt;br /&gt;Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an “AS IS” basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.&lt;br /&gt;See the Licence for the specific language governing permissions and limitations under the Licence.&lt;/p&gt;</source>
        <translation>&lt;h3&gt;Licence&lt;/h3&gt;&lt;p&gt;Cette œuvre est distribuée sous licence EUPL version 1.1 (&lt;i&gt;European Union Public Licence&lt;/i&gt;).&lt;/p&gt;&lt;p&gt;Cela signifie que quiconque est libre des droits suivants :&lt;ul&gt;&lt;li&gt;utiliser l’Œuvre en toute circonstance et pour tout usage ;&lt;/li&gt;&lt;li&gt;reproduire l&apos;Œuvre ;&lt;/li&gt;&lt;li&gt;modifier l’Œuvre Originale, et de faire des Œuvres Dérivées sur la base de l’Œuvre ;&lt;/li&gt;&lt;li&gt;communiquer, présenter ou représenter l&apos;œuvre ou copie de celle-ci au public, en ce compris le droit de mettre celles-ci à la disposition du public ;&lt;/li&gt;&lt;li&gt;distribuer l’Œuvre ou des copies de celles-ci ;&lt;/li&gt;&lt;li&gt;prêter et louer l’Œuvre ou des copies de celle-ci ;&lt;/li&gt;&lt;li&gt;sous-licencier les droits concédés ici sur l’Œuvre ou sur des copies de celles-ci ;&lt;/li&gt;&lt;/ul&gt;selon les termes de la licence EUPL.&lt;/p&gt;&lt;p&gt;Vous ne pouvez utiliser la présente œuvre que conformément à la Licence.&lt;br /&gt;Vous pouvez obtenir une copie de la Licence à l’adresse suivante : &lt;a href=&quot;http://joinup.ec.europa.eu/software/page/eupl/licence-eupl&quot;&gt;http://joinup.ec.europa.eu/software/page/eupl/licence-eupl&lt;/a&gt;.&lt;br /&gt;&lt;br /&gt;Sauf obligation légale ou contractuelle écrite, le logiciel distribué sous la Licence est distribué « en l’état », SANS GARANTIES OU CONDITIONS QUELLES QU’ELLES SOIENT, expresses ou implicites.&lt;br /&gt;Consultez la Licence pour les autorisations et les restrictions spécifiques relevant de la Licence.&lt;/p&gt;</translation>
    </message>
</context>
</TS>
