/*
© Quentin VIGNAUD, 2013

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#ifndef __MECCOMPILER_H__
#define __MECCOMPILER_H__

#include <QtCore>
#include "MecAbstractCompiler.h"
#include "MecProject.h"
#include "MecAbstractElementCompiler.h"
#include "MecPluginsManager.h"

/**
\brief	Compilateur de projets.
*/
class MecCompiler : public QObject, public MecAbstractCompiler
{
	Q_OBJECT

public:
	/**
	\brief	Constructeur.
	\param	Project	Projet à compiler.
	\param	PluginsManager	Gestionnaire de plugins.
	\param	Parent	Objet parent.
	*/
	MecCompiler(MecProject* const Project, MecAbstractPluginsManager* const PluginsManager, QObject* Parent=0);
	/**
	\brief	Destructeur.

	Stoppe la compilation si elle est en cours, tuant si nécessaire les processus de sous-traitement.
	*/
	~MecCompiler();

	/**
	\brief	Types des erreurs possibles.
	*/
	enum Error {
	    ///La compilation a été annulée.
	    Cancelled=-1,
	    ///Pas d'erreur.
	    NoError=0,
	    ///Un ou plusieurs plugins sont manquants pour compiler.
	    NoPlugin=1,
	    ///Impossible d'écrire dans le répertoire de compilation.
	    BuildDirectoryError=2,
	    ///« qmake » est indisponible.
	    QmakeUnavailable=3,
	    ///« make » est indisponible.
	    MakeUnavailable=4,
	    ///Une erreur de compilation dûe au système s'est provoquée (un plugin est mal implémenté ou mal préparé généralement).
	    SystemCompilerError=5,
	    ///Une erreur de compilation dûe à l'utilisateur s'est provoquée (erreur d'implémentation de sa part généralement).
	    UserCompilerError=6
	};

	///Retourne le projet sujet à compilation.
	MecProject* project() const;
	///Retourne le gestionnaire de plugins du compilateur.
	MecAbstractPluginsManager* pluginsManager() const;

	///Retourne la dernière erreur survenue.
	MecCompiler::Error error() const;

	///Retourne le nom du répertoire de compilation.
	QString buildDirectory() const;

	///Retourne le journal de compilation.
	QString log() const;

	///Retourne si la compilation est en cours.
	bool isRunning() const;

public slots:
	///Démarre la compilation.
	void start();
	///Stoppe la compilation.
	void stop();

	/**
	\brief	Fixe le nom du répertoire de compilation.
	\note	La fonction ne fait rien si la compilation est en cours.
	*/
	void setBuildDirectory(QString BuildDirectory);

signals:
	///La compilation a démarré.
	void started();
	///La compilation s'est arrêtée.
	void stopped();
	///Le statut de travail a changé.
	void runningChanged(bool Running);
	/**
	\brief	La compilation s'est terminée.
	\param	Error	Erreur survenue lors de la compilation.
	*/
	void terminate(MecCompiler::Error Error);

	///Le répertoire de compilation a changé.
	void buildDirectoryChanged(QString BuildDirectory);

	///De nouvelles informations à propos de la compilation sont disponibles.
	void news();

private:
	///Projet sujet à compilation.
	MecProject* const m_project;
	///Gestionnaire de plugins.
	MecAbstractPluginsManager* const m_pluginsManager;

	///Erreur survenue.
	MecCompiler::Error m_error;

	///Nom du répertoire de compilation.
	QString m_buildDirectory;
	///Journal de compilation.
	QString m_log;

	///Indicateur de travail.
	bool m_running;

	///Liste des compilateurs d'éléments.
	QList<MecAbstractElementCompiler*> m_elementCompiler;

	///Liste des ressources.
	QList<QResource*> m_resources;
	/**
	\brief	Liste des headers.

	Structure <nom de l'élément, contenu du fichier>.
	*/
	QList<QPair<QString, QString> > m_headers;
	/**
	\brief	Liste des sources.

	Structure <nom de l'élément, contenu du fichier>.
	*/
	QList<QPair<QString, QString> > m_sources;
	///Instructions à inscrire dans le fichier projet.
	QString m_projectInstructions;

	///Processus « qmake »
	QProcess *m_qmake;
	///Processus « make »
	QProcess *m_make;

	///Vérifie le répertoire de compilation.
	void prepareBuildDirectory();

	///Collecte les compilateurs d'éléments.
	void collectCompilers();

	///Collecte les ressources.
	void collectResources();
	///Installe les ressources.
	void installResources();

	///Collecte les headers.
	void collectHeaders();
	///Écrit les fichiers headers.
	void installHeaders();

	///Collecte les sources.
	void collectSources();
	///Écrit les fichiers sources.
	void installSources();

	///Collecte les instructions de projet.
	void collectProjectInstructions();
	///Écrit le fichier projet.
	void installProjectInstructions();

	///Lance l'exécution de « qmake »
	void loadQmake();

	///Lance l'exécution de « make »
	void loadMake();

	///Écrit dans le log.
	void writeLog(QString Text);
	///Remet à zéro le compilateur.
	void reset();

private slots:
	///Stoppe la compilation avec l'erreur \e Error.
	void stop(MecCompiler::Error Error);

	///« qmake » est en erreur.
	void qmakeError(QProcess::ProcessError Error);
	///« make » est en erreur.
	void makeError(QProcess::ProcessError Error);

	///Lit la sortie de « qmake »
	void qmakeReadOutput();
	///Lit la sortie de « make »
	void makeReadOutput();

	///Est activé lors de la clôture de « qmake »
	void qmakeFinished(int ExitCode, QProcess::ExitStatus ExitStatus);
	///Est activé lors de la clôture de « make »
	void makeFinished(int ExitCode, QProcess::ExitStatus ExitStatus);

};

#endif /* __MECCOMPILER_H__ */

