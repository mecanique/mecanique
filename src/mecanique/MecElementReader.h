/*
© Quentin VIGNAUD, 2013 - 2014

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#ifndef __MECELEMENTREADER_H__
#define __MECELEMENTREADER_H__

#include <iostream>
#include <typeinfo>
#include <QtCore>
#include <MecAbstractElement>
#include "MecProject.h"
#include "MecObject.h"
#include "MecFunction.h"
#include "MecSignal.h"
#include "MecVariable.h"

/**
\brief	Lecteur de flux de données d'élément(s).

Après la construction du lecteur de flux, l'élément lu est retourné par element().
*/
class MecElementReader : private QXmlStreamReader
{
public:
	/**
	\brief	Constructeur.
	\param	Device	Flux à lire.
	*/
	MecElementReader(QIODevice *Device);
	/**
	\brief	Constructeur.
	\param	Data	Données à lire.
	*/
	MecElementReader(const QByteArray Data);
	/**
	\brief	Constructeur.
	\param	Data	Données à lire.
	*/
	MecElementReader(const QString Data);
	/**
	\brief	Constructeur.
	\param	Data	Données à lire.
	*/
	MecElementReader(const char *Data);

	/**
	\brief	Élement lu.

	Un pointeur nul est retourné si une erreur de lecture est survenue.
	*/
	MecAbstractElement* element() const;

private:
	///Élément principal.
	MecAbstractElement *m_element;

	///Carte de correspondance signal/addresse de fonction.
	QMultiMap<MecSignal*, QString> m_mapConnections;

	/**
	\brief	Démarre la lecture.

	Assigne notamment m_element.
	*/
	void startReading();
	/**
	\brief	Lit un élément et le crée tout en le liant à son parent.

	Lorsqu'un signal est rencontré, ses connexions sont enregistrées dans m_mapConnections. Il conviendra par la suite d'appeler connect() pour les rendre effectives.

	\return	L'élément créé.
	*/
	MecAbstractElement* readElement(MecAbstractElement *ParentElement);

	///Connecte les signaux avec les fonctions. \see	m_mapConnections
	void connect();
};

#endif /* __MECELEMENTREADER_H__ */

