######################################################################
# Project file implemented for qmake (version 3 with Qt 5)
######################################################################

TEMPLATE = app
CONFIG += c++11
DESTDIR = ../../bin
CODECFORSRC = UTF-8
CODECFORTR = UTF-8
QT += widgets network
TARGET = mecanique
DEPENDPATH += .
INCLUDEPATH += . ../../include/mecanique
RESOURCES += resources.qrc
TRANSLATIONS += ../../share/mecanique/translations/mecanique.fr.ts \
                ../../share/mecanique/translations/mecanique.de.ts

# Input
HEADERS += MecCodeEditor.h \
           MecCompiler.h \
           MecCompilerDialog.h \
           MecDownloader.h \
           MecEditor.h \
           MecUndoRedoManager.h \
           MecElementReader.h \
           MecElementTree.h \
           MecElementWriter.h \
           MecFunction.h \
           MecGraph.h \
           MecGraphDialog.h \
           MecGraphView.h \
           MecObject.h \
           MecPlainTextEdit.h \
           MecPluginsManager.h \
           MecProject.h \
           MecSignal.h \
           MecSingleApplication.h \
           MecSyntaxHighlighter.h \
           MecTypeChooserDialog.h \
           MecTypeChooserDialogItem.h \
           MecUpdater.h \
           MecVariable.h \
           ../../include/mecanique/MecAbstractCodeEditor.h \
           ../../include/mecanique/MecAbstractCompiler.h \
           ../../include/mecanique/MecAbstractEditor.h \
           ../../include/mecanique/MecAbstractElementCompiler.h \
           ../../include/mecanique/MecAbstractElementEditor.h \
           ../../include/mecanique/MecAbstractElement.h \
           ../../include/mecanique/MecAbstractFunction.h \
           ../../include/mecanique/MecAbstractObject.h \
           ../../include/mecanique/MecAbstractPlugin.h \
           ../../include/mecanique/MecAbstractPluginsManager.h \
           ../../include/mecanique/MecAbstractProject.h \
           ../../include/mecanique/MecAbstractSignal.h \
           ../../include/mecanique/MecAbstractVariable.h \
           ../../include/mecanique/MecPluginError.h



SOURCES += main.cpp \
           MecCodeEditor.cpp \
           MecCompiler.cpp \
           MecCompilerDialog.cpp \
           MecDownloader.cpp \
           MecEditor.cpp \
           MecUndoRedoManager.cpp \
           MecElementReader.cpp \
           MecElementTree.cpp \
           MecElementWriter.cpp \
           MecFunction.cpp \
           MecGraph.cpp \
           MecGraphDialog.cpp \
           MecGraphView.cpp \
           MecObject.cpp \
           MecPlainTextEdit.cpp \
           MecPluginsManager.cpp \
           MecProject.cpp \
           MecSignal.cpp \
           MecSingleApplication.cpp \
           MecSyntaxHighlighter.cpp \
           MecTypeChooserDialog.cpp \
           MecTypeChooserDialogItem.cpp \
           MecUpdater.cpp \
           MecVariable.cpp


