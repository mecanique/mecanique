/*
© Quentin VIGNAUD, 2014

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
/****************************************************************************
**
** Copyright (C) 2014 Digia Plc and/or its subsidiary(-ies).
** Contact: http://www.qt-project.org/legal
**
** This file is part of the examples of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:BSD$
** You may use this file under the terms of the BSD license as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of Digia Plc and its Subsidiary(-ies) nor the names
**     of its contributors may be used to endorse or promote products derived
**     from this software without specific prior written permission.
**
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
**
** $QT_END_LICENSE$
**
****************************************************************************/

#ifndef __MECPLAINTEXTEDIT_H__
#define __MECPLAINTEXTEDIT_H__

#include <QPlainTextEdit>
#include <QPainter>
#include <QTextBlock>
#include "MecSyntaxHighlighter.h"

/**
\brief	Classe d'édition de texte brut.

Cette classe ajoute l'affichage des numéros de ligne et la correspondance des parenthèses si utilisée conjointement à MecSyntaxHighlighter.
*/
class MecPlainTextEdit : public QPlainTextEdit
{
	Q_OBJECT

public:
	///Constructeur.
	MecPlainTextEdit(QWidget *Parent=0);

	///Retourne la largeur de la colonne d'affichage des numéros de ligne.
	int lineNumberAreaWidth();

protected:
	///Évènement d'affichage de la colonne des numéros de ligne.
	void lineNumberAreaPaintEvent(QPaintEvent *Event);
	///Évènement de redimensionnement.
	void resizeEvent(QResizeEvent *Event);

private:
	///Widget-colonne d'affichage des numéros de ligne.
	QWidget *lineNumberArea;

	///Type de caractère à rechercher.
	enum CharType {
		Parenthesis,
		Brace
	};
	/**
	\brief	Indique s'il existe une parenthèse correspondante à gauche et colore celle-ci.
	*/
	bool matchLeftParenthesis(QTextBlock currentBlock, int index, int numRightParentheses, CharType type);
	/**
	\brief	Indique s'il existe une parenthèse correspondante à droite et colore celle-ci.
	*/
	bool matchRightParenthesis(QTextBlock currentBlock, int index, int numLeftParentheses, CharType type);
	/**
	\brief	Applique la coloration sur le caractère indiqué par \e pos.
	*/
	void createParenthesisSelection(int pos);

	/**
	\brief	Classe de gestion de la colonne d'affichage des lignes.
	*/
	class LineNumberArea : public QWidget
	{
	public:
		LineNumberArea(MecPlainTextEdit *Editor) : QWidget(Editor) {
			m_editor = Editor;
		}

		QSize sizeHint() const {
			return QSize(m_editor->lineNumberAreaWidth(), 0);
		}

	protected:
		void paintEvent(QPaintEvent *Event) {
			m_editor->lineNumberAreaPaintEvent(Event);
		}

	private:
		MecPlainTextEdit *m_editor;
	};

private slots:
	///Met à jour la largeur de la colonne d'affichage des lignes.
	void updateLineNumberAreaWidth();
	///Met à jour l'aire de la colonne d'affichage des lignes.
	void updateLineNumberArea(const QRect &Rect, int Dy);

	///Surligne la ligne courante.
	void highlightCurrentLine();

	///Effectue la correspondance de parenthèses.
	void matchParentheses();
};

#endif /* __MECPLAINTEXTEDIT_H__ */

