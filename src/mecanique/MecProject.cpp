/*
© Quentin VIGNAUD, 2013

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#include "MecProject.h"

MecProject::MecProject(QString ElementName, QString Title) : m_elementRole (MecAbstractElement::Project), m_parentElement (0)
{
setElementType("Project");
setElementName(ElementName);
setTitle(Title);
}

MecProject::~MecProject()
{
emit destroyed(this);
while (!m_childElements.isEmpty()) delete m_childElements.at(0);//La suppression du sous-élément le fait se retirer lui-même de la présente liste.
setParentElement(0);//Retire l'élément présent de la liste des enfants de son parent.
}

QString MecProject::elementName() const
{
return m_elementName;
}
	
QString MecProject::elementType() const
{
return m_elementType;
}
	
MecAbstractElement::ElementRole MecProject::elementRole() const
{
return m_elementRole;
}
	
MecAbstractElement* MecProject::parentElement() const
{
return m_parentElement;
}

bool MecProject::setParentElement(MecAbstractElement *ParentElement)
{
m_parentElement = 0;
setParent(0);//QObject
return (ParentElement == 0);
}
	
QString MecProject::title() const
{
return m_title;
}
	
QString MecProject::synopsis() const
{
return m_synopsis;
}

QList<MecAbstractElement*> MecProject::childElements() const
{
return m_childElements;
}
	
QList<MecAbstractObject*> MecProject::childObjects() const
{
QList<MecAbstractObject*> tempList;
for (int i=0 ; i < childElements().size() ; i++)
	{
	if (childElements().at(i)->elementRole() == MecAbstractElement::Object)
		{
		tempList.append(static_cast<MecAbstractObject*>(childElements().at(i)));
		}
	}
return tempList;
}

QList<MecAbstractFunction*> MecProject::childFunctions() const
{
QList<MecAbstractFunction*> tempList;
for (int i=0 ; i < childElements().size() ; i++)
	{
	if (childElements().at(i)->elementRole() == MecAbstractElement::Function)
		{
		tempList.append(static_cast<MecAbstractFunction*>(childElements().at(i)));
		}
	}
return tempList;
}

QList<MecAbstractSignal*> MecProject::childSignals() const
{
QList<MecAbstractSignal*> tempList;
for (int i=0 ; i < childElements().size() ; i++)
	{
	if (childElements().at(i)->elementRole() == MecAbstractElement::Signal)
		{
		tempList.append(static_cast<MecAbstractSignal*>(childElements().at(i)));
		}
	}
return tempList;
}

QList<MecAbstractVariable*> MecProject::childVariables() const
{
QList<MecAbstractVariable*> tempList;
for (int i=0 ; i < childElements().size() ; i++)
	{
	if (childElements().at(i)->elementRole() == MecAbstractElement::Variable)
		{
		tempList.append(static_cast<MecAbstractVariable*>(childElements().at(i)));
		}
	}
return tempList;
}

MecAbstractElement* MecProject::childElement(QString Address) const
{
	QStringList tempNames = Address.split('.');
	if (tempNames.size() >= 2 and tempNames.at(0) == elementName())
	{
		for (int i=0 ; i < childElements().size() ; i++)
		{
			if (childElements().at(i)->elementName() == tempNames.at(1) and tempNames.size() != 2)
			{
				tempNames.removeFirst();
				return childElements().at(i)->childElement(tempNames.join('.'));
			}
			else if (childElements().at(i)->elementName() == tempNames.at(1) and tempNames.size() == 2)
			{
				return childElements().at(i);
			}
		}
	}
	
	return 0;
}

QString MecProject::address(MecAbstractElement *RootElement) const
{
	QString tempAddress = elementName();
	const MecAbstractElement *tempParentElement = parentElement();
	while (tempParentElement != 0)
		{
		tempAddress.prepend(tempParentElement->elementName() + '.');
		if (tempParentElement == RootElement) break;
		tempParentElement = tempParentElement->parentElement();
		}
	return tempAddress;
}

void MecProject::setTitle(QString Title)
{
m_title = Title;
emit titleChanged(this);
}

void MecProject::setSynopsis(QString Synopsis)
{
m_synopsis = Synopsis;
emit synopsisChanged(this);
}

bool MecProject::setElementName(QString Name)
{
bool Ok = standardSyntax().exactMatch(Name);//Vérification de la syntaxe standarde.
if (Ok and m_parentElement != 0)//Vérification de la disponibilité du nom.
	{
	QStringList NamesUsed;
	for (int i=0 ; i < m_parentElement->childElements().size() ; i++)//On prend les noms des éléments frères.
		{
		if (m_parentElement->childElements().at(i) != this) NamesUsed.append(m_parentElement->childElements().at(i)->elementName());
		}
	
	Ok = !(NamesUsed.contains(Name));
	}

if (Ok)
	{
	m_elementName = Name;
	emit nameChanged(this);
	return true;
	}
else return false;
}

bool MecProject::setElementType(QString Type)
{
m_elementType = QString("Project");
return (Type == "Project");
}

bool MecProject::changeParentElement(MecAbstractElement *ParentElement)
{
//Fonction inutile pour un MecProject…
setParent(0);//QObject
return (ParentElement == 0);
}

QList<MecAbstractElement*>* MecProject::childList()
{
return &m_childElements;
}

void MecProject::emitChildListChanged()
{	
emit childListChanged(this);
}

bool operator==(MecProject const &a, MecProject const &b)
{
	bool result = (a.elementName() == b.elementName())
				&& (a.elementType() == b.elementType())
				&& (a.title() == b.title())
				&& (a.synopsis() == b.synopsis())
				&& (a.childElements().size() == b.childElements().size());
	
	for (int i=0 ; i < a.childElements().size() && result ; i++)
		{
		if (a.childElements().at(i)->elementRole() == b.childElements().at(i)->elementRole())
			{
			MecAbstractElement *first = a.childElements().at(i);
			MecAbstractElement *second = b.childElements().at(i);
			
			if (first->elementRole() == MecAbstractElement::Object)
				{
				result = ( *(static_cast<MecObject*>(first)) == *(static_cast<MecObject*>(second)) );
				}
			else if (first->elementRole() == MecAbstractElement::Function)
				{
				result = ( *(static_cast<MecFunction*>(first)) == *(static_cast<MecFunction*>(second)) );
				}
			else if (first->elementRole() == MecAbstractElement::Signal)
				{
				result = ( *(static_cast<MecSignal*>(first)) == *(static_cast<MecSignal*>(second)) );
				}
			else if (first->elementRole() == MecAbstractElement::Variable)
				{
				result = ( *(static_cast<MecVariable*>(first)) == *(static_cast<MecVariable*>(second)) );
				}
			
			}
		else result=false;
		}
	
	return result;
}

bool operator!=(MecProject const &a, MecProject const &b)
{
	return !(a==b);
}

