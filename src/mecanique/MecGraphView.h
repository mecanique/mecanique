/*
© Quentin VIGNAUD, 2014

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#ifndef __MECGRAPHVIEW_H__
#define __MECGRAPHVIEW_H__

#include <QLabel>
#include <QWheelEvent>

/**
\brief	Classe de visualisation d'un graphe.
*/
class MecGraphView : public QLabel
{
	Q_OBJECT
	
	public:
	/**
	\brief	Constructeur.
	
	\param	Text	Texte à afficher en attente d'un graphe.
	\param	Parent	Widget parent.
	*/
	MecGraphView(QString Text, QWidget *Parent=0);
	///Destructeur.
	~MecGraphView();
	
	signals:
	///Est émit lorsqu'un zoom avant est demandé.
	void zoomInRequested();
	///Est émit lorsqu'un zoom arrière est demandé.
	void zoomOutRequested();
	
	private:
	///Réimplémentation de QWidget::wheelEvent(QWheelEvent * event);
	void wheelEvent(QWheelEvent *Event);

};

#endif /* __MECGRAPHVIEW_H__ */

