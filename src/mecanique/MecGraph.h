/*
© Quentin VIGNAUD, 2014

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#ifndef __MECGRAPH_H__
#define __MECGRAPH_H__

#include <iostream>
#include <QtCore>
#include <QIcon>
#include "MecAbstractElement.h"
#include "MecAbstractSignal.h"

/**
\brief	Générateur de graphe d'élément.
*/
class MecGraph : public QObject
{
	Q_OBJECT

public:

	/**
	\brief	Mode de génération du graphe.
	*/
	enum Mode {	
		///Graphe "hiérachique" (le script reflète la structure organisationnelle des éléments).
		Hierarchy = 0x1,
		///Graphe de présentation (plus "clair" mais le script généré ne reflète pas la véritable hiérarchie).
		Presentation = 0x2
		};
	
	enum Options {
		///Aucune option.
		None = 0x0,
		///Afficher les connexions.
		ShowConnections = 0x1,
		///Attribuer une couleur différente à chaque connexion.
		ColorConnections = 0x2,
		///Ne pas insérer les variables dans le graphe.
		HideVariables = 0x4,
		///Ne pas utiliser de blocs de structuration.
		NoBlockStructure = 0x8
		};

	/**
	\brief	Constructeur.
	\param	RootElement	Élément racine du graphe.
	\param	Parent	Objet parent.
	*/
	MecGraph(MecAbstractElement* const RootElement=0, QObject* Parent=0);
	/**
	\brief	Destructeur.
	*/
	~MecGraph();

	/**
	\brief	Retourne l'élément racine du graphe.
	*/
	MecAbstractElement* rootElement() const;
	/**
	\brief	Assigne l'élément racine du graphe.
	*/
	void setRootElement(MecAbstractElement* const RootElement);
	
	/**
	\brief	Retourne le mode de génération du graphe.
	*/
	Mode getMode() const;
	/**
	\brief	Assigne le mode de génération du graphe.
	*/
	void setMode(Mode Mode);
	/**
	\brief	Retourne les options de génération du graphe.
	*/
	Options getOptions() const;
	/**
	\brief	Assigne les options de génération du graphe.
	*/
	void setOptions(Options Options);

	/**
	\brief	Retourne le dernier graphe généré.
	*/
	QPixmap graph() const;
	/**
	\brief	Retourne le script correspondant au graphe à générer.

	\note	Si il n'y a pas d'élément racine, retourne une chaîne vide.
	*/
	QString graphScript();

public slots:

	/**
	\brief	Fixe la mode de génération à "hiérarchique".
	*/
	void setHierarchyMode();
	/**
	\brief	Fixe le mode de génération à "présentation".
	*/
	void setPresentationMode();

	/**
	\brief	Démarre la génération du graphe.

	\note	Si rootElement vaut 0 ou qu'une génération de graphe est déjà en cours, ne fait rien.
	*/
	void start();

signals:
	/**
	\brief	Est émis lorsqu'il y a eu un changement d'élément racine.
	*/
	void rootElementChanged(MecAbstractElement* const RootElement);
	
	/**
	\brief	Est émis lorsque le mode de graphe a changé.
	*/
	void modeChanged(MecGraph::Mode Mode);
	/**
	\brief	Est émis lorsque les options ont été changées.
	*/
	void optionsChanged(MecGraph::Options Options);
	
	/**
	\brief	Signale la fin de la génération du graphe.
	*/
	void finished();
	/**
	\brief	Signale l'impossibilité de créer le graphe.
	*/
	void failed();

private:
	///Élément racine.
	MecAbstractElement *m_rootElement;
	///Processus de dessin.
	QProcess *m_graphProcess;
	
	///Mode de génération.
	Mode m_mode;
	///Options de génération.
	Options m_options;

	///Graphe généré.
	QPixmap m_graph;

	///Retourne le nom intégral de l'élément.
	static QString integralNameElement(MecAbstractElement* const Element);

	///Retourne le nœud qui correspond à cet élément.
	static QString nodeElement(MecAbstractElement* const Element);

	///Retourne le sous-graphe qui correspond à cet élément.
	static QString subGraphElement(MecAbstractElement* const Element, Options Options);

private slots:
	///Le processus de graphe est en erreur.
	void graphProcessError(QProcess::ProcessError Error);
	///Est activé lors de la clôture du processus de dessin.
	void graphProcessFinished(int ExitCode, QProcess::ExitStatus ExitStatus);
};

//Pour les flags d'options.
inline MecGraph::Options operator|(MecGraph::Options a, MecGraph::Options b) {return static_cast<MecGraph::Options>(static_cast<int>(a) | static_cast<int>(b));}
inline MecGraph::Options operator&(MecGraph::Options a, MecGraph::Options b) {return static_cast<MecGraph::Options>(static_cast<int>(a) & static_cast<int>(b));}
inline MecGraph::Options operator^(MecGraph::Options a, MecGraph::Options b) {return static_cast<MecGraph::Options>(static_cast<int>(a) ^ static_cast<int>(b));}
inline MecGraph::Options operator~(MecGraph::Options a) {return static_cast<MecGraph::Options>(~static_cast<int>(a));}

#endif /* __MECGRAPH_H__ */

