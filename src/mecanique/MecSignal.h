/*
© Quentin VIGNAUD, 2013

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#ifndef __MECSIGNAL_H__
#define __MECSIGNAL_H__

#include <QtCore>
#include <MecAbstractSignal>
#include "MecFunction.h"
#include "MecVariable.h"

/**
\brief	Classe de représentation d'un signal.
*/
class MecSignal : public MecAbstractSignal
{
	Q_OBJECT

public:
	/**
	\brief	Constructeur.
	\param	Name	Nom du signal.
	\param	ParentElement	Élément parent.
	*/
	MecSignal(QString Name, MecAbstractElement *ParentElement=0);
	///Destructeur.
	~MecSignal();

	///Retourne le nom de l'élément.
	QString elementName() const;

	///Retourne le type de l'élément.
	QString elementType() const;

	///Retourne le rôle de l'élément.
	MecAbstractElement::ElementRole elementRole() const;

	///Retourne l'élément parent.
	MecAbstractElement* parentElement() const;
	/**
	\brief	Assigne le parent.

	Un MecSignal ne peut avoir qu'un MecObject ou MecProject comme parent, la fonction ne fait rien si ce critère n'est pas satisfait.
	\return	\e true si le critère est rempli, sinon \e false.
	*/
	bool setParentElement(MecAbstractElement *ParentElement);

	///Retourne les éléments enfants.
	QList<MecAbstractElement*> childElements() const;
	///Retourne les variables enfants.
	QList<MecAbstractVariable*> childVariables() const;
	/**
	\brief	Retourne l'élément enfant situé à l'adresse indiquée.

	\param	Address	Adresse de l'élément demandé.
	\note	L'élément actuel doît être la racine de l'adresse.

	\return	L'élément situé à l'adresse indiquée, ou 0 en cas d'échec.
	*/
	MecAbstractElement* childElement(QString Address) const;

	/**
	\brief	Retourne l'adresse de l'élément.

	\param	RootElement	Élément à considérer comme la racine de l'adresse.

	L'adresse d'un élément est de la forme « nameroot.nameparentelement.nameelement » ; la fonction retourne une adresse en prenant comme racine RootElement, ou le plus grand parent connu si RootElement ne fait pas partie de l'arbre d'appartenance.
	*/
	QString address(MecAbstractElement *RootElement=0) const;

	///Retourne la liste des fonctions connectées.
	QList<MecAbstractFunction*> connectedFunctions() const;
	/**
	\brief	Ajoute une connexion avec la fonction \e Function.

	Si \e Function est déjà connectée au signal, la fonction ne fait rien.
	Si une fonction venait à être modifiée de manière incompatible avec le signal, celle-ci serait alors retirée de la liste des fonctions connectées.
	\note	La fonction ne devient pas enfant du signal.
	\see	isConnectableFunction(MecFunction*)
	\return	\e true si la connexion est possible, sinon \e false.
	*/
	bool connectFunction(MecAbstractFunction* Function);
	/**
	\brief	Supprime \e Function de la liste des fonctions connectées.
	*/
	void disconnectFunction(MecAbstractFunction* Function);
	/**
	\brief	Indique si \e Function est connectable avec ce signal.

	Pour être connectable, la fonction passée en paramètre doit avoir un nombre de variables égal ou inférieur au signal, et les variables de même position doivent avoir le même type.
	\note	\e Function n'est pas connectée par cette fonction.
	\see	connectFunction(MecFunction*)
	*/
	bool isConnectableFunction(MecAbstractFunction* Function);

public slots:
	/**
	\brief	Fixe un nouveau nom pour l'élément.

	Si \e Name n'est pas respectueux de la syntaxe standarde ou n'est pas disponible à ce niveau, \e false est retourné et l'ancien nom est conservé.
	*/
	bool setElementName(QString Name);

	/**
	\brief	Fixe le type "void" pour le signal.

	\e Type doit valoir "void" pour que la fonction retourne \e true, mais le type "void" sera attribué au signal dans tous les cas.
	*/
	bool setElementType(QString Type);


private:
	///Nom de l'élément.
	QString m_elementName;
	///Type de l'élément.
	QString m_elementType;
	///Rôle de l'élément.
	const MecAbstractElement::ElementRole m_elementRole;

	///Élément parent.
	MecAbstractElement *m_parentElement;
	///Élements enfants.
	QList<MecAbstractElement*> m_childElements;

	///Liste des fonction connectées.
	QList<MecAbstractFunction*> m_connectedFunctions;

private slots:
	/**
	\brief	Effectue les traitements nécessaires pour le changement de parent.
	*/
	bool changeParentElement(MecAbstractElement *ParentElement);
	///Retourne un pointeur sur la liste des enfants.
	QList<MecAbstractElement*>* childList();
	///Provoque l'émission du signal childListChanged(MecAbstractElement*).
	void emitChildListChanged();

	///Activé lorsque les variables enfants du signal sont modifiées.
	void childVariablesChanged();
	///Activé lorsqu'une fonction connectée au signal voit sa liste d'enfants modifiée.
	void functionModified(MecAbstractElement* ElementFunction);
	///Activé lorsqu'une fonction connectée au signal est supprimée.
	void functionDestroyed(QObject* ObjectFunction);//En mode "Qt::DirectConnection" pour éviter certaines erreurs.

};

/**
\brief	Compare toutes les propriétés ainsi que celles des éléments enfants, a l'exception de l'élément parent.
*/
bool operator==(MecSignal const &a, MecSignal const &b);
///\see	operator==(MecSignal const &a, MecSignal const &b)
bool operator!=(MecSignal const &a, MecSignal const &b);

#endif /* __MECSIGNAL_H__ */

