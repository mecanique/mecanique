/*
© Quentin VIGNAUD, 2013

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#include "MecObject.h"

MecObject::MecObject(QString Name, QString Type, MecAbstractElement *ParentElement) : m_elementRole (MecAbstractElement::Object), m_parentElement (0)
{
setParentElement(ParentElement);
setElementName(Name);
setElementType(Type);
}

MecObject::~MecObject()
{
emit destroyed(this);
while (!m_childElements.isEmpty()) delete m_childElements.at(0);//La suppression du sous-élément le fait se retirer lui-même de la présente liste.
setParentElement(0);//Retire l'élément présent de la liste des enfants de son parent.
}

QString MecObject::elementName() const
{
return m_elementName;
}
	
QString MecObject::elementType() const
{
return m_elementType;
}
	
MecAbstractElement::ElementRole MecObject::elementRole() const
{
return m_elementRole;
}
	
MecAbstractElement* MecObject::parentElement() const
{
return m_parentElement;
}

bool MecObject::setParentElement(MecAbstractElement *ParentElement)
{
if (ParentElement == 0)
	{
	changeParentElement(ParentElement);
	return true;
	}
else
	{
	if (ParentElement->elementRole() == MecAbstractElement::Project)
		{
		changeParentElement(ParentElement);
		return true;
		}
	else return false;
	}
}

QList<MecAbstractElement*> MecObject::childElements() const
{
return m_childElements;
}

QList<MecAbstractFunction*> MecObject::childFunctions() const
{
QList<MecAbstractFunction*> tempList;
for (int i=0 ; i < childElements().size() ; i++)
	{
	if (childElements().at(i)->elementRole() == MecAbstractElement::Function)
		{
		tempList.append(static_cast<MecAbstractFunction*>(childElements().at(i)));
		}
	}
return tempList;
}

QList<MecAbstractSignal*> MecObject::childSignals() const
{
QList<MecAbstractSignal*> tempList;
for (int i=0 ; i < childElements().size() ; i++)
	{
	if (childElements().at(i)->elementRole() == MecAbstractElement::Signal)
		{
		tempList.append(static_cast<MecAbstractSignal*>(childElements().at(i)));
		}
	}
return tempList;
}
	
QList<MecAbstractVariable*> MecObject::childVariables() const
{
QList<MecAbstractVariable*> tempList;
for (int i=0 ; i < childElements().size() ; i++)
	{
	if (childElements().at(i)->elementRole() == MecAbstractElement::Variable)
		{
		tempList.append(static_cast<MecAbstractVariable*>(childElements().at(i)));
		}
	}
return tempList;
}

MecAbstractElement* MecObject::childElement(QString Address) const
{
	QStringList tempNames = Address.split('.');
	if (tempNames.size() >= 2 and tempNames.at(0) == elementName())
	{
		for (int i=0 ; i < childElements().size() ; i++)
		{
			if (childElements().at(i)->elementName() == tempNames.at(1) and tempNames.size() != 2)
			{
				tempNames.removeFirst();
				return childElements().at(i)->childElement(tempNames.join('.'));
			}
			else if (childElements().at(i)->elementName() == tempNames.at(1) and tempNames.size() == 2)
			{
				return childElements().at(i);
			}
		}
	}
	
	return 0;
}

QString MecObject::address(MecAbstractElement *RootElement) const
{
	QString tempAddress = elementName();
	const MecAbstractElement *tempParentElement = parentElement();
	while (tempParentElement != 0)
		{
		tempAddress.prepend(tempParentElement->elementName() + '.');
		if (tempParentElement == RootElement) break;
		tempParentElement = tempParentElement->parentElement();
		}
	return tempAddress;
}

bool MecObject::setElementName(QString Name)
{
bool Ok = standardSyntax().exactMatch(Name);//Vérification de la syntaxe standarde.
if (Ok and m_parentElement != 0)//Vérification de la disponibilité du nom.
	{
	QStringList NamesUsed;
	for (int i=0 ; i < m_parentElement->childElements().size() ; i++)//On prend les noms des éléments frères.
		{
		if (m_parentElement->childElements().at(i) != this) NamesUsed.append(m_parentElement->childElements().at(i)->elementName());
		}
	
	Ok = !(NamesUsed.contains(Name));
	}

if (Ok)
	{
	m_elementName = Name;
	emit nameChanged(this);
	return true;
	}
else return false;
}

bool MecObject::setElementType(QString Type)
{
if (standardSyntax().exactMatch(Type))
	{
	m_elementType = Type;
	emit typeChanged(this);
	return true;
	}
else return false;
}

bool MecObject::changeParentElement(MecAbstractElement *ParentElement)
{
if (ParentElement != this)
	{
	if (m_parentElement != 0)
		{
		m_parentElement->childList()->removeAll(this);
		m_parentElement->emitChildListChanged();
		}
	
	m_parentElement = ParentElement;
	if (ParentElement != 0)
		{
		ParentElement->childList()->append(this);
		ParentElement->emitChildListChanged();
		}
	
	blockSignals(true);//On bloque les signaux pour éviter une émission inutile.
	if (!setElementName(m_elementName))//Vérification de la disponibilité du nom dans le nouveau parent.
		{
		blockSignals(false);
		for (QString tempName=m_elementName.prepend("_") ; !setElementName(tempName) ; tempName.prepend("_")) {}
		}
	blockSignals(false);
	
	
	setParent(ParentElement);//QObject
	emit parentChanged(this);
	return true;
	}
else return false;
}

QList<MecAbstractElement*>* MecObject::childList()
{
return &m_childElements;
}

void MecObject::emitChildListChanged()
{
emit childListChanged(this);
}

bool operator==(MecObject const &a, MecObject const &b)
{
	bool result = (a.elementName() == b.elementName())
				&& (a.elementType() == b.elementType())
				&& (a.childElements().size() == b.childElements().size());
	
	for (int i=0 ; i < a.childElements().size() && result ; i++)
		{
		if (a.childElements().at(i)->elementRole() == b.childElements().at(i)->elementRole())
			{
			MecAbstractElement *first = a.childElements().at(i);
			MecAbstractElement *second = b.childElements().at(i);
			
			if (first->elementRole() == MecAbstractElement::Function)
				{
				result = ( *(static_cast<MecFunction*>(first)) == *(static_cast<MecFunction*>(second)) );
				}
			else if (first->elementRole() == MecAbstractElement::Signal)
				{
				result = ( *(static_cast<MecSignal*>(first)) == *(static_cast<MecSignal*>(second)) );
				}
			else if (first->elementRole() == MecAbstractElement::Variable)
				{
				result = ( *(static_cast<MecVariable*>(first)) == *(static_cast<MecVariable*>(second)) );
				}
			
			}
		else result=false;
		}
	
	return result;
}

bool operator!=(MecObject const &a, MecObject const &b)
{
	return !(a==b);
}



