/*
© Quentin VIGNAUD, 2013 - 2014

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#include "MecElementReader.h"

MecElementReader::MecElementReader(QIODevice *Device) : QXmlStreamReader(Device), m_element (0)
{
startReading();
}

MecElementReader::MecElementReader(const QByteArray Data) : QXmlStreamReader(Data), m_element (0)
{
startReading();
}

MecElementReader::MecElementReader(const QString Data) : QXmlStreamReader(Data), m_element (0)
{
startReading();
}

MecElementReader::MecElementReader(const char *Data) : QXmlStreamReader(Data), m_element (0)
{
startReading();
}

MecAbstractElement* MecElementReader::element() const
{
return m_element;
}

void MecElementReader::startReading()
{
readNextStartElement();
m_element = readElement(0);
connect();
}
	
MecAbstractElement* MecElementReader::readElement(MecAbstractElement *ParentElement)
{
if (!atEnd())
	{
	if (name() == "project")//On travaille sur un MecProject
		{
		MecProject *tempProject = new MecProject(attributes().value("name").toString(), "");
		
		while (readNext() and (!(isEndElement() and name() == "project")) and !atEnd())//On lit le suivant et tant que ça n'est pas la fin du projet on continue.
			{
			if (name() == "title")
				{
				tempProject->setTitle(readElementText(QXmlStreamReader::SkipChildElements));
				}
			else if (name() == "synopsis")
				{
				tempProject->setSynopsis(readElementText(QXmlStreamReader::SkipChildElements));
				}
			else readElement(tempProject);
			}
		
		return tempProject;
		}
	else if (name() == "object")//On travaille sur un MecObject
		{
		MecObject *tempObject = new MecObject(attributes().value("name").toString(), attributes().value("type").toString(), ParentElement);
		
		while (readNext() and (!(isEndElement() and name() == "object")) and !atEnd())//On lit le suivant et tant que ça n'est pas la fin de l'objet on continue.
			{
			readElement(tempObject);
			}
		
		return tempObject;
		}
	else if (name() == "function")//On travaille sur une MecFunction
		{
		MecFunction *tempFunction = new MecFunction(attributes().value("name").toString(), attributes().value("type").toString(), ParentElement);
		
		while (readNext() and (!(isEndElement() and name() == "function")) and !atEnd())//On lit le suivant et tant que ça n'est pas la fin de la fonction on continue.
			{
			if (name() == "code")
				{
				tempFunction->setCode(readElementText(QXmlStreamReader::SkipChildElements));
				}
			else readElement(tempFunction);
			}
		
		return tempFunction;
		}
	else if (name() == "signal")//On travaille sur un MecSignal
		{
		MecSignal *tempSignal = new MecSignal(attributes().value("name").toString(), ParentElement);
		
		while (readNext() and (!(isEndElement() and name() == "signal")) and !atEnd())//On lit le suivant et tant que ça n'est pas la fin du signal on continue.
			{
			if (name() == "connection")
				{
				m_mapConnections.insert(tempSignal, attributes().value("function").toString());
				readNext();
				}
			else readElement(tempSignal);
			}
		
		return tempSignal;
		}
	else if (name() == "variable")//On travaille sur une MecVariable
		{
		MecVariable *tempVariable = new MecVariable(attributes().value("name").toString(), attributes().value("type").toString(), QVariant(attributes().value("default").toString()), ParentElement);
		
		while (readNext() and (!(isEndElement() and name() == "variable")) and !atEnd())//On lit le suivant et tant que ça n'est pas la fin de la variable on continue.
			{/* Une variable ne peut avoir d'enfants */}		

		return tempVariable;
		}
	}

return 0;
}

void MecElementReader::connect()
{
if (m_element == 0) return;
QMapIterator<MecSignal*, QString> tempIterator(m_mapConnections);
while (tempIterator.hasNext())
	{
	tempIterator.next();
	QStringList functionAddress = tempIterator.value().split(".");//On découpe l'adresse en une liste de noms.
	MecAbstractElement *tempParentElement = m_element;
	bool Ok = (m_element->elementName() == functionAddress.first());//Variable de validité.
	for (int i=1 ; i < functionAddress.size() and Ok ; i++)//i commence à 1 car l'élément d'origine a déjà été validé.
		{
		bool Find = false;
		int j = 0;
		while (j < tempParentElement->childElements().size() and !Find)
			{
			if (tempParentElement->childElements().at(j)->elementName() == functionAddress.at(i))
				{
				if (i == (functionAddress.size() - 1))//Si c'est le dernier nom de l'adresse, c'est la fonction en question, sinon c'est un élément parent de celle-ci.
					{
					try
						{
						if (tempParentElement->childElements().at(j)->elementRole() == MecAbstractElement::Function)
							{
							MecFunction *tempFunction = dynamic_cast<MecFunction*>(tempParentElement->childElements().at(j));
							tempIterator.key()->connectFunction(tempFunction);
							}
						else std::cout << "The element at the address '" << tempIterator.value().toStdString() << "' is not a function." << std::endl;
						}
					catch (const std::bad_cast& bc)
						{
						std::cout << "The element at the address '" << tempIterator.value().toStdString() << "' is not a function. (bad_cast exception : " << bc.what() << ")" << std::endl;
						}
					}
				else tempParentElement = tempParentElement->childElements().at(j);
				Find = true;
				}
			j++;
			}
		if (j == tempParentElement->childElements().size() and !Find) Ok = false;//Si la liste d'enfants est épuisée et que l'élément parent suivant ou la fonction n'a pas été trouvée, on abandonne.
		}
	if (!Ok) std::cout << "No element at the address '" << tempIterator.value().toStdString() << "'. Abort." << std::endl;
	}
}



