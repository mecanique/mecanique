/*
© Quentin VIGNAUD, 2014

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#ifndef __MECUPDATER_H__
#define __MECUPDATER_H__

#include <QtCore>
#include <QtNetwork>
#include <QtWidgets>

#include "MecPluginsManager.h"
#include "MecDownloader.h"

/**
\brief	Classe de gestion des mises à jour.

\note	Classe modifiée en attente de d'établissement d'une procédure stable, voir updateReceived().
*/
class MecUpdater : public QObject
{
	Q_OBJECT

public:

	/**
	\brief	Structure de symbolisation d'un téléchargement.
	*/
	struct Download {
		Download();
		Download(QString Name, QString Src, qint64 Size, QByteArray Hash);
		QString name;
		QString src;
		qint64 size;
		QByteArray hash;
	};

	/**
	\brief	Structure de symbolisation d'une exécution.
	*/
	struct Execute {
		Execute();
		Execute(QString Name, QString Parameters, bool CloseAll, bool Environment);
		QString name;
		QString parameters;
		bool closeAll;
		bool environment;
	};

	/**
	\brief	Constructeur.
	*/
	MecUpdater(MecPluginsManager *PluginsManager);
	/**
	\brief	Destructeur.
	*/
	~MecUpdater();

public slots:
	/**
	\brief	Exécute le processus de vérification de mise à jour.
	*/
	void run();
	/**
	\brief	Arrête le processus de mise à jour.
	*/
	void cancel();

private:
	///Gestionnaire de greffons.
	MecPluginsManager *m_pluginsManager;
	///Gestionnaire de réseau.
	QNetworkAccessManager *m_network;
	///Réponse réseau à propos des mises à jour.
	QNetworkReply *m_updateReply;

	/**
	\brief	Indique si la MÀJ est une mise à jour (léger) ou mise à niveau (lourd).
	*/
	bool m_isUpgrade;
	///Taille totale de téléchargement à effectuer.
	qint64 m_totalDownloadSize;
	///Quantité de données téléchargées.
	qint64 m_downloadedSize;
	///Actions de mises à jour.
	QVector<void*> m_actions;
	///Spécifie si l'action de MÀJ correspondante dans m_actions est un téléchargement (true) ou une exécution (false).
	QVector<bool> m_actionsType;
	///Index courant dans la liste des actions de MÀJ.
	int m_indexActions;

	///Boîte de dialogue d'indication de progression.
	QProgressDialog *m_progressDialog;

	///Téléchargement courant.
	MecDownloader *m_currentDownload;
	///Exécution courante.
	QProcess *m_currentExecute;

	///Assigne toutes les données de mise à jour selon la structure XML donnée.
	void parseUpdates(QByteArray UpdatesXml);

	///Demande à l'utilisateur s'il souhaite effectuer les MÀJ.
	bool askUser();

	///Effectue l'action de MÀJ indiquée par m_indexActions.
	void doAction();

private slots:
	///Est activé lors de la réception de la réponse à propos des mises à jour.
	void updatesReceived();

	///Est activé lorsq'un téléchargement progresse.
	void downloadProgress(qint64 Number);
	///Est activé lorsq'un téléchargement est terminé.
	void downloadFinished();

	///Est activé lorsq'une exécution est terminée.
	void execFinished();
	///Est activé lorsq'un téléchargement progresse.
	void appQuit();
};

#endif /* __MECUPDATER_H__ */

