/*
© Quentin VIGNAUD, 2013

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#ifndef __MECOBJECT_H__
#define __MECOBJECT_H__

#include <QtCore>
#include <MecAbstractObject>
#include "MecFunction.h"
#include "MecSignal.h"
#include "MecVariable.h"

/**
\brief	Classe de représentation d'un objet.
*/
class MecObject : public MecAbstractObject
{
	Q_OBJECT

public:
	/**
	\brief	Constructeur.
	\param	Name	Nom de l'objet.
	\param	Type	Type de l'objet.
	*/
	MecObject(QString Name, QString Type, MecAbstractElement *ParentElement=0);
	///Destructeur.
	~MecObject();

	///Retourne le nom de l'élément.
	QString elementName() const;

	///Retourne le type de l'élément.
	QString elementType() const;

	///Retourne le rôle de l'élément.
	MecAbstractElement::ElementRole elementRole() const;

	///Retourne l'élément parent.
	MecAbstractElement* parentElement() const;
	/**
	\brief	Assigne le parent.

	Un MecObjet ne peut avoir qu'un MecProject comme parent, la fonction ne fait rien si ce critère n'est pas satisfait.
	\return	\e true si le critère est rempli, sinon \e false.
	*/
	bool setParentElement(MecAbstractElement *ParentElement);

	///Retourne les éléments enfants.
	QList<MecAbstractElement*> childElements() const;
	///Retourne les fonctions enfants.
	QList<MecAbstractFunction*> childFunctions() const;
	///Retourne les signaux enfants.
	QList<MecAbstractSignal*> childSignals() const;
	///Retourne les variables enfants.
	QList<MecAbstractVariable*> childVariables() const;
	/**
	\brief	Retourne l'élément enfant situé à l'adresse indiquée.

	\param	Address	Adresse de l'élément demandé.
	\note	L'élément actuel doît être la racine de l'adresse.

	\return	L'élément situé à l'adresse indiquée, ou 0 en cas d'échec.
	*/
	MecAbstractElement* childElement(QString Address) const;

	/**
	\brief	Retourne l'adresse de l'élément.

	\param	RootElement	Élément à considérer comme la racine de l'adresse.

	L'adresse d'un élément est de la forme « nameroot.nameparentelement.nameelement » ; la fonction retourne une adresse en prenant comme racine RootElement, ou le plus grand parent connu si RootElement ne fait pas partie de l'arbre d'appartenance.
	*/
	QString address(MecAbstractElement *RootElement=0) const;

public slots:
	/**
	\brief	Fixe un nouveau nom pour l'élément.

	Si \e Name n'est pas respectueux de la syntaxe standarde ou n'est pas disponible à ce niveau, \e false est retourné et l'ancien nom est conservé.
	*/
	bool setElementName(QString Name);

	/**
	\brief	Fixe un nouveau type pour l'élément.

	Si \e Type n'est pas respectueux de la syntaxe standarde, \e false est retourné et l'ancien type est conservé.
	*/
	bool setElementType(QString Type);

private:
	///Nom de l'élément.
	QString m_elementName;
	///Type de l'élément.
	QString m_elementType;
	///Rôle de l'élément.
	const MecAbstractElement::ElementRole m_elementRole;

	///Élément parent.
	MecAbstractElement *m_parentElement;
	///Élements enfants.
	QList<MecAbstractElement*> m_childElements;


private slots:
	/**
	\brief	Effectue les traitements nécessaires pour le changement de parent.
	*/
	bool changeParentElement(MecAbstractElement *ParentElement);
	///Retourne un pointeur sur la liste des enfants.
	QList<MecAbstractElement*>* childList();
	///Provoque l'émission du signal childListChanged(MecAbstractElement*).
	void emitChildListChanged();
};

/**
\brief	Compare toutes les propriétés ainsi que celles des éléments enfants, a l'exception de l'élément parent.
*/
bool operator==(MecObject const &a, MecObject const &b);
///\see	operator==(MecObject const &a, MecObject const &b)
bool operator!=(MecObject const &a, MecObject const &b);

#endif /* __MECOBJECT_H__ */

