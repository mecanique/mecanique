/*
© Quentin VIGNAUD, 2014

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#ifndef __MECTYPECHOOSERDIALOG_H__
#define __MECTYPECHOOSERDIALOG_H__

#include <QtWidgets>
#include "MecTypeChooserDialogItem.h"

/**
\brief	Fenêtre de sélection d'un type d'élément.
*/
class MecTypeChooserDialog : public QDialog
{
	Q_OBJECT

public:
	/**
	\brief	Constructeur.
	*/
	MecTypeChooserDialog(QWidget * Parent=0, Qt::WindowFlags F=0);
	///Destructeur.
	~MecTypeChooserDialog();

	/**
	\brief	Ajoute un type en proposition.
	*/
	void addType(QString Type);

	/**
	\brief	Retourne le type sélectionné.

	\return	Le type sélectionné, ou une chaîne vide si aucun.
	*/
	QString getSelectedType() const;

public slots:
	/**
	\brief	Exécute la fenêtre un mode modal.
	*/
	int exec();

private:
	QGridLayout *layoutMain;

	QScrollArea *scrollAreaTypes;
	QWidget *widgetTypes;
	QGridLayout *layoutTypes;

	QPushButton *buttonCancel;

	QList<MecTypeChooserDialogItem*> m_items;

private slots:
	/**
	\brief	Est appelé lorsqu'un type est sélectionné.
	*/
	void typeSelected(QString Type);


};

#endif /* __MECTYPECHOOSERDIALOG_H__ */

