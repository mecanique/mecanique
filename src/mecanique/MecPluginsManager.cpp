/*
© Quentin VIGNAUD, 2013

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#include "MecPluginsManager.h"
#include <iostream>

MecPluginsManager::MecPluginsManager(QWidget *Parent) : QMainWindow(Parent)
{

setWindowTitle(tr("Plugins manager"));
setMinimumSize(850, 300);

treeWidgetPlugins = new QTreeWidget(this);
	treeWidgetPlugins->setHeaderLabels(QStringList() << tr("Title") << tr("Name") << tr("Version") << tr("Role supported") << tr("Type supported"));
	treeWidgetPlugins->header()->setSectionResizeMode(QHeaderView::ResizeToContents);
	connect(treeWidgetPlugins, SIGNAL(itemActivated(QTreeWidgetItem*, int)), SLOT(itemActivated(QTreeWidgetItem*, int)));
setCentralWidget(treeWidgetPlugins);
}

MecPluginsManager::~MecPluginsManager()
{
while (m_pluginsLoaders.size() != 0) m_pluginsLoaders.takeFirst()->unload();
}
	
QList<MecAbstractPlugin*> MecPluginsManager::plugins() const
{
return m_plugins;
}

MecAbstractPlugin* MecPluginsManager::plugin(const MecAbstractElement::ElementRole Role, const QString Type, const QString Name, const double Version) const
{
for (int i=0 ; i < m_plugins.size() ; i++)//On cherche d'abord un plugin spécialisé.
	{
	bool Ok = (m_plugins.at(i)->role() == Role and m_plugins.at(i)->type() == Type);
	if (Ok and !Name.isEmpty()) Ok = (m_plugins.at(i)->name() == Name);
	if (Ok and Version != 0) Ok = (m_plugins.at(i)->version() == Version);
	if (Ok) return m_plugins.at(i);
	}
for (int i=0 ; i < m_plugins.size() ; i++)//Sinon le plugin standard de base (essentiellement utile pour les fonctions, n'ayant souvent pas d'éditeur spécialisé en fonction de leur type de retour).
	{
	if (m_plugins.at(i)->role() == Role and m_plugins.at(i)->type() == QString()) return m_plugins.at(i);
	}
return 0;
}

QString MecPluginsManager::roleText(MecAbstractElement::ElementRole Role, bool Translate)
{
switch (Role)
	{
	case MecAbstractElement::Project :
		if (Translate) return tr("Project");
		else return "Project";
		break;
	case MecAbstractElement::Object :
		if (Translate) return tr("Object");
		else return "Object";
		break;
	case MecAbstractElement::Function :
		if (Translate) return tr("Function");
		else return "Function";
		break;
	case MecAbstractElement::Signal :
		if (Translate) return tr("Signal");
		else return "Signal";
		break;
	case MecAbstractElement::Variable :
		if (Translate) return tr("Variable");
		else return "Variable";
		break;
	default :
		return QString();
		break;
	}
}

void MecPluginsManager::aboutPlugin(MecAbstractPlugin *Plugin)
{
QDialog *aboutDialog = new QDialog(0, Qt::WindowTitleHint | Qt::WindowSystemMenuHint);
aboutDialog->setWindowTitle(Plugin->title());

QVBoxLayout *aboutLayout = new QVBoxLayout(aboutDialog);

QLabel *aboutIcon = new QLabel(aboutDialog);
if (Plugin->type().isEmpty()) aboutIcon->setPixmap(QIcon(":/share/icons/" + roleText(Plugin->role(), false).toLower() + ".png").pixmap(64, 64));
else aboutIcon->setPixmap(QIcon(":/share/icons/types/" + Plugin->type() + ".png").pixmap(64, 64));
aboutIcon->setAlignment(Qt::AlignCenter);
aboutLayout->addWidget(aboutIcon);

QLabel *aboutText = new QLabel(aboutDialog);
aboutText->setText("<h3>" + Plugin->title() + "</h3><p>" + Plugin->name() + " – " + QString::number(Plugin->version(), 'f') + "</p><p>" + Plugin->description() + "</p><p><small>" + Plugin->copyright() + "</small></p>");
aboutText->setAlignment(Qt::AlignCenter);
aboutLayout->addWidget(aboutText);

QTabWidget *tabCredits = new QTabWidget(aboutDialog);
		tabCredits->setWindowTitle(Plugin->title());
		tabCredits->setMinimumSize(350, 250);
	QTextEdit *textDeveloppers = new QTextEdit(tabCredits);
		textDeveloppers->setPlainText(Plugin->developpers());
		textDeveloppers->setReadOnly(true);
		tabCredits->addTab(textDeveloppers, tr("Written by"));
	QTextEdit *textDocumentalists = new QTextEdit(tabCredits);
		textDocumentalists->setPlainText(Plugin->documentalists());
		textDocumentalists->setReadOnly(true);
		tabCredits->addTab(textDocumentalists, tr("Documented by"));
	QTextEdit *textTranslators = new QTextEdit(tabCredits);
		textTranslators->setPlainText(Plugin->translators());
		textTranslators->setReadOnly(true);
		tabCredits->addTab(textTranslators, tr("Translated by"));
aboutLayout->addWidget(tabCredits);

QHBoxLayout *aboutButtonsLayout = new QHBoxLayout();
QPushButton *aboutClose = new QPushButton(QIcon::fromTheme("cancel"), tr("&Close"), aboutDialog);
	connect(aboutClose, SIGNAL(clicked(bool)), aboutDialog, SLOT(close()));
aboutButtonsLayout->addStretch();
aboutButtonsLayout->addWidget(aboutClose);

aboutLayout->addLayout(aboutButtonsLayout);

aboutDialog->exec();
}
	
void MecPluginsManager::load()
{
QDir tempPluginsDir(qApp->applicationDirPath());
tempPluginsDir.cd("../lib/mecanique");

#ifndef Q_OS_WIN
	QStringList tempListPathPlugins = tempPluginsDir.entryList(QDir::Files | QDir::Executable);
#else
	QStringList tempListPathPlugins = tempPluginsDir.entryList(QDir::Files);
#endif

std::cout << tempListPathPlugins.size() << " potential plugins found." << std::endl;
for (int i=0 ; i < tempListPathPlugins.size() ; i++)
	{
		std::cout << "Load of \"" << tempPluginsDir.absoluteFilePath(tempListPathPlugins.at(i)).toStdString() << "\"" << std::endl;
		QPluginLoader *tempPluginLoader = new QPluginLoader(tempPluginsDir.absoluteFilePath(tempListPathPlugins.at(i)), this);
		QObject *tempPluginInstance = tempPluginLoader->instance();
		
		std::cout << "Error(?) : " << tempPluginLoader->errorString().toStdString() << std::endl;
		
		if (tempPluginInstance != 0)
		{
			MecAbstractPlugin *tempPlugin = qobject_cast<MecAbstractPlugin*>(tempPluginInstance);
			if (tempPlugin != 0)
			{
				m_pluginsLoaders.append(tempPluginLoader);
				m_plugins.append(tempPlugin);
				
				QTreeWidgetItem *tempItem = new QTreeWidgetItem(treeWidgetPlugins, QStringList() << tempPlugin->title() << tempPlugin->name() << QString::number(tempPlugin->version(), 'g') << roleText(tempPlugin->role()) << tempPlugin->type());
				tempItem->setIcon(0, QIcon(":/share/icons/types/" + tempPlugin->type() + ".png"));
				if (tempItem->text(4).isEmpty())
					{
					tempItem->setText(4, tr("<all types>"));
					tempItem->setIcon(0, QIcon(":/share/icons/" + roleText(tempPlugin->role(), false).toLower() + ".png"));
					}
			}
			else
			{
				std::cout << "Cast plugin error, load failed." << std::endl;
				delete tempPluginLoader;
				delete tempPluginInstance;
			}
		}
		else
		{
			std::cout << "That's not a plugin, abort." << std::endl;
			delete tempPluginLoader;
			delete tempPluginInstance;
		}
	}
}

void MecPluginsManager::itemActivated(QTreeWidgetItem *Item, int Column)
{
for (int i=0 ; i < m_plugins.size() ; i++)
	{
	if (m_plugins.at(i)->name() == Item->text(1)) aboutPlugin(m_plugins.at(i));
	}
}

