/*
© Quentin VIGNAUD, 2013

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#ifndef __MECPLUGINSMANAGER_H__
#define __MECPLUGINSMANAGER_H__

#include <MecAbstractPluginsManager>
#include <QtWidgets>

/**
\brief	Gestionnaire de plugins.
*/
class MecPluginsManager : public QMainWindow, public MecAbstractPluginsManager
{
	Q_OBJECT

public:
	/**
	\brief	Constructeur.
	*/
	MecPluginsManager(QWidget *Parent=0);
	/**
	\brief	Destructeur.

	Décharge tous les plugins, si un plugin ne peut être déchargé il ne le sera pas, cependant cela peut entraîner des erreurs de segmentation lors de la clôture du programme.
	*/
	~MecPluginsManager();

	///Retourne une liste des plugins.
	QList<MecAbstractPlugin*> plugins() const;
	/**
	\brief	Retourne le plugin correspondant.
	\param	Role	Rôle d'élément du plugin.
	\param	Type	Type d'élément du plugin.
	\param	Name	Nom du plugin.
	\param	Version	Version du plugin.

	Si aucun plugin spécialisé pour le type de l'élément n'a été trouvé, une recherche sur le plugin général de l'élément est effectuée (cela revient à donner un chaîne vide en paramètre pour Type).
	\return	Le plugin correspondant, ou bien 0 si aucun n'a été trouvé.
	*/
	MecAbstractPlugin* plugin(const MecAbstractElement::ElementRole Role, const QString Type, const QString Name=QString(), const double Version=0) const;

	/**
	\brief	Retourne le texte correspondant au rôle d'élément.

	\param	Role	Rôle à traiter.
	\param	Translate	Faut-il traduire le nom.
	*/
	static QString roleText(MecAbstractElement::ElementRole Role, bool Translate=true);

	///Affiche une boîte de dialogue « À propos » pour un plugin spécifié.
	static void aboutPlugin(MecAbstractPlugin *Plugin);

public slots:
	///Procède au chargement des plugins.
	void load();

	///Activation de l'item d'un plugin.
	void itemActivated(QTreeWidgetItem *Item, int Column);


signals:


private:
	///Plugins.
	QList<MecAbstractPlugin*> m_plugins;
	///Chargeurs de plugins.
	QList<QPluginLoader*> m_pluginsLoaders;

	///Arbre de vue des plugins.
	QTreeWidget *treeWidgetPlugins;


private slots:


};


#endif /* __MECPLUGINSMANAGER_H__ */

