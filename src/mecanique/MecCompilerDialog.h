/*
© Quentin VIGNAUD, 2013

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#ifndef __MECCOMPILERDIALOG_H__
#define __MECCOMPILERDIALOG_H__

#include <QtWidgets>
#include "MecCompiler.h"

/**
\brief	Classe de dialogue utilisateur avec le compilateur.
*/
class MecCompilerDialog : public QWidget
{
	Q_OBJECT

public:
	///Constructeur.
	MecCompilerDialog(MecProject* const Project, MecPluginsManager* const PluginsManager, QString DefaultBuildDirectory=QString(), QWidget *Parent=0, Qt::WindowFlags Flags=0);
	///Destructeur.
	~MecCompilerDialog();

	///Retourne le projet sujet à compilation.
	MecProject* project() const;

	///Affiche les informations relatives à la licence de l'œuvre compilée et retourne l'accord de l'utilisateur (true pour l'acceptation, false pour le rejet).
	static bool showLicenceInfos();

public slots:
	///Démarre la compilation.
	void start();
	///Demande à l'utilisateur un répertoire pour la compilation.
	void chooseBuildDirectory();


signals:

protected:
	/**
	\brief	Demande de fermeture.

	Arrête la compilation si elle est en cours et accepte l'évènement de fermeture.
	*/
	void closeEvent(QCloseEvent *Event);


private:
	///Compilateur.
	MecCompiler *m_compiler;

	///Label du répertoire de compilation.
	QLabel *labelBuildDirectory;
	///Ligne d'édition du répertoire de compilation.
	QLineEdit *lineEditBuildDirectory;
	///Bouton de choix du répertoire de compilation.
	QPushButton *pushButtonChooseBuildDirectory;
	///Bouton de lancement et d'arrêt de la compilation.
	QPushButton *pushButtonCompilation;
	///Label des informations de compilation.
	QLabel *labelCompilationLog;
	///Zone de texte des informations de compilation.
	QTextEdit *textEditCompilationLog;
	///Bouton de lancement de l'exécutable compilé.
	QPushButton *pushButtonExec;

	///Layout principal.
	QGridLayout *layoutMain;


private slots:
	///Activé en début de compilation.
	void started();
	///Activé à chaque nouvelle information disponible.
	void newsAvailable();
	///Activé en fin de compilation.
	void terminate(MecCompiler::Error Error);

	///Lance l'exécutable généré.
	void execute();

};

#endif /* __MECCOMPILERDIALOG_H__ */

