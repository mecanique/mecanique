######################################################################
# Project file implemented for qmake (version 3 with Qt 5)
######################################################################

include(../plugin/plugin.pri)
TARGET = standarddoublevariable
DEPENDPATH += .
INCLUDEPATH += .
RESOURCES += resources.qrc
TRANSLATIONS = translations/standarddoublevariable.fr.ts \
               translations/standarddoublevariable.de.ts

OTHER_FILES += metadata.json

HEADERS += \
                MecStandardDoubleVariablePlugin.h \
                MecStandardDoubleVariableCompiler.h \
                MecStandardDoubleVariableEditor.h


SOURCES += \
                MecStandardDoubleVariablePlugin.cpp \
                MecStandardDoubleVariableCompiler.cpp \
                MecStandardDoubleVariableEditor.cpp

