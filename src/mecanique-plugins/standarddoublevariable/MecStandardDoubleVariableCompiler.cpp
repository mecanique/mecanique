/*
© Quentin VIGNAUD, 2013

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#include "MecStandardDoubleVariableCompiler.h"

MecStandardDoubleVariableCompiler::MecStandardDoubleVariableCompiler(MecAbstractVariable* const Variable, MecAbstractCompiler* const MainCompiler) : MecVariableCompiler(Variable, MainCompiler)
{
}

MecStandardDoubleVariableCompiler::~MecStandardDoubleVariableCompiler()
{
}
	
QString MecStandardDoubleVariableCompiler::headerInstructions()
{
QString tempString;
if (variable()->parentElement() != 0)
	{
	if (variable()->parentElement()->elementRole() == MecAbstractElement::Project or variable()->parentElement()->elementRole() == MecAbstractElement::Object)
		{
		tempString = variable()->elementType() + " " + variable()->elementName() + ";\n";
		}
	else if (variable()->parentElement()->elementRole() == MecAbstractElement::Function)
		{
		tempString = variable()->elementType() + " " + variable()->elementName();
		
		if (variable()->defaultValue().canConvert(QMetaType::Double))
			{
			tempString += "=" + QString::number(variable()->defaultValue().toDouble(), 'f');
			}
		}
	else if (variable()->parentElement()->elementRole() == MecAbstractElement::Signal)
		{
		tempString = variable()->elementType() + " " + variable()->elementName();
		}
	}
return tempString;
}

QString MecStandardDoubleVariableCompiler::sourceInstructions()
{
QString tempString;
if (variable()->parentElement() != 0)
	{
	if (variable()->parentElement()->elementRole() == MecAbstractElement::Project or variable()->parentElement()->elementRole() == MecAbstractElement::Object)
		{
		tempString = variable()->elementName();
		if (variable()->defaultValue().canConvert(QMetaType::Double))
			{
			tempString += " = " + QString::number(variable()->defaultValue().toDouble(), 'f') + ";\n";
			}
		else tempString += " = 0;\n";
		}
	}
return tempString;
}


