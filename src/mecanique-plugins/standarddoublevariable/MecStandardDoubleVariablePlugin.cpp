/*
© Quentin VIGNAUD, 2013

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#include "MecStandardDoubleVariablePlugin.h"

MecStandardDoubleVariablePlugin::MecStandardDoubleVariablePlugin() : MecPlugin(QString("standarddoublevariable"), __STANDARDDOUBLEVARIABLE_VERSION__, MecAbstractElement::Variable, QString("double"), QString("Standard double variable"))
{
}

MecStandardDoubleVariablePlugin::~MecStandardDoubleVariablePlugin()
{
}

QString MecStandardDoubleVariablePlugin::description() const
{
return QString(tr("Standard plugin for managing decimal variables."));
}

QString MecStandardDoubleVariablePlugin::copyright() const
{
return QString("Copyright © 2013 – 2015 Quentin VIGNAUD");
}

QString MecStandardDoubleVariablePlugin::developpers() const
{
return QString("Quentin VIGNAUD <quentin.vignaud@mecanique.cc>\n");
}

QString MecStandardDoubleVariablePlugin::documentalists() const
{
return QString("Quentin VIGNAUD <quentin.vignaud@mecanique.cc>\n");
}

QString MecStandardDoubleVariablePlugin::translators() const
{
return QString("Quentin VIGNAUD <quentin.vignaud@mecanique.cc>\n");
}

MecAbstractElementEditor* MecStandardDoubleVariablePlugin::elementEditor(MecAbstractElement* const Element, MecAbstractEditor* MainEditor)
{
if (Element->elementRole() == MecAbstractElement::Variable and Element->elementType() == "double")
	{
	MecStandardDoubleVariableEditor *tempEditor = new MecStandardDoubleVariableEditor(static_cast<MecAbstractVariable*>(Element), MainEditor);
	return tempEditor;
	}
else return 0;
}

MecAbstractElementCompiler* MecStandardDoubleVariablePlugin::elementCompiler(MecAbstractElement* const Element, MecAbstractCompiler* const MainCompiler)
{
if (Element->elementRole() == MecAbstractElement::Variable and Element->elementType() == "double")
	{
	MecStandardDoubleVariableCompiler *tempCompiler = new MecStandardDoubleVariableCompiler(static_cast<MecAbstractVariable*>(Element), MainCompiler);
	return tempCompiler;
	}
else return 0;
}
