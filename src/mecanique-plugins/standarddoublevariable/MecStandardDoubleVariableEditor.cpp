/*
© Quentin VIGNAUD, 2013

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#include "MecStandardDoubleVariableEditor.h"

MecStandardDoubleVariableEditor::MecStandardDoubleVariableEditor(MecAbstractVariable* const Variable, MecAbstractEditor* MainEditor, QWidget * Parent, Qt::WindowFlags F) : MecVariableEditor(Variable, MainEditor, Parent, F)
{
comboBoxType->setEnabled(false);

labelDefaultValue = new QLabel(tr("Default value:"), this);
doubleSpinBoxDefaultValue = new QDoubleSpinBox(this);
	doubleSpinBoxDefaultValue->setRange(DBL_MIN, DBL_MAX);
	doubleSpinBoxDefaultValue->setDecimals(3);
	doubleSpinBoxDefaultValue->setMaximumWidth(100);
	connect(doubleSpinBoxDefaultValue, SIGNAL(valueChanged(double)), SLOT(setDefaultValue(double)));

layoutMain->addWidget(labelDefaultValue);
layoutMain->addWidget(doubleSpinBoxDefaultValue);

defaultValueVariableChanged(variable());
}

MecStandardDoubleVariableEditor::~MecStandardDoubleVariableEditor()
{
}

bool MecStandardDoubleVariableEditor::isContentEditable() const
{
	return doubleSpinBoxDefaultValue->isEnabled();
}

void MecStandardDoubleVariableEditor::defaultValueVariableChanged(MecAbstractVariable *Variable)
{
if (Variable != variable()) return;
doubleSpinBoxDefaultValue->setValue(Variable->defaultValue().toDouble());
}

void MecStandardDoubleVariableEditor::setContentEditable(bool Editable)
{
	doubleSpinBoxDefaultValue->setEnabled(Editable);
}

void MecStandardDoubleVariableEditor::setDefaultValue(double Number)
{
variable()->setDefaultValue(QVariant(Number));
mainEditor()->addEditStep(tr("Change default value of “%1” to “%2”").arg(element()->elementName()).arg(QString::number(Number, 'f')));
}


