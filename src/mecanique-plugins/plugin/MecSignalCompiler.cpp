/*
© Quentin VIGNAUD, 2013

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#include "MecSignalCompiler.h"

MecSignalCompiler::MecSignalCompiler(MecAbstractSignal* const Signal, MecAbstractCompiler* const MainCompiler) : MecElementCompiler(Signal, MainCompiler), m_signal (Signal)
{
for (int i=0 ; i < signal()->childElements().size() ; i++)
	{
	MecAbstractPlugin *tempPlugin = mainCompiler()->pluginsManager()->plugin(signal()->childElements().at(i)->elementRole(), signal()->childElements().at(i)->elementType());
	if (tempPlugin != 0)
		{
		MecAbstractElementCompiler *tempCompiler = tempPlugin->elementCompiler(signal()->childElements().at(i), mainCompiler());
		if (tempCompiler != 0)
			{
			addSubCompiler(tempCompiler);
			}
		else
			{
			addPluginsError(MecPluginError(MecPluginError::CastFailed, tempPlugin->name(), tempPlugin->version(), signal()->childElements().at(i)->elementRole(), signal()->childElements().at(i)->elementType(), signal()->childElements().at(i)));
			}
		}
	else
		{
		addPluginsError(MecPluginError(MecPluginError::Unavailable, "", 0, signal()->childElements().at(i)->elementRole(), signal()->childElements().at(i)->elementType(), signal()->childElements().at(i)));
		}
	}

//Établissement de m_concreteSubCompilers.
for (int i=0 ; i < subCompilers().size() ; i++)
	{
	m_concreteSubCompilers.append(static_cast<MecElementCompiler*>(subCompilers().at(i)));
	}

QList<MecAbstractElementCompiler*> tempList = subCompilers();//Établissement de m_recConcreteSubCompilers.
for (int i=0 ; i < tempList.size() ; i++)
	{
	tempList.append(tempList.at(i)->subCompilers());
	}
for (int i=0 ; i < tempList.size() ; i++)
	{
	m_recConcreteSubCompilers.append(static_cast<MecElementCompiler*>(tempList.at(i)));
	}
}

MecSignalCompiler::~MecSignalCompiler()
{
}
	
MecAbstractSignal* MecSignalCompiler::signal() const
{
return m_signal;
}

QList<QResource*> MecSignalCompiler::resources()
{
return QList<QResource*>();
}

QString MecSignalCompiler::projectInstructions()
{
return QString();
}
	
QString MecSignalCompiler::header()
{
return QString();
}

QString MecSignalCompiler::source()
{
return QString();
}

QString MecSignalCompiler::preprocessorInstructions()
{
return QString();
}

QString MecSignalCompiler::headerInstructions()
{
QString tempString;

tempString += "void " + signal()->elementName() + "(";

for (int i=0 ; i < concreteSubCompilers().size() ; i++)
	{
	tempString += concreteSubCompilers().at(i)->headerInstructions();
	if (i != concreteSubCompilers().size() - 1) tempString += ", ";
	}

tempString += ");\n";

return tempString;
}

QString MecSignalCompiler::sourceInstructions()
{
QString tempString;

for (int i=0 ; i < signal()->connectedFunctions().size() ; i++)
	{
	tempString += "connect(";
	if (element()->parentElement()->elementRole() == MecAbstractElement::Object) tempString += "m_" + element()->parentElement()->elementName();
	else tempString += "this";
	
	tempString += ", SIGNAL(" + signal()->elementName() + "(";
	
	for (int j=0 ; j < signal()->childElements().size() ; j++)
		{
		tempString += signal()->childElements().at(j)->elementType();
		if (j != signal()->childElements().size() - 1) tempString += ", ";
		}
	
	tempString += ")), ";
	
	if (signal()->connectedFunctions().at(i)->parentElement()->elementRole() == MecAbstractElement::Object) tempString += "m_" + signal()->connectedFunctions().at(i)->parentElement()->elementName();
	else tempString += "this";
	
	tempString += ", SLOT(" + signal()->connectedFunctions().at(i)->elementName() + "(";
	
	for (int j=0 ; j < signal()->connectedFunctions().at(i)->childElements().size() ; j++)
		{
		tempString += signal()->connectedFunctions().at(i)->childElements().at(j)->elementType();
		if (j != signal()->connectedFunctions().at(i)->childElements().size() - 1) tempString += ", ";
		}
	
	tempString += ")));\n";
	}
	
return tempString;
}

QList<MecElementCompiler*> MecSignalCompiler::concreteSubCompilers() const
{
return m_concreteSubCompilers;
}

QList<MecElementCompiler*> MecSignalCompiler::recConcreteSubCompilers() const
{
return m_recConcreteSubCompilers;
}

