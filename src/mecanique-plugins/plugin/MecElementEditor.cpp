/*
© Quentin VIGNAUD, 2013

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#include "MecElementEditor.h"

MecElementEditor::MecElementEditor(MecAbstractElement* const Element, MecAbstractEditor* MainEditor, QWidget * Parent, Qt::WindowFlags F) : m_element (Element),  m_editorRole (m_element->elementRole()), m_mainEditor (MainEditor)
{
setParent(Parent, F);

pushButtonIcon = new QPushButton(this);
	//pushButtonIcon->setFlat(true);
	pushButtonIcon->setIconSize(QSize(64, 64));
	pushButtonIcon->setContextMenuPolicy(Qt::CustomContextMenu);
	connect(pushButtonIcon, SIGNAL(clicked(bool)), SLOT(emitEditedElementChange()));
	connect(pushButtonIcon, SIGNAL(customContextMenuRequested(QPoint)), SLOT(elementContextMenu()));
setFocusProxy(pushButtonIcon);

labelType = new QLabel(tr("Type:"), this);
comboBoxType = new QComboBox(this);
	comboBoxType->setMaximumWidth(100);
	comboBoxType->addItems(mainEditor()->baseElements(element()->elementRole()));
	comboBoxType->setCurrentIndex(comboBoxType->findText(element()->elementType()));
	connect(comboBoxType, SIGNAL(currentIndexChanged(QString)), this, SLOT(setElementType(QString)));
	connect(comboBoxType, SIGNAL(highlighted(int)), SLOT(emitEditedElementChange()));

labelName = new QLabel(tr("Name:"), this);
lineEditName = new QLineEdit(this);
	lineEditName->setMaximumWidth(150);
	lineEditName->setValidator(new QRegExpValidator(MecAbstractElement::standardSyntax(), lineEditName));
	connect(lineEditName, SIGNAL(editingFinished()), this, SLOT(setElementName()));

connect(m_element, SIGNAL(childListChanged(MecAbstractElement*)), this, SLOT(childListElementsChanged(MecAbstractElement*)));
connect(m_element, SIGNAL(nameChanged(MecAbstractElement*)), this, SLOT(nameElementChanged(MecAbstractElement*)));
connect(m_element, SIGNAL(typeChanged(MecAbstractElement*)), this, SLOT(typeElementChanged(MecAbstractElement*)));

MecElementEditor::nameElementChanged(element());
MecElementEditor::typeElementChanged(element());
}

MecElementEditor::~MecElementEditor()
{
//Rien de particulier
}

MecAbstractElement* MecElementEditor::element() const
{
return m_element;
}

MecAbstractElement::ElementRole MecElementEditor::editorRole() const
{
return m_editorRole;
}

MecAbstractEditor* MecElementEditor::mainEditor() const
{
return m_mainEditor;
}

QList<MecPluginError> MecElementEditor::pluginsErrors() const
{
return m_pluginsErrors;
}

MecAbstractElementEditor* MecElementEditor::elementEditor(MecAbstractElement* const Element) const
{
if (Element == m_element) return const_cast<MecElementEditor*>(this);
else
	{
	for (int i=0 ; i < m_subEditors.size() ; i++)
		{
		MecAbstractElementEditor *tempEditor = m_subEditors.at(i)->elementEditor(Element);
		if (tempEditor != 0) return tempEditor;
		}
	}
return 0;
}

QVariant MecElementEditor::state()
{
	QHash<QString, QVariant> tempProperties;
	tempProperties.insert("lineEditNameFocus", QVariant(lineEditName->hasFocus()));
	tempProperties.insert("lineEditNamePos", QVariant(lineEditName->cursorPosition()));
	
	//États des sous-éditeurs.
	QHash<QString, QVariant> tempSubProperties;
	for (int i=0 ; i < subEditors().size() ; i++)
		{
		tempSubProperties.insert(subEditors().at(i)->element()->elementName(), subEditors().at(i)->state());
		}
	tempProperties.insert("subEditorsStates", QVariant(tempSubProperties));
	
	return QVariant(tempProperties);
}

void MecElementEditor::setState(QVariant State)
{
	QHash<QString, QVariant> tempProperties = State.toHash();
	if (tempProperties.value("lineEditNameFocus").toBool()) lineEditName->setFocus();
	lineEditName->setCursorPosition(tempProperties.value("lineEditNamePos").toInt());
	
	//États des sous-éditeurs.
	QHash<QString, QVariant> tempSubProperties = tempProperties.value("subEditorsStates").toHash();
	for (int i=0 ; i < subEditors().size() ; i++)
		{
		subEditors().at(i)->setState(tempSubProperties.value(subEditors().at(i)->element()->elementName()));
		}
}
	
bool MecElementEditor::canAddChild(MecAbstractElement::ElementRole ElementRole) const
{
if (ElementRole == MecAbstractElement::Object) return canAddObject();
else if (ElementRole == MecAbstractElement::Function) return canAddFunction();
else if (ElementRole == MecAbstractElement::Signal) return canAddSignal();
else if (ElementRole == MecAbstractElement::Variable) return canAddVariable();
else return false;
}

bool MecElementEditor::isTypeEditable() const
{
	return comboBoxType->isEnabled();
}

bool MecElementEditor::isNameEditable() const
{
	return lineEditName->isEnabled();
}

MecAbstractElement* MecElementEditor::getNewElementDialog(MecAbstractElement::ElementRole ElementRole)
{
if (ElementRole == MecAbstractElement::Signal)
	{
	return mainEditor()->baseElement(MecAbstractElement::Signal, "void");
	}

QString Type = mainEditor()->selectType(mainEditor()->baseElements(ElementRole));

if (Type.isEmpty() == false) {
	return mainEditor()->baseElement(ElementRole, Type);
}
else return 0;
}

void MecElementEditor::addChild(MecAbstractElement::ElementRole ElementRole)
{
if (ElementRole == MecAbstractElement::Object) addObject();
else if (ElementRole == MecAbstractElement::Function) addFunction();
else if (ElementRole == MecAbstractElement::Signal) addSignal();
else if (ElementRole == MecAbstractElement::Variable) addVariable();
}

void MecElementEditor::addObject()
{
MecAbstractElement *tempElement = getNewElementDialog(MecAbstractElement::Object);
if (tempElement != 0)
	{
	tempElement->setParentElement(element());
	mainEditor()->addEditStep(tr("Add the object “%1”").arg(tempElement->elementName()));
	}
}

void MecElementEditor::addFunction()
{
MecAbstractElement *tempElement = getNewElementDialog(MecAbstractElement::Function);
if (tempElement != 0)
	{
	tempElement->setParentElement(element());
	mainEditor()->addEditStep(tr("Add the function “%1”").arg(tempElement->elementName()));
	}
}

void MecElementEditor::addSignal()
{
MecAbstractElement *tempElement = getNewElementDialog(MecAbstractElement::Signal);
if (tempElement != 0)
	{
	tempElement->setParentElement(element());
	mainEditor()->addEditStep(tr("Add the signal “%1”").arg(tempElement->elementName()));
	}
}

void MecElementEditor::addVariable()
{
MecAbstractElement *tempElement = getNewElementDialog(MecAbstractElement::Variable);
if (tempElement != 0)
	{
	tempElement->setParentElement(element());
	mainEditor()->addEditStep(tr("Add the variable “%1”").arg(tempElement->elementName()));
	}
}

void MecElementEditor::setTypeEditable(bool Editable)
{
	comboBoxType->setEnabled(Editable);
}

void MecElementEditor::setNameEditable(bool Editable)
{
	lineEditName->setEnabled(Editable);
}

void MecElementEditor::nameElementChanged(MecAbstractElement *Element)
{
if (Element != element()) return;

//Par confort pour l'utilisateur, on enregistre la position actuelle du curseur.
int tempPosition = lineEditName->cursorPosition();
lineEditName->setText(Element->elementName());
//Puis on l'y replace.
lineEditName->setCursorPosition(tempPosition);
}

void MecElementEditor::typeElementChanged(MecAbstractElement *Element)
{
if (Element != element()) return;

comboBoxType->setCurrentIndex(comboBoxType->findText(Element->elementType()));
pushButtonIcon->setIcon(QIcon(":/share/icons/types/" + element()->elementType() + ".png"));
}

void MecElementEditor::emitEditedElementChange()
{
emit editedElementChange(element());
}

void MecElementEditor::childListElementsChanged(MecAbstractElement* Element)
{
if (m_element != Element) return;

for (int i=0 ; i < m_subEditors.size() ; i++)//On supprime en premier lieu les excédents.
	{
	if (!(m_element->childElements().contains(m_subEditors.at(i)->element())))
		{
		MecAbstractElementEditor *tempEditor = m_subEditors.takeAt(i);
		subEditorAboutToDeleted(tempEditor);
		delete tempEditor;
		i--;
		}
	}

QList<MecAbstractElement*> tempChildElements = m_element->childElements();
QList<MecAbstractElement*> tempSubEditors;
for (int i=0 ; i < m_subEditors.size() ; i++) tempSubEditors.append(m_subEditors.at(i)->element());//Constitution des listes à comparer.

for (int i=0 ; i < tempChildElements.size() ; i++)//Et on ajoute les manquants.
	{
	if (!tempSubEditors.contains(tempChildElements.at(i)))
		{
		MecAbstractElementEditor *tempEditor = newSubEditor(tempChildElements.at(i));
		if (tempEditor != 0)
			{
			m_pluginsErrors.append(tempEditor->pluginsErrors());
			m_subEditors.append(tempEditor);
			connect(tempEditor, SIGNAL(editedElementChange(MecAbstractElement*)), SIGNAL(editedElementChange(MecAbstractElement*)));
			}
		}
	}

reorderSubEditors();
}

QList<MecAbstractElementEditor*> MecElementEditor::subEditors() const
{
return m_subEditors;
}

void MecElementEditor::addPluginsError(MecPluginError Error)
{
m_pluginsErrors.append(Error);
emit pluginsErrorsOccured();
}

void MecElementEditor::setElementName()
{
QString oldName = element()->elementName();
//On vérifie D'ABORD si le nom n'est pas réservé par, puis si l'élément accepte ce nom, sinon on réactualise son nom.
if (mainEditor()->reservedNames().contains(lineEditName->text()) or !element()->setElementName(lineEditName->text()))
	{
	MecElementEditor::nameElementChanged(element());
	}
else
	{
	if (oldName != lineEditName->text()) mainEditor()->addEditStep(tr("Change name of “%1” to “%2”").arg(oldName).arg(element()->elementName()));
	}
}

void MecElementEditor::setElementType(QString Type)
{
QString oldType = element()->elementType();
if (!element()->setElementType(Type))
	{
	MecElementEditor::typeElementChanged(element());
	}
else
	{
	if (oldType != Type) mainEditor()->addEditStep(tr("Change type of “%1” to “%2” from “%3”").arg(element()->elementName()).arg(element()->elementType()).arg(oldType));
	}
}

void MecElementEditor::elementContextMenu()
{
	m_mainEditor->elementContextMenu(m_element, QCursor::pos());
}


