/*
© Quentin VIGNAUD, 2013

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#ifndef __MECVARIABLEEDITOR_H__
#define __MECVARIABLEEDITOR_H__

#include "MecElementEditor.h"
#include <MecAbstractVariable>

/**
\brief	Classe basique d'édition de variable.

Cette classe fournit les précisions nécessaires à l'édition basique d'une variable.
*/
class MecVariableEditor : public MecElementEditor
{
	Q_OBJECT

public:
	/**
	\brief	Constructeur.
	\param	Variable	Variable éditée, doit absolument exister lors de la construction (c.à.d. instanciée et différente de 0) sinon un comportement inattendu pourrait se produire.
	\param	MainEditor	Éditeur principal dont dépend cet éditeur d'élément.
	\param	Parent	Widget parent.
	\param	F	Drapeaux de fenêtre.

	Initialise l'éditeur et ses widgets enfants, et organise ces derniers.
	*/
	MecVariableEditor(MecAbstractVariable* const Variable, MecAbstractEditor* MainEditor, QWidget * Parent=0, Qt::WindowFlags F=0);
	/**
	\brief	Destructeur.
	*/
	virtual ~MecVariableEditor();

	///Retourne la variable éditée.
	MecAbstractVariable* variable() const;

	/**
	\brief	Retourne un QVariant contenant toutes les données indiquant l'état actuel de l'éditeur.

	Cette donnée est vouée à être passée ultérieurement en paramètre à setState(QVariant) pour restaurer l'état visuel de l'éditeur.
	*/
	virtual QVariant state();
	/**
	\brief	Restaure l'état de l'éditeur selon les indications spécifées.

	\see	state()
	*/
	virtual void setState(QVariant State);

	///Indique si un MecObject peut être ajouté, retourne donc \e false.
	bool canAddObject() const;
	///Indique si une MecFunction peut être ajouté, retourne donc \e false.
	bool canAddFunction() const;
	///Indique si un MecSignal peut être ajouté, retourne donc \e false.
	bool canAddSignal() const;
	///Indique si une MecVariable peut être ajouté, retourne donc \e false.
	bool canAddVariable() const;
	///Indique si un élément enfant peut être supprimé, retourne donc \e false.
	bool canRemoveChild(MecAbstractElement* const Element) const;

	/**
	Indique si la valeur par défaut de la variable est éditable.

	\see	setContentEditable(bool)
	*/
	virtual bool isContentEditable() const;

public slots:
	///Est activé lorsque la valeur par défaut de la variable a changé.
	virtual void defaultValueVariableChanged(MecAbstractVariable *Variable);

	/**
	\brief	Dispose l'éditeur de manière à éditer l'élément de manière primaire.

	Sélectionne le nom de la variable.
	*/
	virtual void edit();
	///Ne fait rien.
	void editChild(MecAbstractElement *Element);
	///Ne fait rien.
	void addObject();
	///Ne fait rien.
	void addFunction();
	///Ne fait rien.
	void addSignal();
	///Ne fait rien.
	void addVariable();

	/**
	\brief	Autorise ou non l'édition de la valeur par défaut.
	*/
	virtual void setContentEditable(bool Editable);

protected:
	/**
	\brief	Demande un sous-éditeur pour \e Element.
	\return	0
	*/
	MecAbstractElementEditor* newSubEditor(MecAbstractElement *Element);

	/**
	\brief	Est appelée juste avant la suppression de \e Editor.

	Ne fait rien.
	*/
	void subEditorAboutToDeleted(MecAbstractElementEditor *Editor);

	/**
	\brief	Effectue la vérification de l'ordre des sous-éditeurs.

	Ne fait rien.
	*/
	void reorderSubEditors();

	/**
	\brief	Ajoute un widget à l'affichage des données générales.
	*/
	virtual void addGeneralWidget(QWidget *Widget);

	///Layout d'organisation de l'éditeur.
	QHBoxLayout *layoutMain;


private:
	///Variable éditée.
	MecAbstractVariable* const m_variable;


private slots:


};


#endif /* __MECVARIABLEEDITOR_H__ */

