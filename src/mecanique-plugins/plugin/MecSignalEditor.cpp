/*
© Quentin VIGNAUD, 2013 - 2014

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#include "MecSignalEditor.h"

MecSignalConnectionsList::MecSignalConnectionsList(MecAbstractSignal* const Signal, MecAbstractEditor* MainEditor, QWidget * Parent) : QListWidget(Parent), m_mainEditor (MainEditor), m_signal (Signal)
{
setSelectionMode(QAbstractItemView::ExtendedSelection);

setDragDropMode(QAbstractItemView::DropOnly);
setDropIndicatorShown(true);
setAcceptDrops(true);

connect(m_signal, SIGNAL(connectedFunctionsChanged(MecAbstractSignal*)), this, SLOT(connectedFunctionsChanged(MecAbstractSignal*)));
connectedFunctionsChanged(signal());
}

MecSignalConnectionsList::~MecSignalConnectionsList()
{
}
	
MecAbstractSignal* MecSignalConnectionsList::signal() const
{
	return m_signal;
}

void MecSignalConnectionsList::connectedFunctionsChanged(MecAbstractSignal* Signal)
{
if (Signal != signal()) return;

//On regarde quelles fonctions ne sont plus connectées.
QMapIterator<MecAbstractFunction*, QListWidgetItem*> tempIterator(m_match);
QList<MecAbstractFunction*> tempList;
while (tempIterator.hasNext())
	{
	tempIterator.next();
	if (!Signal->connectedFunctions().contains(tempIterator.key())) tempList.append(tempIterator.key());
	}
while (!tempList.isEmpty())
	{
	disconnect(tempList.first(), SIGNAL(childListChanged(MecAbstractElement*)), this, SLOT(functionChanged(MecAbstractElement*)));
	disconnect(tempList.first(), SIGNAL(nameChanged(MecAbstractElement*)), this, SLOT(functionChanged(MecAbstractElement*)));
	
	delete m_match.take(tempList.takeFirst());
	}

//On regarde quelles fonctions sont connectées mais pas encore listées.
for (int i=0 ; i < Signal->connectedFunctions().size() ; i++)
	{
	if (findItemWithFunction(Signal->connectedFunctions().at(i)) == 0)
		{
		QListWidgetItem *tempItem = new QListWidgetItem(this);
		
		m_match.insert(Signal->connectedFunctions().at(i), tempItem);
		
		connect(Signal->connectedFunctions().at(i), SIGNAL(childListChanged(MecAbstractElement*)), this, SLOT(functionChanged(MecAbstractElement*)));
		connect(Signal->connectedFunctions().at(i), SIGNAL(nameChanged(MecAbstractElement*)), this, SLOT(functionChanged(MecAbstractElement*)));
		
		functionChanged(Signal->connectedFunctions().at(i));
		}
	}
}

void MecSignalConnectionsList::removeSelectedConnections()
{
QList<QListWidgetItem*> tempItems = selectedItems();

for (int i=0 ; i < tempItems.size() ; i++)
	{	
	MecAbstractFunction *tempFunction = findFunctionWithItem(tempItems.at(i));
	if (tempFunction != 0)
		{
		signal()->disconnectFunction(tempFunction);
		}
	}

if (!tempItems.isEmpty()) m_mainEditor->addEditStep(tr("Remove connections with “%1”").arg(signal()->elementName()));
}

void MecSignalConnectionsList::dropEvent(QDropEvent *Event)
{
	if (Event->mimeData()->hasFormat("application/x-mecanique-element-address"))
		{
		QString tempAddress = QString::fromUtf8(Event->mimeData()->data("application/x-mecanique-element-address"));
		MecAbstractElement *tempElement = static_cast<MecAbstractElement*>(m_mainEditor->element()->childElement(tempAddress));
		
		if (tempElement != 0 and tempElement->elementRole() == MecAbstractElement::Function)
			{
			MecAbstractFunction *tempFunction = static_cast<MecAbstractFunction*>(m_mainEditor->element()->childElement(tempAddress));
			if (signal()->connectFunction(tempFunction))
				{
				m_mainEditor->addEditStep(tr("Add connection between “%1” and “%2”").arg(signal()->elementName()).arg(tempFunction->elementName()));
				}
			else Event->ignore();
			}
		else Event->ignore();
		}
	else
		{
		Event->ignore();
		}
}

QStringList MecSignalConnectionsList::mimeTypes() const
{
	return QStringList() << "application/x-mecanique-element-address";
}

Qt::DropActions MecSignalConnectionsList::supportedDropActions() const
{
	return Qt::CopyAction;
}
	
MecAbstractFunction* MecSignalConnectionsList::findFunctionWithItem(QListWidgetItem *Item) const
{
MecAbstractFunction *tempFunction = 0;
QMapIterator<MecAbstractFunction*, QListWidgetItem*> tempIterator(m_match);
while (tempIterator.hasNext() and tempFunction == 0)
	{
	tempIterator.next();
	if (tempIterator.value() == Item) tempFunction = tempIterator.key();
	}
return tempFunction;
}

QListWidgetItem* MecSignalConnectionsList::findItemWithFunction(MecAbstractFunction *Function) const
{
QListWidgetItem *tempItem = 0;

QMapIterator<MecAbstractFunction*, QListWidgetItem*> tempIterator(m_match);

while (tempIterator.hasNext() and tempItem == 0)
	{
	tempIterator.next();

	if (tempIterator.key() == Function) tempItem = tempIterator.value();
	}
return tempItem;
}
	
void MecSignalConnectionsList::functionChanged(MecAbstractElement *Element)
{
QListWidgetItem *tempItem = findItemWithFunction(static_cast<MecAbstractFunction*>(Element));
if (tempItem == 0) return;
else
	{
	QString tempString;
	if (Element->parentElement() != 0 and Element->parentElement()->elementRole() != MecAbstractElement::Project) tempString += Element->parentElement()->elementName() + "::";
	
	tempString += Element->elementName() + "(";
	
	for (int i=0 ; i < Element->childElements().size() ; i++)
		{
		tempString += Element->childElements().at(i)->elementType() + " " + Element->childElements().at(i)->elementName();
		if (i != Element->childElements().size() - 1) tempString += ", ";
		}
	
	tempString += ")";
	
	tempItem->setText(tempString);
	}
}

MecSignalEditor::MecSignalEditor(MecAbstractSignal* const Signal, MecAbstractEditor* MainEditor, QWidget * Parent, Qt::WindowFlags F) : MecElementEditor(Signal, MainEditor, Parent, F), m_signal (Signal)
{

tabWidgetMain = new QTabWidget(this);
	tabWidgetMain->setTabPosition(QTabWidget::West);

//Propriétés
widgetProperties = new QWidget(tabWidgetMain);

//Général
groupBoxGeneral = new QGroupBox(tr("General"), widgetProperties);
//label et lineEdit "Name" sont déjà déclarés,
//en revanche on masque ceux du type…
labelType->hide();
comboBoxType->hide();
pushButtonIcon->hide();
labelPrototype = new QLabel(groupBoxGeneral);
layoutGeneral = new QGridLayout(groupBoxGeneral);
	layoutGeneral->addWidget(labelName, 0, 0);
	layoutGeneral->addWidget(lineEditName, 0, 1);
	layoutGeneral->addWidget(labelPrototype, 1, 0, 1, 3);
	layoutGeneral->setColumnStretch(2, 1);

//Variables
groupBoxVariables = new QGroupBox(tr("Variables"), widgetProperties);

scrollAreaVariables = new QScrollArea(groupBoxVariables);

	widgetEditorsVariables = new QWidget(scrollAreaVariables);
	
	layoutEditorsVariables = new QVBoxLayout(widgetEditorsVariables);
		spacerItemEditorsVariables = new QSpacerItem(0, 0, QSizePolicy::Minimum, QSizePolicy::Expanding);
		layoutEditorsVariables->addSpacerItem(spacerItemEditorsVariables);

scrollAreaVariables->setWidget(widgetEditorsVariables);
	scrollAreaVariables->setWidgetResizable(true);

layoutVariables = new QGridLayout(groupBoxVariables);
	layoutVariables->addWidget(scrollAreaVariables, 0, 0);

layoutProperties = new QGridLayout(widgetProperties);
	layoutProperties->addWidget(groupBoxGeneral, 0, 0);
	layoutProperties->addWidget(groupBoxVariables, 1, 0);

tabWidgetMain->addTab(widgetProperties, QIcon(":/share/icons/properties.png"), tr("Properties"));

//Connections
widgetConnections = new QWidget(tabWidgetMain);

	listConnections = new MecSignalConnectionsList(m_signal, mainEditor(), widgetConnections);
	
	pushButtonAddConnection = new QPushButton(QIcon::fromTheme("list-add"), tr("Add connection"), widgetConnections);
		connect(pushButtonAddConnection, SIGNAL(clicked(bool)), SLOT(addConnection()));
	pushButtonRemoveConnection = new QPushButton(QIcon::fromTheme("list-remove"), tr("Remove connection"), widgetConnections);
		connect(pushButtonRemoveConnection, SIGNAL(clicked(bool)), SLOT(removeConnection()));

layoutConnections = new QGridLayout(widgetConnections);
	layoutConnections->addWidget(listConnections, 0, 0, 3, 2);
	layoutConnections->addWidget(pushButtonAddConnection, 3, 0);
	layoutConnections->addWidget(pushButtonRemoveConnection, 3, 1);

tabWidgetMain->addTab(widgetConnections, QIcon(":/share/icons/connection.png"), tr("Connections"));

//Finition
layoutMain = new QGridLayout(this);
	layoutMain->addWidget(tabWidgetMain, 0, 0);

MecSignalEditor::nameElementChanged(signal());
connect(m_signal, SIGNAL(childListChanged(MecAbstractElement*)), SLOT(refreshLabelPrototype()));
connect(m_signal, SIGNAL(nameChanged(MecAbstractElement*)), SLOT(refreshLabelPrototype()));
connect(m_signal, SIGNAL(typeChanged(MecAbstractElement*)), SLOT(refreshLabelPrototype()));
for (int i=0 ; i < m_signal->childElements().size() ; i++) {
	connect(m_signal->childElements().at(i), SIGNAL(childListChanged(MecAbstractElement*)), SLOT(refreshLabelPrototype()));
	connect(m_signal->childElements().at(i), SIGNAL(nameChanged(MecAbstractElement*)), SLOT(refreshLabelPrototype()));
	connect(m_signal->childElements().at(i), SIGNAL(typeChanged(MecAbstractElement*)), SLOT(refreshLabelPrototype()));
}
refreshLabelPrototype();
}

MecSignalEditor::~MecSignalEditor()
{
//Rien de particulier.
}
	
MecAbstractSignal* MecSignalEditor::signal() const
{
return m_signal;
}

QVariant MecSignalEditor::state()
{
	QHash<QString, QVariant> tempProperties;
	tempProperties.insert("inheritedProperties", MecElementEditor::state());
	
	tempProperties.insert("currentTab", QVariant(tabWidgetMain->currentIndex()));
	tempProperties.insert("posVariable", QVariant(scrollAreaVariables->verticalScrollBar()->value()));
	return QVariant(tempProperties);
}

void MecSignalEditor::setState(QVariant State)
{
	QHash<QString, QVariant> tempProperties = State.toHash();
	MecElementEditor::setState(tempProperties.value("inheritedProperties"));
	
	tabWidgetMain->setCurrentIndex(tempProperties.value("currentTab").toInt());
	scrollAreaVariables->verticalScrollBar()->setValue(tempProperties.value("posVariable").toInt());
}
	
bool MecSignalEditor::canAddObject() const
{
return false;
}

bool MecSignalEditor::canAddFunction() const
{
return false;
}

bool MecSignalEditor::canAddSignal() const
{
return false;
}

bool MecSignalEditor::canAddVariable() const
{
return true;
}

bool MecSignalEditor::canRemoveChild(MecAbstractElement* const Element) const
{
return true;
}

bool MecSignalEditor::isContentEditable() const
{
return true;
}

void MecSignalEditor::edit()
{
	tabWidgetMain->setCurrentWidget(widgetProperties);
	lineEditName->setFocus(Qt::OtherFocusReason);
}

void MecSignalEditor::editChild(MecAbstractElement *Element)
{
MecAbstractElementEditor *tempEditor = elementEditor(Element);
if (tempEditor != 0 and tempEditor != this)
	{
	tabWidgetMain->setCurrentWidget(widgetProperties);
	scrollAreaVariables->ensureWidgetVisible(tempEditor);
	tempEditor->setFocus();
	tempEditor->edit();
	}
}

void MecSignalEditor::setContentEditable(bool Editable)
{
}

MecAbstractElementEditor* MecSignalEditor::newSubEditor(MecAbstractElement *Element)
{
if (Element->elementRole() != MecAbstractElement::Variable) return 0;

MecVariableEditor *tempEditor = new MecVariableEditor(static_cast<MecAbstractVariable*>(Element), mainEditor(), widgetEditorsVariables);
QWidget *tempFocusWidget = scrollAreaVariables->focusWidget();
layoutEditorsVariables->removeItem(spacerItemEditorsVariables);
layoutEditorsVariables->addWidget(tempEditor, 0, Qt::AlignTop);
layoutEditorsVariables->addSpacerItem(spacerItemEditorsVariables);
widgetEditorsVariables->show();
tempEditor->show();
scrollAreaVariables->ensureWidgetVisible(tempFocusWidget);

return tempEditor;
}
	
void MecSignalEditor::subEditorAboutToDeleted(MecAbstractElementEditor *Editor)
{
layoutEditorsVariables->removeWidget(Editor);
}

void MecSignalEditor::reorderSubEditors()
{
	//Variables
	QList<MecAbstractVariable*> tempVariables = m_signal->childVariables();
	layoutEditorsVariables->removeItem(spacerItemEditorsVariables);
	for (int i=0 ; i < tempVariables.size() ; i++)
		{
		layoutEditorsVariables->addWidget(elementEditor(tempVariables.at(i)), 0, Qt::AlignTop);
		}
	layoutEditorsVariables->addSpacerItem(spacerItemEditorsVariables);
}

void MecSignalEditor::typeElementChanged(MecAbstractElement *Element) {}
void MecSignalEditor::addObject() {}
void MecSignalEditor::addFunction() {}
void MecSignalEditor::addSignal() {}

void MecSignalEditor::refreshLabelPrototype()
{
	QString tempText("<font color=\"darkBlue\">" + signal()->elementName() + "</font>(");
	
	for (int i=0 ; i < signal()->childElements().size() ; i++) {
		tempText += "<font color=\"darkGreen\"><b>" + signal()->childElements().at(i)->elementType() + "</b></font> <font color=\"darkMagenta\">" + signal()->childElements().at(i)->elementName()  + "</font>";
		if (i != signal()->childElements().size() - 1) tempText += ", ";
	}
	
	tempText += ");";
	
	labelPrototype->setText(tempText);
}

void MecSignalEditor::addGeneralWidget(QWidget *Widget)
{
	layoutGeneral->addWidget(Widget, 0, layoutGeneral->columnCount());
}
	
void MecSignalEditor::addConnection()
{
MecAbstractElement *tempElement = mainEditor()->selectedElement();
if (tempElement != 0 and tempElement->elementRole() == MecAbstractElement::Function)
	{
	signal()->connectFunction(static_cast<MecAbstractFunction*>(tempElement));
	mainEditor()->addEditStep(tr("Add connection between “%1” and “%2”").arg(signal()->elementName()).arg(tempElement->elementName()));
	}
}

void MecSignalEditor::removeConnection()
{
	listConnections->removeSelectedConnections();
}


