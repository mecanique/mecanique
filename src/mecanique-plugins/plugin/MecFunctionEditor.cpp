/*
© Quentin VIGNAUD, 2013

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#include "MecFunctionEditor.h"

MecFunctionConnectionsList::MecFunctionConnectionsList(MecAbstractFunction* const Function, MecAbstractEditor* MainEditor, QWidget * Parent) : QListWidget(Parent), m_mainEditor (MainEditor), m_function (Function)
{
setSelectionMode(QAbstractItemView::ExtendedSelection);

setDragDropMode(QAbstractItemView::DropOnly);
setDropIndicatorShown(true);
setAcceptDrops(true);

connect(m_function, SIGNAL(connectedSignalsChanged(MecAbstractFunction*)), this, SLOT(connectedSignalsChanged(MecAbstractFunction*)));
connectedSignalsChanged(function());
}

MecFunctionConnectionsList::~MecFunctionConnectionsList()
{
}
	
MecAbstractFunction* MecFunctionConnectionsList::function() const
{
	return m_function;
}

void MecFunctionConnectionsList::connectedSignalsChanged(MecAbstractFunction* Function)
{
if (Function != function()) return;

//On regarde quels signaux ne sont plus connectés.
QMapIterator<MecAbstractSignal*, QListWidgetItem*> tempIterator(m_match);
QList<MecAbstractSignal*> tempList;
while (tempIterator.hasNext())
	{
	tempIterator.next();
	if (!Function->connectedSignals().contains(tempIterator.key())) tempList.append(tempIterator.key());
	}
while (!tempList.isEmpty())
	{
	disconnect(tempList.first(), SIGNAL(childListChanged(MecAbstractElement*)), this, SLOT(signalChanged(MecAbstractElement*)));
	disconnect(tempList.first(), SIGNAL(nameChanged(MecAbstractElement*)), this, SLOT(signalChanged(MecAbstractElement*)));
	
	delete m_match.take(tempList.takeFirst());
	}

//On regarde quels signaux sont connectées mais pas encore listés.
for (int i=0 ; i < Function->connectedSignals().size() ; i++)
	{
	if (findItemWithSignal(Function->connectedSignals().at(i)) == 0)
		{
		QListWidgetItem *tempItem = new QListWidgetItem(this);
		
		m_match.insert(Function->connectedSignals().at(i), tempItem);
		
		connect(Function->connectedSignals().at(i), SIGNAL(childListChanged(MecAbstractElement*)), this, SLOT(signalChanged(MecAbstractElement*)));
		connect(Function->connectedSignals().at(i), SIGNAL(nameChanged(MecAbstractElement*)), this, SLOT(signalChanged(MecAbstractElement*)));
		
		signalChanged(Function->connectedSignals().at(i));
		}
	}
}

void MecFunctionConnectionsList::removeSelectedConnections()
{
QList<QListWidgetItem*> tempItems = selectedItems();

for (int i=0 ; i < tempItems.size() ; i++)
	{	
	MecAbstractSignal *tempSignal = findSignalWithItem(tempItems.at(i));
	if (tempSignal != 0)
		{
		function()->disconnectSignal(tempSignal);
		}
	}

if (!tempItems.isEmpty()) m_mainEditor->addEditStep(tr("Remove connections with “%1”").arg(function()->elementName()));
}

void MecFunctionConnectionsList::dropEvent(QDropEvent *Event)
{
	if (Event->mimeData()->hasFormat("application/x-mecanique-element-address"))
		{
		QString tempAddress = QString::fromUtf8(Event->mimeData()->data("application/x-mecanique-element-address"));
		MecAbstractElement *tempElement = static_cast<MecAbstractElement*>(m_mainEditor->element()->childElement(tempAddress));
		
		if (tempElement != 0 and tempElement->elementRole() == MecAbstractElement::Signal)
			{
			MecAbstractSignal *tempSignal = static_cast<MecAbstractSignal*>(m_mainEditor->element()->childElement(tempAddress));
			if (function()->connectSignal(tempSignal))
				{
				m_mainEditor->addEditStep(tr("Add connection between “%1” and “%2”").arg(tempSignal->elementName()).arg(function()->elementName()));
				}
			else Event->ignore();
			}
		else Event->ignore();
		}
	else
		{
		Event->ignore();
		}
}

QStringList MecFunctionConnectionsList::mimeTypes() const
{
	return QStringList() << "application/x-mecanique-element-address";
}

Qt::DropActions MecFunctionConnectionsList::supportedDropActions() const
{
	return Qt::CopyAction;
}
	
MecAbstractSignal* MecFunctionConnectionsList::findSignalWithItem(QListWidgetItem *Item) const
{
MecAbstractSignal *tempSignal = 0;
QMapIterator<MecAbstractSignal*, QListWidgetItem*> tempIterator(m_match);
while (tempIterator.hasNext() and tempSignal == 0)
	{
	tempIterator.next();
	if (tempIterator.value() == Item) tempSignal = tempIterator.key();
	}
return tempSignal;
}

QListWidgetItem* MecFunctionConnectionsList::findItemWithSignal(MecAbstractSignal *Signal) const
{
QListWidgetItem *tempItem = 0;

QMapIterator<MecAbstractSignal*, QListWidgetItem*> tempIterator(m_match);

while (tempIterator.hasNext() and tempItem == 0)
	{
	tempIterator.next();

	if (tempIterator.key() == Signal) tempItem = tempIterator.value();
	}
return tempItem;
}
	
void MecFunctionConnectionsList::signalChanged(MecAbstractElement *Element)
{
QListWidgetItem *tempItem = findItemWithSignal(static_cast<MecAbstractSignal*>(Element));
if (tempItem == 0) return;
else
	{
	QString tempString;
	if (Element->parentElement() != 0 and Element->parentElement()->elementRole() != MecAbstractElement::Project) tempString += Element->parentElement()->elementName() + "::";
	
	tempString += Element->elementName() + "(";
	
	for (int i=0 ; i < Element->childElements().size() ; i++)
		{
		tempString += Element->childElements().at(i)->elementType() + " " + Element->childElements().at(i)->elementName();
		if (i != Element->childElements().size() - 1) tempString += ", ";
		}
	
	tempString += ")";
	
	tempItem->setText(tempString);
	}
}

MecFunctionEditor::MecFunctionEditor(MecAbstractFunction* const Function, MecAbstractEditor* MainEditor, QWidget * Parent, Qt::WindowFlags F) : MecElementEditor(Function, MainEditor, Parent, F), m_function (Function)
{

tabWidgetMain = new QTabWidget(this);
	tabWidgetMain->setTabPosition(QTabWidget::West);

//Propriétés
widgetProperties = new QWidget(tabWidgetMain);

//Générales
groupBoxGeneral = new QGroupBox(tr("General"), widgetProperties);
labelType->setText(tr("Return type:"));
labelPrototype = new QLabel(groupBoxGeneral);
layoutGeneral = new QGridLayout(groupBoxGeneral);
	layoutGeneral->addWidget(pushButtonIcon, 0, 0, 2, 1);
	layoutGeneral->addWidget(labelType, 0, 1);
	layoutGeneral->addWidget(comboBoxType, 0, 2);
	layoutGeneral->addWidget(labelName, 0, 3);
	layoutGeneral->addWidget(lineEditName, 0, 4);
	layoutGeneral->addWidget(labelPrototype, 1, 1, 1, 5);
	layoutGeneral->setColumnStretch(5, 1);

//Variables
groupBoxVariables = new QGroupBox(tr("Variables"), this);

scrollAreaVariables = new QScrollArea(groupBoxVariables);

	widgetEditorsVariables = new QWidget(scrollAreaVariables);
	
	layoutEditorsVariables = new QVBoxLayout(widgetEditorsVariables);
		spacerItemEditorsVariables = new QSpacerItem(0, 0, QSizePolicy::Minimum, QSizePolicy::Expanding);
		layoutEditorsVariables->addSpacerItem(spacerItemEditorsVariables);

scrollAreaVariables->setWidget(widgetEditorsVariables);
	scrollAreaVariables->setWidgetResizable(true);

layoutVariables = new QGridLayout(groupBoxVariables);
	layoutVariables->addWidget(scrollAreaVariables, 0, 0);

layoutProperties = new QGridLayout(widgetProperties);
	layoutProperties->addWidget(groupBoxGeneral, 0, 0);
	layoutProperties->addWidget(groupBoxVariables, 1, 0);

tabWidgetMain->addTab(widgetProperties, QIcon(":/share/icons/properties.png"), tr("Properties"));

//Code
widgetCode = new QWidget(tabWidgetMain);

codeEditor = mainEditor()->codeEditor(function(), this);

layoutCode = new QGridLayout(widgetCode);
	layoutCode->addWidget(codeEditor, 0, 0);

tabWidgetMain->addTab(widgetCode, QIcon(":/share/icons/code.png"), tr("Code"));

//Connections
widgetConnections = new QWidget(tabWidgetMain);

	listConnections = new MecFunctionConnectionsList(m_function, mainEditor(), widgetConnections);
	
	pushButtonAddConnection = new QPushButton(QIcon::fromTheme("list-add"), tr("Add connection"), widgetConnections);
		connect(pushButtonAddConnection, SIGNAL(clicked(bool)), SLOT(addConnection()));
	pushButtonRemoveConnection = new QPushButton(QIcon::fromTheme("list-remove"), tr("Remove connection"), widgetConnections);
		connect(pushButtonRemoveConnection, SIGNAL(clicked(bool)), SLOT(removeConnection()));

layoutConnections = new QGridLayout(widgetConnections);
	layoutConnections->addWidget(listConnections, 0, 0, 3, 2);
	layoutConnections->addWidget(pushButtonAddConnection, 3, 0);
	layoutConnections->addWidget(pushButtonRemoveConnection, 3, 1);

tabWidgetMain->addTab(widgetConnections, QIcon(":/share/icons/connection.png"), tr("Connections"));

//Finition
layoutMain = new QGridLayout(this);
	layoutMain->addWidget(tabWidgetMain, 0, 0);

connect(m_function, SIGNAL(codeChanged(MecAbstractFunction*)), SLOT(codeFunctionChanged(MecAbstractFunction*)));
MecFunctionEditor::codeFunctionChanged(function());
connect(m_function, SIGNAL(childListChanged(MecAbstractElement*)), SLOT(refreshLabelPrototype()));
connect(m_function, SIGNAL(nameChanged(MecAbstractElement*)), SLOT(refreshLabelPrototype()));
connect(m_function, SIGNAL(typeChanged(MecAbstractElement*)), SLOT(refreshLabelPrototype()));
for (int i=0 ; i < m_function->childElements().size() ; i++) {
	connect(m_function->childElements().at(i), SIGNAL(childListChanged(MecAbstractElement*)), SLOT(refreshLabelPrototype()));
	connect(m_function->childElements().at(i), SIGNAL(nameChanged(MecAbstractElement*)), SLOT(refreshLabelPrototype()));
	connect(m_function->childElements().at(i), SIGNAL(typeChanged(MecAbstractElement*)), SLOT(refreshLabelPrototype()));
}
refreshLabelPrototype();
}

MecFunctionEditor::~MecFunctionEditor()
{
//Rien de particulier.
}
	
MecAbstractFunction* MecFunctionEditor::function() const
{
return m_function;
}

QVariant MecFunctionEditor::state()
{
	QHash<QString, QVariant> tempProperties;
	tempProperties.insert("inheritedProperties", MecElementEditor::state());
	
	tempProperties.insert("currentTab", QVariant(tabWidgetMain->currentIndex()));
	tempProperties.insert("posVariable", QVariant(scrollAreaVariables->verticalScrollBar()->value()));
	tempProperties.insert("codeEditor", codeEditor->state());
	return QVariant(tempProperties);
}

void MecFunctionEditor::setState(QVariant State)
{
	QHash<QString, QVariant> tempProperties = State.toHash();
	MecElementEditor::setState(tempProperties.value("inheritedProperties"));
	
	tabWidgetMain->setCurrentIndex(tempProperties.value("currentTab").toInt());
	scrollAreaVariables->verticalScrollBar()->setValue(tempProperties.value("posVariable").toInt());

	codeEditor->setState(tempProperties.value("codeEditor"));
}
	
bool MecFunctionEditor::canAddObject() const
{
return false;
}

bool MecFunctionEditor::canAddFunction() const
{
return false;
}

bool MecFunctionEditor::canAddSignal() const
{
return false;
}

bool MecFunctionEditor::canAddVariable() const
{
return true;
}

bool MecFunctionEditor::canRemoveChild(MecAbstractElement* const Element) const
{
return true;
}

bool MecFunctionEditor::isContentEditable() const
{
	return (tabWidgetMain->indexOf(widgetCode) == -1) ? false : true;
}

void MecFunctionEditor::addObject() {}
void MecFunctionEditor::addFunction() {}
void MecFunctionEditor::addSignal() {}

void MecFunctionEditor::setContentEditable(bool Editable)
{
	if (Editable && !isContentEditable()) tabWidgetMain->insertTab(1, widgetCode, QIcon(":/share/icons/code.png"), tr("Code"));
	else if (!Editable && isContentEditable()) tabWidgetMain->removeTab(tabWidgetMain->indexOf(widgetCode));
}

void MecFunctionEditor::refreshLabelPrototype()
{
	QString tempText("<font color=\"darkGreen\"><b>" + function()->elementType() + "</b></font> <font color=\"darkBlue\">" + function()->elementName() + "</font>(");
	
	for (int i=0 ; i < function()->childElements().size() ; i++) {
		tempText += "<font color=\"darkGreen\"><b>" + function()->childElements().at(i)->elementType() + "</b></font> <font color=\"darkMagenta\">" + function()->childElements().at(i)->elementName()  + "</font>";
		if (i != function()->childElements().size() - 1) tempText += ", ";
	}
	
	tempText += ");";
	
	labelPrototype->setText(tempText);
}

void MecFunctionEditor::addGeneralWidget(QWidget *Widget)
{
	layoutGeneral->addWidget(Widget, 0, layoutGeneral->columnCount());
}

void MecFunctionEditor::edit()
{
	tabWidgetMain->setCurrentWidget(widgetProperties);
	lineEditName->setFocus(Qt::OtherFocusReason);
}

void MecFunctionEditor::editChild(MecAbstractElement *Element)
{
MecAbstractElementEditor *tempEditor = elementEditor(Element);
if (tempEditor != 0 and tempEditor != this)
	{
	tabWidgetMain->setCurrentWidget(widgetProperties);
	scrollAreaVariables->ensureWidgetVisible(tempEditor);
	tempEditor->setFocus();
	tempEditor->edit();
	}
}

MecAbstractElementEditor* MecFunctionEditor::newSubEditor(MecAbstractElement *Element)
{
if (Element->elementRole() != MecAbstractElement::Variable) return 0;

MecAbstractPlugin *tempPlugin = mainEditor()->pluginsManager()->plugin(Element->elementRole(), Element->elementType());
if (tempPlugin != 0)
	{
	MecAbstractElementEditor *tempEditor = tempPlugin->elementEditor(Element, mainEditor());
	if (tempEditor != 0)
		{
		QWidget *tempFocusWidget = scrollAreaVariables->focusWidget();
		layoutEditorsVariables->removeItem(spacerItemEditorsVariables);
		layoutEditorsVariables->addWidget(tempEditor, 0, Qt::AlignTop);
		layoutEditorsVariables->addSpacerItem(spacerItemEditorsVariables);
		widgetEditorsVariables->show();
		tempEditor->show();
		scrollAreaVariables->ensureWidgetVisible(tempFocusWidget);
		
		return tempEditor;
		}
	else
		{
		addPluginsError(MecPluginError(MecPluginError::CastFailed, tempPlugin->name(), tempPlugin->version(), Element->elementRole(), Element->elementType(), Element));
		return 0;
		}
	}
else
	{
	addPluginsError(MecPluginError(MecPluginError::Unavailable, "", 0, Element->elementRole(), Element->elementType(), Element));
	return 0;
	}
}
	
void MecFunctionEditor::subEditorAboutToDeleted(MecAbstractElementEditor *Editor)
{
layoutEditorsVariables->removeWidget(Editor);
}

void MecFunctionEditor::reorderSubEditors()
{
	//Variables
	QList<MecAbstractVariable*> tempVariables = m_function->childVariables();
	layoutEditorsVariables->removeItem(spacerItemEditorsVariables);
	for (int i=0 ; i < tempVariables.size() ; i++)
		{
		layoutEditorsVariables->addWidget(elementEditor(tempVariables.at(i)), 0, Qt::AlignTop);
		}
	layoutEditorsVariables->addSpacerItem(spacerItemEditorsVariables);
}
	
void MecFunctionEditor::codeFunctionChanged(MecAbstractFunction *Function)
{
if (Function != function()) return;
}

void MecFunctionEditor::addConnection()
{
MecAbstractElement *tempElement = mainEditor()->selectedElement();
if (tempElement != 0 and tempElement->elementRole() == MecAbstractElement::Signal)
	{
	function()->connectSignal(static_cast<MecAbstractSignal*>(tempElement));
	mainEditor()->addEditStep(tr("Add connection between “%1” and “%2”").arg(tempElement->elementName()).arg(function()->elementName()));
	}
}

void MecFunctionEditor::removeConnection()
{
	listConnections->removeSelectedConnections();
}



