/*
© Quentin VIGNAUD, 2013

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#include "MecObjectCompiler.h"

MecObjectCompiler::MecObjectCompiler(MecAbstractObject* const Object, MecAbstractCompiler* const MainCompiler) : MecElementCompiler(Object, MainCompiler), m_object (Object)
{
for (int i=0 ; i < object()->childElements().size() ; i++)
	{
	MecAbstractPlugin *tempPlugin = mainCompiler()->pluginsManager()->plugin(object()->childElements().at(i)->elementRole(), object()->childElements().at(i)->elementType());
	if (tempPlugin != 0)
		{
		MecAbstractElementCompiler *tempCompiler = tempPlugin->elementCompiler(object()->childElements().at(i), mainCompiler());
		if (tempCompiler != 0)
			{
			addSubCompiler(tempCompiler);
			}
		else
			{
			addPluginsError(MecPluginError(MecPluginError::CastFailed, tempPlugin->name(), tempPlugin->version(), object()->childElements().at(i)->elementRole(), object()->childElements().at(i)->elementType(), object()->childElements().at(i)));
			}
		}
	else
		{
		addPluginsError(MecPluginError(MecPluginError::Unavailable, "", 0, object()->childElements().at(i)->elementRole(), object()->childElements().at(i)->elementType(), object()->childElements().at(i)));
		}
	}

//Établissement de m_concreteSubCompilers.
for (int i=0 ; i < subCompilers().size() ; i++)
	{
	m_concreteSubCompilers.append(static_cast<MecElementCompiler*>(subCompilers().at(i)));
	}

QList<MecAbstractElementCompiler*> tempList = subCompilers();//Établissement de m_recConcreteSubCompilers.
for (int i=0 ; i < tempList.size() ; i++)
	{
	tempList.append(tempList.at(i)->subCompilers());
	}
for (int i=0 ; i < tempList.size() ; i++)
	{
	m_recConcreteSubCompilers.append(static_cast<MecElementCompiler*>(tempList.at(i)));
	}
}

MecObjectCompiler::~MecObjectCompiler()
{
}
	
MecAbstractObject* MecObjectCompiler::object() const
{
return m_object;
}

QList<QResource*> MecObjectCompiler::resources()
{
QList<QResource*> tempList;
tempList += new QResource(":/share/icons/types/" + object()->elementType() + ".png");
return tempList;
}

QString MecObjectCompiler::projectInstructions()
{
return QString();
}
	
QString MecObjectCompiler::header()
{
QString tempString;

tempString = "/*\n\
© Quentin VIGNAUD, 2013\n\
\n\
Licensed under the EUPL, Version 1.1 only.\n\
You may not use this work except in compliance with the\n\
Licence.\n\
You may obtain a copy of the Licence at:\n\
\n\
http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available\n\
\n\
Unless required by applicable law or agreed to in\n\
writing, software distributed under the Licence is\n\
distributed on an “AS IS” basis,\n\
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either\n\
express or implied.\n\
See the Licence for the specific language governing\n\
permissions and limitations under the Licence.\n\
*/\n\n";

tempString += "#ifndef __" + object()->elementName().toUpper() + "_H__\n#define __" + object()->elementName().toUpper() + "_H__\n\n#include \"mecanique/Object.h\"\n";

for (int i=0 ; i < recConcreteSubCompilers().size() ; i++)
	{
	tempString += recConcreteSubCompilers().at(i)->preprocessorInstructions();
	}

tempString += "\nclass " + object()->elementName() + " : public " + object()->elementType() + "\n{\nQ_OBJECT\n\tpublic:\n";
tempString += object()->elementName() + "(Project* const Project);\n~" + object()->elementName() + "();\n\nvirtual void more() {}\n#pragma GCC diagnostic ignored \"-Wunused-parameter\"\nvirtual QVariant settings() {return QVariant();}\nvirtual void setSettings(QVariant Settings) {return;}\n#pragma GCC diagnostic pop\n\n\tpublic:\n";

for (int i=0 ; i < concreteSubCompilers().size() ; i++)
	{
	if (concreteSubCompilers().at(i)->element()->elementRole() == MecAbstractElement::Variable) tempString += concreteSubCompilers().at(i)->headerInstructions();
	}

tempString += "\tpublic slots:\n";

for (int i=0 ; i < concreteSubCompilers().size() ; i++)
	{
	if (concreteSubCompilers().at(i)->element()->elementRole() == MecAbstractElement::Function) tempString += concreteSubCompilers().at(i)->headerInstructions();
	}

tempString += "\tsignals:\n";

for (int i=0 ; i < concreteSubCompilers().size() ; i++)
	{
	if (concreteSubCompilers().at(i)->element()->elementRole() == MecAbstractElement::Signal) tempString += concreteSubCompilers().at(i)->headerInstructions();
	}

tempString += "};\n\n\n#endif /* __" + object()->elementName().toUpper() + "_H__ */\n\n";

return tempString;
}

QString MecObjectCompiler::source()
{
QString tempString;

tempString = "/*\n\
© Quentin VIGNAUD, 2013\n\
\n\
Licensed under the EUPL, Version 1.1 only.\n\
You may not use this work except in compliance with the\n\
Licence.\n\
You may obtain a copy of the Licence at:\n\
\n\
http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available\n\
\n\
Unless required by applicable law or agreed to in\n\
writing, software distributed under the Licence is\n\
distributed on an “AS IS” basis,\n\
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either\n\
express or implied.\n\
See the Licence for the specific language governing\n\
permissions and limitations under the Licence.\n\
*/\n\n";

tempString += "\n#include \"" + object()->elementName() + ".h\"\n\n";

tempString += object()->elementName() + "::" + object()->elementName() + "(Project* const Project) : " + object()->elementType() + "(\"" + object()->elementName() + "\", \"" + object()->elementType() + "\", Project)\n{\n";

for (int i=0 ; i < concreteSubCompilers().size() ; i++)
	{
	if (concreteSubCompilers().at(i)->element()->elementRole() == MecAbstractElement::Variable) tempString += concreteSubCompilers().at(i)->sourceInstructions();
	}

tempString += "}\n\n" + object()->elementName() + "::~" + object()->elementName() + "()\n{\n}\n\n";

for (int i=0 ; i < concreteSubCompilers().size() ; i++)
	{
	if (concreteSubCompilers().at(i)->element()->elementRole() == MecAbstractElement::Function) tempString += concreteSubCompilers().at(i)->sourceInstructions();
	}

tempString += "\n\n";

return tempString;
}

QString MecObjectCompiler::preprocessorInstructions()
{
return QString("#include \"" + object()->elementName() + ".h\"\n");
}

QString MecObjectCompiler::headerInstructions()
{
return QString(object()->elementName() + "* m_" + object()->elementName() + ";\n");
}

QString MecObjectCompiler::sourceInstructions()
{
return QString("m_" + object()->elementName() + " = new " + object()->elementName() + "(this);\n");
}

QList<MecElementCompiler*> MecObjectCompiler::concreteSubCompilers() const
{
return m_concreteSubCompilers;
}

QList<MecElementCompiler*> MecObjectCompiler::recConcreteSubCompilers() const
{
return m_recConcreteSubCompilers;
}


