/*
© Quentin VIGNAUD, 2013

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#include "MecPlugin.h"

MecPlugin::MecPlugin(const QString Name, const double Version, const MecAbstractElement::ElementRole Role, const QString Type, const QString Title) : m_name (Name), m_version (Version), m_role (Role), m_type (Type), m_title (Title)
{
m_manager = 0;
//On installe le traducteur du plugin.
QTranslator *pluginTranslator = new QTranslator();
pluginTranslator->load(m_name + QString(".") + QLocale::system().name().section('_', 0, 0), qApp->applicationDirPath() + QString("/../share/mecanique/translations"));
qApp->installTranslator(pluginTranslator);
}
	
QString MecPlugin::name() const
{
return m_name;
}

double MecPlugin::version() const
{
return m_version;
}

MecAbstractElement::ElementRole MecPlugin::role() const
{
return m_role;
}

QString MecPlugin::type() const
{
return m_type;
}

QString MecPlugin::title() const
{
return m_title;
}
	
MecAbstractPluginsManager* MecPlugin::manager() const
{
return m_manager;
}

void MecPlugin::setManager(MecAbstractPluginsManager* Manager)
{
m_manager = Manager;
}

