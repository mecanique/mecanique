######################################################################
# Project file implemented for qmake (version 3 with Qt 5)
######################################################################

TEMPLATE = lib
CONFIG += plugin
QT += widgets
DESTDIR = $$PWD/../../../lib/mecanique
CODECFORSRC = UTF-8
CODECFORTR = UTF-8
DEPENDPATH += $$PWD
INCLUDEPATH += $$PWD $$PWD/../../../include/mecanique

TRANSLATIONS += \
	$$PWD/../../../share/mecanique/translations/plugin.fr.ts

HEADERS += \
	$$PWD/../../../include/mecanique/MecAbstractCodeEditor.h \
	$$PWD/../../../include/mecanique/MecAbstractCompiler.h \
	$$PWD/../../../include/mecanique/MecAbstractEditor.h \
	$$PWD/../../../include/mecanique/MecAbstractElementCompiler.h \
	$$PWD/../../../include/mecanique/MecAbstractElementEditor.h \
	$$PWD/../../../include/mecanique/MecAbstractElement.h \
	$$PWD/../../../include/mecanique/MecAbstractFunction.h \
	$$PWD/../../../include/mecanique/MecAbstractObject.h \
	$$PWD/../../../include/mecanique/MecAbstractPlugin.h \
	$$PWD/../../../include/mecanique/MecAbstractPluginsManager.h \
	$$PWD/../../../include/mecanique/MecAbstractProject.h \
	$$PWD/../../../include/mecanique/MecAbstractSignal.h \
	$$PWD/../../../include/mecanique/MecAbstractVariable.h \
	$$PWD/../../../include/mecanique/MecPluginError.h \
	$$PWD/MecElementCompiler.h \
	$$PWD/MecElementEditor.h \
	$$PWD/MecFunctionCompiler.h \
	$$PWD/MecFunctionEditor.h \
	$$PWD/MecObjectCompiler.h \
	$$PWD/MecObjectEditor.h \
	$$PWD/MecPlugin.h \
	$$PWD/MecProjectCompiler.h \
	$$PWD/MecProjectEditor.h \
	$$PWD/MecSignalCompiler.h \
	$$PWD/MecSignalEditor.h \
	$$PWD/MecVariableCompiler.h \
	$$PWD/MecVariableEditor.h 

SOURCES += \
	$$PWD/MecElementCompiler.cpp \
	$$PWD/MecElementEditor.cpp \
	$$PWD/MecFunctionCompiler.cpp \
	$$PWD/MecFunctionEditor.cpp \
	$$PWD/MecObjectCompiler.cpp \
	$$PWD/MecObjectEditor.cpp \
	$$PWD/MecPlugin.cpp \
	$$PWD/MecProjectCompiler.cpp \
	$$PWD/MecProjectEditor.cpp \
	$$PWD/MecSignalCompiler.cpp \
	$$PWD/MecSignalEditor.cpp \
	$$PWD/MecVariableCompiler.cpp \
	$$PWD/MecVariableEditor.cpp


