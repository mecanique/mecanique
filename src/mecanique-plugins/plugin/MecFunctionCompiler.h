/*
© Quentin VIGNAUD, 2013

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#ifndef __MECFUNCTIONCOMPILER_H__
#define __MECFUNCTIONCOMPILER_H__

#include <MecAbstractFunction>
#include "MecElementCompiler.h"

/**
\brief	Classe de compilation d'une fonction.
*/
class MecFunctionCompiler : public MecElementCompiler
{
public:
	/**
	\brief	Constructeur.
	\param	Function	Fonction compilée, doit absolument exister lors de la construction (c.-à-d. instanciée et différente de 0) sinon un comportement inattendu pourrait se produire.
	\param	MainCompiler	Compilateur principal.
	\see	MecElementCompiler::MecElementCompiler()

	Établit les listes retournées par concreteSubCompilers() et recConcreteSubCompilers().
	*/
	MecFunctionCompiler(MecAbstractFunction* const Function, MecAbstractCompiler* const MainCompiler);
	///Destructeur.
	virtual ~MecFunctionCompiler();

	///Retourne la fonction compilée.
	MecAbstractFunction* function() const;

	/**
	Retourne la liste des ressources à ajouter au répertoire de compilation pour compiler cet élément.

	\return	une liste vide.
	*/
	virtual QList<QResource*> resources();
	/**
	Retourne les instructions à ajouter au fichier projet (".pro").

	\return	une chaîne vide.
	*/
	virtual QString projectInstructions();

	/**
	Retourne le contenu du header de l'élément.

	\return	une chaîne vide.
	*/
	QString header();
	/**
	Retourne le contenu du fichier d'implémentation de l'élément.

	\return	une chaîne vide.
	*/
	QString source();

	/**
	Retourne les instructions préprocesseur à ajouter dans le fichier header de l'élément parent.

	\return	une chaîne vide.
	*/
	virtual QString preprocessorInstructions();
	/**
	Retourne le code C++ à ajouter dans la déclaration de l'élément parent.

	La chaîne retournée est « [type] [nom]([arguments[=valeurs_par_défaut]]);\\n ».
	*/
	virtual QString headerInstructions();
	/**
	Retourne le code C++ à ajouter dans le fichier source de l'élément parent.

	La chaîne retournée si l'élément parent un objet ou un projet est « [inclusion préprocesseur des déclarations d'éléments utilisés] \\n [type] [nom_parent]::[nom]([arguments])\\n { \\n [code] \\n } \\n \\n », sinon cette dernière est vide.
	Le code inséré par cette fonction dans la chaîne de caractères est modifié de manière à ce que les appels directs à un élément soient transformés en appels conformes à l'implémentation finale C++ :
	\verbatim
	CompteARebours->start(2000);
	\endverbatim
	devient, si le parent de la fonction est un projet :
	\verbatim
	(m_CompteARebours)->start(2000);
	\endverbatim
	et s'il est un objet :
	\verbatim
	(static_cast<[NomProjet]*>(project())->m_CompteARebours)->start(2000);
	\endverbatim
	*/
	virtual QString sourceInstructions();

protected:
	///Retourne la liste complète des sous-compilateurs directs.
	QList<MecElementCompiler*> concreteSubCompilers() const;
	///Retourne la liste complète et récursive des sous-compilateurs.
	QList<MecElementCompiler*> recConcreteSubCompilers() const;

private:
	///Fonction compilée.
	MecAbstractFunction* const m_function;

	///Liste complète des sous-compilateurs directs.
	QList<MecElementCompiler*> m_concreteSubCompilers;
	///Liste complète récursive des sous-compilateurs.
	QList<MecElementCompiler*> m_recConcreteSubCompilers;
};


#endif /* __MECFUNCTIONCOMPILER_H__ */

