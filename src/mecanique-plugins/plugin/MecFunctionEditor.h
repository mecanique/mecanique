/*
© Quentin VIGNAUD, 2013

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#ifndef __MECFUNCTIONEDITOR_H__
#define __MECFUNCTIONEDITOR_H__

#include "MecElementEditor.h"
#include <MecAbstractFunction>

/**
\brief	Classe d'affichage des connexions d'une fonction.
*/
class MecFunctionConnectionsList : public QListWidget
{
	Q_OBJECT

public:
	/**
	\brief	Constructeur.
	\param	Function	Fonction éditée, doit absolument exister lors de la construction.
	\param	Parent	Widget parent.

	Initialise l'éditeur et ses widgets enfants, et organise ces derniers.
	*/
	MecFunctionConnectionsList(MecAbstractFunction* const Function, MecAbstractEditor* MainEditor, QWidget * Parent=0);
	/**
	\brief	Destructeur.
	*/
	~MecFunctionConnectionsList();

	///Retourne la fonction éditée.
	MecAbstractFunction* function() const;

public slots:
	/**
	\brief	Est déclenché lorsque la liste des signaux connectés a changé.
	*/
	void connectedSignalsChanged(MecAbstractFunction* Function);

	///Supprime les connections sélectionnées.
	void removeSelectedConnections();

protected:
	///Gère le déposer de cliquer/déposer.
	virtual void dropEvent(QDropEvent *Event);
	///Indique quels types MIME sont supportés.
	virtual QStringList mimeTypes() const;
	///Retourne les actions cliquer/déposer supportées.
	virtual Qt::DropActions supportedDropActions() const;

private:
	///Éditeur principal.
	MecAbstractEditor* m_mainEditor;
	///Fonction dont les connexions sont éditées.
	MecAbstractFunction* const m_function;

	///Correspondance signal/item.
	QMap<MecAbstractSignal*, QListWidgetItem*> m_match;

	/**
	\brief	Retourne le signal correspondant à \e Item.

	0 est retourné s'il n'existe pas de correspondance.
	*/
	MecAbstractSignal* findSignalWithItem(QListWidgetItem *Item) const;
	/**
	\brief	Retourne l'item correspondant à \e Signal.

	0 est retourné s'il n'existe pas de correspondance.
	*/
	QListWidgetItem* findItemWithSignal(MecAbstractSignal *Signal) const;

private slots:
	///Connecté avec les MecAbstractElement::childListChanged(MecElement*) et MecAbstractElement::nameChanged(MecElement*) des signaux.
	void signalChanged(MecAbstractElement *Element);
};

/**
\brief	Classe basique d'édition de fonction.

Cette classe fournit les précisions nécessaires à l'édition basique d'une fonction.
*/
class MecFunctionEditor : public MecElementEditor
{
	Q_OBJECT

public:
	/**
	\brief	Constructeur.
	\param	Function	Fonction éditée, doit absolument exister lors de la construction (c.à.d. instancié et différent de 0) sinon un comportement inattendu pourrait se produire.
	\param	MainEditor	Éditeur principal dont dépend cet éditeur d'élément.
	\param	Parent	Widget parent.
	\param	F	Drapeaux de fenêtre.

	Initialise l'éditeur et ses widgets enfants, et organise ces derniers.
	*/
	MecFunctionEditor(MecAbstractFunction* const Function, MecAbstractEditor* MainEditor, QWidget * Parent=0, Qt::WindowFlags F=0);
	/**
	\brief	Destructeur.
	*/
	virtual ~MecFunctionEditor();

	///Retourne la fonction éditée.
	MecAbstractFunction* function() const;

	/**
	\brief	Retourne un QVariant contenant toutes les données indiquant l'état actuel de l'éditeur.

	Cette donnée est vouée à être passée ultérieurement en paramètre à setState(QVariant) pour restaurer l'état visuel de l'éditeur.
	*/
	virtual QVariant state();
	/**
	\brief	Restaure l'état de l'éditeur selon les indications spécifées.

	\see	state()
	*/
	virtual void setState(QVariant State);

	///Indique si un MecObject peut être ajouté, retourne donc \e false.
	bool canAddObject() const;
	///Indique si une MecFunction peut être ajouté, retourne donc \e false.
	bool canAddFunction() const;
	///Indique si un MecSignal peut être ajouté, retourne donc \e false.
	bool canAddSignal() const;
	///Indique si une MecVariable peut être ajouté, retourne ici \e true.
	virtual bool canAddVariable() const;
	///Indique si un élément enfant peut être supprimé, retourne ici \e true.
	virtual bool canRemoveChild(MecAbstractElement* const Element) const;

	/**
	Indique si le contenu de la fonction est éditable.

	\see	setContentEditable(bool)
	*/
	virtual bool isContentEditable() const;

public slots:
	/**
	\brief	Dispose l'éditeur de manière à éditer l'élément de manière primaire.

	Affiche le panneau des propriétés et sélectionne le nom de la fonction.
	*/
	virtual void edit();
	/**
	\brief	Positionne la vue de manière à voir l'éditeur de \e Element.
	*/
	virtual void editChild(MecAbstractElement *Element);

	/**
	\brief	Demande un sous-éditeur pour \e Element.
	\note	\e Element doit être un élément enfant de element().
	\return	Le sous-éditeur demandé, ou 0 si éditer cet élément n'est pas possible.
	*/
	virtual MecAbstractElementEditor* newSubEditor(MecAbstractElement *Element);

	/**
	\brief	Est appelée juste avant la suppression de \e Editor.
	*/
	virtual void subEditorAboutToDeleted(MecAbstractElementEditor *Editor);

	/**
	\brief	Effectue la vérification de l'ordre des sous-éditeurs.
	*/
	virtual void reorderSubEditors();

	///Est déclenché lorsque le code de la fonction a changé.
	virtual void codeFunctionChanged(MecAbstractFunction *Function);

	//Ces fonctions sont bloquées pour ne rien faire.
	///Ne fait rien.
	void addObject();
	///Ne fait rien.
	void addFunction();
	///Ne fait rien.
	void addSignal();

	/**
	\brief	Autorise ou non l'édition du code de la fonction.
	*/
	virtual void setContentEditable(bool Editable);

	/**
	\brief	Actualise le label du prototype de la fonction.
	*/
	void refreshLabelPrototype();

signals:

protected:

	/**
	\brief	Ajoute un widget à l'affichage des données générales.
	*/
	virtual void addGeneralWidget(QWidget *Widget);

	///Layout d'organisation de l'éditeur.
	QGridLayout *layoutMain;

	///Onglets principaux.
	QTabWidget *tabWidgetMain;

	///Widget des propriétés.
	QWidget *widgetProperties;
	///Layout du widget des propriétés.
	QGridLayout *layoutProperties;

	///Groupe général.
	QGroupBox *groupBoxGeneral;
	///Layout du groupe général.
	QGridLayout *layoutGeneral;
	///Label d'affichage du prototype de la fonction.
	QLabel *labelPrototype;

	///Groupe des variables.
	QGroupBox *groupBoxVariables;
	///Layout des variables.
	QGridLayout *layoutVariables;

	///Zone de défilement des variables.
	QScrollArea *scrollAreaVariables;

	///Widget défilant des variables.
	QWidget *widgetEditorsVariables;
	///Layout d'organisation des variables.
	QVBoxLayout *layoutEditorsVariables;
	QSpacerItem *spacerItemEditorsVariables;

	///Widget du code.
	QWidget *widgetCode;
	///Layout du widget de code.
	QGridLayout *layoutCode;

	///Zone d'édition du code.
	MecAbstractCodeEditor *codeEditor;
	
	///Widget des connections.
	QWidget *widgetConnections;
	///Layout du widget de connection.
	QGridLayout *layoutConnections;

	///Liste d'édition des connections.
	MecFunctionConnectionsList *listConnections;
	///Bouton d'ajout de connection.
	QPushButton *pushButtonAddConnection;
	///Bouton de suppression de connection.
	QPushButton *pushButtonRemoveConnection;

private:
	///Fonction éditée.
	MecAbstractFunction* const m_function;

private slots:
	///Ajoute une connection.
	void addConnection();
	///Supprime une connection.
	void removeConnection();

};

#endif /* __MECFUNCTIONEDITOR_H__ */

