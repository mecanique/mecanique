/*
© Quentin VIGNAUD, 2013

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#ifndef __MECSIGNALCOMPILER_H__
#define __MECSIGNALCOMPILER_H__

#include <MecAbstractSignal>
#include "MecElementCompiler.h"

/**
\brief	Classe de compilation d'un signal.
*/
class MecSignalCompiler : public MecElementCompiler
{
public:
	/**
	\brief	Constructeur.
	\param	Signal	Signal compilé, doit absolument exister lors de la construction (c.-à-d. instancié et différent de 0) sinon un comportement inattendu pourrait se produire.
	\param	MainCompiler	Compilateur maître.
	*/
	MecSignalCompiler(MecAbstractSignal* const Signal, MecAbstractCompiler* const MainCompiler);
	///Destructeur.
	virtual ~MecSignalCompiler();

	///Retourne le signal compilé.
	MecAbstractSignal* signal() const;

	/**
	Retourne la liste des ressources à ajouter au répertoire de compilation pour compiler cet élément.

	\return	Une liste vide.
	*/
	virtual QList<QResource*> resources();
	/**
	Retourne les instructions à ajouter au fichier projet (".pro").

	\return	Une chaîne vide.
	*/
	virtual QString projectInstructions();

	/**
	Retourne le contenu du header de l'élément.

	\return	Une chaîne vide.
	*/
	QString header();
	/**
	Retourne le contenu du fichier d'implémentation de l'élément.

	\return	Une chaîne vide.
	*/
	QString source();

	/**
	Retourne les instructions préprocesseur à ajouter dans le fichier header de l'élément parent.

	\return	Une chaîne vide.
	*/
	virtual QString preprocessorInstructions();
	/**
	Retourne le code C++ à ajouter dans la déclaration de l'élément parent.

	La chaîne retournée est « void [nom]([arguments]);\\n ».
	*/
	virtual QString headerInstructions();
	/**
	Retourne le code C++ à ajouter dans le constructeur du projet.
	\warning	Ce code est celui ajouté directment dans le constructeur du projet général, et non de l'objet parent s'il y a lieu.

	La chaine retournée est « connect(m_[nom_objet_émetteur/this], SIGNAL([nom]([types_des_arguments])), m_[nom_objet_receveur/this], SLOT([nom_fonction]([types_des_arguments])))\\n » (réitéré pour chaque fonction receveuse).
	*/
	virtual QString sourceInstructions();

protected:
	///Retourne la liste complète des sous-compilateurs directs.
	QList<MecElementCompiler*> concreteSubCompilers() const;
	///Retourne la liste complète et récursive des sous-compilateurs.
	QList<MecElementCompiler*> recConcreteSubCompilers() const;

private:
	///Signal compilé.
	MecAbstractSignal* const m_signal;

	///Liste complète des sous-compilateurs directs.
	QList<MecElementCompiler*> m_concreteSubCompilers;
	///Liste complète récursive des sous-compilateurs.
	QList<MecElementCompiler*> m_recConcreteSubCompilers;

};


#endif /* __MECSIGNALCOMPILER_H__ */

