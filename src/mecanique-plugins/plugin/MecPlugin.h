/*
© Quentin VIGNAUD, 2013

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#ifndef __MECPLUGIN_H__
#define __MECPLUGIN_H__

#include <QtCore>
#include <MecAbstractPlugin>

/**
\brief	Interface des plugins.

Tout plugin doit avoir comme classe d'entrée une classe héritant publiquement de MecPlugin pour pouvoir être chargée par l'exécutable principal.
*/
class MecPlugin : public QObject, public MecAbstractPlugin
{
	Q_OBJECT
	Q_INTERFACES(MecAbstractPlugin)

public:
	/**
	\brief	Constructeur.
	\param	Name	Nom du plugin, unique parmi tous les plugins.
	\param	Version	Version du plugin.
	\param	Role	Rôle d'élément correspondant à ce plugin.
	\param	Type	Type de l'élément correspondant à ce plugin.
	*/
	MecPlugin(const QString Name, const double Version, const MecAbstractElement::ElementRole Role, const QString Type, const QString Title);
	///Destructeur.
	virtual ~MecPlugin() {}


	///Retourne le nom du plugin.
	QString name() const;
	///Retourne la version du plugin.
	double version() const;
	///Retourne le rôle d'élément du plugin.
	MecAbstractElement::ElementRole role() const;
	///Retourne le type d'élément du plugin.
	QString type() const;
	///Retourne le titre du plugin.
	QString title() const;

	/**
	Retourne le gestionnaire de plugins.

	\warning	Le gestionnaire n'est pas disponible durant l'instanciation du plugin, ne pas appeler en conséquence cette fonction dans le constructeur.
	*/
	MecAbstractPluginsManager* manager() const;
	///Fixe le gestionnaire de plugins.
	void setManager(MecAbstractPluginsManager* Manager);

private:
	///Nom du plugin.
	const QString m_name;
	///Version du plugin.
	const double m_version;
	///Rôle d'élément du plugin.
	const MecAbstractElement::ElementRole m_role;
	///Type d'élément du plugin.
	const QString m_type;
	///Titre du plugin.
	const QString m_title;

	///Gestionnaire du plugin.
	MecAbstractPluginsManager* m_manager;
};

#endif /* __MECPLUGIN_H__ */

