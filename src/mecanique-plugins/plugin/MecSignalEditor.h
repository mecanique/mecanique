/*
© Quentin VIGNAUD, 2013 - 2014

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#ifndef __MECSIGNALEDITOR_H__
#define __MECSIGNALEDITOR_H__

#include "MecElementEditor.h"
#include <MecAbstractSignal>
#include "MecVariableEditor.h"

/**
\brief	Classe d'affichage des connexions d'un signal.
*/
class MecSignalConnectionsList : public QListWidget
{
	Q_OBJECT

public:
	/**
	\brief	Constructeur.
	\param	Signal	Signal édité, doit absolument exister lors de la construction.
	\param	Parent	Widget parent.

	Initialise l'éditeur et ses widgets enfants, et organise ces derniers.
	*/
	MecSignalConnectionsList(MecAbstractSignal* const Signal, MecAbstractEditor* MainEditor, QWidget * Parent=0);
	/**
	\brief	Destructeur.
	*/
	~MecSignalConnectionsList();

	///Retourne le signal édité.
	MecAbstractSignal* signal() const;

public slots:
	/**
	\brief	Est déclenché lorsque la liste des fonctions connectées a changé.
	*/
	void connectedFunctionsChanged(MecAbstractSignal* Signal);

	///Supprime les connections sélectionnées.
	void removeSelectedConnections();

protected:
	///Gère le déposer de cliquer/déposer.
	virtual void dropEvent(QDropEvent *Event);
	///Indique quels types MIME sont supportés.
	virtual QStringList mimeTypes() const;
	///Retourne les actions cliquer/déposer supportées.
	virtual Qt::DropActions supportedDropActions() const;

private:
	///Éditeur principal.
	MecAbstractEditor* m_mainEditor;
	///Signal dont les connexions sont éditées.
	MecAbstractSignal* const m_signal;

	///Correspondance fonction/item.
	QMap<MecAbstractFunction*, QListWidgetItem*> m_match;

	/**
	\brief	Retourne la fonction correspondant à \e Item.

	0 est retourné s'il n'existe pas de correspondance.
	*/
	MecAbstractFunction* findFunctionWithItem(QListWidgetItem *Item) const;
	/**
	\brief	Retourne l'item correspondant à \e Fonction.

	0 est retourné s'il n'existe pas de correspondance.
	*/
	QListWidgetItem* findItemWithFunction(MecAbstractFunction *Function) const;

private slots:
	///Connecté avec les MecAbstractElement::childListChanged(MecElement*) et MecAbstractElement::nameChanged(MecElement*) des fonctions.
	void functionChanged(MecAbstractElement *Element);
};

/**
\brief	Classe basique d'édition de signal.

Cette classe fournit les précisions nécessaires à l'édition basique d'un signal.
*/
class MecSignalEditor : public MecElementEditor
{
	Q_OBJECT

public:
	/**
	\brief	Constructeur.
	\param	Signal	Signal édité, doit absolument exister lors de la construction (c.à.d. instancié et différent de 0) sinon un comportement inattendu pourrait se produire.
	\param	MainEditor	Éditeur principal dont dépend cet éditeur d'élément.
	\param	Parent	Widget parent.
	\param	F	Drapeaux de fenêtre.

	Initialise l'éditeur et ses widgets enfants, et organise ces derniers.
	*/
	MecSignalEditor(MecAbstractSignal* const Signal, MecAbstractEditor* MainEditor, QWidget * Parent=0, Qt::WindowFlags F=0);
	/**
	\brief	Destructeur.
	*/
	virtual ~MecSignalEditor();

	///Retourne le signal édité.
	MecAbstractSignal* signal() const;

	/**
	\brief	Retourne un QVariant contenant toutes les données indiquant l'état actuel de l'éditeur.

	Cette donnée est vouée à être passée ultérieurement en paramètre à setState(QVariant) pour restaurer l'état visuel de l'éditeur.
	*/
	virtual QVariant state();
	/**
	\brief	Restaure l'état de l'éditeur selon les indications spécifées.

	\see	state()
	*/
	virtual void setState(QVariant State);

	///Indique si un MecObject peut être ajouté, retourne donc \e false.
	bool canAddObject() const;
	///Indique si une MecFunction peut être ajouté, retourne donc \e false.
	bool canAddFunction() const;
	///Indique si un MecSignal peut être ajouté, retourne donc \e false.
	bool canAddSignal() const;
	///Indique si une MecVariable peut être ajouté, retourne donc ici \e true.
	virtual bool canAddVariable() const;
	///Indique si un élément enfant peut être supprimé, retourne donc ici \e true.
	virtual bool canRemoveChild(MecAbstractElement* const Element) const;

	/**
	Indique si le contenu du signal est éditable.

	\return	true
	*/
	virtual bool isContentEditable() const;

public slots:
	/**
	\brief	Dispose l'éditeur de manière à éditer l'élément de manière primaire.

	Affiche le panneau des propriétés et sélectionne le nom du signal.
	*/
	virtual void edit();
	/**
	\brief	Positionne la vue de manière à voir l'éditeur de \e Element.
	*/
	virtual void editChild(MecAbstractElement *Element);

	///Ne fait rien.
	virtual void setContentEditable(bool Editable);

	/**
	\brief	Demande un sous-éditeur pour \e Element.
	\note	\e Element doit être un élément enfant de element().

	Demande un éditeur au gestionnaire de plugins, correspondant aux propriétés de Element, l'intègre dans l'interface utilisateur actuelle, et le retourne.

	\return	Le sous-éditeur demandé, ou 0 si éditer cet élément n'est pas possible.
	*/
	virtual MecAbstractElementEditor* newSubEditor(MecAbstractElement *Element);

	/**
	\brief	Est appelée juste avant la suppression de \e Editor.

	Supprime l'éditeur spécifié de l'interface utilisateur.
	*/
	virtual void subEditorAboutToDeleted(MecAbstractElementEditor *Editor);

	/**
	\brief	Effectue la vérification de l'ordre des sous-éditeurs.
	*/
	virtual void reorderSubEditors();

	///Ne fait rien.
	void typeElementChanged(MecAbstractElement *Element);
	///Ne fait rien.
	void addObject();
	///Ne fait rien.
	void addFunction();
	///Ne fait rien.
	void addSignal();

	/**
	\brief	Actualise le label du prototype du signal.
	*/
	void refreshLabelPrototype();


signals:

protected:

	/**
	\brief	Ajoute un widget à l'affichage des données générales.
	*/
	virtual void addGeneralWidget(QWidget *Widget);

	///Layout d'organisation de l'éditeur.
	QGridLayout *layoutMain;

	///Onglets principaux.
	QTabWidget *tabWidgetMain;

	///Widget des propriétés.
	QWidget *widgetProperties;
	///Layout du widget des propriétés.
	QGridLayout *layoutProperties;

	///Groupe général
	QGroupBox *groupBoxGeneral;
	///Layout du groupe général
	QGridLayout *layoutGeneral;
	///Label d'affichage du prototype.
	QLabel *labelPrototype;

	///Groupe des variables
	QGroupBox *groupBoxVariables;
	///Layout des variables.
	QGridLayout *layoutVariables;

	///Zone de défilement des variables.
	QScrollArea *scrollAreaVariables;

	///Widget défilant des variables.
	QWidget *widgetEditorsVariables;
	///Layout d'organisation des variables.
	QVBoxLayout *layoutEditorsVariables;
	///SpacerItem de « condensation », relatif à layoutEditorsVariables.
	QSpacerItem *spacerItemEditorsVariables;

	///Widget des connections.
	QWidget *widgetConnections;
	///Layout du widget de connection.
	QGridLayout *layoutConnections;

	///Liste d'édition des connections.
	MecSignalConnectionsList *listConnections;
	///Bouton d'ajout de connection.
	QPushButton *pushButtonAddConnection;
	///Bouton de suppression de connection.
	QPushButton *pushButtonRemoveConnection;

private:
	///Signal édité.
	MecAbstractSignal* const m_signal;


private slots:
	///Ajoute une connection.
	void addConnection();
	///Supprime une connection.
	void removeConnection();

};

#endif /* __MECSIGNALEDITOR_H__ */

