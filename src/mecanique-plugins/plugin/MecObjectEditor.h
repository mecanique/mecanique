/*
© Quentin VIGNAUD, 2013

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#ifndef __MECOBJECTEDITOR_H__
#define __MECOBJECTEDITOR_H__

#include "MecElementEditor.h"
#include <MecAbstractObject>

/**
\brief	Classe basique d'édition d'objet.

Cette classe fournit les précisions nécessaires à l'édition basique d'un objet.
*/
class MecObjectEditor : public MecElementEditor
{
	Q_OBJECT

public:
	/**
	\brief	Constructeur.
	\param	Object	Objet édité, doit absolument exister lors de la construction (c.à.d. instancié et différent de 0) sinon un comportement inattendu pourrait se produire.
	\param	MainEditor	Éditeur principal dont dépend cet éditeur d'élément.
	\param	Parent	Widget parent.
	\param	F	Drapeaux de fenêtre.

	Initialise l'éditeur et ses widgets enfants, et organise ces derniers.
	*/
	MecObjectEditor(MecAbstractObject* const Object, MecAbstractEditor* MainEditor, QWidget * Parent=0, Qt::WindowFlags F=0);
	/**
	\brief	Destructeur.
	*/
	virtual ~MecObjectEditor();

	///Retourne l'objet édité.
	MecAbstractObject* object() const;

	/**
	\brief	Retourne un QVariant contenant toutes les données indiquant l'état actuel de l'éditeur.

	Cette donnée est vouée à être passée ultérieurement en paramètre à setState(QVariant) pour restaurer l'état visuel de l'éditeur.
	*/
	virtual QVariant state();
	/**
	\brief	Restaure l'état de l'éditeur selon les indications spécifées.

	\see	state()
	*/
	virtual void setState(QVariant State);

	///Indique si un MecObject peut être ajouté, retourne donc \e false.
	bool canAddObject() const;
	///Indique si une MecFunction peut être ajouté, retourne donc ici \e true.
	virtual bool canAddFunction() const;
	///Indique si un MecSignal peut être ajouté, retourne donc ici \e true.
	virtual bool canAddSignal() const;
	///Indique si une MecVariable peut être ajouté, retourne donc ici \e true.
	virtual bool canAddVariable() const;
	///Indique si un élément enfant peut être supprimé, retourne donc \e true.
	virtual bool canRemoveChild(MecAbstractElement* const Element) const;

	/**
	Indique si le contenu de l'objet est éditable.

	\see	setContentEditable(bool)
	*/
	virtual bool isContentEditable() const;

public slots:
	/**
	\brief	Dispose l'éditeur de manière à éditer l'élément de manière primaire.

	Affiche le panneau des propriétés et sélectionne le nom de l'objet.
	*/
	virtual void edit();
	/**
	\brief	Positionne la vue de manière à voir l'éditeur de \e Element.
	*/
	virtual void editChild(MecAbstractElement *Element);

	/**
	\brief	Autorise ou non l'édition du contenu des sous-éléments de l'objet (les fonctions, signaux et variables).
	*/
	virtual void setContentEditable(bool Editable);

	/**
	\brief	Demande un sous-éditeur pour \e Element.
	\note	\e Element doit être un élément enfant de element().

	Demande un éditeur au gestionnaire de plugins, correspondant aux propriétés de Element, l'intègre dans l'interface utilisateur actuelle, et le retourne.

	\return	Le sous-éditeur demandé, ou 0 si éditer cet élément n'est pas possible.
	*/
	virtual MecAbstractElementEditor* newSubEditor(MecAbstractElement *Element);

	/**
	\brief	Est appelée juste avant la suppression de \e Editor.

	Supprime l'éditeur spécifié de l'interface utilisateur.
	*/
	virtual void subEditorAboutToDeleted(MecAbstractElementEditor *Editor);

	/**
	\brief	Effectue la vérification de l'ordre des sous-éditeurs.
	*/
	virtual void reorderSubEditors();

	///Ne fait rien.
	void addObject();


signals:

protected:

	/**
	\brief	Ajoute un widget à l'affichage des données générales.
	*/
	virtual void addGeneralWidget(QWidget *Widget);

	///Layout d'organisation de l'éditeur.
	QGridLayout *layoutMain;

	///Onglets principaux.
	QTabWidget *tabWidgetMain;

	///Widget des propriétés.
	QWidget *widgetProperties;
	///Layout du widget des propriétés.
	QGridLayout *layoutProperties;

	///Groupe général.
	QGroupBox *groupBoxGeneral;
	///Layout du groupe général.
	QHBoxLayout *layoutGeneral;

	///Groupe référence.
	QGroupBox *groupBoxReference;
	///Layout du groupe référence.
	QGridLayout *layoutReference;

	///Visionneur de référence.
	QTextBrowser *browserReference;

	///Widget des fonction.
	QWidget *widgetFunctions;
	///Layout d'organisation des fonctions.
	QGridLayout *layoutFunctions;

	///Onglets des fonctions.
	QTabWidget *tabWidgetFunctions;

	///Widget des signaux.
	QWidget *widgetSignals;
	///Layout d'organisation des signaux.
	QGridLayout *layoutSignals;

	///Onglets des signaux.
	QTabWidget *tabWidgetSignals;

	///Widget des variables.
	QWidget *widgetVariables;
	///Layout des variables.
	QGridLayout *layoutVariables;

	///Zone de défilement des variables.
	QScrollArea *scrollAreaVariables;

	///Widget défilant des variables.
	QWidget *widgetEditorsVariables;
	///Layout d'organisation des variables.
	QVBoxLayout *layoutEditorsVariables;
	///SpacerItem de « condensation », relatif à layoutEditorsVariables.
	QSpacerItem *spacerItemEditorsVariables;


private:
	///Objet édité.
	MecAbstractObject* const m_object;


private slots:
	//Connecté à chacun des éléments enfants par "nameChanged()"
	void childNameChanged(MecAbstractElement *Element);

};

#endif /* __MECOBJECTEDITOR_H__ */

