/*
© Quentin VIGNAUD, 2013

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#include "MecProjectCompiler.h"

MecProjectCompiler::MecProjectCompiler(MecAbstractProject* const Project, MecAbstractCompiler* const MainCompiler) : MecElementCompiler(Project, MainCompiler), m_project (Project)
{
for (int i=0 ; i < project()->childElements().size() ; i++)
	{
	MecAbstractPlugin *tempPlugin = mainCompiler()->pluginsManager()->plugin(project()->childElements().at(i)->elementRole(), project()->childElements().at(i)->elementType());
	if (tempPlugin != 0)
		{
		MecAbstractElementCompiler *tempCompiler = tempPlugin->elementCompiler(project()->childElements().at(i), mainCompiler());
		if (tempCompiler != 0)
			{
			addSubCompiler(tempCompiler);
			}
		else
			{
			addPluginsError(MecPluginError(MecPluginError::CastFailed, tempPlugin->name(), tempPlugin->version(), project()->childElements().at(i)->elementRole(), project()->childElements().at(i)->elementType(), project()->childElements().at(i)));
			}
		}
	else
		{
		addPluginsError(MecPluginError(MecPluginError::Unavailable, "", 0, project()->childElements().at(i)->elementRole(), project()->childElements().at(i)->elementType(), project()->childElements().at(i)));
		}
	}

//Établissement de m_concreteSubCompilers.
for (int i=0 ; i < subCompilers().size() ; i++)
	{
	m_concreteSubCompilers.append(static_cast<MecElementCompiler*>(subCompilers().at(i)));
	}

QList<MecAbstractElementCompiler*> tempList = subCompilers();//Établissement de m_recConcreteSubCompilers.
for (int i=0 ; i < tempList.size() ; i++)
	{
	tempList.append(tempList.at(i)->subCompilers());
	}
for (int i=0 ; i < tempList.size() ; i++)
	{
	m_recConcreteSubCompilers.append(static_cast<MecElementCompiler*>(tempList.at(i)));
	}
}

MecProjectCompiler::~MecProjectCompiler()
{
}
	
MecAbstractProject* MecProjectCompiler::project() const
{
return m_project;
}

QList<QResource*> MecProjectCompiler::resources()
{
QList<QResource*> tempList;
tempList += new QResource(":/share/icons/types/" + project()->elementType() + ".png");
return tempList;
}

QString MecProjectCompiler::projectInstructions()
{
return QString();
}
	
QString MecProjectCompiler::header()
{
QString tempString;

tempString = "/*\n\
© Quentin VIGNAUD, 2013\n\
\n\
Licensed under the EUPL, Version 1.1 only.\n\
You may not use this work except in compliance with the\n\
Licence.\n\
You may obtain a copy of the Licence at:\n\
\n\
http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available\n\
\n\
Unless required by applicable law or agreed to in\n\
writing, software distributed under the Licence is\n\
distributed on an “AS IS” basis,\n\
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either\n\
express or implied.\n\
See the Licence for the specific language governing\n\
permissions and limitations under the Licence.\n\
*/\n\n";

tempString += "#ifndef __" + project()->elementName().toUpper() + "_H__\n#define __" + project()->elementName().toUpper() + "_H__\n\n#include \"mecanique/Project.h\"\n";

for (int i=0 ; i < recConcreteSubCompilers().size() ; i++)
	{
	tempString += recConcreteSubCompilers().at(i)->preprocessorInstructions();
	}

tempString += "\nclass " + project()->elementName() + " : public " + project()->elementType() + "\n{\nQ_OBJECT\n\tpublic:\n";
tempString += project()->elementName() + "();\n~" + project()->elementName() + "();\n\n";

for (int i=0 ; i < concreteSubCompilers().size() ; i++)
	{
	if (concreteSubCompilers().at(i)->element()->elementRole() == MecAbstractElement::Variable) tempString += concreteSubCompilers().at(i)->headerInstructions();
	}

for (int i=0 ; i < concreteSubCompilers().size() ; i++)
	{
	if (concreteSubCompilers().at(i)->element()->elementRole() == MecAbstractElement::Object) tempString += concreteSubCompilers().at(i)->headerInstructions();
	}

tempString += "\tpublic slots:\n";

for (int i=0 ; i < concreteSubCompilers().size() ; i++)
	{
	if (concreteSubCompilers().at(i)->element()->elementRole() == MecAbstractElement::Function) tempString += concreteSubCompilers().at(i)->headerInstructions();
	}

tempString += "\tsignals:\n";

for (int i=0 ; i < concreteSubCompilers().size() ; i++)
	{
	if (concreteSubCompilers().at(i)->element()->elementRole() == MecAbstractElement::Signal) tempString += concreteSubCompilers().at(i)->headerInstructions();
	}

tempString += "};\n\n\n#endif /* __" + project()->elementName().toUpper() + "_H__ */\n\n";

return tempString;
}

QString MecProjectCompiler::source()
{
QString tempString;

tempString = "/*\n\
© Quentin VIGNAUD, 2013\n\
\n\
Licensed under the EUPL, Version 1.1 only.\n\
You may not use this work except in compliance with the\n\
Licence.\n\
You may obtain a copy of the Licence at:\n\
\n\
http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available\n\
\n\
Unless required by applicable law or agreed to in\n\
writing, software distributed under the Licence is\n\
distributed on an “AS IS” basis,\n\
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either\n\
express or implied.\n\
See the Licence for the specific language governing\n\
permissions and limitations under the Licence.\n\
*/\n\n";

tempString += "\n#include \"" + project()->elementName() + ".h\"\n\n";

tempString += project()->elementName() + "::" + project()->elementName() + "()\n{\n";

tempString += "setTitle(\"" + project()->title() + "\");\n";

tempString += "setSynopsis(\"" + project()->synopsis().replace("\n", "\\n\\\n").replace("\"", "\\\"") + "\");\n";

for (int i=0 ; i < concreteSubCompilers().size() ; i++)
	{
	if (concreteSubCompilers().at(i)->element()->elementRole() == MecAbstractElement::Variable) tempString += concreteSubCompilers().at(i)->sourceInstructions();
	}

for (int i=0 ; i < concreteSubCompilers().size() ; i++)
	{
	if (concreteSubCompilers().at(i)->element()->elementRole() == MecAbstractElement::Object) tempString += concreteSubCompilers().at(i)->sourceInstructions();
	}

QList<MecAbstractElementCompiler*> signalCompilers;//Établissement de la liste des compilateurs de signaux de l'ensemble du projet.
for (int i=0 ; i < concreteSubCompilers().size() ; i++)
	{
	if (concreteSubCompilers().at(i)->element()->elementRole() == MecAbstractElement::Object)
		{
		for (int j=0 ; j < concreteSubCompilers().at(i)->subCompilers().size() ; j++)
			{
			if (concreteSubCompilers().at(i)->subCompilers().at(j)->element()->elementRole() == MecAbstractElement::Signal) signalCompilers.append(concreteSubCompilers().at(i)->subCompilers().at(j));
			}
		}
	else if (concreteSubCompilers().at(i)->element()->elementRole() == MecAbstractElement::Signal) signalCompilers.append(concreteSubCompilers().at(i));
	}

for (int i=0 ; i < signalCompilers.size() ; i++)
	{
	tempString += static_cast<MecElementCompiler*>(signalCompilers.at(i))->sourceInstructions();
	}
 

tempString += "}\n\n" + project()->elementName() + "::~" + project()->elementName() + "()\n{\n}\n\n";

for (int i=0 ; i < concreteSubCompilers().size() ; i++)
	{
	if (concreteSubCompilers().at(i)->element()->elementRole() == MecAbstractElement::Function) tempString += concreteSubCompilers().at(i)->sourceInstructions();
	}

tempString += "\n\n";

return tempString;
}

QString MecProjectCompiler::preprocessorInstructions()
{
return QString("#include \"" + project()->elementName() + ".h\"\n");
}

QString MecProjectCompiler::headerInstructions()
{
return QString(project()->elementType() + "* " + project()->elementName() + ";\n");
}

QString MecProjectCompiler::sourceInstructions()
{
return QString(project()->elementName() + " = new " + project()->elementType() + "();\n");
}

QList<MecElementCompiler*> MecProjectCompiler::concreteSubCompilers() const
{
return m_concreteSubCompilers;
}

QList<MecElementCompiler*> MecProjectCompiler::recConcreteSubCompilers() const
{
return m_recConcreteSubCompilers;
}


