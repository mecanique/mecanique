/*
© Quentin VIGNAUD, 2013

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#ifndef __MECPROJECTCOMPILER_H__
#define __MECPROJECTCOMPILER_H__

#include <MecAbstractProject>
#include "MecElementCompiler.h"

/**
\brief	Classe de compilation d'un projet.
*/
class MecProjectCompiler : public MecElementCompiler
{
public:
	/**
	\brief	Constructeur.
	\param	Project	Projet compilé, doit absolument exister lors de la construction (c.-à-d. instancié et différent de 0) sinon un comportement inattendu pourrait se produire.
	\param	MainCompiler	Compilateur maître.
	*/
	MecProjectCompiler(MecAbstractProject* const Project, MecAbstractCompiler* const MainCompiler);
	///Destructeur.
	virtual ~MecProjectCompiler();

	///Retourne le projet compilé.
	MecAbstractProject* project() const;

	/**
	\brief	Retourne la liste des ressources à ajouter au répertoire de compilation pour compiler cet élément.
	\return	une liste d'une ressource, correspondant à l'icône du type de l'objet.
	*/
	virtual QList<QResource*> resources();
	/**
	\brief	Retourne les instructions à ajouter au fichier projet (".pro").
	\return	une chaîne vide.
	*/
	virtual QString projectInstructions();

	/**
	\brief	Retourne le contenu du header de l'élément.
	\code
	//Licence
	#ifndef __NOMPROJET_H__
	#define __NOMPROJET_H__

	#include "mecanique/Project.h"
	//Instructions préprocesseur relatives aux éléments enfants.

	class nomprojet : public Project
	{
	Q_OBJECT
	public:
	nomprojet();
	~nomprojet();

	//Déclaration des variables.
	public slots:
	//Déclaration des fonctions.
	signals:
	//Déclaration des signaux.
	};


	#endif	// __NOMPROJET_H__
	\endcode
	*/
	virtual QString header();
	/**
	\brief	Retourne le contenu du fichier d'implémentation de l'élément.
	\code
	//Licence
	#include "nomprojet.h"

	nomprojet::nomprojet()
	{
	setTitle("Titre");
	setSynopsis("Synopsis");
	//Valeurs par défaut des variables.
	//Instanciation des objets.
	//Établissement des connexions.
	}

	nomprojet::~nomprojet()
	{
	}

	//Implémentation des fonctions.
	\endcode
	*/
	virtual QString source();

	/**
	Retourne les instructions préprocesseur à ajouter dans le fichier header de l'élément parent.

	\return	« #include nomprojet.h\\n »
	*/
	virtual QString preprocessorInstructions();
	/**
	Retourne le code C++ à ajouter dans la déclaration de l'élément parent.

	\return	« typeprojet* nomprojet;\\n »
	*/
	virtual QString headerInstructions();
	/**
	Retourne le code C++ à ajouter dans le constructeur de l'élément parent.

	\return	« nomprojet = new typeprojet();\\n »
	*/
	virtual QString sourceInstructions();

protected:
	///Retourne la liste complète des sous-compilateurs directs.
	QList<MecElementCompiler*> concreteSubCompilers() const;
	///Retourne la liste complète et récursive des sous-compilateurs.
	QList<MecElementCompiler*> recConcreteSubCompilers() const;

private:
	///Projet compilé.
	MecAbstractProject* const m_project;

	///Liste complète des sous-compilateurs directs.
	QList<MecElementCompiler*> m_concreteSubCompilers;
	///Liste complète récursive des sous-compilateurs.
	QList<MecElementCompiler*> m_recConcreteSubCompilers;

};


#endif /* __MECPROJECTCOMPILER_H__ */

