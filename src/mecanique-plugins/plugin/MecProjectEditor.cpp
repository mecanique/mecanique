/*
© Quentin VIGNAUD, 2013

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#include "MecProjectEditor.h"

MecProjectEditor::MecProjectEditor(MecAbstractProject* const Project, MecAbstractEditor* MainEditor, QWidget * Parent, Qt::WindowFlags F) : MecElementEditor(Project, MainEditor, Parent, F), m_project (Project)
{
tabWidgetMain = new QTabWidget(this);
	tabWidgetMain->setTabPosition(QTabWidget::West);

//Propriétés
widgetProperties = new QWidget(tabWidgetMain);

groupBoxGeneral = new QGroupBox(tr("General"), widgetProperties);
//label et lineEdit "Name" sont déjà déclarés,
//en revanche on masque ceux du type…
labelType->hide();
comboBoxType->hide();
labelTitle = new QLabel(tr("Title:"), groupBoxGeneral);
lineEditTitle = new QLineEdit(groupBoxGeneral);
	connect(lineEditTitle, SIGNAL(textEdited(QString)), this, SLOT(setProjectTitle()));
gridLayoutGeneral = new QGridLayout(groupBoxGeneral);
	gridLayoutGeneral->addWidget(pushButtonIcon, 0, 0, 4, 1, Qt::AlignHCenter | Qt::AlignVCenter);
	gridLayoutGeneral->addWidget(labelName, 0, 1);
	gridLayoutGeneral->addWidget(lineEditName, 1, 1);
	gridLayoutGeneral->addWidget(labelTitle, 2, 1);
	gridLayoutGeneral->addWidget(lineEditTitle, 3, 1);

groupBoxSynopsis = new QGroupBox(tr("Synopsis"), widgetProperties);
textEditSynopsis = new QTextEdit(groupBoxSynopsis);
	textEditSynopsis->setTabStopWidth(40);
	connect(textEditSynopsis, SIGNAL(textChanged()), SLOT(textEditSynopsisChanged()));
gridLayoutSynopsis = new QGridLayout(groupBoxSynopsis);
	gridLayoutSynopsis->addWidget(textEditSynopsis, 0, 0);

layoutProperties = new QGridLayout(widgetProperties);
	layoutProperties->addWidget(groupBoxGeneral, 0, 0, 1, 4);
	layoutProperties->addWidget(groupBoxSynopsis, 1, 0, 4, 4);

tabWidgetMain->addTab(widgetProperties, QIcon(":/share/icons/properties.png"), tr("Properties"));

//Objets
widgetObjects = new QWidget(tabWidgetMain);

tabWidgetObjects = new QTabWidget(widgetObjects);

layoutObjects = new QGridLayout(widgetObjects);
	layoutObjects->addWidget(tabWidgetObjects, 0, 0);

tabWidgetMain->addTab(widgetObjects, QIcon(":/share/icons/object.png"), tr("Objects"));

//Fonctions
widgetFunctions = new QWidget(tabWidgetMain);

tabWidgetFunctions = new QTabWidget(widgetFunctions);

layoutFunctions = new QGridLayout(widgetFunctions);
	layoutFunctions->addWidget(tabWidgetFunctions, 0, 0);

tabWidgetMain->addTab(widgetFunctions, QIcon(":/share/icons/functions.png"), tr("Functions"));

//Signaux
widgetSignals = new QWidget(tabWidgetMain);

tabWidgetSignals = new QTabWidget(widgetSignals);

layoutSignals = new QGridLayout(widgetSignals);
	layoutSignals->addWidget(tabWidgetSignals, 0, 0);

tabWidgetMain->addTab(widgetSignals, QIcon(":/share/icons/signal.png"), tr("Signals"));

//Variables
widgetVariables = new QWidget(tabWidgetMain);

scrollAreaVariables = new QScrollArea(widgetVariables);

	widgetEditorsVariables = new QWidget(scrollAreaVariables);
	
	layoutEditorsVariables = new QVBoxLayout(widgetEditorsVariables);
		spacerItemEditorsVariables = new QSpacerItem(0, 0, QSizePolicy::Minimum, QSizePolicy::Expanding);
		layoutEditorsVariables->addSpacerItem(spacerItemEditorsVariables);

scrollAreaVariables->setWidget(widgetEditorsVariables);
	scrollAreaVariables->setWidgetResizable(true);

layoutVariables = new QGridLayout(widgetVariables);
	layoutVariables->addWidget(scrollAreaVariables, 0, 0);

tabWidgetMain->addTab(widgetVariables, QIcon(":/share/icons/variable.png"), tr("Variables"));

//Finition
layoutMain = new QGridLayout(this);
	layoutMain->addWidget(tabWidgetMain, 0, 0);

connect(project(), SIGNAL(titleChanged(MecAbstractProject*)), SLOT(titleProjectChanged(MecAbstractProject*)));
connect(project(), SIGNAL(synopsisChanged(MecAbstractProject*)), SLOT(synopsisProjectChanged(MecAbstractProject*)));
titleProjectChanged(project());
synopsisProjectChanged(project());
}

MecProjectEditor::~MecProjectEditor()
{
//Rien de particulier
}

MecAbstractProject* MecProjectEditor::project() const
{
return m_project;
}

QVariant MecProjectEditor::state()
{
	QHash<QString, QVariant> tempProperties;
	tempProperties.insert("inheritedProperties", MecElementEditor::state());
	
	tempProperties.insert("lineEditTitleFocus", QVariant(lineEditTitle->hasFocus()));
	tempProperties.insert("lineEditTitlePos", QVariant(lineEditTitle->cursorPosition()));
	tempProperties.insert("textEditSynopsisPos", QVariant(textEditSynopsis->verticalScrollBar()->value()));
	
	tempProperties.insert("currentTab", QVariant(tabWidgetMain->currentIndex()));
	tempProperties.insert("currentObject", QVariant(tabWidgetObjects->currentIndex()));
	tempProperties.insert("currentFunction", QVariant(tabWidgetFunctions->currentIndex()));
	tempProperties.insert("currentSignal", QVariant(tabWidgetSignals->currentIndex()));
	tempProperties.insert("posVariable", QVariant(scrollAreaVariables->verticalScrollBar()->value()));
	return QVariant(tempProperties);
}

void MecProjectEditor::setState(QVariant State)
{
	QHash<QString, QVariant> tempProperties = State.toHash();
	MecElementEditor::setState(tempProperties.value("inheritedProperties"));
	
	if (tempProperties.value("lineEditTitleFocus").toBool()) lineEditTitle->setFocus();
	lineEditTitle->setCursorPosition(tempProperties.value("lineEditTitlePos").toInt());
	textEditSynopsis->verticalScrollBar()->setValue(tempProperties.value("textEditSynopsisPos").toInt());
	
	tabWidgetMain->setCurrentIndex(tempProperties.value("currentTab").toInt());
	tabWidgetObjects->setCurrentIndex(tempProperties.value("currentObject").toInt());
	tabWidgetFunctions->setCurrentIndex(tempProperties.value("currentFunction").toInt());
	tabWidgetSignals->setCurrentIndex(tempProperties.value("currentSignal").toInt());
	scrollAreaVariables->verticalScrollBar()->setValue(tempProperties.value("posVariable").toInt());
}

bool MecProjectEditor::canAddObject() const
{
return true;
}

bool MecProjectEditor::canAddFunction() const
{
return true;
}

bool MecProjectEditor::canAddSignal() const
{
return true;
}

bool MecProjectEditor::canAddVariable() const
{
return true;
}

bool MecProjectEditor::canRemoveChild(MecAbstractElement* const Element) const
{
return true;
}

bool MecProjectEditor::isContentEditable() const
{
	if (tabWidgetMain->indexOf(widgetFunctions) == -1
	 && tabWidgetMain->indexOf(widgetSignals) == -1
	 && tabWidgetMain->indexOf(widgetVariables) == -1) return false;
	else return true;
}

void MecProjectEditor::edit()
{
	tabWidgetMain->setCurrentWidget(widgetProperties);
	lineEditName->setFocus(Qt::OtherFocusReason);
}

void MecProjectEditor::editChild(MecAbstractElement *Element)
{
if (Element == 0) return;
MecAbstractElement *tempIncrement = Element;
while (tempIncrement->parentElement() != 0 and tempIncrement->parentElement() != element())//On cherche l'élément parent de Element qui soit l'enfant direct de element().
	{
	tempIncrement = tempIncrement->parentElement();
	}

if (tempIncrement->parentElement() != 0)//On sait donc que tempIncrement->parentElement() désigne element().
	{
	MecAbstractElementEditor *tempEditor = elementEditor(tempIncrement);
	switch (tempEditor->editorRole())
		{
		case MecAbstractElement::Object :
			tabWidgetMain->setCurrentWidget(widgetObjects);
			tabWidgetObjects->setCurrentWidget(tempEditor);
			break;
		case MecAbstractElement::Function :
			tabWidgetMain->setCurrentWidget(widgetFunctions);
			tabWidgetFunctions->setCurrentWidget(tempEditor);
			break;
		case MecAbstractElement::Signal :
			tabWidgetMain->setCurrentWidget(widgetSignals);
			tabWidgetSignals->setCurrentWidget(tempEditor);
			break;
		case MecAbstractElement::Variable :
			tabWidgetMain->setCurrentWidget(widgetVariables);
			scrollAreaVariables->ensureWidgetVisible(tempEditor);
			break;
		default:
			break;
		}
	if (tempIncrement != Element) tempEditor->editChild(Element);
	else
		{
		tempEditor->setFocus();
		tempEditor->edit();
		}
	}
}
	
void MecProjectEditor::titleProjectChanged(MecAbstractProject *Project)
{
if (Project != project()) return;
//Par confort pour l'utilisateur, on enregistre la position actuelle du curseur.
int tempPosition = lineEditTitle->cursorPosition();
lineEditTitle->setText(project()->title());
//Puis on l'y replace.
lineEditTitle->setCursorPosition(tempPosition);
}

void MecProjectEditor::synopsisProjectChanged(MecAbstractProject *Project)
{
if (Project != project()) return;
if (!textEditSynopsis->hasFocus())//Si la zone d'édition n'a pas le focus (c.-à-d. que l'utilisateur n'est pas à l'origine de la modification), on remplace son contenu.
	{
	textEditSynopsis->blockSignals(true);
	textEditSynopsis->setPlainText(project()->synopsis());
	textEditSynopsis->blockSignals(false);
	}
}

void MecProjectEditor::setContentEditable(bool Editable)
{
	if (Editable && !isContentEditable()) {
		tabWidgetMain->insertTab(1, widgetFunctions, QIcon(":/share/icons/functions.png"), tr("Functions"));
		tabWidgetMain->insertTab(2, widgetSignals, QIcon(":/share/icons/signal.png"), tr("Signals"));
		tabWidgetMain->insertTab(3, widgetVariables, QIcon(":/share/icons/variable.png"), tr("Variables"));
	}
	else if (!Editable && isContentEditable()) {
		tabWidgetMain->removeTab(tabWidgetMain->indexOf(widgetFunctions));
		tabWidgetMain->removeTab(tabWidgetMain->indexOf(widgetSignals));
		tabWidgetMain->removeTab(tabWidgetMain->indexOf(widgetVariables));
	}
}
	
MecAbstractElementEditor* MecProjectEditor::newSubEditor(MecAbstractElement *Element)
{
MecAbstractPlugin *tempPlugin = mainEditor()->pluginsManager()->plugin(Element->elementRole(), Element->elementType());
if (tempPlugin != 0)
	{
	MecAbstractElementEditor *tempEditor = tempPlugin->elementEditor(Element, mainEditor());
	if (tempEditor != 0)
		{
		QWidget *tempVariablesFocusWidget = scrollAreaVariables->focusWidget();
		switch (tempEditor->editorRole())
			{
			case MecAbstractElement::Project :
				delete tempEditor;
				return 0;
				break;
			case MecAbstractElement::Object :
				tabWidgetObjects->addTab(tempEditor, QIcon(":/share/icons/types/" + Element->elementType() + ".png"), Element->elementName());
				connect(Element, SIGNAL(nameChanged(MecAbstractElement*)), SLOT(childNameChanged(MecAbstractElement*)));
				break;
			case MecAbstractElement::Function :
				tabWidgetFunctions->addTab(tempEditor, QIcon(":/share/icons/types/" + Element->elementType() + ".png"), Element->elementName());
				connect(Element, SIGNAL(nameChanged(MecAbstractElement*)), SLOT(childNameChanged(MecAbstractElement*)));
				break;
			case MecAbstractElement::Signal :
				tabWidgetSignals->addTab(tempEditor, QIcon(":/share/icons/types/" + Element->elementType() + ".png"), Element->elementName());
				connect(Element, SIGNAL(nameChanged(MecAbstractElement*)), SLOT(childNameChanged(MecAbstractElement*)));
				break;
			case MecAbstractElement::Variable :
				tempEditor->setParent(widgetEditorsVariables);
				layoutEditorsVariables->removeItem(spacerItemEditorsVariables);
				layoutEditorsVariables->addWidget(tempEditor, 0, Qt::AlignTop);
				layoutEditorsVariables->addSpacerItem(spacerItemEditorsVariables);
				widgetEditorsVariables->show();
				break;
			}
		tempEditor->show();
		scrollAreaVariables->ensureWidgetVisible(tempVariablesFocusWidget);
		
		
		return tempEditor;
		}
	else
		{
		addPluginsError(MecPluginError(MecPluginError::CastFailed, tempPlugin->name(), tempPlugin->version(), Element->elementRole(), Element->elementType(), Element));
		return 0;
		}
	}
else
	{
	addPluginsError(MecPluginError(MecPluginError::Unavailable, "", 0, Element->elementRole(), Element->elementType(), Element));
	return 0;
	}
}
	
void MecProjectEditor::subEditorAboutToDeleted(MecAbstractElementEditor *Editor)
{
switch (Editor->editorRole())
	{
	case MecAbstractElement::Object :
		tabWidgetObjects->removeTab(tabWidgetObjects->indexOf(Editor));
		break;
	case MecAbstractElement::Function :
		tabWidgetFunctions->removeTab(tabWidgetFunctions->indexOf(Editor));
		break;
	case MecAbstractElement::Signal :
		tabWidgetSignals->removeTab(tabWidgetSignals->indexOf(Editor));
		break;
	case MecAbstractElement::Variable :
		layoutEditorsVariables->removeWidget(Editor);
		break;
	default:
		break;
	}
}

void MecProjectEditor::reorderSubEditors()
{
	QWidget *tempCurrentWidget=0;

	//Objets
	QList<MecAbstractObject*> tempObjects = m_project->childObjects();
	tempCurrentWidget = tabWidgetObjects->currentWidget();
	tabWidgetObjects->clear();
	for (int i=0 ; i < tempObjects.size() ; i++)
		{
		tabWidgetObjects->addTab(elementEditor(tempObjects.at(i)), QIcon(":/share/icons/types/" + tempObjects.at(i)->elementType() + ".png"), tempObjects.at(i)->elementName());
		}
	tabWidgetObjects->setCurrentWidget(tempCurrentWidget);
	
	//Fonctions
	QList<MecAbstractFunction*> tempFunctions = m_project->childFunctions();
	tempCurrentWidget = tabWidgetFunctions->currentWidget();
	tabWidgetFunctions->clear();
	for (int i=0 ; i < tempFunctions.size() ; i++)
		{
		tabWidgetFunctions->addTab(elementEditor(tempFunctions.at(i)), QIcon(":/share/icons/types/" + tempFunctions.at(i)->elementType() + ".png"), tempFunctions.at(i)->elementName());
		}
	tabWidgetFunctions->setCurrentWidget(tempCurrentWidget);
	
	//Signaux
	QList<MecAbstractSignal*> tempSignals = m_project->childSignals();
	tempCurrentWidget = tabWidgetSignals->currentWidget();
	tabWidgetSignals->clear();
	for (int i=0 ; i < tempSignals.size() ; i++)
		{
		tabWidgetSignals->addTab(elementEditor(tempSignals.at(i)), QIcon(":/share/icons/types/" + tempSignals.at(i)->elementType() + ".png"), tempSignals.at(i)->elementName());
		}
	tabWidgetSignals->setCurrentWidget(tempCurrentWidget);
	
	//Variables
	QList<MecAbstractVariable*> tempVariables = m_project->childVariables();
	layoutEditorsVariables->removeItem(spacerItemEditorsVariables);
	for (int i=0 ; i < tempVariables.size() ; i++)
		{
		layoutEditorsVariables->addWidget(elementEditor(tempVariables.at(i)), 0, Qt::AlignTop);
		}
	layoutEditorsVariables->addSpacerItem(spacerItemEditorsVariables);
}
	
void MecProjectEditor::typeElementChanged(MecAbstractElement *Element) {}

void MecProjectEditor::addGeneralWidget(QWidget *Widget)
{
	gridLayoutGeneral->addWidget(Widget, gridLayoutGeneral->rowCount(), 0);
}

void MecProjectEditor::setProjectTitle()
{
QString oldTitle = project()->title();
project()->setTitle(lineEditTitle->text());

if (oldTitle != lineEditTitle->text()) mainEditor()->addEditStep(tr("Change title of the project"));
}

void MecProjectEditor::textEditSynopsisChanged()
{
QString oldSynopsis = project()->synopsis();
project()->setSynopsis(textEditSynopsis->toPlainText());

if (oldSynopsis != project()->synopsis()) mainEditor()->addEditStep(tr("Change synopsis of the project"));
}

void MecProjectEditor::childNameChanged(MecAbstractElement *Element)
{
for (int i=0 ;  i < subEditors().size(); i++)
	{
	if (subEditors().at(i)->element() == Element)
		{
		switch (subEditors().at(i)->editorRole())
			{
			case MecAbstractElement::Object :
				tabWidgetObjects->setTabText(tabWidgetObjects->indexOf(subEditors().at(i)), Element->elementName());
				break;
			case MecAbstractElement::Function :
				tabWidgetFunctions->setTabText(tabWidgetFunctions->indexOf(subEditors().at(i)), Element->elementName());
				break;
			case MecAbstractElement::Signal :
				tabWidgetSignals->setTabText(tabWidgetSignals->indexOf(subEditors().at(i)), Element->elementName());
				break;
			default:
				break;
			}
		}
	}
}


