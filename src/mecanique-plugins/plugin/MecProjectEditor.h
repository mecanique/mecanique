/*
© Quentin VIGNAUD, 2013

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#ifndef __MECPROJECTEDITOR_H__
#define __MECPROJECTEDITOR_H__

#include "MecElementEditor.h"
#include <MecAbstractProject>

/**
\brief	Classe basique d'édition de projet.

Cette classe fournit les précisions nécessaires à l'édition basique d'un projet.
*/
class MecProjectEditor : public MecElementEditor
{
	Q_OBJECT

public:
	/**
	\brief	Constructeur.
	\param	Project	Projet édité, doit absolument exister lors de la construction (c.à.d. instancié et différent de 0) sinon un comportement inattendu pourrait se produire.
	\param	MainEditor	Éditeur principal dont dépend cet éditeur d'élément.
	\param	Parent	Widget parent.
	\param	F	Drapeaux de fenêtre.

	Initialise l'éditeur et ses widgets enfants, et organise ces derniers.
	*/
	MecProjectEditor(MecAbstractProject* const Project, MecAbstractEditor* MainEditor, QWidget * Parent=0, Qt::WindowFlags F=0);
	/**
	\brief	Destructeur.
	*/
	virtual ~MecProjectEditor();

	///Retourne le projet édité.
	MecAbstractProject* project() const;

	/**
	\brief	Retourne un QVariant contenant toutes les données indiquant l'état actuel de l'éditeur.

	Cette donnée est vouée à être passée ultérieurement en paramètre à setState(QVariant) pour restaurer l'état visuel de l'éditeur.
	*/
	virtual QVariant state();
	/**
	\brief	Restaure l'état de l'éditeur selon les indications spécifées.

	\see	state()
	*/
	virtual void setState(QVariant State);

	///Indique si un MecObject peut être ajouté, retourne donc ici \e true.
	virtual bool canAddObject() const;
	///Indique si une MecFunction peut être ajouté, retourne donc ici \e true.
	virtual bool canAddFunction() const;
	///Indique si un MecSignal peut être ajouté, retourne donc ici \e true.
	virtual bool canAddSignal() const;
	///Indique si une MecVariable peut être ajouté, retourne donc ici \e true.
	virtual bool canAddVariable() const;
	///Indique si le MecAbstractElement enfant peut être supprimé, retourne donc ici \e true.
	virtual bool canRemoveChild(MecAbstractElement* const Element) const;

	/**
	Indique si le contenu du projet est éditable.

	\see	setContentEditable(bool)
	*/
	virtual bool isContentEditable() const;

public slots:
	/**
	\brief	Dispose l'éditeur de manière à éditer l'élément de manière primaire.

	Affiche le panneau des propriétés et sélectionne le nom du projet.
	*/
	virtual void edit();
	/**
	\brief	Positionne la vue de manière à voir l'éditeur de \e Element.
	*/
	virtual void editChild(MecAbstractElement *Element);

	/**
	\brief	Autorise ou non l'édition du contenu des sous-éléments du projet (les fonctions, signaux et variables).
	*/
	virtual void setContentEditable(bool Editable);

	/**
	\brief	Demande un sous-éditeur pour \e Element.
	\note	\e Element doit être un élément enfant de element().

	Demande un éditeur au gestionnaire de plugins, correspondant aux propriétés de Element, l'intègre dans l'interface utilisateur actuelle, et le retourne.

	\return	Le sous-éditeur demandé, ou 0 si éditer cet élément n'est pas possible.
	*/
	virtual MecAbstractElementEditor* newSubEditor(MecAbstractElement *Element);

	/**
	\brief	Est appelée juste avant la suppression de \e Editor.

	Supprime l'éditeur spécifié de l'interface utilisateur.
	*/
	virtual void subEditorAboutToDeleted(MecAbstractElementEditor *Editor);

	/**
	\brief	Effectue la vérification de l'ordre des sous-éditeurs.
	*/
	virtual void reorderSubEditors();

	/**
	\brief	Est déclenché lorsque le titre du projet a changé.
	*/
	virtual void titleProjectChanged(MecAbstractProject *Project);
	/**
	\brief	Est déclenché lorsque le synopsis du projet a changé.
	*/
	virtual void synopsisProjectChanged(MecAbstractProject *Project);

	///Ne fait rien.
	void typeElementChanged(MecAbstractElement *Element);


signals:

protected:

	/**
	\brief	Ajoute un widget à l'affichage des données générales.
	*/
	virtual void addGeneralWidget(QWidget *Widget);

	///Layout d'organisation de l'éditeur.
	QGridLayout *layoutMain;

	///Onglets principaux.
	QTabWidget *tabWidgetMain;

	///Widget des propriétés.
	QWidget *widgetProperties;
	///Layout d'organisation des propriétés.
	QGridLayout *layoutProperties;

	///Groupe général.
	QGroupBox *groupBoxGeneral;
	///Layout d'organisation du groupe général.
	QGridLayout *gridLayoutGeneral;
	///Label d'indication du titre.
	QLabel *labelTitle;
	///Ligne d'édition du titre.
	QLineEdit *lineEditTitle;

	///Groupe du synopsis.
	QGroupBox *groupBoxSynopsis;
	///Layout d'organisation du groupe du synopsis.
	QGridLayout *gridLayoutSynopsis;
	///Zone d'édition du synopsis.
	QTextEdit *textEditSynopsis;

	///Widget des objets.
	QWidget *widgetObjects;
	///Layout d'organisation des objets.
	QGridLayout *layoutObjects;

	///Onglets des objets.
	QTabWidget *tabWidgetObjects;

	///Widget des fonction.
	QWidget *widgetFunctions;
	///Layout d'organisation des fonctions.
	QGridLayout *layoutFunctions;

	///Onglets des fonctions.
	QTabWidget *tabWidgetFunctions;

	///Widget des signaux.
	QWidget *widgetSignals;
	///Layout d'organisation des signaux.
	QGridLayout *layoutSignals;

	///Onglets des signaux.
	QTabWidget *tabWidgetSignals;

	///Widget des variables.
	QWidget *widgetVariables;
	///Layout des variables.
	QGridLayout *layoutVariables;

	///Zone de défilement des variables.
	QScrollArea *scrollAreaVariables;

	///Widget défilant des variables.
	QWidget *widgetEditorsVariables;
	///Layout d'organisation des variables.
	QVBoxLayout *layoutEditorsVariables;
	///SpacerItem de « condensation », relatif à layoutEditorsVariables.
	QSpacerItem *spacerItemEditorsVariables;


private:
	///Projet édité.
	MecAbstractProject* const m_project;


private slots:
	///Gère l'attribution du nouveau titre.
	void setProjectTitle();
	//Connecté à « textEditSynopsis->textChanged() »
	void textEditSynopsisChanged();

	//Connecté à chacun des éléments enfants par "nameChanged()"
	void childNameChanged(MecAbstractElement *Element);

};

#endif /* __MECPROJECTEDITOR_H__ */

