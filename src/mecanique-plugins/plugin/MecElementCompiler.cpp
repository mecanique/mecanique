/*
© Quentin VIGNAUD, 2013

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#include "MecElementCompiler.h"

MecElementCompiler::MecElementCompiler(MecAbstractElement* const Element, MecAbstractCompiler* const MainCompiler) : m_element (Element), m_compilerRole (m_element->elementRole()), m_mainCompiler (MainCompiler)
{

}

MecElementCompiler::~MecElementCompiler()
{
}
	
MecAbstractElement* MecElementCompiler::element() const
{
return m_element;
}

MecAbstractElement::ElementRole MecElementCompiler::compilerRole() const
{
return m_compilerRole;
}

MecAbstractCompiler* MecElementCompiler::mainCompiler() const
{
return m_mainCompiler;
}

QList<MecAbstractElementCompiler*> MecElementCompiler::subCompilers() const
{
return m_subCompilers;
}

QList<MecPluginError> MecElementCompiler::pluginsErrors() const
{
return m_pluginsErrors;
}

void MecElementCompiler::addSubCompiler(MecAbstractElementCompiler* Compiler)
{
m_subCompilers.append(Compiler);
}

void MecElementCompiler::addPluginsError(MecPluginError Error)
{
m_pluginsErrors.append(Error);
}



