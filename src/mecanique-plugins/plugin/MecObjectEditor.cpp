/*
© Quentin VIGNAUD, 2013

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#include "MecObjectEditor.h"

MecObjectEditor::MecObjectEditor(MecAbstractObject* const Object, MecAbstractEditor* MainEditor, QWidget * Parent, Qt::WindowFlags F) : MecElementEditor(Object, MainEditor, Parent, F), m_object (Object)
{
tabWidgetMain = new QTabWidget(this);
	tabWidgetMain->setTabPosition(QTabWidget::West);

//Propriétés
widgetProperties = new QWidget(tabWidgetMain);

//Générales
comboBoxType->setEnabled(false);
groupBoxGeneral = new QGroupBox(tr("General"), widgetProperties);
layoutGeneral = new QHBoxLayout(groupBoxGeneral);
	layoutGeneral->addWidget(pushButtonIcon);
	layoutGeneral->addWidget(labelType);
	layoutGeneral->addWidget(comboBoxType);
	layoutGeneral->addWidget(labelName);
	layoutGeneral->addWidget(lineEditName);
	layoutGeneral->addStretch();

//Référence
groupBoxReference = new QGroupBox(tr("Reference"), widgetProperties);
browserReference = new QTextBrowser(groupBoxReference);
	browserReference->setOpenLinks(true);
	browserReference->setOpenExternalLinks(true);
	QString locale = QLocale::system().name().section('_', 0, 0);
	QString fileName(qApp->applicationDirPath() + "/../share/mecanique/reference/" + element()->elementType() + "." + locale + ".html");
	if (QFileInfo(fileName).exists()) browserReference->setSource(QUrl::fromLocalFile(fileName));
	else browserReference->setSource(QUrl::fromLocalFile(qApp->applicationDirPath() + QString("/../share/mecanique/reference/") + element()->elementType() + ".en.html"));
layoutReference = new QGridLayout(groupBoxReference);
	layoutReference->addWidget(browserReference, 0, 0);

layoutProperties = new QGridLayout(widgetProperties);
	layoutProperties->addWidget(groupBoxGeneral, 0, 0, Qt::AlignTop);
	layoutProperties->addWidget(groupBoxReference, 1, 0);

tabWidgetMain->addTab(widgetProperties, QIcon(":/share/icons/properties.png"), tr("Properties"));

//Fonctions
widgetFunctions = new QWidget(tabWidgetMain);

tabWidgetFunctions = new QTabWidget(widgetFunctions);

layoutFunctions = new QGridLayout(widgetFunctions);
	layoutFunctions->addWidget(tabWidgetFunctions, 0, 0);

tabWidgetMain->addTab(widgetFunctions, QIcon(":/share/icons/functions.png"), tr("Functions"));

//Signaux
widgetSignals = new QWidget(tabWidgetMain);

tabWidgetSignals = new QTabWidget(widgetSignals);

layoutSignals = new QGridLayout(widgetSignals);
	layoutSignals->addWidget(tabWidgetSignals, 0, 0);

tabWidgetMain->addTab(widgetSignals, QIcon(":/share/icons/signal.png"), tr("Signals"));

//Variables
widgetVariables = new QWidget(tabWidgetMain);

scrollAreaVariables = new QScrollArea(widgetVariables);

	widgetEditorsVariables = new QWidget(scrollAreaVariables);
	
	layoutEditorsVariables = new QVBoxLayout(widgetEditorsVariables);
		spacerItemEditorsVariables = new QSpacerItem(0, 0, QSizePolicy::Minimum, QSizePolicy::Expanding);
		layoutEditorsVariables->addSpacerItem(spacerItemEditorsVariables);

scrollAreaVariables->setWidget(widgetEditorsVariables);
	scrollAreaVariables->setWidgetResizable(true);

layoutVariables = new QGridLayout(widgetVariables);
	layoutVariables->addWidget(scrollAreaVariables, 0, 0);

tabWidgetMain->addTab(widgetVariables, QIcon(":/share/icons/variable.png"), tr("Variables"));

//Finition
layoutMain = new QGridLayout(this);
	layoutMain->addWidget(tabWidgetMain, 0, 0);
}

MecObjectEditor::~MecObjectEditor()
{
//Rien de particulier.
}

MecAbstractObject* MecObjectEditor::object() const
{
return m_object;
}

QVariant MecObjectEditor::state()
{
	QHash<QString, QVariant> tempProperties;
	tempProperties.insert("inheritedProperties", MecElementEditor::state());
	
	tempProperties.insert("currentTab", QVariant(tabWidgetMain->currentIndex()));
	tempProperties.insert("currentFunction", QVariant(tabWidgetFunctions->currentIndex()));
	tempProperties.insert("currentSignal", QVariant(tabWidgetSignals->currentIndex()));
	tempProperties.insert("posVariable", QVariant(scrollAreaVariables->verticalScrollBar()->value()));
	return QVariant(tempProperties);
}

void MecObjectEditor::setState(QVariant State)
{
	QHash<QString, QVariant> tempProperties = State.toHash();
	MecElementEditor::setState(tempProperties.value("inheritedProperties"));
	
	tabWidgetMain->setCurrentIndex(tempProperties.value("currentTab").toInt());
	tabWidgetFunctions->setCurrentIndex(tempProperties.value("currentFunction").toInt());
	tabWidgetSignals->setCurrentIndex(tempProperties.value("currentSignal").toInt());
	scrollAreaVariables->verticalScrollBar()->setValue(tempProperties.value("posVariable").toInt());
}
	
bool MecObjectEditor::canAddObject() const
{
return false;
}

bool MecObjectEditor::canAddFunction() const
{
return true;
}

bool MecObjectEditor::canAddSignal() const
{
return true;
}

bool MecObjectEditor::canAddVariable() const
{
return true;
}

bool MecObjectEditor::canRemoveChild(MecAbstractElement* const Element) const
{
return true;
}

bool MecObjectEditor::isContentEditable() const
{
	if (tabWidgetMain->indexOf(widgetFunctions) == -1
	 && tabWidgetMain->indexOf(widgetSignals) == -1
	 && tabWidgetMain->indexOf(widgetVariables) == -1) return false;
	else return true;
}

void MecObjectEditor::addGeneralWidget(QWidget *Widget)
{
	layoutGeneral->addWidget(Widget);
}

void MecObjectEditor::edit()
{
	tabWidgetMain->setCurrentWidget(widgetProperties);
	lineEditName->setFocus(Qt::OtherFocusReason);
}

void MecObjectEditor::editChild(MecAbstractElement *Element)
{
if (Element == 0) return;
MecAbstractElement *tempIncrement = Element;
while (tempIncrement->parentElement() != 0 and tempIncrement->parentElement() != element())//On cherche l'élément parent de Element qui soit l'enfant direct de element().
	{
	tempIncrement = tempIncrement->parentElement();
	}

if (tempIncrement->parentElement() != 0)//On sait donc que tempIncrement->parentElement() désigne element().
	{
	MecAbstractElementEditor *tempEditor = elementEditor(tempIncrement);
	switch (tempEditor->editorRole())
		{
		case MecAbstractElement::Function :
			tabWidgetMain->setCurrentWidget(widgetFunctions);
			tabWidgetFunctions->setCurrentWidget(tempEditor);
			break;
		case MecAbstractElement::Signal :
			tabWidgetMain->setCurrentWidget(widgetSignals);
			tabWidgetSignals->setCurrentWidget(tempEditor);
			break;
		case MecAbstractElement::Variable :
			tabWidgetMain->setCurrentWidget(widgetVariables);
			scrollAreaVariables->ensureWidgetVisible(tempEditor);
			break;
		default:
			break;
		}
	if (tempIncrement != Element) tempEditor->editChild(Element);
	else
		{
		tempEditor->setFocus();
		tempEditor->edit();
		}
	}

}

void MecObjectEditor::setContentEditable(bool Editable)
{
	if (Editable && !isContentEditable()) {
		tabWidgetMain->insertTab(1, widgetFunctions, QIcon(":/share/icons/functions.png"), tr("Functions"));
		tabWidgetMain->insertTab(2, widgetSignals, QIcon(":/share/icons/signal.png"), tr("Signals"));
		tabWidgetMain->insertTab(3, widgetVariables, QIcon(":/share/icons/variable.png"), tr("Variables"));
	}
	else if (!Editable && isContentEditable()) {
		tabWidgetMain->removeTab(tabWidgetMain->indexOf(widgetFunctions));
		tabWidgetMain->removeTab(tabWidgetMain->indexOf(widgetSignals));
		tabWidgetMain->removeTab(tabWidgetMain->indexOf(widgetVariables));
	}
}
	
MecAbstractElementEditor* MecObjectEditor::newSubEditor(MecAbstractElement *Element)
{
MecAbstractPlugin *tempPlugin = mainEditor()->pluginsManager()->plugin(Element->elementRole(), Element->elementType());
if (tempPlugin != 0)
	{
	MecAbstractElementEditor *tempEditor = tempPlugin->elementEditor(Element, mainEditor());
	if (tempEditor != 0)
		{
		QWidget *tempVariablesFocusWidget = scrollAreaVariables->focusWidget();
		switch (tempEditor->editorRole())
			{
			case MecAbstractElement::Function :
				tabWidgetFunctions->addTab(tempEditor, QIcon(":/share/icons/types/" + Element->elementType() + ".png"), Element->elementName());
				connect(Element, SIGNAL(nameChanged(MecAbstractElement*)), SLOT(childNameChanged(MecAbstractElement*)));
				break;
			case MecAbstractElement::Signal :
				tabWidgetSignals->addTab(tempEditor, QIcon(":/share/icons/types/" + Element->elementType() + ".png"), Element->elementName());
				connect(Element, SIGNAL(nameChanged(MecAbstractElement*)), SLOT(childNameChanged(MecAbstractElement*)));
				break;
			case MecAbstractElement::Variable :
				tempEditor->setParent(widgetEditorsVariables);
				layoutEditorsVariables->removeItem(spacerItemEditorsVariables);
				layoutEditorsVariables->addWidget(tempEditor, 0, Qt::AlignTop);
				layoutEditorsVariables->addSpacerItem(spacerItemEditorsVariables);
				widgetEditorsVariables->show();
				break;
			default :
				delete tempEditor;
				return 0;
				break;
			}
		tempEditor->show();
		scrollAreaVariables->ensureWidgetVisible(tempVariablesFocusWidget);
		
		
		return tempEditor;
		}
	else
		{
		addPluginsError(MecPluginError(MecPluginError::CastFailed, tempPlugin->name(), tempPlugin->version(), Element->elementRole(), Element->elementType(), Element));
		return 0;
		}
	}
else
	{
	addPluginsError(MecPluginError(MecPluginError::Unavailable, "", 0, Element->elementRole(), Element->elementType(), Element));
	return 0;
	}
}
	
void MecObjectEditor::subEditorAboutToDeleted(MecAbstractElementEditor *Editor)
{
switch (Editor->editorRole())
	{
	case MecAbstractElement::Function :
		tabWidgetFunctions->removeTab(tabWidgetFunctions->indexOf(Editor));
		break;
	case MecAbstractElement::Signal :
		tabWidgetSignals->removeTab(tabWidgetSignals->indexOf(Editor));
		break;
	case MecAbstractElement::Variable :
		layoutEditorsVariables->removeWidget(Editor);
		break;
	default:
		break;
	}
}

void MecObjectEditor::reorderSubEditors()
{
	QWidget *tempCurrentWidget=0;
	
	//Fonctions
	QList<MecAbstractFunction*> tempFunctions = m_object->childFunctions();
	tempCurrentWidget = tabWidgetFunctions->currentWidget();
	tabWidgetFunctions->clear();
	for (int i=0 ; i < tempFunctions.size() ; i++)
		{
		tabWidgetFunctions->addTab(elementEditor(tempFunctions.at(i)), QIcon(":/share/icons/types/" + tempFunctions.at(i)->elementType() + ".png"), tempFunctions.at(i)->elementName());
		}
	tabWidgetFunctions->setCurrentWidget(tempCurrentWidget);
	
	//Signaux
	QList<MecAbstractSignal*> tempSignals = m_object->childSignals();
	tempCurrentWidget = tabWidgetSignals->currentWidget();
	tabWidgetSignals->clear();
	for (int i=0 ; i < tempSignals.size() ; i++)
		{
		tabWidgetSignals->addTab(elementEditor(tempSignals.at(i)), QIcon(":/share/icons/types/" + tempSignals.at(i)->elementType() + ".png"), tempSignals.at(i)->elementName());
		}
	tabWidgetSignals->setCurrentWidget(tempCurrentWidget);
	
	//Variables
	QList<MecAbstractVariable*> tempVariables = m_object->childVariables();
	layoutEditorsVariables->removeItem(spacerItemEditorsVariables);
	for (int i=0 ; i < tempVariables.size() ; i++)
		{
		layoutEditorsVariables->addWidget(elementEditor(tempVariables.at(i)), 0, Qt::AlignTop);
		}
	layoutEditorsVariables->addSpacerItem(spacerItemEditorsVariables);
}
	
void MecObjectEditor::addObject() {}

void MecObjectEditor::childNameChanged(MecAbstractElement *Element)
{
for (int i=0 ;  i < subEditors().size(); i++)
	{
	if (subEditors().at(i)->element() == Element)
		{
		switch (subEditors().at(i)->editorRole())
			{
			case MecAbstractElement::Function :
				tabWidgetFunctions->setTabText(tabWidgetFunctions->indexOf(subEditors().at(i)), Element->elementName());
				break;
			case MecAbstractElement::Signal :
				tabWidgetSignals->setTabText(tabWidgetSignals->indexOf(subEditors().at(i)), Element->elementName());
				break;
			default:
				break;
			}
		}
	}
}


