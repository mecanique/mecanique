/*
© Quentin VIGNAUD, 2013

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#ifndef __MECOBJECTCOMPILER_H__
#define __MECOBJECTCOMPILER_H__

#include <MecAbstractObject>
#include "MecElementCompiler.h"

/**
\brief	Classe de compilation d'un objet.
*/
class MecObjectCompiler : public MecElementCompiler
{
public:
	/**
	\brief	Constructeur.
	\param	Object	Objet compilé, doit absolument exister lors de la construction (c.-à-d. instancié et différent de 0) sinon un comportement inattendu pourrait se produire.
	\param	MainCompiler	Compilateur maître.
	*/
	MecObjectCompiler(MecAbstractObject* const Object, MecAbstractCompiler* const MainCompiler);
	///Destructeur.
	virtual ~MecObjectCompiler();

	///Retourne l'objet compilé.
	MecAbstractObject* object() const;

	/**
	\brief	Retourne la liste des ressources à ajouter au répertoire de compilation pour compiler cet élément.

	\return	Une liste d'une ressource, correspondant à l'icône du type de l'objet.
	*/
	virtual QList<QResource*> resources();
	/**
	\brief	Retourne les instructions à ajouter au fichier projet (".pro").

	\return	Une chaîne vide.
	*/
	virtual QString projectInstructions();

	/**
	\brief	Retourne le contenu du header de l'élément.

	\code
	//Licence
	#ifndef __NOMOBJET_H__
	#define __NOMOBJET_H__

	#include "mecanique/Object.h"
	//Instructions préprocesseur relatives aux éléments enfants.

	class nomobjet : public Object
	{
	Q_OBJECT
	public:
	nomobjet(Project* const Project);
	~nomobjet();

	virtual void more() {}
	virtual QVariant settings() {return QVariant();}
	virtual void setSettings(QVariant Settings) {return;}

	public:
	//Déclaration des variables.
	public slots:
	//Déclaration des fonctions.
	signals:
	//Déclaration des signaux.
	};


	#endif	// __NOMOBJET_H__

	\endcode
	*/
	virtual QString header();
	/**
	\brief	Retourne le contenu du fichier d'implémentation de l'élément.

	\code
	#include "nomobjet.h"

	nomobjet::nomobjet(Project* const Project) : Object("nomobjet", "Object", Project)
	{
	//Valeurs par défaut des variables.
	}

	nomobjet::~nomobjet()
	{
	}

	//Implémentation des fonctions.

	\endcode
	\see	MecVariableCompiler
	\see	MecFunctionCompiler
	*/
	virtual QString source();

	/**
	\brief	Retourne les instructions préprocesseur à ajouter dans le fichier header de l'élément parent.

	La chaîne retournée est « #include "[nom].h" »
	*/
	virtual QString preprocessorInstructions();
	/**
	\brief	Retourne le code C++ à ajouter dans la déclaration de l'élément parent.

	La chaîne retournée est « [nom]* m_[nom];\\n ».
	*/
	virtual QString headerInstructions();
	/**
	\brief	Retourne le code C++ à ajouter dans le constructeur de l'élément parent.

	La chaîne retournée est « m_[nom] = new [nom](this);\\n ».
	*/
	virtual QString sourceInstructions();

protected:
	///Retourne la liste complète des sous-compilateurs directs.
	QList<MecElementCompiler*> concreteSubCompilers() const;
	///Retourne la liste complète et récursive des sous-compilateurs.
	QList<MecElementCompiler*> recConcreteSubCompilers() const;

private:
	///Objet compilé.
	MecAbstractObject* const m_object;

	///Liste complète des sous-compilateurs directs.
	QList<MecElementCompiler*> m_concreteSubCompilers;
	///Liste complète récursive des sous-compilateurs.
	QList<MecElementCompiler*> m_recConcreteSubCompilers;

};


#endif /* __MECOBJECTCOMPILER_H__ */

