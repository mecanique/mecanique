/*
© Quentin VIGNAUD, 2013

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#include "MecFunctionCompiler.h"

MecFunctionCompiler::MecFunctionCompiler(MecAbstractFunction* const Function, MecAbstractCompiler* const MainCompiler) : MecElementCompiler(Function, MainCompiler), m_function (Function)
{
for (int i=0 ; i < function()->childElements().size() ; i++)
	{
	MecAbstractPlugin *tempPlugin = mainCompiler()->pluginsManager()->plugin(function()->childElements().at(i)->elementRole(), function()->childElements().at(i)->elementType());
	if (tempPlugin != 0)
		{
		MecAbstractElementCompiler *tempCompiler = tempPlugin->elementCompiler(function()->childElements().at(i), mainCompiler());
		if (tempCompiler != 0)
			{
			addSubCompiler(tempCompiler);
			}
		else
			{
			addPluginsError(MecPluginError(MecPluginError::CastFailed, tempPlugin->name(), tempPlugin->version(), function()->childElements().at(i)->elementRole(), function()->childElements().at(i)->elementType(), function()->childElements().at(i)));
			}
		}
	else
		{
		addPluginsError(MecPluginError(MecPluginError::Unavailable, "", 0, function()->childElements().at(i)->elementRole(), function()->childElements().at(i)->elementType(), function()->childElements().at(i)));
		}
	}

//Établissement de m_concreteSubCompilers.
for (int i=0 ; i < subCompilers().size() ; i++)
	{
	m_concreteSubCompilers.append(static_cast<MecElementCompiler*>(subCompilers().at(i)));
	}

QList<MecAbstractElementCompiler*> tempList = subCompilers();//Établissement de m_recConcreteSubCompilers.
for (int i=0 ; i < tempList.size() ; i++)
	{
	tempList.append(tempList.at(i)->subCompilers());
	}
for (int i=0 ; i < tempList.size() ; i++)
	{
	m_recConcreteSubCompilers.append(static_cast<MecElementCompiler*>(tempList.at(i)));
	}
}

MecFunctionCompiler::~MecFunctionCompiler()
{
}
	
MecAbstractFunction* MecFunctionCompiler::function() const
{
return m_function;
}

QList<QResource*> MecFunctionCompiler::resources()
{
return QList<QResource*>();
}

QString MecFunctionCompiler::projectInstructions()
{
return QString();
}
	
QString MecFunctionCompiler::header()
{
return QString();
}

QString MecFunctionCompiler::source()
{
return QString();
}

QString MecFunctionCompiler::preprocessorInstructions()
{
return QString();
}

QString MecFunctionCompiler::headerInstructions()
{
QString tempString;

tempString += function()->elementType() + " " + function()->elementName() + "(";

for (int i=0 ; i < concreteSubCompilers().size() ; i++)
	{
	tempString += concreteSubCompilers().at(i)->headerInstructions();
	if (i != concreteSubCompilers().size() - 1) tempString += ", ";
	}

tempString += ");\n";

return tempString;
}

QString MecFunctionCompiler::sourceInstructions()
{
QString tempCode = "\n" + function()->code() + "\n";//Les "\n" permettent de s'assurer de la reconnaissance par les regex de la non-présence d'un caractère alphanumérique.
//On traite le code de la fonction pour remplacer les noms d'objet et/ou de projet par leur appel strict.
if (element()->parentElement()->elementRole() == MecAbstractElement::Project)
	{
	for (int i=0 ; i < element()->parentElement()->childElements().size() ; i++)
		{
		if (element()->parentElement()->childElements().at(i)->elementRole() == MecAbstractElement::Object)
			{
			tempCode.replace(QRegExp("(\\W)" + element()->parentElement()->childElements().at(i)->elementName() + "(\\W)"),
							"\\1(m_" + element()->parentElement()->childElements().at(i)->elementName() + ")\\2");
			}
		}
	}
else if (element()->parentElement()->elementRole() == MecAbstractElement::Object)
	{
	tempCode.replace(QRegExp("(\\W)" + element()->parentElement()->parentElement()->elementName() + "(\\W)"), "\\1(static_cast<" + element()->parentElement()->parentElement()->elementName() + "*>(project())\\2");
	for (int i=0 ; i < element()->parentElement()->parentElement()->childElements().size() ; i++)
		{
		if (element()->parentElement()->parentElement()->childElements().at(i)->elementRole() == MecAbstractElement::Object)
			{
			tempCode.replace(QRegExp("(\\W)" + element()->parentElement()->parentElement()->childElements().at(i)->elementName() + "(\\W)"),
							"\\1(static_cast<" + element()->parentElement()->parentElement()->elementName() + "*>(project())->m_" + element()->parentElement()->parentElement()->childElements().at(i)->elementName() + ")\\2");
			}
		}
	}


QString tempString;//Chaîne retournée au final.
if (element()->parentElement()->elementRole() == MecAbstractElement::Object)//Si le parent est un objet,
	{
	tempString.append("#include \"" + element()->parentElement()->parentElement()->elementName() + ".h\"\n");//alors on inclus le header du projet (pour les transtypages à effectuer dessus pour les appels d'objets frères).
	}

if (function()->parentElement() != 0)
	{
	if (function()->parentElement()->elementRole() == MecAbstractElement::Project or function()->parentElement()->elementRole() == MecAbstractElement::Object)
		{
		tempString += function()->elementType() + " " + function()->parentElement()->elementName() + "::" + function()->elementName() + "(";
		
		for (int i=0 ; i < subCompilers().size() ; i++)
			{
			tempString += subCompilers().at(i)->element()->elementType() + " " + subCompilers().at(i)->element()->elementName();
			if (i != subCompilers().size() - 1) tempString += ", ";
			}
		
		tempString += ")\n{\n" + tempCode + "\n}\n\n";
		}
	}
return tempString;
}

QList<MecElementCompiler*> MecFunctionCompiler::concreteSubCompilers() const
{
return m_concreteSubCompilers;
}

QList<MecElementCompiler*> MecFunctionCompiler::recConcreteSubCompilers() const
{
return m_recConcreteSubCompilers;
}


