/*
© Quentin VIGNAUD, 2013

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#ifndef __MECELEMENTCOMPILER_H__
#define __MECELEMENTCOMPILER_H__

#include <QtCore>
#include <MecAbstractElementCompiler>

/**
\brief	Classe virtuelle de compilation d'un élément.
*/
class MecElementCompiler : public MecAbstractElementCompiler
{

public:
	/**
	\brief	Constructeur.
	\param	Element	Élément compilé, doit absolument exister lors de la construction (c.-à-d. instancié et différent de 0) sinon un comportement inattendu pourrait se produire.
	\param	MainCompiler	Compilateur maître.
	*/
	MecElementCompiler(MecAbstractElement* const Element, MecAbstractCompiler* const MainCompiler);
	/**
	\brief	Destructeur.
	*/
	virtual ~MecElementCompiler();

	///Élément compilé.
	MecAbstractElement* element() const;
	///Rôle du compilateur.
	MecAbstractElement::ElementRole compilerRole() const;

	///Compilateur principal.
	MecAbstractCompiler* mainCompiler() const;

	/**
	Retourne une liste des compilateurs subordonnés, correspondants à des éléments enfants, nécessaires pour compiler.

	\param	PluginsError	Liste des éléments dont le plugin approprié n'a pas pu être trouvé.
	*/
	QList<MecAbstractElementCompiler*> subCompilers() const;
	/**
	\brief	Retourne les erreurs de plugins.
	*/
	QList<MecPluginError> pluginsErrors() const;

	/**
	Retourne les instructions préprocesseur à ajouter dans le fichier header de l'élément parent.
	*/
	virtual QString preprocessorInstructions() = 0;
	/**
	Retourne le code C++ à ajouter dans la déclaration de l'élément parent.
	*/
	virtual QString headerInstructions() = 0;
	/**
	Retourne le code C++ à ajouter dans le constructeur de l'élément parent.
	*/
	virtual QString sourceInstructions() = 0;


protected:
	/**
	\brief	Ajoute un sous-compilateur.
	*/
	void addSubCompiler(MecAbstractElementCompiler* Compiler);
	/**
	\brief	Ajoute une erreur de plugin.
	*/
	void addPluginsError(MecPluginError Error);


private:
	///Élément compilé.
	MecAbstractElement* const m_element;
	///Rôle du compilateur.
	const MecAbstractElement::ElementRole m_compilerRole;
	///Compilateur principal.
	MecAbstractCompiler* const m_mainCompiler;
	///Liste des sous-compilateurs.
	QList<MecAbstractElementCompiler*> m_subCompilers;
	///Liste des erreurs de plugin.
	QList<MecPluginError> m_pluginsErrors;


};

#endif /* __MECELEMENTCOMPILER_H__ */

