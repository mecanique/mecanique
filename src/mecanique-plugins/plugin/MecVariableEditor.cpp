/*
© Quentin VIGNAUD, 2013

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#include "MecVariableEditor.h"

MecVariableEditor::MecVariableEditor(MecAbstractVariable* const Variable, MecAbstractEditor* MainEditor, QWidget * Parent, Qt::WindowFlags F) : MecElementEditor(Variable, MainEditor, Parent, F), m_variable (Variable)
{
layoutMain = new QHBoxLayout(this);
	layoutMain->addWidget(pushButtonIcon);
	layoutMain->addWidget(labelType);
	layoutMain->addWidget(comboBoxType);
	layoutMain->addWidget(labelName);
	layoutMain->addWidget(lineEditName);
	layoutMain->addStretch();
}

MecVariableEditor::~MecVariableEditor()
{
//Rien de particulier.
}
	
MecAbstractVariable* MecVariableEditor::variable() const
{
return m_variable;
}

QVariant MecVariableEditor::state()
{
	return MecElementEditor::state();
}

void MecVariableEditor::setState(QVariant State)
{
	MecElementEditor::setState(State);
}
	
bool MecVariableEditor::canAddObject() const
{
return false;
}

bool MecVariableEditor::canAddFunction() const
{
return false;
}

bool MecVariableEditor::canAddSignal() const
{
return false;
}

bool MecVariableEditor::canAddVariable() const
{
return false;
}

bool MecVariableEditor::canRemoveChild(MecAbstractElement* const Element) const
{
return false;
}

bool MecVariableEditor::isContentEditable() const
{
	return true;
}

void MecVariableEditor::defaultValueVariableChanged(MecAbstractVariable *Variable)
{
}

void MecVariableEditor::edit()
{
	lineEditName->setFocus(Qt::OtherFocusReason);
}

//Ces fonctions sont bloquées pour ne rien faire.
void MecVariableEditor::editChild(MecAbstractElement *Element) {}
void MecVariableEditor::addObject() {}
void MecVariableEditor::addFunction() {}
void MecVariableEditor::addSignal() {}
void MecVariableEditor::addVariable() {}

void MecVariableEditor::setContentEditable(bool Editable)
{
}
	
MecAbstractElementEditor* MecVariableEditor::newSubEditor(MecAbstractElement *Element)
{
return 0;
}
	
void MecVariableEditor::subEditorAboutToDeleted(MecAbstractElementEditor *Editor) {}

void MecVariableEditor::reorderSubEditors() {}

void MecVariableEditor::addGeneralWidget(QWidget *Widget)
{
	layoutMain->addWidget(Widget);
}

