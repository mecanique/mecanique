/*
© Quentin VIGNAUD, 2013

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#ifndef __MECVARIABLECOMPILER_H__
#define __MECVARIABLECOMPILER_H__

#include <MecAbstractVariable>
#include "MecElementCompiler.h"

/**
\brief	Classe de compilation d'une variable.
*/
class MecVariableCompiler : public MecElementCompiler
{
public:
	/**
	\brief	Constructeur.
	\param	Variable	Variable compilée, doit absolument exister lors de la construction (c.-à-d. instanciée et différente de 0) sinon un comportement inattendu pourrait se produire.
	\param	MainCompiler	Compilateur maître.
	*/
	MecVariableCompiler(MecAbstractVariable* const Variable, MecAbstractCompiler* const MainCompiler);
	///Destructeur.
	virtual ~MecVariableCompiler();

	///Retourne la variable compilée.
	MecAbstractVariable* variable() const;

	/**
	Retourne la liste des ressources à ajouter au répertoire de compilation pour compiler cet élément.

	\return	Une liste vide.
	*/
	virtual QList<QResource*> resources();
	/**
	Retourne les instructions à ajouter au fichier projet (".pro").

	\return	Une chaîne vide.
	*/
	virtual QString projectInstructions();

	/**
	Retourne le contenu du header de l'élément.

	\return	Une chaîne vide.
	*/
	QString header();
	/**
	Retourne le contenu du fichier d'implémentation de l'élément.

	\return	Une chaîne vide.
	*/
	QString source();

	/**
	Retourne les instructions préprocesseur à ajouter dans le fichier header de l'élément parent.

	\return	Une chaîne vide.
	*/
	virtual QString preprocessorInstructions();

private:
	///Variable compilée.
	MecAbstractVariable* const m_variable;

};

#endif /* __MECVARIABLECOMPILER_H__ */

