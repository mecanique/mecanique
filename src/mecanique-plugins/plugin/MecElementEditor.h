/*
© Quentin VIGNAUD, 2013

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#ifndef __MECELEMENTEDITOR_H__
#define __MECELEMENTEDITOR_H__

#include <QtWidgets>
#include <MecAbstractElementEditor>

/**
\brief	Classe virtuelle d'édition d'un élément.
*/
class MecElementEditor : public MecAbstractElementEditor
{
	Q_OBJECT

public:
	/**
	\brief	Constructeur.
	\param	Element	Élément édité, doit absolument exister lors de la construction (c.à.d. instancié et différent de 0) sinon un comportement inattendu pourrait se produire.
	\param	MainEditor	Éditeur principal dont dépend cet éditeur d'élément.
	\param	Parent	Widget parent.
	\param	F	Drapeaux de fenêtre.

	Initialise l'éditeur et ses widgets enfants, et organise ces derniers.

	Après la construction d'un éditeur, il faut faire un appel à childListElementsChanged() avec Element en paramètre pour que l'éditeur charge les sous-éditeurs nécessaires.
	*/
	MecElementEditor(MecAbstractElement* const Element, MecAbstractEditor* MainEditor, QWidget * Parent=0, Qt::WindowFlags F=0);
	/**
	\brief	Destructeur.
	*/
	virtual ~MecElementEditor();

	///Élément édité.
	MecAbstractElement* element() const;
	///Rôle de l'éditeur.
	MecAbstractElement::ElementRole editorRole() const;

	///Éditeur principal.
	MecAbstractEditor* mainEditor() const;

	/**
	\brief	Retourne les erreurs de plugins.

	La structure est <Élément sans plugin, Nom éventuel du plugin>.
	*/
	QList<MecPluginError> pluginsErrors() const;

	/**
	\brief	Retourne l'éditeur de l'élément si il fait partie de ses sous-éditeurs.

	Cette fonction s'appelle récursivement sur les éléments enfants, ce qui fait que tout l'arbre, dont l'éditeur est la racine, est inspecté.

	\return	0 si l'éditeur de cet élément ne fait pas partie de ses sous-éditeurs.
	*/
	MecAbstractElementEditor* elementEditor(MecAbstractElement* const Element) const;

	/**
	\brief	Retourne un QVariant contenant toutes les données indiquant l'état actuel de l'éditeur.

	Cette donnée est vouée à être passée ultérieurement en paramètre à setState(QVariant) pour restaurer l'état visuel de l'éditeur.
	*/
	virtual QVariant state();
	/**
	\brief	Restaure l'état de l'éditeur selon les indications spécifées.

	\see	state()
	*/
	virtual void setState(QVariant State);

	///Indique si un élément de ce rôle peut être ajouté.
	bool canAddChild(MecAbstractElement::ElementRole ElementRole) const;

	///Indique si le type de l'élément est éditable.
	virtual bool isTypeEditable() const;
	///Indique si le nom de l'élément est éditable.
	virtual bool isNameEditable() const;

	/**
	\brief	Ouvre une boîte de dialogue de choix de nouvel élément.
	\param	ElementRole	Rôle d'élément à demander.

	Cette fonction demande à l'utilisateur un nouvel élément du rôle spécifié, parmi ceux proposés par MecAbstractEditor::baseElements(). Elle ne modifie en rien l'élément en lui-même et ne l'ajoute pas comme enfant à l'élément actuellement édité.

	\return	l'élément choisi, ou 0 si aucun.
	*/
	MecAbstractElement* getNewElementDialog(MecAbstractElement::ElementRole ElementRole);

public slots:
	/**
	\brief	Ajoute un élément de type \e ElementRole à l'élément édité.

	Appelle une de ces fonctions : addObject(), addFunction(), addSignal() ou addVariable().
	*/
	void addChild(MecAbstractElement::ElementRole ElementRole);

	/**
	\brief	Ajoute un objet à l'élément.

	Demande l'objet au moyen de getNewElementDialog().

	\see	addChild(MecAbstractElement::ElementRole ElementRole)
	*/
	virtual void addObject();
	/**
	\brief	Ajoute une fonction à l'élément.

	Demande la fonction au moyen de getNewElementDialog().

	\see	addChild(MecAbstractElement::ElementRole ElementRole)
	*/
	virtual void addFunction();
	/**
	\brief	Ajoute un signal à l'élément.

	Demande le signal au moyen de getNewElementDialog().

	\see	addChild(MecAbstractElement::ElementRole ElementRole)
	*/
	virtual void addSignal();
	/**
	\brief	Ajoute une variable à l'élément.

	Demande la variable au moyen de getNewElementDialog().

	\see	addChild(MecAbstractElement::ElementRole ElementRole)
	*/
	virtual void addVariable();

	/**
	\brief	Autorise ou non l'édition du type de l'élément.
	*/
	virtual void setTypeEditable(bool Editable);
	/**
	\brief	Autorise ou non l'édition du nom de l'élément.
	*/
	virtual void setNameEditable(bool Editable);

	/**
	\brief	Met à jour les données concernant le nom de l'élément.
	Met à jour :
		- la ligne d'édition du nom, lineEditName.

	Est connecté à MecAbstractElement::nameChanged().
	*/
	virtual void nameElementChanged(MecAbstractElement *Element);
	/**
	\brief	Met à jour les données concernant le type de l'élément.
	Met à jour :
		- la boite de sélection du type, comboBoxType,
		- l'icône du bouton principal de l'éditeur, pushButtonIcon,

	Est connecté à MecAbstractElement::typeChanged().
	*/
	virtual void typeElementChanged(MecAbstractElement *Element);

	/**
	\brief	Émet le signal "editedElementChange(MecAbstractElement *Element)".
	*/
	void emitEditedElementChange();

	/**
	\brief	Met à jour les données concernant les élément enfants.

	Est connecté au signal MecAbstractElement::childListChanged(MecAbstractElement *I) de son élément.\n

	Inspecte la liste des éléments enfants et la compare avec celle des sous-éditeurs, si un sous-éditeur est en trop, la fonction subEditorAboutToDeleted(MecAbstractElementEditor *Editor) est appelée puis le sous éditeur en question est détruit ; à l'inverse, la fonction newSubEditor(MecAbstractElement *Element) est appelée si il manque un sous-éditeur pour un élément.\n

	L'appel de cette fonction à la fin de la construction d'un éditeur d'élément permet le chargement automatique des sous-éditeurs, avec l'aide des fonctions newSubEditor() et subEditorAboutToDeleted(). \n
	*/
	void childListElementsChanged(MecAbstractElement* Element);


protected:
	///Retourne la liste des sous-éditeurs.
	QList<MecAbstractElementEditor*> subEditors() const;

	/**
	\brief	Ajoute une erreur de plugin et émet pluginsErrorsOccured().
	*/
	void addPluginsError(MecPluginError Error);

	/**
	\brief	Demande un sous-éditeur pour \e Element.
	\note	\e Element doit être un élément enfant de element().
	\return	Le sous-éditeur demandé, ou 0 si éditer cet élément n'est pas possible.
	*/
	virtual MecAbstractElementEditor* newSubEditor(MecAbstractElement *Element) = 0;

	/**
	\brief	Est appelée juste avant la suppression de \e Editor.
	*/
	virtual void subEditorAboutToDeleted(MecAbstractElementEditor *Editor) = 0;

	/**
	\brief	Effectue la vérification de l'ordre des sous-éditeurs.
	*/
	virtual void reorderSubEditors() = 0;

	/**
	\brief	Ajoute un widget à l'affichage des données générales.
	*/
	virtual void addGeneralWidget(QWidget *Widget) = 0;


	///Bouton-icône de l'éditeur.
	QPushButton *pushButtonIcon;
	///Label du type.
	QLabel *labelType;
	///Zone de sélection du type.
	QComboBox *comboBoxType;
	///Label du nom.
	QLabel *labelName;
	///Zone d'édition du nom.
	QLineEdit *lineEditName;


private:
	///Élément édité.
	MecAbstractElement* const m_element;
	///Rôle de l'éditeur.
	const MecAbstractElement::ElementRole m_editorRole;

	///Éditeur général.
	MecAbstractEditor *m_mainEditor;

	///Liste des sous-éditeurs.
	QList<MecAbstractElementEditor*> m_subEditors;

	/**
	\brief	Liste des erreurs dûes à un plugin manquant.
	*/
	QList<MecPluginError> m_pluginsErrors;

private slots:
	///Gère l'attribution du nouveau nom.
	void setElementName();
	///Gère l'attribution du nouveau type.
	void setElementType(QString Type);
	
	///Gère l'appel au menu contextuel de l'élément.
	void elementContextMenu();

};

#endif /* __MECELEMENTEDITOR_H__ */

