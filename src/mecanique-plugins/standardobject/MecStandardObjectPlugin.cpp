/*
© Quentin VIGNAUD, 2013

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#include "MecStandardObjectPlugin.h"

MecStandardObjectPlugin::MecStandardObjectPlugin() : MecPlugin(QString("standardobject"), __STANDARDOBJECT_VERSION__, MecAbstractElement::Object, "Object", QString("Standard object"))
{
}

MecStandardObjectPlugin::~MecStandardObjectPlugin()
{
}

QString MecStandardObjectPlugin::description() const
{
return QString(tr("Standard plugin for managing general objects."));
}

QString MecStandardObjectPlugin::copyright() const
{
return QString("Copyright © 2013 – 2015 Quentin VIGNAUD");
}

QString MecStandardObjectPlugin::developpers() const
{
return QString("Quentin VIGNAUD <quentin.vignaud@mecanique.cc>\n");
}

QString MecStandardObjectPlugin::documentalists() const
{
return QString("Quentin VIGNAUD <quentin.vignaud@mecanique.cc>\n");
}

QString MecStandardObjectPlugin::translators() const
{
return QString("Quentin VIGNAUD <quentin.vignaud@mecanique.cc>\n");
}

MecAbstractElementEditor* MecStandardObjectPlugin::elementEditor(MecAbstractElement* const Element, MecAbstractEditor* MainEditor)
{
if (Element->elementRole() == MecAbstractElement::Object)
	{
	MecObjectEditor *tempEditor = new MecObjectEditor(static_cast<MecAbstractObject*>(Element), MainEditor);
	tempEditor->childListElementsChanged(Element);
	return tempEditor;
	}
else
	{
	return 0;
	}
}

MecAbstractElementCompiler* MecStandardObjectPlugin::elementCompiler(MecAbstractElement* const Element, MecAbstractCompiler* const MainCompiler)
{
if (Element->elementRole() == MecAbstractElement::Object)
	{
	MecObjectCompiler *tempCompiler = new MecObjectCompiler(static_cast<MecAbstractObject*>(Element), MainCompiler);
	return tempCompiler;
	}
else
	{
	return 0;
	}
}


