######################################################################
# Project file implemented for qmake (version 3 with Qt 5)
######################################################################

include(../plugin/plugin.pri)
TARGET = standardobject
DEPENDPATH += .
INCLUDEPATH += .
RESOURCES += resources.qrc
TRANSLATIONS = translations/standardobject.fr.ts \
               translations/standardobject.de.ts

OTHER_FILES += metadata.json

HEADERS += \
                MecStandardObjectPlugin.h


SOURCES += \
                MecStandardObjectPlugin.cpp

