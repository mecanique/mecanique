<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="fr_FR">
<context>
    <name>MecElementEditor</name>
    <message>
        <location filename="../../plugin/MecElementEditor.cpp" line="32"/>
        <source>Type:</source>
        <translation>Type :</translation>
    </message>
    <message>
        <location filename="../../plugin/MecElementEditor.cpp" line="40"/>
        <source>Name:</source>
        <translation>Nom :</translation>
    </message>
    <message>
        <source>New object</source>
        <translation type="obsolete">Nouvel objet</translation>
    </message>
    <message>
        <source>Type of the new object:</source>
        <translation type="obsolete">Type du nouvel objet :</translation>
    </message>
    <message>
        <source>New function</source>
        <translation type="obsolete">Nouvelle fonction</translation>
    </message>
    <message>
        <source>Return type of the new function:</source>
        <translation type="obsolete">Type de retour de la nouvelle fonction :</translation>
    </message>
    <message>
        <source>New signal</source>
        <translation type="obsolete">Nouveau signal</translation>
    </message>
    <message>
        <source>Type of the new signal:</source>
        <translation type="obsolete">Type du nouveau signal :</translation>
    </message>
    <message>
        <source>New variable</source>
        <translation type="obsolete">Nouvelle variable</translation>
    </message>
    <message>
        <source>Type of the new variable:</source>
        <translation type="obsolete">Type de la nouvelle variable :</translation>
    </message>
    <message>
        <location filename="../../plugin/MecElementEditor.cpp" line="172"/>
        <source>Add the object “%1”</source>
        <translation>Ajouter l&apos;objet « %1 »</translation>
    </message>
    <message>
        <location filename="../../plugin/MecElementEditor.cpp" line="182"/>
        <source>Add the function “%1”</source>
        <translation>Ajouter la fonction « %1 »</translation>
    </message>
    <message>
        <location filename="../../plugin/MecElementEditor.cpp" line="192"/>
        <source>Add the signal “%1”</source>
        <translation>Ajouter le signal « %1 »</translation>
    </message>
    <message>
        <location filename="../../plugin/MecElementEditor.cpp" line="202"/>
        <source>Add the variable “%1”</source>
        <translation>Ajouter la variable « %1 »</translation>
    </message>
    <message>
        <location filename="../../plugin/MecElementEditor.cpp" line="297"/>
        <source>Change name of “%1” to “%2”</source>
        <translation>Changer le nom de « %1 » en « %2 »</translation>
    </message>
    <message>
        <location filename="../../plugin/MecElementEditor.cpp" line="310"/>
        <source>Change type of “%1” to “%2” from “%3”</source>
        <translation>Changer le type de « %1 » de « %2 » à « %3 »</translation>
    </message>
</context>
<context>
    <name>MecFunctionEditor</name>
    <message>
        <location filename="../../plugin/MecFunctionEditor.cpp" line="32"/>
        <source>General</source>
        <translation type="unfinished">Général</translation>
    </message>
    <message>
        <location filename="../../plugin/MecFunctionEditor.cpp" line="33"/>
        <source>Return type:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugin/MecFunctionEditor.cpp" line="45"/>
        <source>Variables</source>
        <translation type="unfinished">Variables</translation>
    </message>
    <message>
        <location filename="../../plugin/MecFunctionEditor.cpp" line="65"/>
        <source>Properties</source>
        <translation type="unfinished">Propriétés</translation>
    </message>
    <message>
        <location filename="../../plugin/MecFunctionEditor.cpp" line="75"/>
        <location filename="../../plugin/MecFunctionEditor.cpp" line="162"/>
        <source>Code</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MecObjectEditor</name>
    <message>
        <location filename="../../plugin/MecObjectEditor.cpp" line="32"/>
        <source>General</source>
        <translation type="unfinished">Général</translation>
    </message>
    <message>
        <location filename="../../plugin/MecObjectEditor.cpp" line="42"/>
        <source>Reference</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugin/MecObjectEditor.cpp" line="57"/>
        <source>Properties</source>
        <translation type="unfinished">Propriétés</translation>
    </message>
    <message>
        <location filename="../../plugin/MecObjectEditor.cpp" line="67"/>
        <location filename="../../plugin/MecObjectEditor.cpp" line="222"/>
        <source>Functions</source>
        <translation type="unfinished">Fonctions</translation>
    </message>
    <message>
        <location filename="../../plugin/MecObjectEditor.cpp" line="77"/>
        <location filename="../../plugin/MecObjectEditor.cpp" line="223"/>
        <source>Signals</source>
        <translation type="unfinished">Signaux</translation>
    </message>
    <message>
        <location filename="../../plugin/MecObjectEditor.cpp" line="96"/>
        <location filename="../../plugin/MecObjectEditor.cpp" line="224"/>
        <source>Variables</source>
        <translation type="unfinished">Variables</translation>
    </message>
</context>
<context>
    <name>MecProjectEditor</name>
    <message>
        <location filename="../../plugin/MecProjectEditor.cpp" line="30"/>
        <source>General</source>
        <translation>Général</translation>
    </message>
    <message>
        <location filename="../../plugin/MecProjectEditor.cpp" line="35"/>
        <source>Title:</source>
        <translation>Titre :</translation>
    </message>
    <message>
        <location filename="../../plugin/MecProjectEditor.cpp" line="45"/>
        <source>Synopsis</source>
        <translation>Synopsis</translation>
    </message>
    <message>
        <location filename="../../plugin/MecProjectEditor.cpp" line="56"/>
        <source>Properties</source>
        <translation>Propriétés</translation>
    </message>
    <message>
        <location filename="../../plugin/MecProjectEditor.cpp" line="66"/>
        <source>Objects</source>
        <translation>Objets</translation>
    </message>
    <message>
        <location filename="../../plugin/MecProjectEditor.cpp" line="76"/>
        <location filename="../../plugin/MecProjectEditor.cpp" line="265"/>
        <source>Functions</source>
        <translation>Fonctions</translation>
    </message>
    <message>
        <location filename="../../plugin/MecProjectEditor.cpp" line="86"/>
        <location filename="../../plugin/MecProjectEditor.cpp" line="266"/>
        <source>Signals</source>
        <translation>Signaux</translation>
    </message>
    <message>
        <location filename="../../plugin/MecProjectEditor.cpp" line="105"/>
        <location filename="../../plugin/MecProjectEditor.cpp" line="267"/>
        <source>Variables</source>
        <translation>Variables</translation>
    </message>
    <message>
        <location filename="../../plugin/MecProjectEditor.cpp" line="407"/>
        <source>Change title of the project</source>
        <translation>Changer le titre du projet</translation>
    </message>
    <message>
        <location filename="../../plugin/MecProjectEditor.cpp" line="415"/>
        <source>Change synopsis of the project</source>
        <translation>Changer le synopsis du projet</translation>
    </message>
</context>
<context>
    <name>MecSignalConnectionsList</name>
    <message>
        <location filename="../../plugin/MecSignalEditor.cpp" line="93"/>
        <source>Remove connections with “%1”</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugin/MecSignalEditor.cpp" line="108"/>
        <source>Add connection between “%1” and “%2”</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MecSignalEditor</name>
    <message>
        <location filename="../../plugin/MecSignalEditor.cpp" line="190"/>
        <source>General</source>
        <translation type="unfinished">Général</translation>
    </message>
    <message>
        <location filename="../../plugin/MecSignalEditor.cpp" line="204"/>
        <source>Variables</source>
        <translation type="unfinished">Variables</translation>
    </message>
    <message>
        <location filename="../../plugin/MecSignalEditor.cpp" line="224"/>
        <source>Properties</source>
        <translation type="unfinished">Propriétés</translation>
    </message>
    <message>
        <location filename="../../plugin/MecSignalEditor.cpp" line="231"/>
        <source>Add connection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugin/MecSignalEditor.cpp" line="233"/>
        <source>Remove connection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugin/MecSignalEditor.cpp" line="241"/>
        <source>Connections</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugin/MecSignalEditor.cpp" line="403"/>
        <source>Add connection between “%1” and “%2”</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MecStandardProjectPlugin</name>
    <message>
        <location filename="../MecStandardProjectPlugin.cpp" line="32"/>
        <source>Standard plugin for managing projects.</source>
        <translation>Greffon standard de gestion des projets.</translation>
    </message>
</context>
</TS>
