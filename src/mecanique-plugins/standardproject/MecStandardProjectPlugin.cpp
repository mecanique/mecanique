/*
© Quentin VIGNAUD, 2013

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#include "MecStandardProjectPlugin.h"

MecStandardProjectPlugin::MecStandardProjectPlugin() : MecPlugin(QString("standardproject"), __STANDARDPROJECT_VERSION__, MecAbstractElement::Project, QString("Project"), QString("Standard project"))
{
}

MecStandardProjectPlugin::~MecStandardProjectPlugin()
{
}

QString MecStandardProjectPlugin::description() const
{
return QString(tr("Standard plugin for managing projects."));
}

QString MecStandardProjectPlugin::copyright() const
{
return QString("Copyright © 2013 – 2015 Quentin VIGNAUD");
}

QString MecStandardProjectPlugin::developpers() const
{
return QString("Quentin VIGNAUD <quentin.vignaud@mecanique.cc>\n");
}

QString MecStandardProjectPlugin::documentalists() const
{
return QString("Quentin VIGNAUD <quentin.vignaud@mecanique.cc>\n");
}

QString MecStandardProjectPlugin::translators() const
{
return QString("Quentin VIGNAUD <quentin.vignaud@mecanique.cc>\n");
}
	
MecAbstractElementEditor* MecStandardProjectPlugin::elementEditor(MecAbstractElement* const Element, MecAbstractEditor* MainEditor)
{
if (Element->elementRole() == MecAbstractElement::Project)
	{
	MecProjectEditor *tempEditor = new MecProjectEditor(static_cast<MecAbstractProject*>(Element), MainEditor);
	tempEditor->childListElementsChanged(Element);
	return tempEditor;
	}
else
	{
	return 0;
	}
}

MecAbstractElementCompiler* MecStandardProjectPlugin::elementCompiler(MecAbstractElement* const Element, MecAbstractCompiler* const MainCompiler)
{
if (Element->elementRole() == MecAbstractElement::Project)
	{
	MecProjectCompiler *tempCompiler = new MecProjectCompiler(static_cast<MecAbstractProject*>(Element), MainCompiler);
	return tempCompiler;
	}
else
	{
	return 0;
	}
}

