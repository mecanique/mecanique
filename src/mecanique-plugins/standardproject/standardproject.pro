######################################################################
# Project file implemented for qmake (version 3 with Qt 5)
######################################################################

include(../plugin/plugin.pri)
TARGET = standardproject
DEPENDPATH += .
INCLUDEPATH += .
RESOURCES += resources.qrc
TRANSLATIONS = translations/standardproject.fr.ts \
               translations/standardproject.de.ts

OTHER_FILES += metadata.json

HEADERS += \
                MecStandardProjectPlugin.h 


SOURCES += \
                MecStandardProjectPlugin.cpp 

