######################################################################
# Project file implemented for qmake (version 3 with Qt 5)
######################################################################

include(../plugin/plugin.pri)
TARGET = standardsignal
DEPENDPATH += .
INCLUDEPATH += .
RESOURCES += resources.qrc
TRANSLATIONS = translations/standardsignal.fr.ts \
               translations/standardsignal.de.ts

OTHER_FILES += metadata.json

HEADERS += \
                MecStandardSignalPlugin.h


SOURCES += \
                MecStandardSignalPlugin.cpp

