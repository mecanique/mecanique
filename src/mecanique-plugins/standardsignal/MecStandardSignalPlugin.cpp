/*
© Quentin VIGNAUD, 2013

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#include "MecStandardSignalPlugin.h"

MecStandardSignalPlugin::MecStandardSignalPlugin() : MecPlugin(QString("standardsignal"), __STANDARDSIGNAL_VERSION__, MecAbstractElement::Signal, QString("void"), QString("Standard signal"))
{
}

MecStandardSignalPlugin::~MecStandardSignalPlugin()
{
}

QString MecStandardSignalPlugin::description() const
{
return QString(tr("Standard plugin for managing signals."));
}

QString MecStandardSignalPlugin::copyright() const
{
return QString("Copyright © 2013 – 2015 Quentin VIGNAUD");
}

QString MecStandardSignalPlugin::developpers() const
{
return QString("Quentin VIGNAUD <quentin.vignaud@mecanique.cc>\n");
}

QString MecStandardSignalPlugin::documentalists() const
{
return QString("Quentin VIGNAUD <quentin.vignaud@mecanique.cc>\n");
}

QString MecStandardSignalPlugin::translators() const
{
return QString("Quentin VIGNAUD <quentin.vignaud@mecanique.cc>\n");
}

MecAbstractElementEditor* MecStandardSignalPlugin::elementEditor(MecAbstractElement* const Element, MecAbstractEditor* MainEditor)
{
if (Element->elementRole() == MecAbstractElement::Signal and Element->elementType() == "void")
	{
	MecSignalEditor *tempEditor = new MecSignalEditor(static_cast<MecAbstractSignal*>(Element), MainEditor);
	tempEditor->childListElementsChanged(Element);
	return tempEditor;
	}
else
	{
	return 0;
	}
}

MecAbstractElementCompiler* MecStandardSignalPlugin::elementCompiler(MecAbstractElement* const Element, MecAbstractCompiler* const MainCompiler)
{
if (Element->elementRole() == MecAbstractElement::Signal and Element->elementType() == "void")
	{
	MecSignalCompiler *tempCompiler = new MecSignalCompiler(static_cast<MecAbstractSignal*>(Element), MainCompiler);
	return tempCompiler;
	}
else
	{
	return 0;
	}
}


