######################################################################
# Project file implemented for qmake (version 3 with Qt 5)
######################################################################

include(../plugin/plugin.pri)
TARGET = standardstringvariable
DEPENDPATH += .
INCLUDEPATH += .
RESOURCES += resources.qrc
TRANSLATIONS = translations/standardstringvariable.fr.ts \
               translations/standardstringvariable.de.ts

OTHER_FILES += metadata.json

HEADERS += \
                MecStandardStringVariableCompiler.h \
                MecStandardStringVariableEditor.h \
                MecStandardStringVariablePlugin.h


SOURCES += \
                MecStandardStringVariableCompiler.cpp \
                MecStandardStringVariableEditor.cpp \
                MecStandardStringVariablePlugin.cpp

