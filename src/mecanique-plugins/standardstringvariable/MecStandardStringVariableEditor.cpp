/*
© Quentin VIGNAUD, 2013

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#include "MecStandardStringVariableEditor.h"

MecStandardStringVariableEditor::MecStandardStringVariableEditor(MecAbstractVariable* const Variable, MecAbstractEditor* MainEditor, QWidget * Parent, Qt::WindowFlags F) : MecVariableEditor(Variable, MainEditor, Parent, F)
{
comboBoxType->setEnabled(false);

labelDefaultValue = new QLabel(tr("Default value:"), this);
lineEditDefaultValue = new QLineEdit(this);
	connect(lineEditDefaultValue, SIGNAL(textEdited(QString)), SLOT(setDefaultValue(QString)));

layoutMain->addWidget(labelDefaultValue);
layoutMain->addWidget(lineEditDefaultValue);

defaultValueVariableChanged(variable());
}

MecStandardStringVariableEditor::~MecStandardStringVariableEditor()
{
}

bool MecStandardStringVariableEditor::isContentEditable() const
{
	return lineEditDefaultValue->isEnabled();
}

void MecStandardStringVariableEditor::defaultValueVariableChanged(MecAbstractVariable *Variable)
{
if (Variable != variable()) return;
lineEditDefaultValue->setText(Variable->defaultValue().toString());
}

void MecStandardStringVariableEditor::setContentEditable(bool Editable)
{
	lineEditDefaultValue->setEnabled(Editable);
}

void MecStandardStringVariableEditor::setDefaultValue(QString String)
{
variable()->setDefaultValue(QVariant(String));
mainEditor()->addEditStep(tr("Change default value of “%1” to “%2”").arg(element()->elementName()).arg(String));
}


