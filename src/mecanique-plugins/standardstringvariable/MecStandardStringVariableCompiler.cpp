/*
© Quentin VIGNAUD, 2013

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#include "MecStandardStringVariableCompiler.h"

MecStandardStringVariableCompiler::MecStandardStringVariableCompiler(MecAbstractVariable* const Variable, MecAbstractCompiler* const MainCompiler) : MecVariableCompiler(Variable, MainCompiler)
{
}

MecStandardStringVariableCompiler::~MecStandardStringVariableCompiler()
{
}

QList<QResource*> MecStandardStringVariableCompiler::resources()
{
QList<QResource*> tempList;
tempList.append(new QResource(":/src/mecanique/string.h"));
return tempList;
}

QString MecStandardStringVariableCompiler::projectInstructions()
{
return QString("HEADERS += ./mecanique/string.h\n");
}

QString MecStandardStringVariableCompiler::preprocessorInstructions()
{
return QString("#include \"mecanique/string.h\"\n");
}

QString MecStandardStringVariableCompiler::headerInstructions()
{
QString tempString;
if (variable()->parentElement() != 0)
	{
	if (variable()->parentElement()->elementRole() == MecAbstractElement::Project or variable()->parentElement()->elementRole() == MecAbstractElement::Object)
		{
		tempString = variable()->elementType() + " " + variable()->elementName() + ";\n";
		}
	else if (variable()->parentElement()->elementRole() == MecAbstractElement::Function)
		{
		tempString = variable()->elementType() + " " + variable()->elementName();
		
		if (variable()->defaultValue().canConvert(QMetaType::QString))
			{
			tempString += "=\"" + variable()->defaultValue().toString().replace("\\", "\\\\").replace("\"", "\\\"") + "\"";
			}
		}
	else if (variable()->parentElement()->elementRole() == MecAbstractElement::Signal)
		{
		tempString = variable()->elementType() + " " + variable()->elementName();
		}
	}
return tempString;
}

QString MecStandardStringVariableCompiler::sourceInstructions()
{
QString tempString;
if (variable()->parentElement() != 0)
	{
	if (variable()->parentElement()->elementRole() == MecAbstractElement::Project or variable()->parentElement()->elementRole() == MecAbstractElement::Object)
		{
		tempString = variable()->elementName();
		if (variable()->defaultValue().canConvert(QMetaType::QString))
			{
			tempString += " = \"" + variable()->defaultValue().toString().replace("\\", "\\\\").replace("\"", "\\\"") + "\";\n";
			}
		else tempString += " = \"\";\n";
		}
	}
return tempString;
}


