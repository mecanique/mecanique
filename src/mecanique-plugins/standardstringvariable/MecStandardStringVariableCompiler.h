/*
© Quentin VIGNAUD, 2013

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#ifndef __MECSTANDARDSTRINGVARIABLECOMPILER_H__
#define __MECSTANDARDSTRINGVARIABLECOMPILER_H__

#include <MecVariableCompiler.h>

/**
\brief	Classe de compilation d'une variable de type string.
*/
class MecStandardStringVariableCompiler : public MecVariableCompiler
{
public:
	///Constructeur.
	MecStandardStringVariableCompiler(MecAbstractVariable* const Variable, MecAbstractCompiler* const MainCompiler);
	///Destructeur.
	~MecStandardStringVariableCompiler();

	/**
	Retourne la liste des ressources à ajouter au répertoire de compilation pour compiler cet élément.

	\return	la ressource « :/src/mecanique/string.h ».
	*/
	QList<QResource*> resources();
	/**
	Retourne les instructions à ajouter au fichier projet (".pro").

	\return	« HEADERS += ./mecanique/string.h\\n ».
	*/
	QString projectInstructions();

	/**
	Retourne les instructions préprocesseur à ajouter dans le fichier header de l'élément parent.

	\return	« #include "mecanique/string.h"\\n ».
	*/
	QString preprocessorInstructions();
	/**
	Retourne le code C++ à ajouter dans la déclaration de l'élément parent.

	Si l'élément parent est un projet ou un objet, la chaîne retournée est « string [nom];\\n », si c'est une fonction « string [nom]=\"[valeur par défaut]\" » et si c'est un signal « string [nom] ».
	*/
	QString headerInstructions();
	/**
	Retourne le code C++ à ajouter dans le constructeur de l'élément parent.

	Si l'élément parent est un project ou un objet, la chaîne retournée est « [nom] = \"[valeur par défaut]\";\\n », sinon cette dernière est vide.
	*/
	QString sourceInstructions();


};

#endif /* __MECSTANDARDSTRINGVARIABLECOMPILER_H__ */

