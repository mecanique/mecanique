/*
© Quentin VIGNAUD, 2013

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#include "MecStandardIntVariablePlugin.h"

MecStandardIntVariablePlugin::MecStandardIntVariablePlugin() : MecPlugin(QString("standardintvariable"), __STANDARDINTVARIABLE_VERSION__, MecAbstractElement::Variable, QString("int"), QString("Standard integer variable"))
{
}

MecStandardIntVariablePlugin::~MecStandardIntVariablePlugin()
{
}

QString MecStandardIntVariablePlugin::description() const
{
return QString(tr("Standard plugin for managing integer variables."));
}

QString MecStandardIntVariablePlugin::copyright() const
{
return QString("Copyright © 2013 – 2015 Quentin VIGNAUD");
}

QString MecStandardIntVariablePlugin::developpers() const
{
return QString("Quentin VIGNAUD <quentin.vignaud@mecanique.cc>\n");
}

QString MecStandardIntVariablePlugin::documentalists() const
{
return QString("Quentin VIGNAUD <quentin.vignaud@mecanique.cc>\n");
}

QString MecStandardIntVariablePlugin::translators() const
{
return QString("Quentin VIGNAUD <quentin.vignaud@mecanique.cc>\n");
}

MecAbstractElementEditor* MecStandardIntVariablePlugin::elementEditor(MecAbstractElement* const Element, MecAbstractEditor* MainEditor)
{
if (Element->elementRole() == MecAbstractElement::Variable and Element->elementType() == "int")
	{
	MecStandardIntVariableEditor *tempEditor = new MecStandardIntVariableEditor(static_cast<MecAbstractVariable*>(Element), MainEditor);
	return tempEditor;
	}
else return 0;
}

MecAbstractElementCompiler* MecStandardIntVariablePlugin::elementCompiler(MecAbstractElement* const Element, MecAbstractCompiler* const MainCompiler)
{
if (Element->elementRole() == MecAbstractElement::Variable and Element->elementType() == "int")
	{
	MecStandardIntVariableCompiler *tempCompiler = new MecStandardIntVariableCompiler(static_cast<MecAbstractVariable*>(Element), MainCompiler);
	return tempCompiler;
	}
else return 0;
}
