/*
© Quentin VIGNAUD, 2013

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#ifndef __MECSTANDARDINTVARIABLEEDITOR_H__
#define __MECSTANDARDINTVARIABLEEDITOR_H__

#include <MecVariableEditor.h>
#include <climits>

/**
\brief	Classe d'édition d'une variable int.
*/
class MecStandardIntVariableEditor : public MecVariableEditor
{
	Q_OBJECT

public:
	///Constructeur. \see	MecVariableEditor::MecVariableEditor()
	MecStandardIntVariableEditor(MecAbstractVariable* const Variable, MecAbstractEditor* MainEditor, QWidget * Parent=0, Qt::WindowFlags F=0);
	///Destructeur. \see	MecVariableEditor::~MecVariableEditor()
	~MecStandardIntVariableEditor();

	/**
	Indique si la valeur par défaut de la variable est éditable.

	\see	setContentEditable(bool)
	*/
	virtual bool isContentEditable() const;

public slots:
	///Est activé lorsque la valeur par défaut de la variable a changé.
	void defaultValueVariableChanged(MecAbstractVariable *Variable);

	/**
	\brief	Autorise ou non l'édition de la valeur par défaut.
	*/
	virtual void setContentEditable(bool Editable);

signals:


private:
	///Label d'indication de la valeur par défaut.
	QLabel *labelDefaultValue;
	///Spinbox contenant la valeur par défaut.
	QSpinBox *spinBoxDefaultValue;


private slots:
	///Fixe la valeur par défaut dans la variable.
	void setDefaultValue(int Number);

};

#endif /* __MECSTANDARDINTVARIABLEEDITOR_H__ */

