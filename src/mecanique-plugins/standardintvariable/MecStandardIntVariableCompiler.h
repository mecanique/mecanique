/*
© Quentin VIGNAUD, 2013

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#ifndef __MECSTANDARDINTVARIABLECOMPILER_H__
#define __MECSTANDARDINTVARIABLECOMPILER_H__

#include <MecVariableCompiler.h>

/**
\brief	Classe de compilation d'une variable de type int.
*/
class MecStandardIntVariableCompiler : public MecVariableCompiler
{
public:
	///Constructeur.
	MecStandardIntVariableCompiler(MecAbstractVariable* const Variable, MecAbstractCompiler* const MainCompiler);
	///Destructeur.
	~MecStandardIntVariableCompiler();

	/**
	Retourne le code C++ à ajouter dans la déclaration de l'élément parent.

	Si l'élément parent est un projet ou un objet, la chaîne retournée est « int [nom];\\n », si c'est une fonction « int [nom]=[valeur par défaut] » et si c'est un signal « int [nom] ».
	*/
	QString headerInstructions();
	/**
	Retourne le code C++ à ajouter dans le constructeur de l'élément parent.

	Si l'élément parent est un project ou un objet, la chaîne retournée est « [nom] = [valeur par défaut];\\n », sinon cette dernière est vide.
	*/
	QString sourceInstructions();


};

#endif /* __MECSTANDARDINTVARIABLECOMPILER_H__ */

