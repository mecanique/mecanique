######################################################################
# Project file implemented for qmake (version 3 with Qt 5)
######################################################################

include(../plugin/plugin.pri)
TARGET = standardintvariable
DEPENDPATH += .
INCLUDEPATH += .
RESOURCES += resources.qrc
TRANSLATIONS = translations/standardintvariable.fr.ts \
               translations/standardintvariable.de.ts

OTHER_FILES += metadata.json

HEADERS += \
                MecStandardIntVariablePlugin.h \
                MecStandardIntVariableCompiler.h \
                MecStandardIntVariableEditor.h


SOURCES += \
                MecStandardIntVariablePlugin.cpp \
                MecStandardIntVariableCompiler.cpp \
                MecStandardIntVariableEditor.cpp

