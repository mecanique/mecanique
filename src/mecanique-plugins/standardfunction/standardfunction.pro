######################################################################
# Project file implemented for qmake (version 3 with Qt 5)
######################################################################

include(../plugin/plugin.pri)
TARGET = standardfunction
DEPENDPATH += .
INCLUDEPATH += .
RESOURCES += resources.qrc
TRANSLATIONS = translations/standardfunction.fr.ts \
               translations/standardfunction.de.ts

OTHER_FILES += metadata.json

HEADERS += \
                MecStandardFunctionPlugin.h


SOURCES += \
                MecStandardFunctionPlugin.cpp

