/*
© Quentin VIGNAUD, 2013

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#include "MecStandardFunctionPlugin.h"

MecStandardFunctionPlugin::MecStandardFunctionPlugin() : MecPlugin(QString("standardfunction"), __STANDARDFUNCTION_VERSION__, MecAbstractElement::Function, QString(), QString("Standard function"))
{
}

MecStandardFunctionPlugin::~MecStandardFunctionPlugin()
{
}

QString MecStandardFunctionPlugin::description() const
{
return QString(tr("Standard plugin for managing functions (all return types)."));
}

QString MecStandardFunctionPlugin::copyright() const
{
return QString("Copyright © 2013 – 2015 Quentin VIGNAUD");
}

QString MecStandardFunctionPlugin::developpers() const
{
return QString("Quentin VIGNAUD <quentin.vignaud@mecanique.cc>\n");
}

QString MecStandardFunctionPlugin::documentalists() const
{
return QString("Quentin VIGNAUD <quentin.vignaud@mecanique.cc>\n");
}

QString MecStandardFunctionPlugin::translators() const
{
return QString("Quentin VIGNAUD <quentin.vignaud@mecanique.cc>\n");
}

MecAbstractElementEditor* MecStandardFunctionPlugin::elementEditor(MecAbstractElement* const Element, MecAbstractEditor* MainEditor)
{
if (Element->elementRole() == MecAbstractElement::Function)
	{
	MecFunctionEditor *tempEditor = new MecFunctionEditor(static_cast<MecAbstractFunction*>(Element), MainEditor);
	tempEditor->childListElementsChanged(Element);
	return tempEditor;
	}
else
	{
	return 0;
	}
}

MecAbstractElementCompiler* MecStandardFunctionPlugin::elementCompiler(MecAbstractElement* const Element, MecAbstractCompiler* const MainCompiler)
{
if (Element->elementRole() == MecAbstractElement::Function)
	{
	MecFunctionCompiler *tempCompiler = new MecFunctionCompiler(static_cast<MecAbstractFunction*>(Element), MainCompiler);
	return tempCompiler;
	}
else
	{
	return 0;
	}
}


