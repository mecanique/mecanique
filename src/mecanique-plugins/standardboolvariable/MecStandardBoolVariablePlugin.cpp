/*
© Quentin VIGNAUD, 2013

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#include "MecStandardBoolVariablePlugin.h"

MecStandardBoolVariablePlugin::MecStandardBoolVariablePlugin() : MecPlugin(QString("standardboolvariable"), __STANDARDBOOLVARIABLE_VERSION__, MecAbstractElement::Variable, QString("bool"), QString("Standard boolean variable"))
{
}

MecStandardBoolVariablePlugin::~MecStandardBoolVariablePlugin()
{
}

QString MecStandardBoolVariablePlugin::description() const
{
return QString(tr("Standard plugin for managing boolean variables."));
}

QString MecStandardBoolVariablePlugin::copyright() const
{
return QString("Copyright © 2013 – 2015 Quentin VIGNAUD");
}

QString MecStandardBoolVariablePlugin::developpers() const
{
return QString("Quentin VIGNAUD <quentin.vignaud@mecanique.cc>\n");
}

QString MecStandardBoolVariablePlugin::documentalists() const
{
return QString("Quentin VIGNAUD <quentin.vignaud@mecanique.cc>\n");
}

QString MecStandardBoolVariablePlugin::translators() const
{
return QString("Quentin VIGNAUD <quentin.vignaud@mecanique.cc>\n");
}

MecAbstractElementEditor* MecStandardBoolVariablePlugin::elementEditor(MecAbstractElement* const Element, MecAbstractEditor* MainEditor)
{
if (Element->elementRole() == MecAbstractElement::Variable and Element->elementType() == "bool")
	{
	MecStandardBoolVariableEditor *tempEditor = new MecStandardBoolVariableEditor(static_cast<MecAbstractVariable*>(Element), MainEditor);
	return tempEditor;
	}
else return 0;
}

MecAbstractElementCompiler* MecStandardBoolVariablePlugin::elementCompiler(MecAbstractElement* const Element, MecAbstractCompiler* const MainCompiler)
{
if (Element->elementRole() == MecAbstractElement::Variable and Element->elementType() == "bool")
	{
	MecStandardBoolVariableCompiler *tempCompiler = new MecStandardBoolVariableCompiler(static_cast<MecAbstractVariable*>(Element), MainCompiler);
	return tempCompiler;
	}
else return 0;
}
