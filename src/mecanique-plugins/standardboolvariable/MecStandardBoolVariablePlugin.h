/*
© Quentin VIGNAUD, 2013

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#ifndef __MECSTANDARDBOOLVARIABLEPLUGIN_H__
#define __MECSTANDARDBOOLVARIABLEPLUGIN_H__

#include <MecPlugin.h>
///Version du plugin.
#define __STANDARDBOOLVARIABLE_VERSION__ 1.000

#include "MecStandardBoolVariableCompiler.h"
#include "MecStandardBoolVariableEditor.h"

/**
\brief	Classe d'interface du plugin « Standard bool variable »

\par	Propriétés
Nom : standardboolvariable \n
Version : 1.000 \n
Rôle : Variable \n
Type : bool \n
Titre : Standard bool variable \n

*/
class MecStandardBoolVariablePlugin : public MecPlugin
{
	Q_OBJECT
	Q_PLUGIN_METADATA(IID "Mecanique.MecAbstractPlugin" FILE "metadata.json")

public:
	///Constructeur.
	MecStandardBoolVariablePlugin();
	///Destructeur.
	~MecStandardBoolVariablePlugin();

	///Retourne la description du plugin.
	QString description() const;
	///Retourne le copyright du plugin.
	QString copyright() const;
	///Retourne la liste des développeurs du plugin.
	QString developpers() const;
	///Retourne la liste des documentalistes.
	QString documentalists() const;
	///Retourne la liste des traducteurs.
	QString translators() const;

	/**
	\brief	Retourne un éditeur de l'élément spécifié.
	\param	Element	Doit correspondre aux données du plugin, sinon 0 est retourné.
	*/
	MecAbstractElementEditor* elementEditor(MecAbstractElement* const Element, MecAbstractEditor* MainEditor);
	/**
	\brief	Retourne un compilateur de l'élément spécifié.
	\param	Element	Doit correspondre aux données du plugin, sinon 0 est retourné.
	*/
	MecAbstractElementCompiler* elementCompiler(MecAbstractElement* const Element, MecAbstractCompiler* const MainCompiler);

};

#endif /* __MECSTANDARDBOOLVARIABLEPLUGIN_H__ */

