######################################################################
# Project file implemented for qmake (version 3 with Qt 5)
######################################################################

include(../plugin/plugin.pri)
TARGET = standardboolvariable
DEPENDPATH += .
INCLUDEPATH += .
RESOURCES += resources.qrc
TRANSLATIONS = translations/standardboolvariable.fr.ts \
               translations/standardboolvariable.de.ts

OTHER_FILES += metadata.json

HEADERS += \
                MecStandardBoolVariablePlugin.h \
                MecStandardBoolVariableCompiler.h \
                MecStandardBoolVariableEditor.h


SOURCES += \
                MecStandardBoolVariablePlugin.cpp \
                MecStandardBoolVariableCompiler.cpp \
                MecStandardBoolVariableEditor.cpp

