/*
© Quentin VIGNAUD, 2013

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#include "MecStandardBoolVariableEditor.h"

MecStandardBoolVariableEditor::MecStandardBoolVariableEditor(MecAbstractVariable* const Variable, MecAbstractEditor* MainEditor, QWidget * Parent, Qt::WindowFlags F) : MecVariableEditor(Variable, MainEditor, Parent, F)
{

comboBoxType->setEnabled(false);

labelDefaultValue = new QLabel(tr("Default value:"), this);
comboBoxDefaultValue = new QComboBox(this);
	comboBoxDefaultValue->addItems(QStringList() << tr("false") << tr("true"));
	connect(comboBoxDefaultValue, SIGNAL(activated(int)), SLOT(setDefaultValue(int)));

layoutMain->addWidget(labelDefaultValue);
layoutMain->addWidget(comboBoxDefaultValue);


defaultValueVariableChanged(variable());
}

MecStandardBoolVariableEditor::~MecStandardBoolVariableEditor()
{
}

bool MecStandardBoolVariableEditor::isContentEditable() const
{
	return comboBoxDefaultValue->isEnabled();
}

void MecStandardBoolVariableEditor::defaultValueVariableChanged(MecAbstractVariable *Variable)
{
if (Variable != variable()) return;
if (Variable->defaultValue().toBool())
	{
	comboBoxDefaultValue->setCurrentIndex(1);
	}
else comboBoxDefaultValue->setCurrentIndex(0);
}

void MecStandardBoolVariableEditor::setContentEditable(bool Editable)
{
	comboBoxDefaultValue->setEnabled(Editable);
}

void MecStandardBoolVariableEditor::setDefaultValue(int Index)
{
bool defVal = (Index != 0);
variable()->setDefaultValue(QVariant(defVal));
QString textValue;
if (defVal) textValue = "true";
else textValue = "false";
mainEditor()->addEditStep(tr("Change default value of “%1” to “%2”").arg(element()->elementName()).arg(textValue));
}


