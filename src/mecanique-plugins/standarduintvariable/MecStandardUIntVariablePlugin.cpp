/*
© Quentin VIGNAUD, 2013

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#include "MecStandardUIntVariablePlugin.h"

MecStandardUIntVariablePlugin::MecStandardUIntVariablePlugin() : MecPlugin(QString("standarduintvariable"), __STANDARDUINTVARIABLE_VERSION__, MecAbstractElement::Variable, QString("uint"), QString("Standard unsigned integer variable"))
{
}

MecStandardUIntVariablePlugin::~MecStandardUIntVariablePlugin()
{
}

QString MecStandardUIntVariablePlugin::description() const
{
return QString(tr("Standard plugin for managing unsigned integer variables."));
}

QString MecStandardUIntVariablePlugin::copyright() const
{
return QString("Copyright © 2013 – 2015 Quentin VIGNAUD");
}

QString MecStandardUIntVariablePlugin::developpers() const
{
return QString("Quentin VIGNAUD <quentin.vignaud@mecanique.cc>\n");
}

QString MecStandardUIntVariablePlugin::documentalists() const
{
return QString("Quentin VIGNAUD <quentin.vignaud@mecanique.cc>\n");
}

QString MecStandardUIntVariablePlugin::translators() const
{
return QString("Quentin VIGNAUD <quentin.vignaud@mecanique.cc>\n");
}

MecAbstractElementEditor* MecStandardUIntVariablePlugin::elementEditor(MecAbstractElement* const Element, MecAbstractEditor* MainEditor)
{
if (Element->elementRole() == MecAbstractElement::Variable and Element->elementType() == "uint")
	{
	MecStandardUIntVariableEditor *tempEditor = new MecStandardUIntVariableEditor(static_cast<MecAbstractVariable*>(Element), MainEditor);
	return tempEditor;
	}
else return 0;
}

MecAbstractElementCompiler* MecStandardUIntVariablePlugin::elementCompiler(MecAbstractElement* const Element, MecAbstractCompiler* const MainCompiler)
{
if (Element->elementRole() == MecAbstractElement::Variable and Element->elementType() == "uint")
	{
	MecStandardUIntVariableCompiler *tempCompiler = new MecStandardUIntVariableCompiler(static_cast<MecAbstractVariable*>(Element), MainCompiler);
	return tempCompiler;
	}
else return 0;
}


