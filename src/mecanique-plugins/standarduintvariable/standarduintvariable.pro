######################################################################
# Project file implemented for qmake (version 3 with Qt 5)
######################################################################

include(../plugin/plugin.pri)
TARGET = standarduintvariable
DEPENDPATH += .
INCLUDEPATH += .
RESOURCES += resources.qrc
TRANSLATIONS = translations/standarduintvariable.fr.ts \
               translations/standarduintvariable.de.ts

OTHER_FILES += metadata.json

HEADERS += \
                MecStandardUIntVariableEditor.h \
                MecStandardUIntVariableCompiler.h \
                MecStandardUIntVariablePlugin.h


SOURCES += \
                MecStandardUIntVariableEditor.cpp \
                MecStandardUIntVariableCompiler.cpp \
                MecStandardUIntVariablePlugin.cpp

