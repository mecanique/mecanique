/*
© Quentin VIGNAUD, 2013

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#include "MecStandardUIntVariableEditor.h"

MecStandardUIntVariableEditor::MecStandardUIntVariableEditor(MecAbstractVariable* const Variable, MecAbstractEditor* MainEditor, QWidget * Parent, Qt::WindowFlags F) : MecVariableEditor(Variable, MainEditor, Parent, F)
{
comboBoxType->setEnabled(false);

labelDefaultValue = new QLabel(tr("Default value:"), this);
spinBoxDefaultValue = new QSpinBox(this);
	spinBoxDefaultValue->setRange(0, INT_MAX);
	connect(spinBoxDefaultValue, SIGNAL(valueChanged(int)), SLOT(setDefaultValue(int)));

layoutMain->addWidget(labelDefaultValue);
layoutMain->addWidget(spinBoxDefaultValue);

defaultValueVariableChanged(variable());
}

MecStandardUIntVariableEditor::~MecStandardUIntVariableEditor()
{
}

bool MecStandardUIntVariableEditor::isContentEditable() const
{
	return spinBoxDefaultValue->isEnabled();
}

void MecStandardUIntVariableEditor::defaultValueVariableChanged(MecAbstractVariable *Variable)
{
if (Variable != variable()) return;
spinBoxDefaultValue->setValue(Variable->defaultValue().toInt());
}

void MecStandardUIntVariableEditor::setContentEditable(bool Editable)
{
	spinBoxDefaultValue->setEnabled(Editable);
}

void MecStandardUIntVariableEditor::setDefaultValue(int Number)
{
variable()->setDefaultValue(QVariant(Number));
mainEditor()->addEditStep(tr("Change default value of “%1” to “%2”").arg(element()->elementName()).arg(QString::number(Number)));
}


