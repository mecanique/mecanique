/**
\mainpage	Documentation

\author	Quentin Vignaud


\section	main-description	Description

Ceci est la documentation du système du greffon « Standard string variable ».

\see	MecStandardStringVariablePlugin
\see	MecStandardStringVariableEditor
\see	MecStandardStringVariableCompiler


\section	main-mentions	Mentions légales

Cette documentation, ainsi que le logiciel auquel elle est liée, est concédée par son auteur, Quentin Vignaud, sous la Licence Publique de l'Union Européenne (soit l'«European Union Public Licence», EUPL) (<a href="#main-licence">Licence</a>).

Qt® est la propriété de Digia plc (http://qt.digia.com/ , http://www.digia.com/) , concédée sous licence «GNU Lesser General Public License» (GNU LGPL) version 2.1 (http://qt-project.org/doc/qt-5.1/lgpl.html).


Tous les autres éléments d'origine externe ou ne relevant pas de la propriété intellectuelle de l'auteur présents dans cette documentation sont soumis aux droits et/ou aux licences de concession de leurs auteurs respectifs.

\section	main-licence	Licence

Ce logiciel, ainsi que sa présente documentation, est concédée sous licence EUPL, version 1.1 uniquement.
Vous ne pouvez utiliser la présente œuvre que conformément à la Licence.
Vous pouvez obtenir une copie actualisée de la Licence à l’adresse suivante:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — disponible en 22 langues

Sauf obligation légale ou contractuelle écrite, le logiciel distribué sous la Licence est distribué « en l’état », SANS GARANTIES OU CONDITIONS QUELLES QU’ELLES SOIENT, expresses ou implicites.
Consultez la Licence pour les autorisations et les restrictions linguistiques spécifiques relevant de la Licence.


*/
