<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="fr_FR">
<context>
    <name>MecCodeEditor</name>
    <message>
        <location filename="../../../src/mecanique/MecCodeEditor.cpp" line="85"/>
        <source>Change code of “%1”</source>
        <translation>Changement du code de « %1 »</translation>
    </message>
</context>
<context>
    <name>MecCompiler</name>
    <message>
        <location filename="../../../src/mecanique/MecCompiler.cpp" line="109"/>
        <location filename="../../../src/mecanique/MecCompiler.cpp" line="114"/>
        <location filename="../../../src/mecanique/MecCompiler.cpp" line="119"/>
        <location filename="../../../src/mecanique/MecCompiler.cpp" line="142"/>
        <location filename="../../../src/mecanique/MecCompiler.cpp" line="162"/>
        <location filename="../../../src/mecanique/MecCompiler.cpp" line="210"/>
        <location filename="../../../src/mecanique/MecCompiler.cpp" line="220"/>
        <location filename="../../../src/mecanique/MecCompiler.cpp" line="231"/>
        <location filename="../../../src/mecanique/MecCompiler.cpp" line="261"/>
        <location filename="../../../src/mecanique/MecCompiler.cpp" line="291"/>
        <location filename="../../../src/mecanique/MecCompiler.cpp" line="314"/>
        <location filename="../../../src/mecanique/MecCompiler.cpp" line="321"/>
        <location filename="../../../src/mecanique/MecCompiler.cpp" line="374"/>
        <location filename="../../../src/mecanique/MecCompiler.cpp" line="452"/>
        <location filename="../../../src/mecanique/MecCompiler.cpp" line="457"/>
        <location filename="../../../src/mecanique/MecCompiler.cpp" line="466"/>
        <location filename="../../../src/mecanique/MecCompiler.cpp" line="471"/>
        <location filename="../../../src/mecanique/MecCompiler.cpp" line="496"/>
        <location filename="../../../src/mecanique/MecCompiler.cpp" line="512"/>
        <source>Error #</source>
        <translation>Erreur n°</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecCompiler.cpp" line="109"/>
        <location filename="../../../src/mecanique/MecCompiler.cpp" line="119"/>
        <source>: The build directory “</source>
        <translation> : le répertoire de compilation « </translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecCompiler.cpp" line="109"/>
        <source>” is not existing.
</source>
        <translation> » n&apos;est pas existant.
</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecCompiler.cpp" line="114"/>
        <source>: “</source>
        <translation> : « </translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecCompiler.cpp" line="114"/>
        <source>” is not a directory.
</source>
        <translation> » n&apos;est pas un dossier.
</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecCompiler.cpp" line="119"/>
        <source>” is not writable.
</source>
        <translation> » n&apos;est pas inscriptible.
</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecCompiler.cpp" line="132"/>
        <source>Build directory “</source>
        <translation>Répertoire de compilation « </translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecCompiler.cpp" line="132"/>
        <source>” checked.
</source>
        <translation> » préparé.
</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecCompiler.cpp" line="142"/>
        <source>: The project “</source>
        <translation> : le projet « </translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecCompiler.cpp" line="142"/>
        <source>” don&apos;t have appropriate plugin.
</source>
        <translation> » n&apos;a pas de greffon approprié.
</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecCompiler.cpp" line="162"/>
        <source>: The element “</source>
        <translation> : l&apos;élément « </translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecCompiler.cpp" line="162"/>
        <source>” (type “</source>
        <translation> » (de type « </translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecCompiler.cpp" line="162"/>
        <source>”) don&apos;t have appropriate plugin.</source>
        <translation> » n&apos;a pas de greffon approprié.</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecCompiler.cpp" line="163"/>
        <source>No particular plugin wanted.
</source>
        <translation>Aucun greffon particulier désiré.</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecCompiler.cpp" line="164"/>
        <source>Plugin “</source>
        <translation>Greffon « </translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecCompiler.cpp" line="164"/>
        <source>” wanted.
</source>
        <translation> » demandé.
</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecCompiler.cpp" line="210"/>
        <source>: The resource “</source>
        <translation> : la ressource « </translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecCompiler.cpp" line="210"/>
        <source>” cannot be read. This is a internal error.
</source>
        <translation>» ne peut être lue. Ceci est une erreur interne.
</translation>
    </message>
    <message>
        <source>” can&apos;t be read. This is a internal error.
</source>
        <translation type="obsolete"> » ne peut pas être lue. Ceci est une erreur interne.
</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecCompiler.cpp" line="220"/>
        <source>: The directory “</source>
        <translation> : le dossier « </translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecCompiler.cpp" line="220"/>
        <source>” cannot be created.
</source>
        <translation> » ne peut pas être créé.
</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecCompiler.cpp" line="231"/>
        <location filename="../../../src/mecanique/MecCompiler.cpp" line="261"/>
        <location filename="../../../src/mecanique/MecCompiler.cpp" line="291"/>
        <location filename="../../../src/mecanique/MecCompiler.cpp" line="321"/>
        <location filename="../../../src/mecanique/MecCompiler.cpp" line="374"/>
        <source>: The file “</source>
        <translation> : le fichier « </translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecCompiler.cpp" line="231"/>
        <location filename="../../../src/mecanique/MecCompiler.cpp" line="261"/>
        <location filename="../../../src/mecanique/MecCompiler.cpp" line="291"/>
        <location filename="../../../src/mecanique/MecCompiler.cpp" line="321"/>
        <location filename="../../../src/mecanique/MecCompiler.cpp" line="374"/>
        <source>” cannot be written.
</source>
        <translation> » ne peut être écrit.
</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecCompiler.cpp" line="314"/>
        <source>: The original file “main.cpp” cannot be read. This is an internal error.
</source>
        <translation> : Le fichier original « main.cpp » ne peut être lu. Ceci est une erreur interne.
</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecCompiler.cpp" line="328"/>
        <source>#####
#Generated automatically by Mécanique
#
</source>
        <translation>#####
#Généré automatiquement par Mécanique
#</translation>
    </message>
    <message>
        <source>” cannot be writted.
</source>
        <translation type="obsolete"> » ne peut pas être écrit.</translation>
    </message>
    <message>
        <source>#####
#Generate automatically by Mécanique
#
</source>
        <translation type="obsolete">#####
#Généré automatiquement par « Mécanique »
#
</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecCompiler.cpp" line="452"/>
        <source>: The sub-process “qmake” cannot be loaded.
</source>
        <translation> : le sous-processus « qmake » ne peut pas être lancé.
</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecCompiler.cpp" line="457"/>
        <source>: The sub-process “qmake” has crashed.
</source>
        <translation> : le sous-processus « qmake » a crashé.
</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecCompiler.cpp" line="466"/>
        <source>: The sub-process “make” cannot be loaded.
</source>
        <translation> : le sous-processus « make » ne peut pas être lancé.
</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecCompiler.cpp" line="471"/>
        <source>: The sub-process “make” has crashed.
</source>
        <translation> : le sous-processus « make » a crashé.
</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecCompiler.cpp" line="496"/>
        <source>: Error detected by “qmake” sub-process. This is a internal error.
</source>
        <translation> : erreur détectée par le sous-processus « qmake ». Ceci est une erreur interne.
</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecCompiler.cpp" line="507"/>
        <source>Compilation complete.
</source>
        <translation>Compilation terminée.
</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecCompiler.cpp" line="512"/>
        <source>: Error detected by “make” sub-process.
</source>
        <translation> : erreur détectée par le sous-processus « make ».
</translation>
    </message>
</context>
<context>
    <name>MecCompilerDialog</name>
    <message>
        <location filename="../../../src/mecanique/MecCompilerDialog.cpp" line="25"/>
        <source>Compilation</source>
        <translation>Compilation</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecCompilerDialog.cpp" line="32"/>
        <source>Build directory :</source>
        <translation>Répertoire de compilation :</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecCompilerDialog.cpp" line="39"/>
        <source>Choose…</source>
        <translation>Choisir…</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecCompilerDialog.cpp" line="43"/>
        <location filename="../../../src/mecanique/MecCompilerDialog.cpp" line="134"/>
        <source>Compile</source>
        <translation>Compiler</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecCompilerDialog.cpp" line="46"/>
        <source>Compilation log : </source>
        <translation>Rapport de compilation :</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecCompilerDialog.cpp" line="51"/>
        <source>Execute</source>
        <translation>Exécuter</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecCompilerDialog.cpp" line="78"/>
        <source>Informations about the licence...</source>
        <translation>Informations à propos de la licence…</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecCompilerDialog.cpp" line="83"/>
        <source>&lt;h3&gt;Informations about the licence&lt;/h3&gt;&lt;p&gt;The program which is going to be generated will integrate of the code under license EUPL (&quot;European Union Public Licence&quot;), which will apply as a consequence to the whole work which it constitutes as well as in the works which will be diverted from it and/or which will integrate it of any way, as a video, a show, or any other shape of work.&lt;/p&gt;&lt;p&gt;It means that you grant to whoever the following rights ones: &lt;ul&gt;&lt;li&gt;use the work in any circumstance and for all usage,&lt;/li&gt;&lt;li&gt;reproduce the work,&lt;/li&gt;&lt;li&gt;modify the original work, and make derivative works based upon the work,&lt;/li&gt;&lt;li&gt;communicate to the public, including the right to make available or display the work or copies thereof to the public and perform publicly, as the case may be, the work,&lt;/li&gt;&lt;li&gt;distribute the work or copies thereof,&lt;/li&gt;&lt;li&gt;lend and rent the work or copies thereof,&lt;/li&gt;&lt;li&gt;sub-license rights in the Work or copies thereof,&lt;/li&gt;&lt;/ul&gt;and that you guarantee the concession of these rights, by the terms of the EUPL.&lt;/p&gt;&lt;p&gt;If you do not arrange right necessities to grant so the work or its sub-works, either that you do not wish to make it, it is necessary to you to acquire another license.&lt;/p&gt;&lt;p&gt;&lt;a href=&quot;http://joinup.ec.europa.eu/software/page/eupl/licence-eupl&quot;&gt;EUPL in 22 languages&lt;/a&gt;&lt;br /&gt;&lt;a href=&quot;https://joinup.ec.europa.eu/system/files/EN/EUPL v.1.1 - Licence.pdf&quot;&gt;Text of the licence in English&lt;/a&gt;&lt;/p&gt;</source>
        <translation>&lt;h3&gt;Informations relatives à la licence&lt;/h3&gt;&lt;p&gt;Le programme qui va être généré intègrera du code sous licence EUPL (&lt;i&gt;« European Union Public Licence »&lt;/i&gt;), qui s&apos;appliquera en conséquence à l&apos;ensemble de l&apos;œuvre informatique qu&apos;il constitue ainsi qu&apos;aux œuvres qui en seront dérivées et/ou qui l&apos;intègreront d&apos;une quelconque manière, comme une vidéo, un spectacle, ou toute autre forme d&apos;œuvre.&lt;/p&gt;&lt;p&gt;Cela signifie que vous concédez à quiconque les droits suivants sur celles-ci : &lt;ul&gt;&lt;li&gt;utiliser l’œuvre en toutes circonstances et pour tout usage,&lt;/li&gt;&lt;li&gt;reproduire l&apos;œuvre,&lt;/li&gt;&lt;li&gt;modifier l’œuvre originale, et de faire des œuvres dérivées sur la base de l’œuvre,&lt;/li&gt;&lt;li&gt;communiquer, présenter ou représenter l&apos;œuvre ou copie de celle-ci au public, en ce compris le droit de mettre celles-ci à la disposition du public,&lt;/li&gt;&lt;li&gt;distribuer l’œuvre ou des copies de celles-ci,&lt;/li&gt;&lt;li&gt;prêter et louer l’œuvre ou des copies de celles-ci,&lt;/li&gt;&lt;li&gt;sous-licencier les droits concédés ici sur l’œuvre ou sur des copies de celles-ci,&lt;/li&gt;&lt;/ul&gt;et que vous vous portez garant de la concession de ces droits, d&apos;après les termes de la licence EUPL.&lt;/p&gt;&lt;p&gt;Si vous ne disposez pas des droits nécessaires pour concéder ainsi l&apos;œuvre ou ses dérivés, ou bien que vous ne souhaitez pas le faire, il vous faut acquérir une autre licence.&lt;/p&gt;&lt;p&gt;&lt;a href=\&quot;http://joinup.ec.europa.eu/software/page/eupl/licence-eupl\&quot;&gt;Page d&apos;accès au texte de la licence (22 langues)&lt;/a&gt;&lt;br /&gt;&lt;a href=\&quot;http://joinup.ec.europa.eu/system/files/FR/EUPL v.1.1 - Licence.pdf\&quot;&gt;Texte de la licence en français&lt;/a&gt;&lt;/p&gt;</translation>
    </message>
    <message>
        <source>Informations about the licence…</source>
        <translation type="obsolete">Informations à propos de la licence…</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecCompilerDialog.cpp" line="80"/>
        <source>Accept</source>
        <translation>Accepter</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecCompilerDialog.cpp" line="81"/>
        <source>Reject</source>
        <translation>Refuser</translation>
    </message>
    <message>
        <source>&lt;h3&gt;Informations relatives à la licence&lt;/h3&gt;&lt;p&gt;Le programme qui va être généré intègrera du code sous licence EUPL (&lt;i&gt;« European Union Public Licence »&lt;/i&gt;), qui s&apos;appliquera en conséquence à l&apos;ensemble de l&apos;œuvre informatique qu&apos;il constitue ainsi qu&apos;aux œuvres qui en seront dérivées et/ou qui l&apos;intègreront d&apos;une quelconque manière, comme une vidéo, un spectacle, ou toute autre forme d&apos;œuvre.&lt;/p&gt;&lt;p&gt;Cela signifie que vous concédez à quiconque les droits suivants sur celles-ci : &lt;ul&gt;&lt;li&gt;utiliser l’œuvre en toute circonstance et pour tout usage,&lt;/li&gt;&lt;li&gt;reproduire l&apos;œuvre,&lt;/li&gt;&lt;li&gt;modifier l’œuvre originale, et de faire des œuvres dérivées sur la base de l’œuvre,&lt;/li&gt;&lt;li&gt;communiquer, présenter ou représenter l&apos;œuvre ou copie de celle-ci au public, en ce compris le droit de mettre celles-ci à la disposition du public,&lt;/li&gt;&lt;li&gt;distribuer l’œuvre ou des copies de celles-ci,&lt;/li&gt;&lt;li&gt;prêter et louer l’œuvre ou des copies de celles-ci,&lt;/li&gt;&lt;li&gt;sous-licencier les droits concédés ici sur l’œuvre ou sur des copies de celles-ci,&lt;/li&gt;&lt;/ul&gt;et que vous vous portez garant de la concession de ces droits, d&apos;après les termes de la licence EUPL.&lt;/p&gt;&lt;p&gt;Si vous ne disposez pas des droits nécéssaires pour concéder ainsi l&apos;œuvre ou ses dérivés, ou bien que vous ne souhaitez pas le faire, il vous faut acquérir une autre licence.&lt;/p&gt;&lt;p&gt;&lt;a href=&quot;http://joinup.ec.europa.eu/software/page/eupl/licence-eupl&quot;&gt;Page d&apos;accès au texte de la licence (22 langues)&lt;/a&gt;&lt;br /&gt;&lt;a href=&quot;http://joinup.ec.europa.eu/system/files/FR/EUPL v.1.1 - Licence.pdf&quot;&gt;Texte de la licence en français&lt;/a&gt;&lt;/p&gt;&lt;small&gt;[FR-français]&lt;/small&gt;</source>
        <translation type="obsolete">&lt;h3&gt;Informations relatives à la licence&lt;/h3&gt;&lt;p&gt;Le programme qui va être généré intègrera du code sous licence EUPL (&lt;i&gt;« European Union Public Licence »&lt;/i&gt;), qui s&apos;appliquera en conséquence à l&apos;ensemble de l&apos;œuvre informatique qu&apos;il constitue ainsi qu&apos;aux œuvres qui en seront dérivées et/ou qui l&apos;intègreront d&apos;une quelconque manière, comme une vidéo, un spectacle, ou toute autre forme d&apos;œuvre.&lt;/p&gt;&lt;p&gt;Cela signifie que vous concédez à quiconque les droits suivants sur celles-ci : &lt;ul&gt;&lt;li&gt;utiliser l’œuvre en toute circonstance et pour tout usage,&lt;/li&gt;&lt;li&gt;reproduire l&apos;œuvre,&lt;/li&gt;&lt;li&gt;modifier l’œuvre originale, et de faire des œuvres dérivées sur la base de l’œuvre,&lt;/li&gt;&lt;li&gt;communiquer, présenter ou représenter l&apos;œuvre ou copie de celle-ci au public, en ce compris le droit de mettre celles-ci à la disposition du public,&lt;/li&gt;&lt;li&gt;distribuer l’œuvre ou des copies de celles-ci,&lt;/li&gt;&lt;li&gt;prêter et louer l’œuvre ou des copies de celles-ci,&lt;/li&gt;&lt;li&gt;sous-licencier les droits concédés ici sur l’œuvre ou sur des copies de celles-ci,&lt;/li&gt;&lt;/ul&gt;et que vous vous portez garant de la concession de ces droits, d&apos;après les termes de la licence EUPL.&lt;/p&gt;&lt;p&gt;Si vous ne disposez pas des droits nécéssaires pour concéder ainsi l&apos;œuvre ou ses dérivés, ou bien que vous ne souhaitez pas le faire, il vous faut acquérir une autre licence.&lt;/p&gt;&lt;p&gt;&lt;a href=&quot;http://joinup.ec.europa.eu/software/page/eupl/licence-eupl&quot;&gt;Page d&apos;accès au texte de la licence (22 langues)&lt;/a&gt;&lt;br /&gt;&lt;a href=&quot;http://joinup.ec.europa.eu/system/files/FR/EUPL v.1.1 - Licence.pdf&quot;&gt;Texte de la licence en français&lt;/a&gt;&lt;/p&gt;&lt;br /&gt;</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecCompilerDialog.cpp" line="102"/>
        <source>Choose build directory for compilation</source>
        <translation>Choisir un répertoire de compilation</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecCompilerDialog.cpp" line="119"/>
        <source>Stop</source>
        <translation>Arrêter</translation>
    </message>
</context>
<context>
    <name>MecEditor</name>
    <message>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="24"/>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="145"/>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="583"/>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="611"/>
        <source>Mécanique</source>
        <translation>Mécanique</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="27"/>
        <source>&lt;center&gt;No elements to edit.&lt;/center&gt;</source>
        <translation>&lt;center&gt;Pas d&apos;éléments à éditer.&lt;/center&gt;</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="33"/>
        <source>&amp;File</source>
        <translation>&amp;Fichier</translation>
    </message>
    <message>
        <source>&amp;Create…</source>
        <translation type="obsolete">&amp;Créer…</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="34"/>
        <source>&amp;Create...</source>
        <translation>&amp;Créer…</translation>
    </message>
    <message>
        <source>&amp;Open…</source>
        <translation type="obsolete">&amp;Ouvrir…</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="36"/>
        <source>&amp;Save</source>
        <translation>&amp;Enregistrer</translation>
    </message>
    <message>
        <source>S&amp;ave as…</source>
        <translation type="obsolete">Enregistrer &amp;sous…</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="42"/>
        <source>&amp;Close</source>
        <translation>&amp;Fermer</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="43"/>
        <source>&amp;Quit</source>
        <translation>&amp;Quitter</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="45"/>
        <source>&amp;Edit</source>
        <translation>&amp;Éditer</translation>
    </message>
    <message>
        <source>Add &amp;object…</source>
        <translation type="obsolete">Ajouter un &amp;objet…</translation>
    </message>
    <message>
        <source>Add &amp;function…</source>
        <translation type="obsolete">Ajouter une &amp;fonction…</translation>
    </message>
    <message>
        <source>Add &amp;signal…</source>
        <translation type="obsolete">Ajouter un &amp;signal…</translation>
    </message>
    <message>
        <source>Add &amp;variable…</source>
        <translation type="obsolete">Ajouter une &amp;variable…</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="59"/>
        <source>Remove element</source>
        <translation>Supprimer l&apos;élément</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="68"/>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="94"/>
        <source>&amp;Tools</source>
        <translation>&amp;Outils</translation>
    </message>
    <message>
        <source>&amp;Compile…</source>
        <translation type="obsolete">&amp;Compiler…</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="75"/>
        <source>&amp;Plugins manager</source>
        <translation>&amp;Gestionnaire de greffons</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="77"/>
        <source>&amp;Help</source>
        <translation>&amp;Aide</translation>
    </message>
    <message>
        <source>Summary…</source>
        <translation type="obsolete">Sommaire…</translation>
    </message>
    <message>
        <source>About &amp;Mécanique…</source>
        <translation type="obsolete">À propos de &amp;Mécanique…</translation>
    </message>
    <message>
        <source>About &amp;Qt…</source>
        <translation type="obsolete">À propos de &amp;Qt…</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="35"/>
        <source>&amp;Open...</source>
        <translation>&amp;Ouvrir…</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="38"/>
        <source>S&amp;ave as...</source>
        <translation>Enregistrer &amp;sous…</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="41"/>
        <source>&amp;New window</source>
        <translation>&amp;Nouvelle fenêtre</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="51"/>
        <source>Add &amp;object...</source>
        <translation>Ajouter un &amp;objet…</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="53"/>
        <source>Add &amp;function...</source>
        <translation>Ajouter une &amp;fonction…</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="55"/>
        <source>Add &amp;signal...</source>
        <translation>Ajouter un &amp;signal…</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="57"/>
        <source>Add &amp;variable...</source>
        <translation>Ajouter une &amp;variable…</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="62"/>
        <source>Show reference</source>
        <translation>Voir la référence</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="65"/>
        <source>&amp;Import...</source>
        <translation>&amp;Importer…</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="69"/>
        <source>Show &amp;graph...</source>
        <translation>Voir le &amp;graphique…</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="72"/>
        <source>&amp;Compile...</source>
        <translation>&amp;Compiler…</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="78"/>
        <source>Summary...</source>
        <translation>Sommaire…</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="80"/>
        <source>Report a bug...</source>
        <translation>Rapporter un bug…</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="82"/>
        <source>About &amp;Mécanique...</source>
        <translation>À propos de &amp;Mécanique… </translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="83"/>
        <source>About &amp;Qt...</source>
        <translation>À propos de &amp;Qt…</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="116"/>
        <source>Element&apos;s tree</source>
        <translation>Arborescence</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="485"/>
        <source>Plugin error</source>
        <translation>Erreur de greffon</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="485"/>
        <source>&lt;h3&gt;Plugin error&lt;/h3&gt;&lt;p&gt;The element “</source>
        <translation>&lt;h3&gt;Erreur de greffon&lt;/h3&gt;&lt;p&gt;L&apos;élément « </translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="485"/>
        <source>” (type “</source>
        <translation> » (de type « </translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="485"/>
        <source>”) don&apos;t have appropriate plugin.&lt;/p&gt;</source>
        <translation> ») n&apos;a pas de greffon approprié.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="764"/>
        <source>&lt;h3&gt;About the editor&lt;/h3&gt;&lt;p&gt;Version %1&lt;/p&gt;&lt;p&gt;Copyrigth © 2013 – 2015 Quentin VIGNAUD&lt;/p&gt;&lt;p&gt;Licensed under the EUPL, version 1.1 only.&lt;br /&gt;You may not use this work except in compliance with the Licence. You may obtain a copy of the Licence at: &lt;a href=&quot;http://joinup.ec.europa.eu/software/page/eupl/licence-eupl&quot;&gt;http://joinup.ec.europa.eu/software/page/eupl/licence-eupl&lt;/a&gt;&lt;/p&gt;&lt;p&gt;Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an “AS IS” basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.&lt;br /&gt;See the Licence for the specific language governing permissions and limitations under the Licence.&lt;/p&gt;</source>
        <translation>&lt;h3&gt;À propos de l&apos;éditeur&lt;/h3&gt;&lt;p&gt;Version %1&lt;/p&gt;&lt;p&gt;Copyrigth © 2013 – 2015 Quentin VIGNAUD&lt;/p&gt;&lt;p&gt;Programme concédé sous licence EUPL, version 1.1 uniquement.&lt;br /&gt;Vous ne pouvez utiliser la présente œuvre que conformément à la Licence. Vous pouvez obtenir une copie de la Licence à l’adresse suivante : &lt;a href=&quot;http://joinup.ec.europa.eu/software/page/eupl/licence-eupl&quot;&gt;http://joinup.ec.europa.eu/software/page/eupl/licence-eupl&lt;/a&gt;&lt;/p&gt;&lt;p&gt;Sauf obligation légale ou contractuelle écrite, le logiciel distribué sous la Licence est distribué « en l’état », SANS GARANTIES OU CONDITIONS QUELLES QU’ELLES SOIENT, expresses ou implicites.&lt;br /&gt;Consultez la Licence pour les autorisations et les restrictions spécifiques relevant de la Licence.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;h3&gt;About the editor&lt;/h3&gt;&lt;p&gt;Version 1.000&lt;/p&gt;&lt;p&gt;Copyrigth © 2013 – 2015 Quentin VIGNAUD&lt;/p&gt;&lt;p&gt;Licensed under the EUPL, version 1.1 only.&lt;br /&gt;You may not use this work except in compliance with the Licence. You may obtain a copy of the Licence at: &lt;a href=&quot;http://joinup.ec.europa.eu/software/page/eupl/licence-eupl&quot;&gt;http://joinup.ec.europa.eu/software/page/eupl/licence-eupl&lt;/a&gt;&lt;/p&gt;&lt;p&gt;Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an “AS IS” basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.&lt;br /&gt;See the Licence for the specific language governing permissions and limitations under the Licence.&lt;/p&gt;</source>
        <translation type="obsolete">&lt;h3&gt;À propos de l&apos;éditeur&lt;/h3&gt;&lt;p&gt;Version 1.000&lt;/p&gt;&lt;p&gt;Copyrigth © 2013 –2015 Quentin VIGNAUD&lt;/p&gt;&lt;p&gt;Programme concédé sous licence EUPL, version 1.1 uniquement.&lt;br /&gt;Vous ne pouvez utiliser la présente œuvre que conformément à la Licence. Vous pouvez obtenir une copie de la Licence à l’adresse suivante : &lt;a href=\&quot;http://joinup.ec.europa.eu/software/page/eupl/licence-eupl\&quot;&gt;http://joinup.ec.europa.eu/software/page/eupl/licence-eupl&lt;/a&gt;&lt;/p&gt;&lt;p&gt;Sauf obligation légale ou contractuelle écrite, le logiciel distribué sous la Licence est distribué « en l’état », SANS GARANTIES OU CONDITIONS QUELLES QU’ELLES SOIENT, expresses ou implicites.&lt;br /&gt;Consultez la Licence pour les autorisations et les restrictions spécifiques relevant de la Licence.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="160"/>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="166"/>
        <source>Loading error</source>
        <translation>Erreur de chargement</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="46"/>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="633"/>
        <source>&amp;Undo</source>
        <translation>Ann&amp;uler</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="48"/>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="643"/>
        <source>&amp;Redo</source>
        <translation>&amp;Rétablir</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="160"/>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="166"/>
        <source>&lt;h3&gt;Loading error&lt;/h3&gt;&lt;p&gt;The file “</source>
        <translation>&lt;h3&gt;Erreur de chargement&lt;/h3&gt;&lt;p&gt;Le fichier « </translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="160"/>
        <source>” contains no mécanique data.&lt;/p&gt;</source>
        <translation> » ne contient pas de données « Mécanique ».&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="166"/>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="193"/>
        <source>” could not be opened.&lt;/p&gt;</source>
        <translation> » ne peut pas être ouvert.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="182"/>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="193"/>
        <source>Writing error</source>
        <translation>Erreur d&apos;écriture</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="182"/>
        <source>&lt;h3&gt;Writing error&lt;/h3&gt;&lt;p&gt;</source>
        <translation>&lt;h3&gt;Erreur d&apos;écriture&lt;/h3&gt;&lt;p&gt;</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="182"/>
        <source>&lt;/p&gt;</source>
        <translation>&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="193"/>
        <source>&lt;h3&gt;Writing error&lt;/h3&gt;&lt;p&gt;The file “</source>
        <translation>&lt;h3&gt;Erreur d&apos;écriture&lt;/h3&gt;&lt;p&gt;Le fichier « </translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="555"/>
        <source>Project</source>
        <translation>Projet</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="558"/>
        <source>Object</source>
        <translation>Objet</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="561"/>
        <source>Function</source>
        <translation>Fonction</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="564"/>
        <source>Signal</source>
        <translation>Signal</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="567"/>
        <source>Variable</source>
        <translation>Variable</translation>
    </message>
    <message>
        <source>Create a project</source>
        <translation type="obsolete">Créer un projet</translation>
    </message>
    <message>
        <source>Choose the base to the new project:</source>
        <translation type="obsolete">Choisissez la base d&apos;un nouveau projet :</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="590"/>
        <source>Open...</source>
        <translation>Ouvrir…</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="604"/>
        <source>Save as...</source>
        <translation>Enregistrer sous…</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="604"/>
        <source>Mécanique file (*.mec)</source>
        <translation>Fichier Mécanique (*.mec)</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="628"/>
        <source>&amp;Undo %1</source>
        <translation>Ann&amp;uler %1</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="638"/>
        <source>&amp;Redo %1</source>
        <translation>&amp;Rétablir %1</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="710"/>
        <source>Remove element “%1”</source>
        <translation>Supprimer l&apos;élément %1</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="722"/>
        <source>Import...</source>
        <translation>Importer…</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="735"/>
        <source>Import impossible</source>
        <translation>Import impossible</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="735"/>
        <source>&lt;h3&gt;Import impossible&lt;/h3&gt;&lt;p&gt;The import is impossible: the file specified contains no importable project element.&lt;/p&gt;</source>
        <translation>&lt;h3&gt;Import impossible&lt;/h3&gt;&lt;p&gt;L&apos;import est impossible : le fichier spécifié ne contient pas d&apos;élément de projet importable.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="764"/>
        <source>About the editor...</source>
        <translation>À propos de l&apos;éditeur…</translation>
    </message>
    <message>
        <source>&lt;h3&gt;About the editor&lt;/h3&gt;&lt;p&gt;Version 0.2&lt;/p&gt;&lt;p&gt;Copyrigth © 2013 Quentin VIGNAUD&lt;/p&gt;&lt;p&gt;Licensed under the EUPL, version 1.1 only.&lt;br /&gt;You may not use this work except in compliance with the Licence. You may obtain a copy of the Licence at: &lt;a href=&quot;http://joinup.ec.europa.eu/software/page/eupl/licence-eupl&quot;&gt;http://joinup.ec.europa.eu/software/page/eupl/licence-eupl&lt;/a&gt;&lt;/p&gt;&lt;p&gt;Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an “AS IS” basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.&lt;br /&gt;See the Licence for the specific language governing permissions and limitations under the Licence.&lt;/p&gt;</source>
        <translation type="obsolete">&lt;h3&gt;À propos de l&apos;éditeur&lt;/h3&gt;&lt;p&gt;Version 0.2&lt;/p&gt;&lt;p&gt;Copyrigth © 2013 Quentin VIGNAUD&lt;/p&gt;&lt;p&gt;L&apos;éditeur de projets « Mécanique » est une interface de gestion, d&apos;édition et de compilation des différents éléments des projets, reposant sur l&apos;utilisation de greffons adaptés aux informations et aux tâches demandées.&lt;/p&gt;&lt;p&gt;Programme concédé sous licence EUPL, version 1.1 uniquement.&lt;br /&gt;Vous ne pouvez utiliser la présente œuvre que conformément à la Licence. Vous pouvez obtenir une copie de la Licence à l’adresse suivante : &lt;a href=\&quot;http://joinup.ec.europa.eu/software/page/eupl/licence-eupl\&quot;&gt;http://joinup.ec.europa.eu/software/page/eupl/licence-eupl&lt;/a&gt;&lt;/p&gt;&lt;p&gt;Sauf obligation légale ou contractuelle écrite, le logiciel distribué sous la Licence est distribué « en l’état », SANS GARANTIES OU CONDITIONS QUELLES QU’ELLES SOIENT, expresses ou implicites.&lt;br /&gt;Consultez la Licence pour les autorisations et les restrictions spécifiques relevant de la Licence.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="787"/>
        <source>&lt;h3&gt;Unsaved changes: %1&lt;/h3&gt;&lt;p&gt;Do you want to save changes occurred with %1?&lt;/p&gt;</source>
        <translation>&lt;h3&gt;Changements non sauvegardés : %1&lt;/h3&gt;&lt;p&gt;Voulez-vous enregistrer les changement effectués sur %1 ?&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="788"/>
        <source>&lt;h3&gt;Unsaved changes: %1&lt;/h3&gt;&lt;p&gt;Do you want to save changes occurred with %1 in “%2”?&lt;/p&gt;</source>
        <translation>&lt;h3&gt;Changements non sauvegardés : %1&lt;/h3&gt;&lt;p&gt;Voulez-vous enregistrer les changement effectués avec %1 sur %2 ?&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="790"/>
        <source>Unsaved changes...</source>
        <translation>Changements non sauvegardés…</translation>
    </message>
    <message>
        <source>&lt;h3&gt;About the editor&lt;/h3&gt;&lt;p&gt;First version&lt;/p&gt;&lt;p&gt;Copyrigth © 2013 Quentin VIGNAUD&lt;/p&gt;&lt;p&gt;Licensed under the EUPL, version 1.1 only.&lt;br /&gt;You may not use this work except in compliance with the Licence. You may obtain a copy of the Licence at: &lt;a href=&quot;http://joinup.ec.europa.eu/software/page/eupl/licence-eupl&quot;&gt;http://joinup.ec.europa.eu/software/page/eupl/licence-eupl&lt;/a&gt;&lt;/p&gt;&lt;p&gt;Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an “AS IS” basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.&lt;br /&gt;See the Licence for the specific language governing permissions and limitations under the Licence.&lt;/p&gt;</source>
        <translation type="obsolete">&lt;h3&gt;À propos de l&apos;éditeur&lt;/h3&gt;&lt;p&gt;Première version&lt;/p&gt;&lt;p&gt;Copyrigth © 2013 Quentin VIGNAUD&lt;/p&gt;&lt;p&gt;L&apos;éditeur de projets « Mécanique » est une interface de gestion, d&apos;édition et de compilation des différents éléments des projets, reposant sur l&apos;utilisation de greffons adaptés aux informations et aux tâches demandées.&lt;/p&gt;&lt;p&gt;Programme concédé sous licence EUPL, version 1.1 uniquement.&lt;br /&gt;Vous ne pouvez utiliser la présente œuvre que conformément à la Licence. Vous pouvez obtenir une copie de la Licence à l’adresse suivante : &lt;a href=\&quot;http://joinup.ec.europa.eu/software/page/eupl/licence-eupl\&quot;&gt;http://joinup.ec.europa.eu/software/page/eupl/licence-eupl&lt;/a&gt;&lt;/p&gt;&lt;p&gt;Sauf obligation légale ou contractuelle écrite, le logiciel distribué sous la Licence est distribué « en l’état », SANS GARANTIES OU CONDITIONS QUELLES QU’ELLES SOIENT, expresses ou implicites.&lt;br /&gt;Consultez la Licence pour les autorisations et les restrictions spécifiques relevant de la Licence.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>Open…</source>
        <translation type="obsolete">Ouvrir…</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="590"/>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="722"/>
        <source>Mécanique file (*.mec);;All files (*.*)</source>
        <translation>Fichier Mécanique (*.mec);;Tous les fichiers (*.*)</translation>
    </message>
    <message>
        <source>Save as…</source>
        <translation type="obsolete">Enregistrer sous…</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="748"/>
        <source>Compilation impossible</source>
        <translation>Compilation impossible</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="748"/>
        <source>&lt;h3&gt;Compilation impossible&lt;/h3&gt;&lt;p&gt;The root element is not a project, the compilation cannot be made.&lt;/p&gt;</source>
        <translation>&lt;h3&gt;Compilation impossible&lt;/h3&gt;&lt;p&gt;L&apos;élément racine n&apos;est pas un project, la compilation ne peut pas être effectuée.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>À propos de l&apos;éditeur…</source>
        <translation type="obsolete">À propos de l&apos;éditeur…</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="813"/>
        <source>&lt;h3&gt;Plugins errors&lt;/h3&gt;&lt;p&gt;The plugins corresponding to the following datas are required:&lt;br /&gt;&lt;table&gt;&lt;tr&gt;&lt;th&gt;Error&lt;/th&gt;&lt;th&gt;Name&lt;/th&gt;&lt;th&gt;Version&lt;/th&gt;&lt;th&gt;Role&lt;/th&gt;&lt;th&gt;Type&lt;/th&gt;&lt;/tr&gt;</source>
        <translation>&lt;h3&gt;Erreurs de greffons&lt;/h3&gt;&lt;p&gt;Les greffons correspondants aux données suivantes sont requis :&lt;br /&gt;&lt;table&gt;&lt;tr&gt;&lt;th&gt;Erreur&lt;/th&gt;&lt;th&gt;Nom&lt;/th&gt;&lt;th&gt;Version&lt;/th&gt;&lt;th&gt;Rôle&lt;/th&gt;&lt;th&gt;Type&lt;/th&gt;&lt;/tr&gt;</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="823"/>
        <source>Undefined error</source>
        <translation>Erreur indéfinie</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="826"/>
        <source>Plugin unavailable</source>
        <translation>Greffon indisponible</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="829"/>
        <source>Plugin uncompatible</source>
        <translation>Greffon incompatible</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="838"/>
        <source>No special plugin</source>
        <translation>Pas de greffon particulier</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="845"/>
        <source>No special version</source>
        <translation>Pas de version particulière</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="857"/>
        <source>&lt;/table&gt;&lt;br /&gt;&lt;/p&gt;&lt;p&gt;See the plugins manager for more information.&lt;/p&gt;</source>
        <translation>&lt;/table&gt;&lt;br /&gt;&lt;/p&gt;&lt;p&gt;Voir le gestionnaire de greffons pour plus d&apos;informations.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;h3&gt;À propos de l&apos;éditeur&lt;/h3&gt;&lt;p&gt;Version Juillet 2013 (1.0)&lt;/p&gt;&lt;p&gt;Copyrigth © 2013 Quentin VIGNAUD&lt;/p&gt;&lt;p&gt;L&apos;éditeur de projets « Mécanique » est une interface de gestion, d&apos;édition et de compilation des différents éléments des projets, reposant sur l&apos;utilisation de greffons adaptés aux informations et aux tâches demandées.&lt;/p&gt;&lt;p&gt;Programme concédé sous licence EUPL, version 1.1 ou – dès leur approbation par la Commission européenne – versions ultérieures de l’EUPL (la « Licence »).&lt;br /&gt;Vous ne pouvez utiliser la présente œuvre que conformément à la Licence. Vous pouvez obtenir une copie de la Licence à l’adresse suivante : &lt;a href=&quot;http://joinup.ec.europa.eu/software/page/eupl/licence-eupl&quot;&gt;http://joinup.ec.europa.eu/software/page/eupl/licence-eupl&lt;/a&gt;&lt;/p&gt;&lt;p&gt;Sauf obligation légale ou contractuelle écrite, le logiciel distribué sous la Licence est distribué « en l’état », SANS GARANTIES OU CONDITIONS QUELLES QU’ELLES SOIENT, expresses ou implicites.&lt;br /&gt;Consultez la Licence pour les autorisations et les restrictions spécifiques relevant de la Licence.&lt;/p&gt;&lt;small&gt;[FR] français&lt;/small&gt;</source>
        <translation type="obsolete">&lt;h3&gt;À propos de l&apos;éditeur&lt;/h3&gt;&lt;p&gt;Version Juillet 2013 (1.0)&lt;/p&gt;&lt;p&gt;Copyrigth © 2013 Quentin VIGNAUD&lt;/p&gt;&lt;p&gt;L&apos;éditeur de projets « Mécanique » est une interface de gestion, d&apos;édition et de compilation des différents éléments des projets, reposant sur l&apos;utilisation de greffons adaptés aux informations et aux tâches demandées.&lt;/p&gt;&lt;p&gt;Programme concédé sous licence EUPL, version 1.1 ou – dès leur approbation par la Commission européenne – versions ultérieures de l’EUPL (la « Licence »).&lt;br /&gt;Vous ne pouvez utiliser la présente œuvre que conformément à la Licence. Vous pouvez obtenir une copie de la Licence à l’adresse suivante : &lt;a href=&quot;http://joinup.ec.europa.eu/software/page/eupl/licence-eupl&quot;&gt;http://joinup.ec.europa.eu/software/page/eupl/licence-eupl&lt;/a&gt;&lt;/p&gt;&lt;p&gt;Sauf obligation légale ou contractuelle écrite, le logiciel distribué sous la Licence est distribué « en l’état », SANS GARANTIES OU CONDITIONS QUELLES QU’ELLES SOIENT, expresses ou implicites.&lt;br /&gt;Consultez la Licence pour les autorisations et les restrictions spécifiques relevant de la Licence.&lt;/p&gt;&lt;br /&gt;</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="860"/>
        <source>Plugins errors…</source>
        <translation>Erreurs de greffons…</translation>
    </message>
</context>
<context>
    <name>MecElementTree</name>
    <message>
        <location filename="../../../src/mecanique/MecElementTree.cpp" line="24"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecElementTree.cpp" line="24"/>
        <source>Type</source>
        <translation>Type</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecElementTree.cpp" line="166"/>
        <source>Move “%1”</source>
        <translation>Déplacer « %1 »</translation>
    </message>
</context>
<context>
    <name>MecGraphDialog</name>
    <message>
        <location filename="../../../src/mecanique/MecGraphDialog.cpp" line="24"/>
        <source>Mécanique graph</source>
        <translation>Graphique Mécanique</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecGraphDialog.cpp" line="36"/>
        <source>No graph.</source>
        <translation>Aucun graphe.</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecGraphDialog.cpp" line="44"/>
        <source>&amp;File</source>
        <translation>&amp;Fichier</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecGraphDialog.cpp" line="45"/>
        <source>S&amp;ave as...</source>
        <translation>Enregistrer &amp;sous…</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecGraphDialog.cpp" line="48"/>
        <source>Refresh graph</source>
        <translation>Actualiser le graphe</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecGraphDialog.cpp" line="50"/>
        <source>&amp;Close</source>
        <translation>&amp;Fermer</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecGraphDialog.cpp" line="52"/>
        <source>&amp;Display</source>
        <translation>&amp;Affichage</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecGraphDialog.cpp" line="53"/>
        <source>Zoom &amp;in</source>
        <translation>Zoom &amp;+</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecGraphDialog.cpp" line="55"/>
        <source>Zoom &amp;out</source>
        <translation>Zoom &amp;-</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecGraphDialog.cpp" line="57"/>
        <source>&amp;Normal size</source>
        <translation>Taille &amp;normale</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecGraphDialog.cpp" line="60"/>
        <source>&amp;Fit to window</source>
        <translation>Ajuster à la &amp;fenêtre</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecGraphDialog.cpp" line="64"/>
        <source>&amp;Graph</source>
        <translation>&amp;Graphe</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecGraphDialog.cpp" line="65"/>
        <source>&amp;Mode</source>
        <translation>&amp;Mode</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecGraphDialog.cpp" line="66"/>
        <source>&amp;Hierarchy</source>
        <translation>&amp;Hiérarchie</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecGraphDialog.cpp" line="68"/>
        <source>&amp;Presentation</source>
        <translation>&amp;Présentation</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecGraphDialog.cpp" line="71"/>
        <source>&amp;Show connections</source>
        <translation>&amp;Voir les connexions</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecGraphDialog.cpp" line="73"/>
        <source>&amp;Color connections</source>
        <translation>&amp;Colorer le connexions</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecGraphDialog.cpp" line="75"/>
        <source>Hide &amp;variables</source>
        <translation>Masquer les &amp;variables</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecGraphDialog.cpp" line="77"/>
        <source>No block &amp;structure</source>
        <translation>Pas de blocs de &amp;structure</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecGraphDialog.cpp" line="80"/>
        <source>&amp;Tools</source>
        <translation>&amp;Outils</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecGraphDialog.cpp" line="117"/>
        <source>Save as...</source>
        <translation>Enregistrer sous…</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecGraphDialog.cpp" line="117"/>
        <source>Portable Network Graphics (*.png)</source>
        <translation>Portable Network Graphics (*.png)</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecGraphDialog.cpp" line="123"/>
        <source>Saving error</source>
        <translation>Erreur d&apos;enregistrement</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecGraphDialog.cpp" line="123"/>
        <source>&lt;h3&gt;Saving error&lt;/h3&gt;&lt;p&gt;The graph could not be saved.&lt;/p&gt;</source>
        <translation>&lt;h3&gt;Erreur d&apos;enregistrement&lt;/h3&gt;&lt;p&gt;Le graphique n&apos;a pu être sauvegardé.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecGraphDialog.cpp" line="182"/>
        <source>An error has occured while the creation of the graph.</source>
        <translation>Une erreur est survenue durant la création du graphe.</translation>
    </message>
</context>
<context>
    <name>MecPluginsManager</name>
    <message>
        <location filename="../../../src/mecanique/MecPluginsManager.cpp" line="26"/>
        <source>Plugins manager</source>
        <translation>Gestionnaire de greffons</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecPluginsManager.cpp" line="30"/>
        <source>Title</source>
        <translation>Titre</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecPluginsManager.cpp" line="30"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecPluginsManager.cpp" line="30"/>
        <source>Version</source>
        <translation>Version</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecPluginsManager.cpp" line="30"/>
        <source>Role supported</source>
        <translation>Rôle supporté</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecPluginsManager.cpp" line="30"/>
        <source>Type supported</source>
        <translation>Type supporté</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecPluginsManager.cpp" line="67"/>
        <source>Project</source>
        <translation>Projet</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecPluginsManager.cpp" line="71"/>
        <source>Object</source>
        <translation>Objet</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecPluginsManager.cpp" line="75"/>
        <source>Function</source>
        <translation>Fonction</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecPluginsManager.cpp" line="79"/>
        <source>Signal</source>
        <translation>Signal</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecPluginsManager.cpp" line="83"/>
        <source>Variable</source>
        <translation>Variable</translation>
    </message>
    <message>
        <source>Credits</source>
        <translation type="obsolete">Crédits</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecPluginsManager.cpp" line="116"/>
        <source>Written by</source>
        <translation>Écrit par</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecPluginsManager.cpp" line="120"/>
        <source>Documented by</source>
        <translation>Documenté par</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecPluginsManager.cpp" line="124"/>
        <source>Translated by</source>
        <translation>Traduit par</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecPluginsManager.cpp" line="128"/>
        <source>&amp;Close</source>
        <translation>&amp;Fermer</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecPluginsManager.cpp" line="170"/>
        <source>&lt;all types&gt;</source>
        <translation>&lt;tous types&gt;</translation>
    </message>
</context>
<context>
    <name>MecTypeChooserDialog</name>
    <message>
        <location filename="../../../src/mecanique/MecTypeChooserDialog.cpp" line="24"/>
        <source>Choose type…</source>
        <translation>Choix de type…</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecTypeChooserDialog.cpp" line="33"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Annuler</translation>
    </message>
</context>
<context>
    <name>MecUpdater</name>
    <message>
        <location filename="../../../src/mecanique/MecUpdater.cpp" line="47"/>
        <source>Download in progress...</source>
        <translation>Téléchargement en cours…</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecUpdater.cpp" line="47"/>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecUpdater.cpp" line="245"/>
        <source>Download size:</source>
        <translation>Tailler du téléchargement :</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecUpdater.cpp" line="256"/>
        <source>Mécanique and all related programs should be stopped.</source>
        <translation>Mécanique et tous les programmes liés devront être arrêtés.</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecUpdater.cpp" line="262"/>
        <source>An update of the components of Mécanique is available, do you want to download and apply it now?</source>
        <comment>Text corresponding to an update / minor changes.</comment>
        <translation>Une mise à jour des composants de Mécanique est disponible, voulez-vous procéder à leur téléchargement et application maintenant ?</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecUpdater.cpp" line="263"/>
        <source>Update available</source>
        <translation>Mises à jour disponibles</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecUpdater.cpp" line="267"/>
        <source>An upgrade of Mécanique is available, do you want to download and install it now?</source>
        <comment>Text corresponding to an upgrade of Mécanique / change version / major changes.</comment>
        <translation>Une mise à niveau de Mécanique est disponible, voulez-vous procéder au téléchargement et à l&apos;installation de celle-ci maintenant ?</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecUpdater.cpp" line="268"/>
        <source>New version available</source>
        <translation>Nouvelle version disponible</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecUpdater.cpp" line="326"/>
        <source>Mécanique closure required</source>
        <translation>Arrêt de Mécanique requis</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecUpdater.cpp" line="326"/>
        <source>&lt;h3&gt;Mécanique closing required&lt;/h3&gt;&lt;p&gt;Mécanique must be closed to continue its update.&lt;/p&gt;</source>
        <translation>&lt;h3&gt;Arrêt de Mécanique requis&lt;/h3&gt;&lt;p&gt;Mécanique doit être arrêté pour appliquer les mises à jour.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecUpdater.cpp" line="375"/>
        <source>Updates available...</source>
        <translation>Mises à jour disponibles…</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecUpdater.cpp" line="375"/>
        <source>&lt;h3&gt;Updates available&lt;/h3&gt;&lt;p&gt;Updates of Mécanique are available, to download it, see following:&lt;br /&gt;&lt;a href=&quot;%1&quot;&gt;%1&lt;/a&gt;&lt;/p&gt;</source>
        <translation>&lt;h3&gt;Mises à jour disponibles&lt;/h3&gt;&lt;p&gt;Des mises à jour de Mécanique sont disponibles, pour les télécharger voir la page suivante :&lt;br /&gt;&lt;a href=&quot;%1&quot;&gt;%1&lt;/a&gt;&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecUpdater.cpp" line="394"/>
        <location filename="../../../src/mecanique/MecUpdater.cpp" line="425"/>
        <location filename="../../../src/mecanique/MecUpdater.cpp" line="441"/>
        <source>Download error</source>
        <translation>Erreur de téléchargement</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecUpdater.cpp" line="394"/>
        <source>&lt;h3&gt;Download error&lt;/h3&gt;&lt;p&gt;The download of the file “%1” from “%2” has failed.&lt;br /&gt;What do you want to do?&lt;/p&gt;</source>
        <translation>&lt;h3&gt;Erreur de téléchargement&lt;/h3&gt;&lt;p&gt;Le téléchargement du fichier « %1 » depuis « %2 » a échoué.&lt;br /&gt;Que souhaitez-vous faire ?&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecUpdater.cpp" line="425"/>
        <source>&lt;h3&gt;Download error&lt;/h3&gt;&lt;p&gt;The checking of the file “%1” from “%2” has failed, he has been probably corrupted during the download.&lt;br /&gt;What do you want to do?&lt;/p&gt;</source>
        <translation>&lt;h3&gt;Erreur de téléchargement&lt;/h3&gt;&lt;p&gt;La vérification du fichier « %1 » en provenance de « %2 » a échoué, il a probablement été corrompu durant le téléchargement.&lt;br /&gt;Que souhaitez-vous faire ?&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecUpdater.cpp" line="441"/>
        <source>&lt;h3&gt;Download error&lt;/h3&gt;&lt;p&gt;The reading of the file “%1” from “%2” has failed.&lt;br /&gt;What do you want to do?&lt;/p&gt;</source>
        <translation>&lt;h3&gt;Erreur de téléchargement&lt;/h3&gt;&lt;p&gt;La lecture du fichier « %1 » en provenance de « %2 » a échoué.&lt;br /&gt;Que souhaitez-vous faire ?&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecUpdater.cpp" line="467"/>
        <source>Update error</source>
        <translation>Erreur de mise à jour</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecUpdater.cpp" line="467"/>
        <source>&lt;h3&gt;Update error&lt;/h3&gt;&lt;p&gt;The update process has failed.&lt;/p&gt;</source>
        <translation>&lt;h3&gt;Erreur de mise à jour&lt;/h3&gt;&lt;p&gt;Le processus de mise à jour a échoué.&lt;/p&gt;</translation>
    </message>
</context>
</TS>
