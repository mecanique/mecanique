<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="de_DE">
<context>
    <name>MecCodeEditor</name>
    <message>
        <location filename="../../../src/mecanique/MecCodeEditor.cpp" line="85"/>
        <source>Change code of “%1”</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MecCompiler</name>
    <message>
        <location filename="../../../src/mecanique/MecCompiler.cpp" line="109"/>
        <location filename="../../../src/mecanique/MecCompiler.cpp" line="114"/>
        <location filename="../../../src/mecanique/MecCompiler.cpp" line="119"/>
        <location filename="../../../src/mecanique/MecCompiler.cpp" line="142"/>
        <location filename="../../../src/mecanique/MecCompiler.cpp" line="162"/>
        <location filename="../../../src/mecanique/MecCompiler.cpp" line="210"/>
        <location filename="../../../src/mecanique/MecCompiler.cpp" line="220"/>
        <location filename="../../../src/mecanique/MecCompiler.cpp" line="231"/>
        <location filename="../../../src/mecanique/MecCompiler.cpp" line="261"/>
        <location filename="../../../src/mecanique/MecCompiler.cpp" line="291"/>
        <location filename="../../../src/mecanique/MecCompiler.cpp" line="314"/>
        <location filename="../../../src/mecanique/MecCompiler.cpp" line="321"/>
        <location filename="../../../src/mecanique/MecCompiler.cpp" line="374"/>
        <location filename="../../../src/mecanique/MecCompiler.cpp" line="452"/>
        <location filename="../../../src/mecanique/MecCompiler.cpp" line="457"/>
        <location filename="../../../src/mecanique/MecCompiler.cpp" line="466"/>
        <location filename="../../../src/mecanique/MecCompiler.cpp" line="471"/>
        <location filename="../../../src/mecanique/MecCompiler.cpp" line="496"/>
        <location filename="../../../src/mecanique/MecCompiler.cpp" line="512"/>
        <source>Error #</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecCompiler.cpp" line="109"/>
        <location filename="../../../src/mecanique/MecCompiler.cpp" line="119"/>
        <source>: The build directory “</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecCompiler.cpp" line="109"/>
        <source>” is not existing.
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecCompiler.cpp" line="114"/>
        <source>: “</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecCompiler.cpp" line="114"/>
        <source>” is not a directory.
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecCompiler.cpp" line="119"/>
        <source>” is not writable.
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecCompiler.cpp" line="132"/>
        <source>Build directory “</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecCompiler.cpp" line="132"/>
        <source>” checked.
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecCompiler.cpp" line="142"/>
        <source>: The project “</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecCompiler.cpp" line="142"/>
        <source>” don&apos;t have appropriate plugin.
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecCompiler.cpp" line="162"/>
        <source>: The element “</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecCompiler.cpp" line="162"/>
        <source>” (type “</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecCompiler.cpp" line="162"/>
        <source>”) don&apos;t have appropriate plugin.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecCompiler.cpp" line="163"/>
        <source>No particular plugin wanted.
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecCompiler.cpp" line="164"/>
        <source>Plugin “</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecCompiler.cpp" line="164"/>
        <source>” wanted.
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecCompiler.cpp" line="210"/>
        <source>: The resource “</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecCompiler.cpp" line="210"/>
        <source>” cannot be read. This is a internal error.
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecCompiler.cpp" line="220"/>
        <source>: The directory “</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecCompiler.cpp" line="220"/>
        <source>” cannot be created.
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecCompiler.cpp" line="231"/>
        <location filename="../../../src/mecanique/MecCompiler.cpp" line="261"/>
        <location filename="../../../src/mecanique/MecCompiler.cpp" line="291"/>
        <location filename="../../../src/mecanique/MecCompiler.cpp" line="321"/>
        <location filename="../../../src/mecanique/MecCompiler.cpp" line="374"/>
        <source>: The file “</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecCompiler.cpp" line="231"/>
        <location filename="../../../src/mecanique/MecCompiler.cpp" line="261"/>
        <location filename="../../../src/mecanique/MecCompiler.cpp" line="291"/>
        <location filename="../../../src/mecanique/MecCompiler.cpp" line="321"/>
        <location filename="../../../src/mecanique/MecCompiler.cpp" line="374"/>
        <source>” cannot be written.
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecCompiler.cpp" line="314"/>
        <source>: The original file “main.cpp” cannot be read. This is an internal error.
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecCompiler.cpp" line="328"/>
        <source>#####
#Generated automatically by Mécanique
#
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecCompiler.cpp" line="452"/>
        <source>: The sub-process “qmake” cannot be loaded.
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecCompiler.cpp" line="457"/>
        <source>: The sub-process “qmake” has crashed.
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecCompiler.cpp" line="466"/>
        <source>: The sub-process “make” cannot be loaded.
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecCompiler.cpp" line="471"/>
        <source>: The sub-process “make” has crashed.
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecCompiler.cpp" line="496"/>
        <source>: Error detected by “qmake” sub-process. This is a internal error.
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecCompiler.cpp" line="507"/>
        <source>Compilation complete.
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecCompiler.cpp" line="512"/>
        <source>: Error detected by “make” sub-process.
</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MecCompilerDialog</name>
    <message>
        <location filename="../../../src/mecanique/MecCompilerDialog.cpp" line="25"/>
        <source>Compilation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecCompilerDialog.cpp" line="32"/>
        <source>Build directory :</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecCompilerDialog.cpp" line="39"/>
        <source>Choose…</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecCompilerDialog.cpp" line="43"/>
        <location filename="../../../src/mecanique/MecCompilerDialog.cpp" line="134"/>
        <source>Compile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecCompilerDialog.cpp" line="46"/>
        <source>Compilation log : </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecCompilerDialog.cpp" line="51"/>
        <source>Execute</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecCompilerDialog.cpp" line="78"/>
        <source>Informations about the licence...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecCompilerDialog.cpp" line="83"/>
        <source>&lt;h3&gt;Informations about the licence&lt;/h3&gt;&lt;p&gt;The program which is going to be generated will integrate of the code under license EUPL (&quot;European Union Public Licence&quot;), which will apply as a consequence to the whole work which it constitutes as well as in the works which will be diverted from it and/or which will integrate it of any way, as a video, a show, or any other shape of work.&lt;/p&gt;&lt;p&gt;It means that you grant to whoever the following rights ones: &lt;ul&gt;&lt;li&gt;use the work in any circumstance and for all usage,&lt;/li&gt;&lt;li&gt;reproduce the work,&lt;/li&gt;&lt;li&gt;modify the original work, and make derivative works based upon the work,&lt;/li&gt;&lt;li&gt;communicate to the public, including the right to make available or display the work or copies thereof to the public and perform publicly, as the case may be, the work,&lt;/li&gt;&lt;li&gt;distribute the work or copies thereof,&lt;/li&gt;&lt;li&gt;lend and rent the work or copies thereof,&lt;/li&gt;&lt;li&gt;sub-license rights in the Work or copies thereof,&lt;/li&gt;&lt;/ul&gt;and that you guarantee the concession of these rights, by the terms of the EUPL.&lt;/p&gt;&lt;p&gt;If you do not arrange right necessities to grant so the work or its sub-works, either that you do not wish to make it, it is necessary to you to acquire another license.&lt;/p&gt;&lt;p&gt;&lt;a href=&quot;http://joinup.ec.europa.eu/software/page/eupl/licence-eupl&quot;&gt;EUPL in 22 languages&lt;/a&gt;&lt;br /&gt;&lt;a href=&quot;https://joinup.ec.europa.eu/system/files/EN/EUPL v.1.1 - Licence.pdf&quot;&gt;Text of the licence in English&lt;/a&gt;&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecCompilerDialog.cpp" line="80"/>
        <source>Accept</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecCompilerDialog.cpp" line="81"/>
        <source>Reject</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecCompilerDialog.cpp" line="102"/>
        <source>Choose build directory for compilation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecCompilerDialog.cpp" line="119"/>
        <source>Stop</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MecEditor</name>
    <message>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="24"/>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="145"/>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="583"/>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="611"/>
        <source>Mécanique</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="27"/>
        <source>&lt;center&gt;No elements to edit.&lt;/center&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="33"/>
        <source>&amp;File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="36"/>
        <source>&amp;Save</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="42"/>
        <source>&amp;Close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="43"/>
        <source>&amp;Quit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="45"/>
        <source>&amp;Edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="59"/>
        <source>Remove element</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="68"/>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="94"/>
        <source>&amp;Tools</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="75"/>
        <source>&amp;Plugins manager</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="77"/>
        <source>&amp;Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="34"/>
        <source>&amp;Create...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="35"/>
        <source>&amp;Open...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="38"/>
        <source>S&amp;ave as...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="41"/>
        <source>&amp;New window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="51"/>
        <source>Add &amp;object...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="53"/>
        <source>Add &amp;function...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="55"/>
        <source>Add &amp;signal...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="57"/>
        <source>Add &amp;variable...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="65"/>
        <source>&amp;Import...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="69"/>
        <source>Show &amp;graph...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="72"/>
        <source>&amp;Compile...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="78"/>
        <source>Summary...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="80"/>
        <source>Report a bug...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="82"/>
        <source>About &amp;Mécanique...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="83"/>
        <source>About &amp;Qt...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="116"/>
        <source>Element&apos;s tree</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="485"/>
        <source>Plugin error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="485"/>
        <source>&lt;h3&gt;Plugin error&lt;/h3&gt;&lt;p&gt;The element “</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="485"/>
        <source>” (type “</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="485"/>
        <source>”) don&apos;t have appropriate plugin.&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="160"/>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="166"/>
        <source>Loading error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="46"/>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="633"/>
        <source>&amp;Undo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="48"/>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="643"/>
        <source>&amp;Redo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="62"/>
        <source>Show reference</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="160"/>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="166"/>
        <source>&lt;h3&gt;Loading error&lt;/h3&gt;&lt;p&gt;The file “</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="160"/>
        <source>” contains no mécanique data.&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="166"/>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="193"/>
        <source>” could not be opened.&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="182"/>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="193"/>
        <source>Writing error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="182"/>
        <source>&lt;h3&gt;Writing error&lt;/h3&gt;&lt;p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="182"/>
        <source>&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="193"/>
        <source>&lt;h3&gt;Writing error&lt;/h3&gt;&lt;p&gt;The file “</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="555"/>
        <source>Project</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="558"/>
        <source>Object</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="561"/>
        <source>Function</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="564"/>
        <source>Signal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="567"/>
        <source>Variable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="590"/>
        <source>Open...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="604"/>
        <source>Save as...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="604"/>
        <source>Mécanique file (*.mec)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="628"/>
        <source>&amp;Undo %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="638"/>
        <source>&amp;Redo %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="710"/>
        <source>Remove element “%1”</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="722"/>
        <source>Import...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="735"/>
        <source>Import impossible</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="735"/>
        <source>&lt;h3&gt;Import impossible&lt;/h3&gt;&lt;p&gt;The import is impossible: the file specified contains no importable project element.&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="764"/>
        <source>About the editor...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="590"/>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="722"/>
        <source>Mécanique file (*.mec);;All files (*.*)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="748"/>
        <source>Compilation impossible</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="748"/>
        <source>&lt;h3&gt;Compilation impossible&lt;/h3&gt;&lt;p&gt;The root element is not a project, the compilation cannot be made.&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="764"/>
        <source>&lt;h3&gt;About the editor&lt;/h3&gt;&lt;p&gt;Version %1&lt;/p&gt;&lt;p&gt;Copyrigth © 2013 – 2015 Quentin VIGNAUD&lt;/p&gt;&lt;p&gt;Licensed under the EUPL, version 1.1 only.&lt;br /&gt;You may not use this work except in compliance with the Licence. You may obtain a copy of the Licence at: &lt;a href=&quot;http://joinup.ec.europa.eu/software/page/eupl/licence-eupl&quot;&gt;http://joinup.ec.europa.eu/software/page/eupl/licence-eupl&lt;/a&gt;&lt;/p&gt;&lt;p&gt;Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an “AS IS” basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.&lt;br /&gt;See the Licence for the specific language governing permissions and limitations under the Licence.&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="787"/>
        <source>&lt;h3&gt;Unsaved changes: %1&lt;/h3&gt;&lt;p&gt;Do you want to save changes occurred with %1?&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="788"/>
        <source>&lt;h3&gt;Unsaved changes: %1&lt;/h3&gt;&lt;p&gt;Do you want to save changes occurred with %1 in “%2”?&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="790"/>
        <source>Unsaved changes...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="813"/>
        <source>&lt;h3&gt;Plugins errors&lt;/h3&gt;&lt;p&gt;The plugins corresponding to the following datas are required:&lt;br /&gt;&lt;table&gt;&lt;tr&gt;&lt;th&gt;Error&lt;/th&gt;&lt;th&gt;Name&lt;/th&gt;&lt;th&gt;Version&lt;/th&gt;&lt;th&gt;Role&lt;/th&gt;&lt;th&gt;Type&lt;/th&gt;&lt;/tr&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="823"/>
        <source>Undefined error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="826"/>
        <source>Plugin unavailable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="829"/>
        <source>Plugin uncompatible</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="838"/>
        <source>No special plugin</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="845"/>
        <source>No special version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="857"/>
        <source>&lt;/table&gt;&lt;br /&gt;&lt;/p&gt;&lt;p&gt;See the plugins manager for more information.&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecEditor.cpp" line="860"/>
        <source>Plugins errors…</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MecElementTree</name>
    <message>
        <location filename="../../../src/mecanique/MecElementTree.cpp" line="24"/>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecElementTree.cpp" line="24"/>
        <source>Type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecElementTree.cpp" line="166"/>
        <source>Move “%1”</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MecGraphDialog</name>
    <message>
        <location filename="../../../src/mecanique/MecGraphDialog.cpp" line="24"/>
        <source>Mécanique graph</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecGraphDialog.cpp" line="36"/>
        <source>No graph.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecGraphDialog.cpp" line="44"/>
        <source>&amp;File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecGraphDialog.cpp" line="45"/>
        <source>S&amp;ave as...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecGraphDialog.cpp" line="48"/>
        <source>Refresh graph</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecGraphDialog.cpp" line="50"/>
        <source>&amp;Close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecGraphDialog.cpp" line="52"/>
        <source>&amp;Display</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecGraphDialog.cpp" line="53"/>
        <source>Zoom &amp;in</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecGraphDialog.cpp" line="55"/>
        <source>Zoom &amp;out</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecGraphDialog.cpp" line="57"/>
        <source>&amp;Normal size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecGraphDialog.cpp" line="60"/>
        <source>&amp;Fit to window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecGraphDialog.cpp" line="64"/>
        <source>&amp;Graph</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecGraphDialog.cpp" line="65"/>
        <source>&amp;Mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecGraphDialog.cpp" line="66"/>
        <source>&amp;Hierarchy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecGraphDialog.cpp" line="68"/>
        <source>&amp;Presentation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecGraphDialog.cpp" line="71"/>
        <source>&amp;Show connections</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecGraphDialog.cpp" line="73"/>
        <source>&amp;Color connections</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecGraphDialog.cpp" line="75"/>
        <source>Hide &amp;variables</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecGraphDialog.cpp" line="77"/>
        <source>No block &amp;structure</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecGraphDialog.cpp" line="80"/>
        <source>&amp;Tools</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecGraphDialog.cpp" line="117"/>
        <source>Save as...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecGraphDialog.cpp" line="117"/>
        <source>Portable Network Graphics (*.png)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecGraphDialog.cpp" line="123"/>
        <source>Saving error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecGraphDialog.cpp" line="123"/>
        <source>&lt;h3&gt;Saving error&lt;/h3&gt;&lt;p&gt;The graph could not be saved.&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecGraphDialog.cpp" line="182"/>
        <source>An error has occured while the creation of the graph.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MecPluginsManager</name>
    <message>
        <location filename="../../../src/mecanique/MecPluginsManager.cpp" line="26"/>
        <source>Plugins manager</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecPluginsManager.cpp" line="30"/>
        <source>Title</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecPluginsManager.cpp" line="30"/>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecPluginsManager.cpp" line="30"/>
        <source>Version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecPluginsManager.cpp" line="30"/>
        <source>Role supported</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecPluginsManager.cpp" line="30"/>
        <source>Type supported</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecPluginsManager.cpp" line="67"/>
        <source>Project</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecPluginsManager.cpp" line="71"/>
        <source>Object</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecPluginsManager.cpp" line="75"/>
        <source>Function</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecPluginsManager.cpp" line="79"/>
        <source>Signal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecPluginsManager.cpp" line="83"/>
        <source>Variable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecPluginsManager.cpp" line="116"/>
        <source>Written by</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecPluginsManager.cpp" line="120"/>
        <source>Documented by</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecPluginsManager.cpp" line="124"/>
        <source>Translated by</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecPluginsManager.cpp" line="128"/>
        <source>&amp;Close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecPluginsManager.cpp" line="170"/>
        <source>&lt;all types&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MecTypeChooserDialog</name>
    <message>
        <location filename="../../../src/mecanique/MecTypeChooserDialog.cpp" line="24"/>
        <source>Choose type…</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecTypeChooserDialog.cpp" line="33"/>
        <source>&amp;Cancel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MecUpdater</name>
    <message>
        <location filename="../../../src/mecanique/MecUpdater.cpp" line="47"/>
        <source>Download in progress...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecUpdater.cpp" line="47"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecUpdater.cpp" line="245"/>
        <source>Download size:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecUpdater.cpp" line="256"/>
        <source>Mécanique and all related programs should be stopped.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecUpdater.cpp" line="262"/>
        <source>An update of the components of Mécanique is available, do you want to download and apply it now?</source>
        <comment>Text corresponding to an update / minor changes.</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecUpdater.cpp" line="263"/>
        <source>Update available</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecUpdater.cpp" line="267"/>
        <source>An upgrade of Mécanique is available, do you want to download and install it now?</source>
        <comment>Text corresponding to an upgrade of Mécanique / change version / major changes.</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecUpdater.cpp" line="268"/>
        <source>New version available</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecUpdater.cpp" line="326"/>
        <source>Mécanique closure required</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecUpdater.cpp" line="326"/>
        <source>&lt;h3&gt;Mécanique closing required&lt;/h3&gt;&lt;p&gt;Mécanique must be closed to continue its update.&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecUpdater.cpp" line="375"/>
        <source>Updates available...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecUpdater.cpp" line="375"/>
        <source>&lt;h3&gt;Updates available&lt;/h3&gt;&lt;p&gt;Updates of Mécanique are available, to download it, see following:&lt;br /&gt;&lt;a href=&quot;%1&quot;&gt;%1&lt;/a&gt;&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecUpdater.cpp" line="394"/>
        <location filename="../../../src/mecanique/MecUpdater.cpp" line="425"/>
        <location filename="../../../src/mecanique/MecUpdater.cpp" line="441"/>
        <source>Download error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecUpdater.cpp" line="394"/>
        <source>&lt;h3&gt;Download error&lt;/h3&gt;&lt;p&gt;The download of the file “%1” from “%2” has failed.&lt;br /&gt;What do you want to do?&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecUpdater.cpp" line="425"/>
        <source>&lt;h3&gt;Download error&lt;/h3&gt;&lt;p&gt;The checking of the file “%1” from “%2” has failed, he has been probably corrupted during the download.&lt;br /&gt;What do you want to do?&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecUpdater.cpp" line="441"/>
        <source>&lt;h3&gt;Download error&lt;/h3&gt;&lt;p&gt;The reading of the file “%1” from “%2” has failed.&lt;br /&gt;What do you want to do?&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecUpdater.cpp" line="467"/>
        <source>Update error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique/MecUpdater.cpp" line="467"/>
        <source>&lt;h3&gt;Update error&lt;/h3&gt;&lt;p&gt;The update process has failed.&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
