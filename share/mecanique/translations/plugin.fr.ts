<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="fr_FR">
<context>
    <name>MecElementEditor</name>
    <message>
        <location filename="../../../src/mecanique-plugins/plugin/MecElementEditor.cpp" line="32"/>
        <source>Type:</source>
        <translation>Type :</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique-plugins/plugin/MecElementEditor.cpp" line="40"/>
        <source>Name:</source>
        <translation>Nom :</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique-plugins/plugin/MecElementEditor.cpp" line="172"/>
        <source>Add the object “%1”</source>
        <translation>Ajout de l&apos;objet « %1 »</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique-plugins/plugin/MecElementEditor.cpp" line="182"/>
        <source>Add the function “%1”</source>
        <translation>Ajout de la fonction « %1 »</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique-plugins/plugin/MecElementEditor.cpp" line="192"/>
        <source>Add the signal “%1”</source>
        <translation>Ajout du signal « %1 »</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique-plugins/plugin/MecElementEditor.cpp" line="202"/>
        <source>Add the variable “%1”</source>
        <translation>Ajout de la variable « %1 »</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique-plugins/plugin/MecElementEditor.cpp" line="297"/>
        <source>Change name of “%1” to “%2”</source>
        <translation>Renommage de « %1 » en « %2 »</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique-plugins/plugin/MecElementEditor.cpp" line="310"/>
        <source>Change type of “%1” to “%2” from “%3”</source>
        <translation>Changement du type de « %1 » depuis « %2 » vers « %3 »</translation>
    </message>
</context>
<context>
    <name>MecFunctionEditor</name>
    <message>
        <location filename="../../../src/mecanique-plugins/plugin/MecFunctionEditor.cpp" line="32"/>
        <source>General</source>
        <translation>Général</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique-plugins/plugin/MecFunctionEditor.cpp" line="33"/>
        <source>Return type:</source>
        <translation>Type de retour :</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique-plugins/plugin/MecFunctionEditor.cpp" line="45"/>
        <source>Variables</source>
        <translation>Variables</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique-plugins/plugin/MecFunctionEditor.cpp" line="65"/>
        <source>Properties</source>
        <translation>Propriétés</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique-plugins/plugin/MecFunctionEditor.cpp" line="75"/>
        <location filename="../../../src/mecanique-plugins/plugin/MecFunctionEditor.cpp" line="162"/>
        <source>Code</source>
        <translation>Code</translation>
    </message>
</context>
<context>
    <name>MecObjectEditor</name>
    <message>
        <location filename="../../../src/mecanique-plugins/plugin/MecObjectEditor.cpp" line="32"/>
        <source>General</source>
        <translation>Général</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique-plugins/plugin/MecObjectEditor.cpp" line="42"/>
        <source>Reference</source>
        <translation>Référence</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique-plugins/plugin/MecObjectEditor.cpp" line="57"/>
        <source>Properties</source>
        <translation>Propriétés</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique-plugins/plugin/MecObjectEditor.cpp" line="67"/>
        <location filename="../../../src/mecanique-plugins/plugin/MecObjectEditor.cpp" line="222"/>
        <source>Functions</source>
        <translation>Fonctions</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique-plugins/plugin/MecObjectEditor.cpp" line="77"/>
        <location filename="../../../src/mecanique-plugins/plugin/MecObjectEditor.cpp" line="223"/>
        <source>Signals</source>
        <translation>Signaux</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique-plugins/plugin/MecObjectEditor.cpp" line="96"/>
        <location filename="../../../src/mecanique-plugins/plugin/MecObjectEditor.cpp" line="224"/>
        <source>Variables</source>
        <translation>Variables</translation>
    </message>
</context>
<context>
    <name>MecProjectEditor</name>
    <message>
        <location filename="../../../src/mecanique-plugins/plugin/MecProjectEditor.cpp" line="30"/>
        <source>General</source>
        <translation>Général</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique-plugins/plugin/MecProjectEditor.cpp" line="35"/>
        <source>Title:</source>
        <translation>Titre :</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique-plugins/plugin/MecProjectEditor.cpp" line="45"/>
        <source>Synopsis</source>
        <translation>Synopsis</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique-plugins/plugin/MecProjectEditor.cpp" line="56"/>
        <source>Properties</source>
        <translation>Propriétés</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique-plugins/plugin/MecProjectEditor.cpp" line="66"/>
        <source>Objects</source>
        <translation>Objets</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique-plugins/plugin/MecProjectEditor.cpp" line="76"/>
        <location filename="../../../src/mecanique-plugins/plugin/MecProjectEditor.cpp" line="265"/>
        <source>Functions</source>
        <translation>Fonctions</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique-plugins/plugin/MecProjectEditor.cpp" line="86"/>
        <location filename="../../../src/mecanique-plugins/plugin/MecProjectEditor.cpp" line="266"/>
        <source>Signals</source>
        <translation>Signaux</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique-plugins/plugin/MecProjectEditor.cpp" line="105"/>
        <location filename="../../../src/mecanique-plugins/plugin/MecProjectEditor.cpp" line="267"/>
        <source>Variables</source>
        <translation>Variables</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique-plugins/plugin/MecProjectEditor.cpp" line="407"/>
        <source>Change title of the project</source>
        <translation>Changement du titre du projet</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique-plugins/plugin/MecProjectEditor.cpp" line="415"/>
        <source>Change synopsis of the project</source>
        <translation>Changement du synopsis du projet</translation>
    </message>
</context>
<context>
    <name>MecSignalConnectionsList</name>
    <message>
        <location filename="../../../src/mecanique-plugins/plugin/MecSignalEditor.cpp" line="93"/>
        <source>Remove connections with “%1”</source>
        <translation>Supprimer les connections avec « %1 »</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique-plugins/plugin/MecSignalEditor.cpp" line="108"/>
        <source>Add connection between “%1” and “%2”</source>
        <translation>Ajout de connexion entre « %1 » et « %2 »</translation>
    </message>
</context>
<context>
    <name>MecSignalEditor</name>
    <message>
        <location filename="../../../src/mecanique-plugins/plugin/MecSignalEditor.cpp" line="190"/>
        <source>General</source>
        <translation>Général</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique-plugins/plugin/MecSignalEditor.cpp" line="204"/>
        <source>Variables</source>
        <translation>Variables</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique-plugins/plugin/MecSignalEditor.cpp" line="224"/>
        <source>Properties</source>
        <translation>Propriétés</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique-plugins/plugin/MecSignalEditor.cpp" line="231"/>
        <source>Add connection</source>
        <translation>Ajouter une connexion</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique-plugins/plugin/MecSignalEditor.cpp" line="233"/>
        <source>Remove connection</source>
        <translation>Supprimer la connexion</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique-plugins/plugin/MecSignalEditor.cpp" line="241"/>
        <source>Connections</source>
        <translation>Connexions</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique-plugins/plugin/MecSignalEditor.cpp" line="403"/>
        <source>Add connection between “%1” and “%2”</source>
        <translation>Ajout de connexion entre « %1 » et « %2 »</translation>
    </message>
</context>
</TS>
