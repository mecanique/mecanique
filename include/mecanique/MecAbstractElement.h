/*
© Quentin VIGNAUD, 2013

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#ifndef __MECABSTRACTELEMENT_H__
#define __MECABSTRACTELEMENT_H__

#include <QObject>
#include <QRegExp>

/**
\brief	Classe de représentation abstraite d'un élément.
*/
class MecAbstractElement : public QObject
{
	Q_OBJECT

	public:
	/**
	\brief	Énumération des rôles d'éléments possibles.
	*/
	enum ElementRole {
		///L'élément est le projet.	\see	MecAbstractProject
		Project = 0,
		///L'élément est un objet.	\see	MecAbstractObject
		Object = 1,
		///L'élément est une fonction.	\see	MecAbstractFunction
		Function = 2,
		///L'élément est un signal.	\see	MecAbstractSignal
		Signal = 3,
		///L'élément est une variable.	\see	MecAbstractVariable
		Variable = 4
		};

	/**
	\brief	Destructeur.
	\note	Détruit également tous les éléments enfants.
	*/
	virtual ~MecAbstractElement() {};
	
	///Retourne le nom de l'élément.
	virtual QString elementName() const = 0;
	
	///Retourne le type de l'élément.
	virtual QString elementType() const = 0;
	
	///Retourne le rôle de l'élément.
	virtual MecAbstractElement::ElementRole elementRole() const = 0;
	
	///Retourne l'élément parent.
	virtual MecAbstractElement* parentElement() const = 0;
	/**
	\brief	Fixe l'élément parent.
	
	Si il n'est pas possible que \e ParentElement soit le parent de l'élément, \e false est retourné et l'ancien parent est conservé.
	*/
	virtual bool setParentElement(MecAbstractElement *ParentElement) = 0;
	
	///Retourne les éléments enfants.
	virtual QList<MecAbstractElement*> childElements() const = 0;
	/**
	\brief	Retourne l'élément enfant situé à l'adresse indiquée.
	
	\param	Address	Adresse de l'élément demandé.
	\note	L'élément actuel doît être la racine de l'adresse.
	
	\return	L'élément situé à l'adresse indiquée, ou 0 en cas d'échec.
	*/
	virtual MecAbstractElement* childElement(QString Address) const = 0;
	
	/**
	\brief	Retourne l'adresse de l'élément.
	
	\param	RootElement	Élément à considérer comme la racine de l'adresse.
	
	L'adresse d'un élément est de la forme « nameroot.nameparentelement.nameelement » ; la fonction retourne une adresse en prenant comme racine RootElement, ou le plus grand parent connu si RootElement ne fait pas partie de l'arbre d'appartenance.
	*/
	virtual QString address(MecAbstractElement *RootElement=0) const = 0;
	
	///Retourne la regex de la syntaxe standarde.
	static QRegExp standardSyntax() { return QRegExp("^(?!mec_)[a-zA-PR-Z_]+([0-9a-zA-Z_])*"); };

	public slots:
	/**
	\brief	Fixe un nouveau nom pour l'élément.
	
	Si \e Name n'est pas respectueux de la syntaxe standarde ou n'est pas disponible à ce niveau, \e false est retourné et l'ancien nom est conservé.
	*/
	virtual bool setElementName(QString Name) = 0;
	
	/**
	\brief	Fixe un nouveau type pour l'élément.
	
	Si \e Type n'est pas respectueux de la syntaxe standarde, \e false est retourné et l'ancien type est conservé.
	*/
	virtual bool setElementType(QString Type) = 0;
	
	signals:
	/**
	\brief	L'élément est sur le point d'être supprimé.
	
	Ce signal est émis lorsque la destruction de l'élément va avoir lieu, son rôle est similaire à QObject::destroyed(QObject*) à ceci près qu'il permet d'intervenir sur l'élément en tant que MecAbstractElement et non seulement en tant que QObject, en raison de l'ordre des appels de destructeurs.
	*/
	void destroyed(MecAbstractElement *I);
	
	///Le nom de l'élément a été changé.	\param	I	pointe sur l'élément émetteur.
	void nameChanged(MecAbstractElement *I);
	///Le type de l'élément a été changé.	\param	I	pointe sur l'élément émetteur.
	void typeChanged(MecAbstractElement *I);
	///Le parent de l'élément a été changé.	\param	I	pointe sur l'élément émetteur.
	void parentChanged(MecAbstractElement *I);
	///La liste des enfants a changé.	\param	I	pointe sur l'élément émetteur.
	void childListChanged(MecAbstractElement *I);
	
	public:
	/**
	\brief	Effectue les traitements nécessaires pour le changement de parent.
	
	\warning	Bien que publique, cette fonction n'est pas vouée à être utilisée autrement que par les objets héritant de cette classe.
	*/
	virtual bool changeParentElement(MecAbstractElement *ParentElement) = 0;
	/**
	\brief	Retourne un pointeur sur la liste des enfants.
	
	\warning	Bien que publique, cette fonction n'est pas vouée à être utilisée autrement que par les objets héritant de cette classe.
	*/
	virtual QList<MecAbstractElement*>* childList() = 0;
	/**
	\brief	Provoque l'émission du signal childListChanged(MecAbstractElement*).
	
	\warning	Bien que publique, cette fonction n'est pas vouée à être utilisée autrement que par les objets héritant de cette classe.
	*/
	virtual void emitChildListChanged() = 0;
};

#endif /* __MECABSTRACTELEMENT_H__ */

