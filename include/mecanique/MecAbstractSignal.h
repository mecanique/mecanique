/*
© Quentin VIGNAUD, 2013

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#ifndef __MECABSTRACTSIGNAL_H__
#define __MECABSTRACTSIGNAL_H__

#include "MecAbstractElement.h"
#include "MecAbstractVariable.h"

class MecAbstractFunction;

/**
\brief	Classe virtuelle de représentation d'un signal.
*/
class MecAbstractSignal : public MecAbstractElement
{
	Q_OBJECT

	public:
	///Destructeur.
	virtual ~MecAbstractSignal() {}
	
	///Retourne les variables enfants.
	virtual QList<MecAbstractVariable*> childVariables() const = 0;
	
	///Retourne la liste des fonctions connectées.
	virtual QList<MecAbstractFunction*> connectedFunctions() const = 0;
	/**
	\brief	Ajoute une connexion avec la fonction \e Function.
	
	Si \e Function est déjà connectée au signal, la fonction ne fait rien.
	Si une fonction venait à être modifiée de manière incompatible avec le signal, celle-ci serait alors retirée de la liste des fonctions connectées.
	\note	La fonction ne devient pas enfant du signal.
	\see	isConnectableFunction(MecAbstractFunction*)
	\return	\e true si la connexion est possible, sinon \e false.
	*/
	virtual bool connectFunction(MecAbstractFunction* Function) = 0;
	/**
	\brief	Supprime \e Function de la liste des fonctions connectées.
	*/
	virtual void disconnectFunction(MecAbstractFunction* Function) = 0;
	/**
	\brief	Indique si \e Function est connectable avec ce signal.
	
	Pour être connectable, la fonction passée en paramètre doit avoir un nombre de variables égal ou inférieur au signal, et les variables de même position doivent avoir le même type.
	\note	\e Function n'est pas connectée par cette fonction.
	\see	connectFunction(MecFunction*)
	*/
	virtual bool isConnectableFunction(MecAbstractFunction* Function) = 0;
	
	signals:
	///La liste des fonctions connectées a changée.	\param	I	pointe sur le signal émetteur.
	void connectedFunctionsChanged(MecAbstractSignal *I);
};

#include "MecAbstractFunction.h"

#endif /* __MECABSTRACTSIGNAL_H__ */

