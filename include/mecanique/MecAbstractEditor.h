/*
© Quentin VIGNAUD, 2013

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#ifndef __MECABSTRACTEDITOR_H__
#define __MECABSTRACTEDITOR_H__

#include <QWidget>
#include "MecAbstractPluginsManager.h"
#include "MecAbstractElement.h"
#include "MecAbstractFunction.h"
class MecAbstractCodeEditor;//Pour éviter les erreurs de non/ré-inclusion.
#include "MecAbstractCodeEditor.h"

/**
\brief	Classe de représentation abstraite de l'éditeur.
*/
class MecAbstractEditor
{
	public:
	///Destructeur.
	virtual ~MecAbstractEditor() {}
	
	/**
	\brief	Lit le fichier \e FileName et retourne l'élément correspondant.
	\return	L'élément racine du fichier, ou bien 0 si une erreur est survenue.
	*/
	virtual MecAbstractElement* read(QString FileName) = 0;
	/**
	\brief	Écrit dans le fichier \e FileName l'élément \e Element.
	\return	\e true si l'écriture a aboutit, sinon \e false.
	*/
	virtual bool write(MecAbstractElement *Element, QString FileName) = 0;
	
	/**
	\brief	Retourne la liste des noms d'éléments de base du rôle indiqué.
	
	Cette liste correspond directement avec les fichiers de ressources en «.mec» contenus dans «:/share/base/[role]/» (non-récursivement), les noms retournés sont ceux des fichiers sans l'extension.
	*/
	virtual QStringList baseElements(MecAbstractElement::ElementRole Role) = 0;
	/**
	\brief	Retourne l'élément de base de rôle et de nom indiqué.
	\param	Nom du type d'élément de base à retourner.
	\return	0 si rien n'a été trouvé à ce nom.
	\see	baseElements()
	*/
	virtual MecAbstractElement* baseElement(MecAbstractElement::ElementRole Role, QString Name) = 0;
	/**
	\brief	Demande à l'utilisateur de choisir parmi les types proposés.
	
	\return	Le type sélectionné, ou une chaîne vide si aucun.
	*/
	virtual QString selectType(QStringList Types) = 0;
	/**
	\brief	Retourne la liste des noms réservés.
	
	Les noms réservés sont, par exemple, les noms de types (int, uint, Object, Project, etc…) et ne peuvent servir comme noms pour des éléments ou des variables internes à des fonctions définies par l'utilisateur.
	*/
	virtual QStringList reservedNames() const = 0;
	/**
	\brief	Retourne un éditeur de code pour la fonction spécifiée.
	
	Le widget \e Parent est assigné comme parent de l'éditeur retourné.
	*/
	virtual MecAbstractCodeEditor* codeEditor(MecAbstractFunction* const Function, QWidget* Parent=0) = 0;
	
	///Retourne le gestionnaire de plugins.
	virtual MecAbstractPluginsManager* pluginsManager() const = 0;
	
	///Retourne l'élément racine.
	virtual MecAbstractElement* element() const = 0;
	///Retourne l'élément actuellement sélectionné.
	virtual MecAbstractElement* selectedElement() const = 0;
	
	///Ajoute un pas d'édition (annuler/rétablir).
	virtual void addEditStep(QString Name) const = 0;
	
	/**
	\brief	Ouvre le menu contextuel correspondant à l'élément spécifié, à l'emplacement donné.
	
	\param	Element		Élément sur lequel l'appel a eu lieu.
	\param	Position	Position globale de l'appel.
	*/
	virtual void elementContextMenu(MecAbstractElement *Element, QPoint Pos) = 0;
	
	/**
	\brief	Retourne le texte correspondant au rôle d'élément.
	
	Le texte retourné est déjà traduit s'il y a lieu.
	*/
	virtual QString roleText(MecAbstractElement::ElementRole Role) const = 0;
};

#endif /* __MECABSTRACTEDITOR_H__ */

