/*
© Quentin VIGNAUD, 2013

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#ifndef __MECABSTRACTCOMPILER_H__
#define __MECABSTRACTCOMPILER_H__

#include "MecAbstractPluginsManager.h"

/**
\brief	Classe de représentation abstraite du compilateur.
*/
class MecAbstractCompiler
{
	public:
	///Destructeur.
	virtual ~MecAbstractCompiler() {}
	
	///Retourne le gestionnaire de plugins du compilateur.
	virtual MecAbstractPluginsManager* pluginsManager() const = 0;
};

#endif /* __MECABSTRACTCOMPILER_H__ */

