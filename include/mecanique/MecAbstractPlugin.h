/*
© Quentin VIGNAUD, 2013

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#ifndef __MECABSTRACTPLUGIN_H__
#define __MECABSTRACTPLUGIN_H__

#include <QtPlugin>
#include "MecAbstractElement.h"

class MecAbstractEditor;//Pour éviter les erreurs de non/ré-inclusion, les véritables includes sont en fin de fichiers.
class MecAbstractCompiler;
class MecAbstractPluginsManager;
class MecAbstractElementEditor;
class MecAbstractElementCompiler;


/**
\brief	Interface des plugins.
*/
class MecAbstractPlugin
{
	public:
	///Destructeur.
	virtual ~MecAbstractPlugin() {}
	
	///Retourne le nom du plugin.
	virtual QString name() const = 0;
	///Retourne la version du plugin.
	virtual double version() const = 0;
	///Retourne le rôle d'élément du plugin.
	virtual MecAbstractElement::ElementRole role() const = 0;
	///Retourne le type d'élément du plugin.
	virtual QString type() const = 0;
	///Retourne le titre du plugin.
	virtual QString title() const = 0;
	///Retourne la description du plugin.
	virtual QString description() const = 0;
	
	///Retourne le copyright du plugin.
	virtual QString copyright() const = 0;
	///Retourne la liste des développeurs du plugin.
	virtual QString developpers() const = 0;
	///Retourne la liste des documentalistes.
	virtual QString documentalists() const = 0;
	///Retourne la liste des traducteurs.
	virtual QString translators() const = 0;
	
	
	/**
	\brief	Retourne un éditeur de l'élément spécifié.
	\param	Element	Doit correspondre aux données du plugin, sinon 0 est retourné.
	*/
	virtual MecAbstractElementEditor* elementEditor(MecAbstractElement* const Element, MecAbstractEditor* MainEditor) = 0;
	/**
	\brief	Retourne un compilateur de l'élément spécifié.
	\param	Element	Doit correspondre aux données du plugin, sinon 0 est retourné.
	*/
	virtual MecAbstractElementCompiler* elementCompiler(MecAbstractElement* const Element, MecAbstractCompiler* const MainCompiler) = 0;
	
	///Retourne le gestionnaire de plugins.
	virtual MecAbstractPluginsManager* manager() const = 0;
	///Fixe le gestionnaire de plugins.
	virtual void setManager(MecAbstractPluginsManager* Manager) = 0;
};

Q_DECLARE_INTERFACE(MecAbstractPlugin, "Mecanique.MecAbstractPlugin")

#include "MecAbstractEditor.h"
#include "MecAbstractCompiler.h"
#include "MecAbstractPluginsManager.h"
#include "MecAbstractElementEditor.h"
#include "MecAbstractElementCompiler.h"

#endif /* __MECABSTRACTPLUGIN_H__ */

