/*
© Quentin VIGNAUD, 2013

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#ifndef __MECPLUGINERROR_H__
#define __MECPLUGINERROR_H__

class MecAbstractElement;

/**
\brief	Structure de représentation d'une erreur de plugin.

Cette structure est utilisée lorsqu'une erreur dûe à un plugin est détectée.
*/
struct MecPluginError
{
	///Énumération des natures d'erreurs possibles.
	enum Error
	{
		///Erreur indéfinie.
		Undefined=0,
		///Plugin indisponible ou inexistant.
		Unavailable=1,
		///Le transtypage a échoué.
		CastFailed=2
	};
	
	///Constructeur.
	MecPluginError() { error=MecPluginError::Undefined; element=0; }
	///Constructeur.
	MecPluginError(MecPluginError::Error Error, QString Name, double Version, MecAbstractElement::ElementRole Role, QString Type, MecAbstractElement *Element=0)
		{
		error = Error;
		name = Name;
		version = Version;
		role = Role;
		type = Type;
		element = Element;
		}

	///Nature de l'erreur.
	MecPluginError::Error error;
	///Nom du plugin en erreur.
	QString name;
	///Version du plugin en erreur.
	double version;
	///Rôle du plugin en erreur.
	MecAbstractElement::ElementRole role;
	///Type du plugin en erreur.
	QString type;
	///Élément éventuel pour lequel l'erreur s'est déclenchée.
	MecAbstractElement *element;
};

#endif /* __MECPLUGINERROR_H__ */

