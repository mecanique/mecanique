/*
© Quentin VIGNAUD, 2013

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#ifndef __MECABSTRACTPROJECT_H__
#define __MECABSTRACTPROJECT_H__

#include "MecAbstractElement.h"
#include "MecAbstractObject.h"
#include "MecAbstractFunction.h"
#include "MecAbstractSignal.h"
#include "MecAbstractVariable.h"

/**
\brief	Classe virtuelle de représentation d'un projet.
*/
class MecAbstractProject : public MecAbstractElement
{
	Q_OBJECT

	public:
	///Destructeur.
	virtual ~MecAbstractProject() {}
	
	///Titre du projet.
	virtual QString title() const = 0;
	
	///Synopsis du projet.
	virtual QString synopsis() const = 0;
	
	///Retourne les objets enfants.
	virtual QList<MecAbstractObject*> childObjects() const = 0;
	///Retourne les fonctions enfants.
	virtual QList<MecAbstractFunction*> childFunctions() const = 0;
	///Retourne les signaux enfants.
	virtual QList<MecAbstractSignal*> childSignals() const = 0;
	///Retourne les variables enfants.
	virtual QList<MecAbstractVariable*> childVariables() const = 0;
	
	public slots:
	///Fixe le titre du projet.
	virtual void setTitle(QString Title) = 0;
	///Fixe le synopsis du projet.
	virtual void setSynopsis(QString Synopsis) = 0;
	
	signals:
	/**
	\brief	Le titre du projet a été changé.
	\param	I	pointe sur le projet émetteur.
	*/
	void titleChanged(MecAbstractProject* I);
	/**
	\brief	Le synopsis du projet a été changé.
	\param	I	pointe sur le projet émetteur.
	*/
	void synopsisChanged(MecAbstractProject* I);
};

#endif /* __MECABSTRACTPROJECT_H__ */

