/*
© Quentin VIGNAUD, 2013

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#ifndef __MECABSTRACTPLUGINSMANAGER_H__
#define __MECABSTRACTPLUGINSMANAGER_H__

#include "MecAbstractPlugin.h"
class MecAbstractPlugin;//Pour éviter les erreurs de non/ré-inclusion.

/**
\brief	Classe virtuelle de gestion des plugins.
*/
class MecAbstractPluginsManager
{

	public:
	///Destructeur.
	virtual ~MecAbstractPluginsManager() {}
	
	///Retourne une liste des plugins.
	virtual QList<MecAbstractPlugin*> plugins() const = 0;
	/**
	\brief	Retourne le plugin correspondant.
	\param	Role	Rôle d'élément du plugin.
	\param	Type	Type d'élément du plugin.
	\param	Name	Nom du plugin.
	\param	Version	Version du plugin.
	\return	Le plugin correspondant, ou bien 0 si aucun n'a été trouvé.
	*/
	virtual MecAbstractPlugin* plugin(const MecAbstractElement::ElementRole Role, const QString Type, const QString Name=QString(), const double Version=0) const = 0;
	
	
	private:
	
	
};

#endif /* __MECABSTRACTPLUGINSMANAGER_H__ */

