/*
© Quentin VIGNAUD, 2013 – 2015

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#ifndef __MECABSTRACTFUNCTION_H__
#define __MECABSTRACTFUNCTION_H__

#include "MecAbstractElement.h"
#include "MecAbstractVariable.h"

class MecAbstractSignal;

/**
\brief	Classe de représentation abstraite d'une fonction.
*/
class MecAbstractFunction : public MecAbstractElement
{
	Q_OBJECT

	public:
	/**
	\brief	Destructeur.
	\see	MecAbstractElement::~MecAbstractElement()
	*/
	virtual ~MecAbstractFunction() {}
	
	///Code de la fonction.
	virtual QString code() const = 0;
	
	///Retourne les variables enfants.
	virtual QList<MecAbstractVariable*> childVariables() const = 0;
	
	///Retourne la liste des signaux connectés.
	virtual QList<MecAbstractSignal*> connectedSignals() const = 0;
	/**
	\brief	Ajoute une connexion avec le signal \e Signal.
	
	Si \e Signal est déjà connecté à la fonction, la fonction ne fait rien.
	Si un signal venait à être modifiée de manière incompatible avec la fonction, celui-ci serait alors retiré de la liste des signaux connectés.
	\note	Le signal ne devient pas enfant de la fonction.
	\see	isConnectableSignal(MecAbstractSignal*)
	\return	\e true si la connexion est possible, sinon \e false.
	*/
	virtual bool connectSignal(MecAbstractSignal* Signal) = 0;
	/**
	\brief	Supprime \e Signal de la liste des signaux connectés.
	*/
	virtual void disconnectSignal(MecAbstractSignal* Signal) = 0;
	/**
	\brief	Indique si \e Signal est connectable avec cette fonction.
	
	\note	\e Signal n'est pas connecté par cette fonction.
	\see	MecAbstractSignal::isConnectableFunction(MecAbstractFunction*)
	*/
	virtual bool isConnectableSignal(MecAbstractSignal* Signal) = 0;
	
	public slots:
	///Fixe le code de la fonction.
	virtual void setCode(QString Code) = 0;
	
	signals:
	/**
	\brief	Le code de la fonction a été changé.
	\param	I	pointe sur la fonction émettrice.
	*/
	void codeChanged(MecAbstractFunction* I);
	
	///La liste des signaux connectés a changé.	\param	I	pointe sur la fonction émettrice.
	void connectedSignalsChanged(MecAbstractFunction *I);
};

#include "MecAbstractSignal.h"

#endif /* __MECABSTRACTFUNCTION_H__ */

