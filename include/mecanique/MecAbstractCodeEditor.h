/*
© Quentin VIGNAUD, 2014

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#ifndef __MECABSTRACTCODEEDITOR_H__
#define __MECABSTRACTCODEEDITOR_H__

#include <QWidget>
#include "MecAbstractFunction"
class MecAbstractEditor;//Pour éviter les erreurs de non/ré-inclusion.
#include "MecAbstractEditor"

/**
\brief	Classe de représentation abstraite d'un éditeur de code de fonction.
*/
class MecAbstractCodeEditor : public QWidget
{
	Q_OBJECT

	public:
	///Destructeur.
	virtual ~MecAbstractCodeEditor() {}
	
	///Fonction dont le code est édité.
	virtual MecAbstractFunction* function() const = 0;
	
	///Éditeur principal.
	virtual MecAbstractEditor* mainEditor() const = 0;
	
	/**
	\brief	Retourne un QVariant contenant toutes les données indiquant l'état actuel de l'éditeur.
	
	Cette donnée est vouée à être passée ultérieurement en paramètre à setState(QVariant) pour restaurer l'état visuel de l'éditeur.
	*/
	virtual QVariant state() = 0;
	/**
	\brief	Restaure l'état de l'éditeur selon les indications spécifées.
	
	\see	state()
	*/
	virtual void setState(QVariant State) = 0;
};

#endif /* __MECABSTRACTCODEEDITOR_H__ */

