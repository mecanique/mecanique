/*
© Quentin VIGNAUD, 2013

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#ifndef __MECABSTRACTELEMENTEDITOR_H__
#define __MECABSTRACTELEMENTEDITOR_H__

#include <QWidget>
#include "MecAbstractElement.h"
#include "MecAbstractEditor.h"
class MecAbstractEditor;//Pour éviter les erreurs de non/ré-inclusion.
#include "MecPluginError.h"

/**
\brief	Classe virtuelle d'éditeur d'élément.
*/
class MecAbstractElementEditor : public QWidget
{
	Q_OBJECT

	public:
	///Destructeur.
	virtual ~MecAbstractElementEditor() {}
	
	///Élément édité.
	virtual MecAbstractElement* element() const = 0;
	///Rôle de l'éditeur.
	virtual MecAbstractElement::ElementRole editorRole() const = 0;
	
	///Éditeur principal.
	virtual MecAbstractEditor* mainEditor() const = 0;
	
	/**
	\brief	Retourne les erreurs de plugins.
	*/
	virtual QList<MecPluginError> pluginsErrors() const = 0;
	
	/**
	\brief	Retourne l'éditeur de l'élément si il fait partie de ses sous-éditeurs.
	
	\return	0 si l'éditeur de cet élément ne fait pas partie de ses sous-éditeurs.
	*/
	virtual MecAbstractElementEditor* elementEditor(MecAbstractElement* const Element) const = 0;
	
	/**
	\brief	Retourne un QVariant contenant toutes les données indiquant l'état actuel de l'éditeur.
	
	Cette donnée est vouée à être passée ultérieurement en paramètre à setState(QVariant) pour restaurer l'état visuel de l'éditeur.
	*/
	virtual QVariant state() = 0;
	/**
	\brief	Restaure l'état de l'éditeur selon les indications spécifées.
	
	\see	state()
	*/
	virtual void setState(QVariant State) = 0;
	
	///Indique si un MecAbstractElement de ce rôle peut être ajouté.
	virtual bool canAddChild(MecAbstractElement::ElementRole ElementRole) const = 0;
	///Indique si un MecObject peut être ajouté.
	virtual bool canAddObject() const = 0;
	///Indique si une MecFunction peut être ajouté.
	virtual bool canAddFunction() const = 0;
	///Indique si un MecSignal peut être ajouté.
	virtual bool canAddSignal() const = 0;
	///Indique si une MecVariable peut être ajouté.
	virtual bool canAddVariable() const = 0;
	
	///Indique si l'élément enfant peut être supprimé.
	virtual bool canRemoveChild(MecAbstractElement* const Element) const = 0;
	
	///Indique si le type de l'élément est éditable.
	virtual bool isTypeEditable() const = 0;
	///Indique si le nom de l'élément est éditable.
	virtual bool isNameEditable() const = 0;
	/**
	Indique si le contenu de l'élément est éditable.
	
	\see	setContentEditable(bool)
	*/
	virtual bool isContentEditable() const = 0;
	
	public slots:
	/**
	\brief	Dispose l'éditeur de manière à éditer l'élément de manière primaire.
	*/
	virtual void edit() = 0;
	/**
	\brief	Positionne la vue de manière à voir l'éditeur de \e Element.
	*/
	virtual void editChild(MecAbstractElement *Element) = 0;
	
	/**
	\brief	Ajoute un élément de type \e ElementRole à l'élément édité.
	
	Appelle une de ces fonctions : addObject(), addFunction(), addSignal() ou addVariable().
	*/
	virtual void addChild(MecAbstractElement::ElementRole ElementRole) = 0;
	/**
	\brief	Ajoute un objet à l'élément.
	
	\see	addChild(MecAbstractElement::ElementRole ElementRole)
	*/
	virtual void addObject() = 0;
	/**
	\brief	Ajoute une fonction à l'élément.
	
	\see	addChild(MecAbstractElement::ElementRole ElementRole)
	*/
	virtual void addFunction() = 0;
	/**
	\brief	Ajoute un signal à l'élément.
	
	\see	addChild(MecAbstractElement::ElementRole ElementRole)
	*/
	virtual void addSignal() = 0;
	/**
	\brief	Ajoute une variable à l'élément.
	
	\see	addChild(MecAbstractElement::ElementRole ElementRole)
	*/
	virtual void addVariable() = 0;
	
	/**
	\brief	Autorise ou non l'édition du type de l'élément.
	*/
	virtual void setTypeEditable(bool Editable) = 0;
	/**
	\brief	Autorise ou non l'édition du nom de l'élément.
	*/
	virtual void setNameEditable(bool Editable) = 0;
	/**
	\brief	Autorise ou non l'édition du contenu de l'élément.
	
	Selon le rôle de l'élément, ce qui est considéré comme son contenu diffère :
		- Project & Object : les fonctions, signaux et variables ;
		- Function : le code ;
		- Signal : rien ;
		- Variable : la valeur par défaut.
	*/
	virtual void setContentEditable(bool Editable) = 0;
	
	signals:
	/**
	\brief	L'élément édité n'est plus le même.
	
	Signale que l'édition ne porte plus sur le même élément.
	
	\param	Element	Élément désormais en cours d'édition.
	*/
	void editedElementChange(MecAbstractElement *Element);
	///Une erreur de plugin s'est déclenchée.
	void pluginsErrorsOccured();
	
	
};

#endif /* __MECABSTRACTELEMENTEDITOR_H__ */

