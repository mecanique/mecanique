/*
© Quentin VIGNAUD, 2013

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#ifndef __MECABSTRACTELEMENTCOMPILER_H__
#define __MECABSTRACTELEMENTCOMPILER_H__

#include "MecAbstractElement.h"
#include "MecAbstractCompiler.h"
#include "MecPluginError.h"
#include <QResource>

/**
\brief	Classe de représentation abstraite d'un compilateur d'élément.
*/
class MecAbstractElementCompiler
{
	public:
	/**
	\brief	Destructeur.
	*/
	virtual ~MecAbstractElementCompiler() {}
	
	///Élément compilé.
	virtual MecAbstractElement* element() const = 0;
	///Rôle du compilateur.
	virtual MecAbstractElement::ElementRole compilerRole() const = 0;
	
	///Compilateur principal.
	virtual MecAbstractCompiler* mainCompiler() const = 0;
	
	/**
	Retourne une liste des compilateurs subordonnés, correspondants à des éléments enfants, nécessaires pour compiler.
	*/
	virtual QList<MecAbstractElementCompiler*> subCompilers() const = 0;
	/**
	\brief	Retourne les erreurs de plugins.
	*/
	virtual QList<MecPluginError> pluginsErrors() const = 0;
	/**
	Retourne la liste des ressources à ajouter au répertoire de compilation pour compiler cet élément.
	
	\note	Le nom de fichier des ressources est repris pour leur emplacement dans le répertoire de compilation, ainsi ":/src/MaRessource.res" deviendra "[répertoire de compilation]/src/MaRessource.res"
	*/
	virtual QList<QResource*> resources() = 0;
	/**
	Retourne les instructions à ajouter au fichier projet (".pro").
	*/
	virtual QString projectInstructions() = 0;
	/**
	Retourne le contenu du header de l'élément.
	
	\note	Le nom du fichier header de l'élément sera nécéssairement "[répertoire de compilation]/src/[nom de l'élément].h".
	\note	La ligne "HEADERS += [nom de l'élément].h" sera automatiquement ajoutée au fichier projet (".pro").
	*/
	virtual QString header() = 0;
	/**
	Retourne le contenu du fichier d'implémentation de l'élément.
	
	\note	Le nom du fichier d'implémentation de l'élément sera nécéssairement "[répertoire de compilation]/src/[nom de l'élément].cpp".
	\note	La ligne "SOURCES += [nom de l'élément].cpp" sera automatiquement ajoutée au fichier projet (".pro").
	*/
	virtual QString source() = 0;
};

#endif /* __MECABSTRACTELEMENTCOMPILER_H__ */

