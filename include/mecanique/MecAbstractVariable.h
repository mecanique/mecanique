/*
© Quentin VIGNAUD, 2013

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#ifndef __MECABSTRACTVARIABLE_H__
#define __MECABSTRACTVARIABLE_H__

#include "MecAbstractElement.h"
#include <QVariant>

/**
\brief	Classe virtuelle de représentation d'une variable.
*/
class MecAbstractVariable : public MecAbstractElement
{
	Q_OBJECT

	public:
	virtual ~MecAbstractVariable() {};
	
	///Retourne la valeur par défaut de la variable.
	virtual QVariant defaultValue() const = 0;
	
	public slots:
	///Fixe la valeur par défaut de la variable.
	virtual void setDefaultValue(QVariant DefaultValue) = 0;
	
	
	signals:
	///La valeur par défaut a changé.
	void defaultValueChanged(MecAbstractVariable *I);
};

#endif /* __MECABSTRACTVARIABLE_H__ */

