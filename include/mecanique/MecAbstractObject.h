/*
© Quentin VIGNAUD, 2013

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#ifndef __MECABSTRACTOBJECT_H__
#define __MECABSTRACTOBJECT_H__

#include "MecAbstractElement.h"
#include "MecAbstractFunction.h"
#include "MecAbstractSignal.h"
#include "MecAbstractVariable.h"

/**
\brief	Classe virtuelle de représentation d'un objet.
*/
class MecAbstractObject : public MecAbstractElement
{
	Q_OBJECT

	public:
	///Destructeur.
	virtual ~MecAbstractObject() {}
	
	///Retourne les fonctions enfants.
	virtual QList<MecAbstractFunction*> childFunctions() const = 0;
	///Retourne les signaux enfants.
	virtual QList<MecAbstractSignal*> childSignals() const = 0;
	///Retourne les variables enfants.
	virtual QList<MecAbstractVariable*> childVariables() const = 0;
};

#endif /* __MECABSTRACTOBJECT_H__ */

